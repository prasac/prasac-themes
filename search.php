<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package NCTM
 */
get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <section class="col-sm-12 col-md-12">
                <?php
                if ( have_posts() ) : ?>
                    <header class="page-title">
                        <h3><?php printf( esc_html__( '[:en]Your search results for​:  %s[:kh]លទ្ធផលសម្រាប់ការស្វែងរករបស់អ្នក​៖  %s', 'nctm' ), '<span>' . get_search_query() . '</span>' ); ?></h3>
                    </header><!-- .page-header -->
                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) : the_post();
                        /**
                         * Run the loop for the search to output the results.
                         * If you want to overload this in a child theme then include a file
                         * called content-search.php and that will be used instead.
                         */
                        get_template_part( 'template-parts/content', 'search' );
                    endwhile;
                    the_posts_navigation();
                else :
                    get_template_part( 'template-parts/content', 'none' );
                endif; ?>
            </section>
        </main>
    </div>
</div>

<?php get_footer(); ?>



        <img id="khmer_new_year_lefts" class="teverda-left slide-down-left hidden-xs invisible" style="left:0px;" src="<?php echo  get_template_directory_uri()?>/assets/HappyKhmerNewYearLeft.gif">
        <img id="khmer_new_year_rights" class="teverda-right slide-down-right hidden-xs invisible" style="right:0px;" src="<?php echo  get_template_directory_uri()?>/assets/HappyKhmerNewYearRight.gif">
        <div class="container-fluid">
            <footer class="footer_wrapper">
                <section id="footer" class="footer">
                    <div class="footer_wrapper container">
                        <div class="row">
                            <div class="hidden-xs col-sm-3 col-md-3">
                                <?php
                                wp_nav_menu( array(
                                    'menu'            => 'Footer',
                                    'depth'           => 4,
                                    'container'       => '',
                                    'menu_class'      => 'footer_menu list-unstyle',
                                    'menu_id'         => 'footer_menu' )
                                );
                                ?>
                            </div>
                            <div class="footer_address col-xs-6 col-sm-3 col-md-3">
                                <?php
                                wp_nav_menu( array(
                                    'menu'            => 'Footer block3',
                                    'depth'           => 4,
                                    'container'       => '',
                                    'menu_class'      => 'footer_menu list-unstyle ',
                                    'menu_id'         => 'footer_menu' )
                                );
                                ?>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 hidden-sm hidden-md hidden-lg">
                                <?php
                                wp_nav_menu( array(
                                    'menu'            => 'Footer block4',
                                    'depth'           => 4,
                                    'container'       => '',
                                    'menu_class'      => 'footer_menu list-unstyle footer-menu-right',
                                    'menu_id'         => 'footer_menu' )
                                );
                                ?>
                            </div>
                            <div class="footer_address col-xs-12 col-sm-3 col-md-3">

                                <p class="white"><?php _e('[:en]Follow Us [:kh]ទទួលព័ត៌មានតាមបណ្តាញសង្គម[:]');?></p>
                                <ul class="footer_follow-us unstyle list-inline">
                                    <?php
                                    $icons = get_contacticons();
                                    iconlist($icons);
                                    ?>
                                </ul>
                                <p></p>
                                <p class="white"><?php _e('[:en]Subscribe Us [:kh]ចុះឈ្មោះទទួលព័ត៌មានថ្មីៗ[:]');?></p>
                                <p></p>
                                <form class="subscribe-form" action="" method="POST">
                                    <div class="input-group add-on">
                                        <input class="form-control" placeholder="<?php _e('[:en]Enter Your Email...[:kh]បញ្ចូល​អុី​ម៉ែល​លោក​អ្នក​...[:]');?>" name="subscribe-email" id="subscribe-email" type="text">
                                        <div class="input-group-btn btn-subcribe">
                                            <input type="hidden" name="action" value="addSubscriber" />
                                            <button class="btn btn-warning" type="submit" id="btn-subscribe"><?php _e('[:en]Subscribe[:kh]ចុះឈ្មោះ[:]');?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                            <div class="footer_address hidden-xs col-sm-3 col-md-3 text-right">
                                <h4 class="white"><?php echo get_field('footer_title', 'option'); ?></h4>
                                <address class="">
                                    <p><?= get_field('footer_address', 'option'); ?></p>
                                    <p><?= get_field('footer_mobile', 'option'); ?></p>
                                    <p><a href="mailto:<?= get_field('footer_email', 'option'); ?>" class="white"><?= get_field('footer_email', 'option'); ?></a></p>
                                </address>
                            </div>

                        </div>
                    </div>
                </section>
                <section class="footer_bar">
                    <div class="container">
                        <div class="row">
                            <div id="copyright" class="col-xs-12 col-sm-6 col-md-6">©&nbsp<?php echo date("Y");?> - <?php _e('[:en]All rights reserved[:kh]រក្សា​សិទ្ធិ​គ្រប់​យ៉ាង​[:]'); ?></div>
                            <div class="col-xs-12 col-sm-6 col-md-6 text-right"><?php _e('[:en]Incorporation Registration №[:kh]ចុះបញ្ជីពាណិជ្ជកម្មលេខ៖[:] 00001157'); ?> | <?php _e('[:en]Private Company[:kh]គ្រឹះស្ថានឯកជន​​[:]')?></div>
                        </div>
                    </div>
                </section>

                <!--Modal -->
                <div class="modal fade in" id="popupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="vertical-alignment-helper">
                        <div class="modal-dialog vertical-align-center">
                            <div class="modal-content">
                                <div class="modal-header modal-header-info">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel"> <?php echo  get_field('html_popup_title', 'option')?get_field('html_popup_title', 'option'):'' ?></h4>
                                </div>
                                <div class="modal-body">
                                <?php echo get_field("html_popup", "option"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--Modal Subscribe-->
                <div class="modal fade" id="subscribe_modal" tabindex="-1" role="dialog" aria-labelledby="titleLabel" aria-hidden="true">
                    <div class="modal-dialog subscribe-popup">
                        <div class="panel">
                            <div class="panel-heading">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="panel-title" id="titleLabel"></h4>
                            </div>
                            <form action="#" method="post" accept-charset="utf-8">
                                <div class="modal-body">
                                
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>


                <!--Modal homepage popup-->
                <div class="modal fade" id="homepage_popup_modal" tabindex="-1" role="dialog" aria-labelledby="titleLabel" aria-hidden="true">
                    <div class="modal-dialog homepage-popup">
                        <div class="panel">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-window-close"></i></button>
                            <div class="modal-body" style="padding:0;">
                                
                            </div>
                            <div style="margin:0 auto;width:45px;">
                                <span class="pull-right" style="color:#ffffff;">&nbsp;Sec </span>
                                <span id="counter" class="pull-right" style="color:#ffffff;"> </span>
                            </div>
                        </div>
                    </div>
                </div>

                <!--=========Footer aler infomation==============-->
                <div class="alert-wraper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class=" shadow">
                                    <div class="alert alert-default text-center" style="margin:0;padding:10px;">
                                        <button type="button" class="close close_footer" data-dismiss="alerts" aria-hidden="false" style="opacity:1;color:red;">×</button>
                                        <p>
                                        <?php echo get_field('footer_info_alert', 'option') ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="#0" class="cd-top">Top</a>
            </footer>
            </div>
        </div>
    </body>
</html>
<?php
wp_footer();
include ( get_template_directory() . '/inc/scripts-config.php' );
?>
<style>
body,.navbar-static-top{
    position:relative;
}
img.teverda-left{
    position:absolute;
    top:-4rem;
    left:0;
    z-index:1200;
    width:200px;
}

img.teverda-right{
    position:absolute;
    top:-4rem;
    right:0;
    z-index:1200;
    width:200px;
}

.blink:before{
    font-family:FontAwesome;
    content:'\f0a4';
}
.blink{
    color:red;
    animation: blink 1s linear infinite;
    padding:1px 4px;
}


#sound_switcher{position:fixed;background-color:#4db848;z-index:9999999999;bottom:3rem;left:0;width:36px;height:36px;line-height:36px;opacity:.5;transition:.2s ease all;-webkit-transition:.2s ease all;border-radius:50%;-webkit-border-radius:50%;box-shadow:0 0 15px 5px #fff;cursor:pointer;text-align:center}#sound_switcher .fa{color:#fff;line-height:36px}#sound_switcher:hover{opacity:.7!important}#sound_switcher .fa-play{display:none;position:relative;right:-1px;top:1px}.play_sound_switcher .fa-pause{display:none!important}.play_sound_switcher .fa-play{display:block!important}


.slide-down-right{
	-webkit-animation: slide-down-right 4500ms linear 1ms both;
    animation: slide-down-right 4500ms linear 1ms both;
}


.slide-down-left{
	-webkit-animation: slide-down-left 4500ms linear 1ms both;
    animation: slide-down-left 4500ms linear 1ms both;
}
/* ----------------------------------------------
 * Generated by Animista on 2019-4-2 15:34:24
 * w: http://animista.net, t: @cssanimista
 * ---------------------------------------------- */

/**
 * ----------------------------------------
 * animation slide-bottom
 * ----------------------------------------
 */
@-webkit-keyframes slide-down-right {
  0% {
    -webkit-transform:scale(0.1) translate(0,0);
            transform:scale(0.1)  translate(0,0);
    
  }
  100% {
    -webkit-transform: scale(1) translate(-10px,150px);
            transform:  scale(1)  translate(-10px,150px);
            
  }
}

 @keyframes slide-down-right {
  0% {
        -webkit-transform: scale(0.1)  translate(0,0);
            transform: scale(0.1)  translate(0,0);
           
  }
  100% {
    -webkit-transform: scale(1)  translate(-10px,0);
            transform: scale(1)  translate(-10px,150px);
            
  }
}

@-webkit-keyframes slide-down-left {
0% {
    -webkit-transform:scale(0.1)  translate(0,0);
            transform:scale(0.1)  translate(0,0);
  }
  100% {
    -webkit-transform:scale(1) translate(10px,-150px);
            transform:scale(1) translate(10px,150px);
  }
}

@keyframes slide-down-left {
    0% {
        -webkit-transform:scale(0.1)  translate(0,0);
            transform:scale(0.1)  translate(0,0);
  }
  100% {
    -webkit-transform:scale(1) translate(10px,-10px);
            transform:scale(1) translate(10px,150px);
  }
}
</style>

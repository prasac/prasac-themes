<?php get_header(); ?>
<div id="site-main" class="container-fluid">
    <div class="container">
        <main class="row">
            <aside class="col-sm-3 col-md-3" id="category_sidebar_top">
                <div class="sidebar"><br/>
                    <div class="sidebar_title"><?php _e('[:en]P.Point[:kh]P.Point[:]'); ?></div>
                    <ul class="sidebar_content list" >
                        <?php
                            $terms  = get_terms('merchant_tag');
                            foreach( $terms as $term ) : 
                                $term_link = get_term_link( $term );
                                $term_name = $term->name;
                                $term_slug = $term->slug;
                                _e('
                                    <li class="page_item ">
                                        <a class="" href="'.esc_url($term_link).'" title="'.$term_slug.'">'.$term_name.'</a>
                                    </li>
                                ');
                            endforeach;
                        ?>
                    </ul>
                </div>
            </aside>
            <section class="col-sm-9 col-md-9"><br />
            <!-- <h4 class="green"><?php //single_cat_title(); ?><span class="border"></span></h4> -->
            <?php
            if( is_tax( 'merchant_tag', 'dinning-entertainment' ) ) :
                echo do_shortcode('[ajax_filter post="merchant" tax="locations" active="all-term" per_page="9" second_tax="merchant_tag" second_term="dinning-entertainment"]');

            elseif( is_tax( 'merchant_tag', 'education' ) ) :
                echo do_shortcode('[ajax_filter post="merchant" tax="locations" active="all-term" per_page="9" second_tax="merchant_tag" second_term="education"]');

            elseif( is_tax( 'merchant_tag', 'health-beauty' ) ) :
                echo do_shortcode('[ajax_filter post="merchant" tax="locations" active="all-term" per_page="9" second_tax="merchant_tag" second_term="health-beauty"]');

            elseif( is_tax( 'merchant_tag', 'shopping-retail' ) ) :
                echo do_shortcode('[ajax_filter post="merchant" tax="locations" active="all-term" per_page="9" second_tax="merchant_tag" second_term="shopping-retail"]');

            elseif( is_tax( 'merchant_tag', 'traveling-accommodation' ) ) :
                echo do_shortcode('[ajax_filter post="merchant" tax="locations" active="all-term" per_page="9" second_tax="merchant_tag" second_term="traveling-accommodation"]');
            endif;
            ?>
            </section>
        </main>
    </div>
</div>

<?php get_footer(); ?>
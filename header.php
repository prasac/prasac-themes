<!doctype html>
<html <?php language_attributes(); ?> class="no-js" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
    <?php
    if(is_front_page()){
        echo bloginfo('name');
    }else if(is_singular('post') || is_singular('page') || is_singular('office') || is_singular('people') || is_singular('career') || is_singular('merchant')){
        echo wp_title('');
    }else{
        echo single_term_title();
    }
    ?>
    </title>
    <meta http-equiv="Content-Security-Policy: default-src https://www.prasac.com.kh" content="default-src 'self'; img-src https://*; child-src 'none';">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="google-site-verification" content="Yl-LcMJO-U-INKQgHq5ihAd5ff-N-6sAfPjNRJv2FNo" />
    <link href="https://www.google-analytics.com" rel="dns-prefetch">
    <link href="<?= get_field('favicon', 'option'); ?>" rel="shortcut icon">
    <link href="https://fonts.googleapis.com/css?family=Hanuman|Moul|Roboto" rel="stylesheet">
    <?php wp_head(); ?>

    <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NTML5R');</script>
    <!-- End Google Tag Manager -->

<script type="text/javascript">
  //              DO NOT IMPLEMENT                //
  //       this code through the following        //
  //                                              //
  //   Floodlight Pixel Manager                   //
  //   DCM Pixel Manager                          //
  //   Any system that places code in an iframe   //
    // (function () {
    //     var s = document.createElement('script');
    //     s.type = 'text/javascript';
    //     s.async = true;
    //     s.src = ('https:' == document.location.protocol ? 'https://s' : 'http://i')
    //       + '.po.st/static/v4/post-widget.js#publisherKey=jr3d3hbdk2hmov0r59f9';
    //     var x = document.getElementsByTagName('script')[0];
    //     x.parentNode.insertBefore(s, x);
    //  })();
</script>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.3&appId=100597433778841&autoLogAppEvents=1"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '913781538705307');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=913781538705307&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NTML5R"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="container-fluid" id="main-container-fluid">
<?php

    /* Navigation menu */
    require get_template_directory() . '/inc/Mobile_Detect.php';
    $detect = new Mobile_Detect;
    if ( $detect->isMobile() && !$detect->isTablet()){
        get_template_part('template-parts/navbar', 'mobile');
    }else{
        get_template_part('template-parts/navbar', 'desktop');
    }
   if( is_front_page() ): ?>
        <!-- Hero slider -->
        <?php
        if( have_rows('slider','option')):
            $desktop_image_source = '[:en]slider_image_desktop_en[:kh]slider_image_desktop_kh[:]';
            $desktop_image_source =   __($desktop_image_source);
            $mobile_image_source = '[:en]slider_image_mobile_en[:kh]slider_image_mobile_kh[:]';
            $mobile_image_source =   __($mobile_image_source);
        ?>
                <div class="slider-main-wrapper">
                    <div id="single-item">
                        <?php
                        while( have_rows('slider','option') ): the_row();?>
                                <?php 
                                //var_dump(get_sub_field($slide_desktop_image));
                                if( $detect->isMobile() && !$detect->isTablet()){
                                    if(get_sub_field($mobile_image_source)){
                                    ?>
                                    <div class="item mobile">
                                        <a href="<?= get_sub_field('slider_page_link'); ?>">
                                        <img class="img-responsive" data-lazy="<?= get_sub_field($mobile_image_source); ?>" src="<?= get_sub_field($mobile_image_source); ?>" alt="Hero Slider" style="margin:0 auto;">
                                        </a>
                                    </div>
                                    <?php 
                                    }
                                }else if(get_sub_field($desktop_image_source)){
                                ?>
                                    <section class="slick-slide">
                                        <a class="slide-item" href="<?= get_sub_field('slider_page_link'); ?>">
                                        <img data-lazy="<?= get_sub_field($desktop_image_source); ?>" src="<?= get_sub_field($desktop_image_source); ?>" alt="" class="img-responsive" title="">
                                        </a>
                                    </section>
                                    <?php
                                }
                                ?>
                        <?php endwhile; ?>
                    </div> 
                </div>
            <?php 
            $marquee = get_field('marquee', 'option');
            if($marquee):
            ?>
             <section class="hidden-xs" style="margin-top:0;width:100%;margin:0 auto;">
                    <div class="text-center marquee-wrapper">
                        <div class="marquee-with-options">
                            <?php
                                foreach( $marquee as $value ) :
                                    echo '<article class="article-item">'.$value["description"].'</article>';
                                endforeach;
                            ?>
                        </div>
                    </div>
                </section> 
        <?php endif;?>
        <div class="clearfix hidden-sm hidden-md"></div>
    <?php endif; ?>
<?php endif;?>

  <!-- End Hero Slider -->


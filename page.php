<?php get_header(); ?>
<?php 
    if ( have_rows('flexible_content') ) :
        while ( have_rows('flexible_content') ) : the_row();
            if ( get_row_layout() == 'page_link' ) :
            $post_id = get_sub_field_object('link')['value']->ID;
            echo '<div class="container-fluid">';?>
                        <img class="img-responsive" src="<?php echo get_sub_field('photo')?>" alt=""  onClick='window.open("<?php echo get_permalink($post_id)?>","_self");'>
                        <?php
            echo '</div>';
            endif;
        endwhile;
        wp_reset_postdata();
        //End flexible content
    endif;
?>
<div class="container-fluid">
  <div class="container">
    <main class="row">
        <aside class="col-xs-12 col-sm-3 col-md-3 hidden-xs hidden-sm">
            <div class="sidebar">
                <?php get_sidebar(); ?>
            </div>
        </aside>
            <?php 
            if(is_page('privacy-and-security-statement')){?>
                <section class="col-sm-9 col-md-9 content">
                    <div class="hidden-xs hidden-sm">
                        <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                    </div>
                    <?php
                    if (have_posts()):
                        while (have_posts()): the_post();
                            get_template_part('template-parts/content', 'page');
                        endwhile;
                    else :
                        get_template_part('template-parts/content', 'none');
                    endif;
                    ?>
                </section>
            <?php 
            } elseif(is_page('site-map')) {
                ?>
                    <section class="col-xs-12 col-sm-12 col-md-12 content">
                        <div class="row hidden-xs hidden-sm">
                            <div class="col-xs-12 col-md-12">
                                <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                            </div>
                        </div>
                    <?php
                    if (have_posts()):
                        while (have_posts()): the_post();
                            get_template_part('template-parts/content', 'page');
                        endwhile;
                    else :
                        get_template_part('template-parts/content', 'none');
                    endif;
                    ?>
                </section>
                <?php
            }elseif(is_page('exchange')){
                ?>
                    <section <?= post_class('col-xs-12 col-sm-7 col-md-7'); ?> >
                    <div class="hidden-xs hidden-sm">
                        <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                    </div>
                    <?php
                    if (have_posts()):
                        while (have_posts()): the_post();
                            get_template_part('template-parts/content', 'page');
                        endwhile;
                    else :
                        get_template_part('template-parts/content', 'none');
                    endif;
                    ?>
                </section>
                <?php
            } else {
                ?>
                    <section <?= post_class('col-xs-12 col-sm-9 col-md-9'); ?> >
                    <div class="row hidden-xs hidden-sm">
                        <div class="col-xs-12 col-md-12">
                                <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                                <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                                    <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                                    <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                                </ul>
                            </div>
                    </div>
                    <?php
                    if (have_posts()):
                        while (have_posts()): the_post();
                            get_template_part('template-parts/content', 'page');
                        endwhile;
                    else :
                        get_template_part('template-parts/content', 'none');
                    endif;
                    ?>
                </section>
                <?php
            }
        ?>  
            <aside class="col-xs-12 col-sm-3 col-md-3 visible-sm">
                <div class="sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </aside>
        </main>
    </div>
</div>
<?php get_footer(); ?>

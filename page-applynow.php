<?php
 /*
* template name: Apply Now
*/
get_header();
?>

<?php 
    if ( have_rows('flexible_content') ) :
        while ( have_rows('flexible_content') ) : the_row();
            if ( get_row_layout() == 'page_link' ) :
            echo '<div class="container-fluid">';?>
                <img class="hidden-xs img-responsive" src="<?php echo get_sub_field('photo')?>" alt="" >
                        <?php
            echo '</div>';
            endif;
        endwhile;
        wp_reset_postdata();
    endif;
?>

<style>
form.apply-form input[type=radio] {
    margin-top: 8px !important;
}
form.apply-form{
    font-size:16px;
}

form.apply-form textarea,
form.apply-form textarea.form.apply-form-control {
    padding-top: 10px;
    padding-bottom: 10px;
    line-height: 30px;
}
form.apply-form input[type="text"]:-moz-placeholder,
form.apply-form input[type="password"]:-moz-placeholder,
form.apply-form textarea:-moz-placeholder,
form.apply-form textarea.form.apply-form-control:-moz-placeholder {
    color: #888;
}

form.apply-form input[type="text"]:-ms-input-placeholder,
form.apply-form input[type="password"]:-ms-input-placeholder,
form.apply-form textarea:-ms-input-placeholder,
form.apply-form textarea.form.apply-form-control:-ms-input-placeholder {
    color: #888;
}

form.apply-form input[type="text"]::-webkit-input-placeholder,
form.apply-form input[type="password"]::-webkit-input-placeholder,
form.apply-form textarea::-webkit-input-placeholder,
form.apply-form textarea.form.apply-form-control::-webkit-input-placeholder {
    color: #888;
}

form.apply-form button.btn:hover {
    color: #fff;
}

form.apply-form button.btn:active {
    outline: 0;
    color: #fff;
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none;
}

form.apply-form button.btn:focus {
    outline: 0;
    background: #4db848;
    color: #fff;
}

form.apply-form button.btn:active:focus,
form.apply-form button.btn.active:focus {
    outline: 0;
    background: #4db848;
    color: #fff;
}

form.apply-form strong {
    font-weight: 500;
}
form.apply-form ul.service-list{
    padding:10px 0;
    margin:0;
}

form.apply-form ul.service-list li a,
form.apply-form ul.service-list li  a:hover,
form.apply-form ul.service-list li  a:focus {
    color: #4db848;
    text-decoration: none;
    -o-transition: all .3s;
    -moz-transition: all .3s;
    -webkit-transition: all .3s;
    -ms-transition: all .3s;
    transition: all .3s;
}

form.apply-form h1,
form.apply-form h2 {
    margin-top: 10px;
    font-size: 38px;
    font-weight: 100;
    color: #ffffff;
    line-height: 50px;
}

form.apply-form h3 {
    font-size: 22px;
    font-weight: 300;
    line-height: 30px;
    margin-bottom: 15px;
}

form.apply-form .btn-link-1 {
    display: inline-block;
    height: 50px;
    margin: 0 5px;
    padding: 16px 20px 0 20px;
    background: #4db848;
    font-size: 16px;
    font-weight: 300;
    line-height: 16px;
    color: #fff;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
}

form.apply-form .btn-link-1:hover,
form.apply-form .btn-link-1:focus,
form.apply-form .btn-link-1:active {
    outline: 0;
    opacity: 0.6;
    color: #fff;
}

form.apply-form .btn-link-2 {
    display: inline-block;
    height: 50px;
    margin: 0 5px;
    padding: 15px 20px 0 20px;
    background: rgba(0, 0, 0, 0.3);
    border: 1px solid #fff;
    font-size: 16px;
    font-weight: 300;
    line-height: 16px;
    color: #fff;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
}

form.apply-form .btn-link-2:hover,
form.apply-form .btn-link-2:focus,
form.apply-form .btn-link-2:active,
form.apply-form .btn-link-2:active:focus {
    outline: 0;
    opacity: 0.6;
    background: rgba(0, 0, 0, 0.3);
    color: #fff;
}


/***** Top content *****/

.assessment-container .form-box {
    padding:15px 0;
    overflow:hidden;
}

.assessment-container .form-box fieldset{font-size:16px;}

form.apply-form .form-top {
    overflow: hidden;
    padding-bottom: 10px;
    -moz-border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
    border-radius: 4px;
    text-align: left;
    transition: opacity .3s ease-in-out;
}

form.apply-form .form-bottom {
    padding: 25px 25px 30px 25px;
    background: #eee;
    -moz-border-radius: 0 0 4px 4px;
    -webkit-border-radius: 0 0 4px 4px;
    border-radius: 0 0 4px 4px;
    text-align: left;
    transition: all .4s ease-in-out;
}

form.apply-form .form-bottom:hover {
    -webkit-box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
}

form.apply-form .form-bottom button.btn {
    min-width: 105px;
}

form.apply-form .form-bottom .input-error {
    border-color: #d03e3e;
    color: #d03e3e;
}

.form-box fieldset {
    display: none;
    padding:0;
}
fieldset .chapterSteps ul li {
    float: left;
    padding: 0 5px;
    text-align: center;
    width:32.33%;
    margin:1rem 0;
    list-style: none;
}

fieldset#deposit .chapterSteps ul li {
    float: left;
    padding: 0 5px;
    text-align: center;
    width:25%;
    margin:1rem 0;
    list-style: none;
}
fieldset .chapterSteps ul.service-list li a span.chapterIcons {
    background:#ffffff;
    border-radius: 100%;
    color: #4db848;
    display: inline-block;
    font-size: 25px;
    height:100px;
    line-height: 50px;
    position: relative;
    text-align: center;
    width:100px;
    transition: all 400ms ease 0s;
}

fieldset .chapterSteps ul.service-list li a:hover span.chapterIcons {
    color:#4db848;
    transform: scale(0.9);
    transition-timing-function: cubic-bezier(0.47, 2.02, 0.31, -0.36);
}
fieldset .chapterSteps ul.service-list li a span.chapterIcons:before {
    content: "";
    height:100px;
    position: absolute;
    width:100px;
    -moz-border-radius: 100%;
    -webkit-border-radius: 100%;
    border-radius: 100%;
    left:0;
    top:-5px;
}


fieldset .chapterSteps ul.service-list li a span.chapterIcons:before{
    border:1px solid #4db848;
}
fieldset .chapterSteps ul.service-list li a:hover span.chapterIcons:before{
    box-shadow: 0 0 2px 10px #4db84833;
}
fieldset .chapterSteps ul.service-list li a span.chapterIcons img{
    width:90px;
}

fieldset .chapterSteps ul.service-list li a.active span.chapterIcons{
    transform: scale(0.9);
    transition-timing-function: cubic-bezier(0.47, 2.02, 0.31, -0.36);
}
fieldset .chapterSteps ul.service-list li a.active span.chapterIcons:before,
fieldset .chapterSteps ul.service-list li a:hover span.chapterIcons:before
{
    height: 105px;
    left: -3px;
    position: absolute;
    top: -7px;
    width: 105px;
    border: 1px solid #4db848;
    /* box-shadow: 0 0 2px 10px #4db84833; */
}
fieldset .chapterSteps ul.service-list li a.active span.chapterLink,
fieldset .chapterSteps ul.service-list li a:hover span.chapterLink
{
    transform: scale(1.1);
    transition: all .3s ease-in-out;
}

fieldset .chapterSteps ul li a span.chapterLink {
    color: #4db848;
    display: block;
    font-size: 16px;
    margin-top: 10px;
    text-align: center;
    line-height: 22px;
}


.bannerSection .upperBanner .newChapter .button-section {
    height: auto;
    margin-top: 20px;
    width: 100%;
    float: left;
    text-align: center;
}

form.apply-form a{
    color:#fff;
}

fieldset div.button-section {
    height: auto;
    float: left;
    text-align: center;
}
.button-section .btn-primary {
    background-color:#4db848;
    border: 1px solid #4db848;
    -moz-border-radius:2rem;
    -webkit-border-radius: 2rem;
    color:#ffffff;
    font-size: 20px;
    padding: 0.3rem 1rem;
    text-transform:capitalize;
    border-radius:0.3rem;
    margin:0 5px;
}
.button-section .btn-primary:after{
    border: 2px solid red;
}

.button-section .btn-primary:hover,.button-section .btn-primary:focus, .button-section .btn-primary.focus, .button-section .btn-primary:active:focus,
.btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active.focus, .btn-primary:active:focus,
.btn-primary:active:hover, .open>.dropdown-toggle.btn-primary.focus, .open>.dropdown-toggle.btn-primary:focus,
.open>.dropdown-toggle.btn-primary:hover{
    background:#4db848;
    color: #ffffff;
    box-shadow:0 2px 5px #616161;
    border-color:#4db848;
}

form.apply-form .chapterSteps h2{
    font-size:2em;
    padding-top: 1.2rem;
    padding-bottom: 0.8rem;
    color:#4db848;
    margin: 0;
    font-family:Hanuman,Roboto, sans-serif;
}
form.apply-form .alert-danger.step-verification{
    position:absolute;
    overflow:hidden;
    padding:5px;
    margin:0;
    border-radius:0;
    background:rgba(242, 176, 29,.7);
    border:rgb(242, 176, 29);
    bottom:5em;
}
form.apply-form input#amount{
    background: url("<?php echo get_template_directory_uri(); ?>/assets/icons/dollarIcon.png") no-repeat 10px 10px #fff;
    color: #848484 !important;
    cursor: default;
    display: inline-block;
    font-weight: 300;
    height:38px;
    line-height:2;
    padding: 0 20px 0 25px;
    text-align: left;
    border: none;
    width:200px;
    margin:15px 0;
    font-size:1.5em;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{
    background:#eea236;
}

form.apply-form .form-group{
    position: relative;
    margin:0;
    padding:10px;
}

form.apply-form .form-label{
  position: absolute;
  left:2em;
  top:1.5em;
  color: #151414;
  background-color:transparent;
  z-index: 10;
  transition: font-size 150ms ease-out, -webkit-transform 150ms ease-out;
  transition: transform 150ms ease-out, font-size 150ms ease-out;
  transition: transform 150ms ease-out, font-size 150ms ease-out, -webkit-transform 150ms ease-out;
  font-weight:lighter;
}
form.apply-form .form-group.form-select .form-label{
    left:1rem;
    top:-0.8rem;
    font-weight:lighter;
}
form.apply-form .focused .form-label {
  -webkit-transform: translateY(-125%);
   transform: translateY(-125%);
   left:15px;
   top:15px;
}

form.apply-form .form-input {
  position: relative;
  padding: 12px 0px 5px 0;
  width: 100%;
  outline: 0;
  border: 0;
}
input#service_name_other::placeholder{
    color: #ffffff;
}

form.apply-form input.form-input:focus,form.apply-form input.form-control:focus,form.apply-form textarea.form-control:focus,select.form-control,select.form-control:focus,.has-success .form-control:focus,form .input-group .form-control:focus,form .form-input.form-control:focus{
    box-shadow: inset 0 0 5px rgba(0,0,0,.3);
    border-color: #008329;
}
label.control-label{
    color:#ffffff;
}

form.apply-form input.form-control, form.apply-form select.form-control,form.apply-form span.input-group-addon{
    background:transparent;
    border-radius:0;
    border:0.5px solid #CCCCCC;
    padding:0 10px;
    box-shadow:none;
    -webkit-box-shadow:none;
    z-index:22;
    height:38px;
}
form.apply-form textarea.form-control{
    background:transparent;
    border-radius:0;
    border:0.5px solid #CCCCCC;
    padding:0 10px;
    box-shadow:none;
    -webkit-box-shadow:none;
    z-index:22;
}
input#captcha_deposit,input#captcha_career{
    height:38px;
}
input#captcha_deposit+.form-label,input#captcha_career+.form-label{
    top:2.3rem;
}

.form-group.focused .form-label,.form-group.focused .form-label,form.apply-form .form-group.form-select .form-label{
    color:#006917;
}

.form-group.focused input#captcha_deposit+.form-label,.form-group.focused input#captcha_career+.form-label{
    top:1rem;
}

#loader {
  display: none;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  background: rgba(0,0,0,0.75) url(<?php echo get_template_directory_uri(); ?>/assets/images/prasac_loading.gif) no-repeat center center;
  z-index: 10000;
}
.has-success .control-label{
    color:#fff;
}

select.branch option,select.province option,select.district option,select.commune option,select.village option{
    color:#66686a;
}
h2.main-title{
    font-family: "Khmer OS Moul", Roboto, sans-serif;
    z-index: 1;
    text-align: center;
    padding-bottom:20px !important;
    line-height:30px !important;
}
.has-error .help-block{
    color:#a94442;
    position:absolute;
    right:1rem;
    bottom: -0.65rem;
}
fieldset form#applynow .form-group.required:after{
    content:" *";
    color:#a94442;
    position:absolute;
    right:0;
    top:40%;
    font-size:16px;
}
.st1{
    fill:red !important;
    color:red;
}
.loading-spinner-wrapper {
  position: relative;
  width: 100%;
  left: 0;
  text-align: center;
  z-index: 1000;
  margin:10px 0;
  opacity: 0;
  -webkit-transition: all 0.3s ease;
  -moz-transition: all 0.3s ease;
  -ms-transition: all 0.3s ease;
  -o-transition: all 0.3s ease;
  transition: all 0.3s ease; }

.loading-spinner {
  position: relative;
  vertical-align: middle;
  width: 50px;
  height: 12px;
  text-align: center;
  margin: 0 auto;
  display: block; }

.loading-spinner i {
  position: absolute;
  top: 0;
  width: 11px;
  height: 11px;
  border-radius: 50%;
  background: #f89f1b;
  -webkit-animation: pulse 1s infinite ease-in-out;
  -moz-animation: pulse 1s infinite ease-in-out;
  animation: pulse 1s infinite ease-in-out;
  -webkit-animation-fill-mode: both;
  -moz-animation-fill-mode: both;
  animation-fill-mode: both; }

.loading-spinner i.one {
  left: 0;
  -webkit-animation-delay: -.32s;
  -moz-animation-delay: -.32s;
  animation-delay: -.32s;
  }

.loading-spinner i.two {
  left: 50%;
  -webkit-transform: translateX(-50%);
  -moz-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  -o-transform: translateX(-50%);
  transform: translateX(-50%);
  -webkit-animation-delay: -.16s;
  -moz-animation-delay: -.16s;
  animation-delay: -.16s; }

.loading-spinner i.three {
  right: 0; }

.loading-spinner i {
  background: #f89f1b; 
}

#applicant_dob .input-group-addon{
    cursor:pointer;
}

/*Bootstrap Calendar*/
.datepicker {
  border-radius: 0;
  padding: 0;
}
.datepicker-days table thead,
.datepicker-days table tbody,
.datepicker-days table tfoot {
  padding: 10px;
  display: list-item;
  overflow:hidden;
}
.datepicker-days table thead,
.datepicker-months table thead,
.datepicker-years table thead,
.datepicker-decades table thead,
.datepicker-centuries table thead {
  background:#4cb849;
  color: #ffffff;
  border-radius: 0;
}

.datepicker .datepicker-days table tbody tr td.active{
    background:#4cb849;
    color: #ffffff;
}

.datepicker-days table thead tr:nth-child(2n + 0) td,
.datepicker-days table thead tr:nth-child(2n + 0) th {
  border-radius: 3px;
}
.datepicker-days table thead tr:nth-child(3n + 0) {
  text-transform: uppercase;
  font-weight: 300 !important;
  font-size: 12px;
  color: rgba(255, 255, 255, 0.85);
}
.table-condensed > thead > tr > td,
.table-condensed > thead > tr > th {
    padding:5px 9px;
    font-weight: lighter;
}

.table-condensed > tfoot tr:nth-child(1){
    float:left;
}
.table-condensed > tfoot tr:nth-child(1) th,.table-condensed > tfoot tr:nth-child(2) th{
    width: 50%;
    border-radius: 0;
    background-color:#FFFFFF;
    color:#4cb849;
    font-weight: lighter;
    border:1px solid #4cb849;
}
.table-condensed > tfoot tr:nth-child(1) th:hover,.table-condensed > tfoot tr:nth-child(2) th:hover{
    background-color:#4cb849;
    color:#FFFFFF;
}
.table-condensed > tfoot tr:nth-child(2){
    float:right;
}
.table-condensed > tbody > tr > td,
.table-condensed > tbody > tr > th,
.table-condensed > tfoot > tr > td,
.table-condensed > tfoot > tr > th{
  padding: 11px 13px;
}
.datepicker-months table thead td,
.datepicker-months table thead th,
.datepicker-years table thead td,
.datepicker-years table thead th,
.datepicker-decades table thead td,
.datepicker-decades table thead th,
.datepicker-centuries table thead td,
.datepicker-centuries table thead th {
  border-radius: 0;
}
.datepicker td,
.datepicker th {
  border-radius: 50%;
  padding: 0 12px;
}
.datepicker-days table thead,
.datepicker-months table thead,
.datepicker-years table thead,
.datepicker-decades table thead,
.datepicker-centuries table thead {
  background:#4cb849;
  color: #ffffff;
  border-radius: 0;
}
form.apply-form .has-error input.form-control,
form.apply-form .has-error select.form-control{
    border-color: #a94442;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
}
.datepicker table tr td.active,
.datepicker table tr td.active:hover,
.datepicker table tr td.active.disabled,
.datepicker table tr td.active.disabled:hover {
  background-image: none;
}
.datepicker .prev,
.datepicker .next {
  color: rgba(255, 255, 255, 0.5);
  transition: 0.3s;
  width: 37px;
  height: 37px;
}
.datepicker .prev:hover,
.datepicker .next:hover {
  background: transparent;
  color: rgba(255, 255, 255, 0.99);
  font-size: 21px;
}
.datepicker .datepicker-switch {
  font-size: 24px;
  font-weight: 400;
  transition: 0.3s;
}
.datepicker .datepicker-switch:hover {
  color: rgba(255, 255, 255, 0.7);
  background: transparent;
}
.datepicker table tr td span {
  border-radius: 2px;
  margin: 3%;
  width: 27%;
}
.datepicker table tr td span.active,
.datepicker table tr td span.active:hover,
.datepicker table tr td span.active.disabled,
.datepicker table tr td span.active.disabled:hover {
  background-color: #3546b3;
  background-image: none;
}
.dropdown-menu {
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
}
.datepicker-dropdown.datepicker-orient-top:before {
  border-top: 7px solid rgba(0, 0, 0, 0.1);
}

#progress-bar-wrap {
  min-height: 20px;
  display: none;
  margin-bottom: 0;
}

#progress-bar-wrap .encouragement { display: none; }

#progress-bar-wrap .close:before {
  content: "\f0d7";
  display: inline-block;
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

#progress-bar-wrap span { line-height: 1.75em; }

#progress-bar-wrap.collapsed h4 {
  font-size: 14px;
  color: #999999;
}

#progress-bar-wrap.collapsed .encouragement { display: none; }

#progress-bar-wrap.collapsed .progress { display: none; }
#progress-bar-wrap .progress {
    height: 10px;
    margin-bottom: 0;
    overflow: hidden;
    background-color: #f5f5f5;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
    box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
}

#progress-bar-wrap.collapsed .close:before {
  content: "\f0d8";
  display: inline-block;
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
  line-height: .75em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
.progress-bar-info{
    background-color:#4cb849;
}
.info.alert{
    font-size:14px;
}
.ui-dialog .ui-dialog-content,.ui-dialog-buttonset,.ui-dialog .ui-dialog-title,.ui-dialog .ui-dialog-buttonpane button{
    font-family:Hanuman,Roboto, sans-serif;
    font-weight:lighter;
}
.ui-dialog-buttonset>button,.ui-dialog-buttonset>button:focus,.ui-dialog-buttonset>button:visited{
    border:1px solid red;
    color:red;
}
.ui-dialog .ui-dialog-titlebar-close{
    visibility: hidden;
}
.ui-widget-overlay.ui-front{
    z-index:1000;
    opacity:0.8;
}
.ui-widget.ui-widget-content{
    z-index:1001;
}
.ui-draggable .ui-dialog-titlebar{
    background-color:#4cb8491f;
    color:#4cb849;
}
.datepicker-days table thead tr:nth-child(2n + 0) td, .datepicker-days table thead tr:nth-child(2n + 0) th{
    font-size:16px;
}
.text-left{
    margin:10px 0 0;
    font-weight:normal;
    font-size:16px;
}
fieldset .chapterSteps ul.position_list{padding:0;}
fieldset .chapterSteps ul.position_list li{
    padding: 5px 10px;
    margin: 0.5%;
    cursor:pointer;
    border-radius:20px;
    background-color:#f5f5f5;
}
fieldset .chapterSteps ul.position_list li:hover{
    color:#4cb849;
    text-decoration:underline;
}

#overlay {
   position: fixed;
   height: 100%; 
   width: 100%;
   top: 0;
   right: 0;  
   bottom: 0;
   left: 0;
   background: rgba(0,0,0,0.8);
   display: none;
   z-index:1000;
}

#popup {
   max-width: 600px;
   width: 80%;
   max-height: 300px;
   padding: 20px;
   position: absolute;
   background: #fff;
   right:0;
   left:0;
   top:50%;
   margin:0 auto;
   text-align:center;
}

#close {
   position: absolute;
   top: 10px;
   right: 10px;
   cursor: pointer;
   color:#b71919;
}
  
@-webkit-keyframes pulse {
    0%, 100%, 80% {
    opacity: 0; 
    }
    40% {
        opacity: 1;
    }
}

@-moz-keyframes pulse {
    0%, 100%, 80% {
        opacity: 0; 
    }
    40% {
        opacity: 1;
    }
}

@keyframes pulse {
    0%, 100%, 80% {
    opacity: 0;
    }
    40% {
        opacity: 1;
    }
}
@media only screen and (max-width : 480px) {
    fieldset .chapterSteps ul li {
        float: left;
        padding: 0 5px;
        text-align: center;
        width:100%;
        margin:1rem 0;
        list-style: none;
    }
    fieldset .chapterSteps ul.position_list li{
        margin-bottom:5px;
    }
    form.apply-form .chapterSteps h2{
        font-size:22px;
    }
}
</style>

<div class="container-fluid">
    <div class="col-xs-12 col-md-12">
        <div class="container">
            <div class="assessment-container">
                <div class="row">
                    <div class="form-box">
                        <fieldset class="col-xs-12 col-md-10 col-md-offset-1">
                            <form class="apply-form" id="applynow" method="POST" enctype="multipart/form-data">
                                <div class="form-top chapterSteps">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="row" style="padding:0 10px;">
                                            <h2 class="main-title text-center"><?php _e("[:kh]ស្នើសុំបម្រើការងារ[:en]Application for Employment[:]");?></h2>
                                            <p class="text-left green"><?php _e("[:kh]១. ព័ត៌មានបេក្ខជន[:en]1. Personal Information[:]");?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="colxs-12 col-md-12">
                                            <div class="form-group col-xs-8 col-sm-8 col-md-4 required">
                                                <input id="application_name" name="Name" class="form-input form-control" type="text" placeholder="<?php _e("[:kh]នាមត្រកូល និងនាមខ្លួន[:en]Last Name and Given Name[:]");?>" />
                                            </div>
                                            <div class="form-group col-xs-4 col-sm-4 col-md-2 form-select required">
                                                <select id="application_gender" name="Gender" class="form-input form-control">
                                                    <option value="" disabled​ selected><?php _e("[:kh]ភេទ[:en]Sex[:]");?></option>
                                                    <option value="Male"><?php _e("[:kh]ប្រុស[:en]Male[:]");?></option>
                                                    <option value="Female"><?php _e("[:kh]ស្រី[:en]Female[:]");?></option>
                                                </select>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12 col-md-6 required">
                                                <input type="text" name="Phone_Numer"  class="form-input form-control" id="phone_number" placeholder="<?php _e("[:kh]លេខទូរស័ព្ទ[:en]Phone Number[:]");?>" required/>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12 col-md-6 required">
                                                <div class="input-group date"  id="applicant_dob">
                                                    <input type="text" id="date_of_birth" name="DOB" class="form-input form-control" readonly placeholder="<?php _e("[:kh]ថ្ងៃខែឆ្នាំកំណើត[:en]Date of Birth[:]");?>"/>
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar green" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12 col-md-6 required">
                                                <input type="text" name="National_ID"  class="form-input form-control" id="national_id" placeholder="<?php _e("[:kh]លេខអត្តសញ្ញាណប័ណ្ណសញ្ជាតិខ្មែរ[:en]Khmer Identity Card Number[:]");?>" required/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-12">
                                        <div class="row" style="padding:0 10px;">
                                            <p class="text-left green"><?php _e("[:kh]២. ទីលំនៅអចិន្រ្តៃយ៍[:en]2. Permanent Address[:]");?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="colxs-12 col-md-12">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6 required form-select">
                                                <select autofocus name="Province" class="form-input form-control province" id="province_dob" required>
                                                    <option value="" disabled selected><?php _e("[:kh]រាជធានី/ខេត្ត[:en]Province/Capital[:]");?></option>
                                                    <?php echo getlocation(1); ?>
                                                </select>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12 col-md-6 required form-select">
                                                <select autofocus name="District" class="form-input form-control district" id="district" required>
                                                    <option value="" disabled selected><?php _e("[:kh]ស្រុក/ខណ្ឌ[:en]District/Khan[:]");?></option>
                                                    <?php echo getlocation(2); ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-12">
                                        <div class="row" style="padding:0 10px;">
                                            <p class="text-left green"><?php _e("[:kh]៣. ប្រវត្តិសិក្សារអប់រំចុងក្រោយ[:en]3. The Last Education Background[:]");?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="colxs-12 col-md-12">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-6 required">
                                                <select name="Degree" id="degree" class="form-input form-control">
                                                    <option value="" disabled selected><?php _e("[:kh]កម្រិតសិក្សា[:en]Degree[:]");?></option>
                                                    <option value="Under BacII"><?php _e("[:kh]ធ្លាក់បាក់ឌុប[:en]Under BacII[:]");?></option>
                                                    <option value="BacII"><?php _e("[:kh]ជាប់បាក់ឌុក[:en]BacII[:]");?></option>
                                                    <option value="Association Degree Year1"><?php _e("[:kh]បរិញ្ញាបត្ររង ឆ្នាំទី១[:en]Association Degree Year1[:]");?></option>
                                                    <option value="Association Degree Year2"><?php _e("[:kh]បរិញ្ញាបត្ររង ឆ្នាំទី២[:en]Association Degree Year2[:]");?></option>
                                                    <option value="Graduated Association Degree"><?php _e("[:kh]បញ្ចប់បរិញ្ញាបត្ររង[:en]Graduated Association Degree[:]");?></option>
                                                    <option value="Bachelor Degree​ Year1"><?php _e("[:kh]បរិញ្ញាបត្រ ឆ្នាំទី១[:en]Bachelor Degree Year1[:]");?></option>
                                                    <option value="Bachelor Degree Year2"><?php _e("[:kh]បរិញ្ញាបត្រ ឆ្នាំទី២[:en]Bachelor Degree Year2[:]");?></option>
                                                    <option value="Bachelor Degree Year3"><?php _e("[:kh]បរិញ្ញាបត្រ ឆ្នាំទី៣[:en]Bachelor Degree Year3[:]");?></option>
                                                    <option value="Bachelor Degree Year4"><?php _e("[:kh]បរិញ្ញាបត្រ ឆ្នាំទី៤[:en]Bachelor Degree Year4[:]");?></option>
                                                    <option value="Graduated Bachelor Degree"><?php _e("[:kh]បញ្ចប់បរិញ្ញាបត្រ[:en]Graduated Bachelor Degree[:]");?></option>
                                                    <option value="Master Degree Year1"><?php _e("[:kh]បរិញ្ញាបត្រជាន់ខ្ពស់ ឆ្នាំទី១[:en]Master Degree Year1[:]");?></option>
                                                    <option value="Master Degree Year2"><?php _e("[:kh]បរិញ្ញាបត្រជាន់ខ្ពស់ ឆ្នាំទី២[:en]Master Degree Year2[:]");?></option>
                                                    <option value="Graduated Master Degree"><?php _e("[:kh]បញ្ចប់បរិញ្ញាបត្រជាន់ខ្ពស់[:en]Graduated Master Degree[:]");?></option>
                                                </select>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12 col-md-6 required">
                                                <input type="text" name="Major"  class="form-input form-control" id="major" placeholder="<?php _e("[:kh]ជំនាញ[:en]Major[:]");?>" required/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-xs-12 col-md-12">
                                        <div class="row" style="padding:0 10px;">
                                            <p class="text-left green"><?php _e("[:kh]៤. បទពិសោធន៍ការងារចុងក្រោយ[:en]4. The Last Experience[:]");?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="colxs-12 col-md-12">
                                            <div class="form-group col-xs-12 col-sm-8 col-md-4">
                                                <input name="Position" id="position" class="form-input form-control" type="text" placeholder="<?php _e("[:kh]តួនាទី[:en]Position[:]");?>" />
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-4 col-md-2 form-select">
                                                <div class="input-group">
                                                    <input name="Duration" id="duration" class="form-input form-control" type="text" placeholder="<?php _e("[:kh]រយៈពេល[:en]Duration[:]");?>" />
                                                    <div class="input-group-addon"><?php _e("[:kh]ឆ្នាំ[:en]Year[:]");?></div>
                                                </div>
                                            </div>

                                            <div class="form-group col-xs-12 col-sm-12 col-md-6 ">
                                                <input type="text" name="Company/Org"  class="form-input form-control" id="company" placeholder="<?php _e("[:kh]ស្ថាប័ន/ក្រុមហ៊ុន[:en]Company/Organisation[:]");?>"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-md-12">
                                        <div class="row" style="padding:0 10px;">
                                            <p class="text-left green"><?php _e("[:kh]៥. តួនាទី និងសាខាសុំបម្រើការងារ[:en]5. Apply for a Position and Preferred Branch[:]");?></p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="colxs-12 col-md-12">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-3 required">
                                            <?php 
                                            // $default_pos = array('credit-officer'=>'[:kh]មន្រ្ដីឥណទាន[:en]Credit Officer[:]','teller'=>'[:kh]បេឡាធិការ[:en]Teller[:]','service-assistant'=>'[:kh]មន្រ្ដីជំនួយការសេវាកម្ម[:en]Service Assistant[:]','loan-admin-offer'=>'[:kh]មន្រ្ដីរដ្ឋបាលឥណទាន[:en]Loan Admin Officer[:]','customer-service-officer'=>'[:kh]មន្រ្ដីបម្រើសេវាអតិថិជន[:en]Customer Service Officer[:]');
                                            global $post; 
                                            // $current_pos = array();
                                            $current_date = current_time('Ymd');
                                            query_posts(array(
                                                'post_type' => 'career',
                                                'posts_per_page' => '-1',
                                                'post_status' => 'publish',
                                                'meta_query'     => array(
                                                    'relation' => 'OR',
                                                    array(
                                                    'key'     => 'job_deadline',
                                                    'compare' => '>=',
                                                    'value'   => $current_date,
                                                    'type'    => 'DATE'
                                                    ),
                                                    array(
                                                    'key'     => 'job_dateType',
                                                    'compare' => '==',
                                                    'value'   => 'no',
                                                    'type'    => 'STRING'
                                                    )
                                                    ),
                                                'orderby'     =>'post_date',
                                                'order'       => 'DESC'
                                            ));
                                            if (have_posts() ):
                                                echo '<select name="Position_Apply" id="position_apply" class="form-input form-control">';
                                                echo '<option value="" disabled selected>';
                                                    _e("[:kh]តួនាទី[:en]Position[:]");
                                                echo '</option>';
                                                $get_pos = isset($_GET['pos'])?$_GET['pos']:'';
                                                $current_pos = array();
                                                while(have_posts() ):the_post();
                                                    $post_title = get_the_title();
                                                    $post_slug = $post->post_name;
                                                    if($post_slug && $post_slug != ''){
                                                        $current_pos[$post_slug] = $post_title;
                                                        echo '<option value="';
                                                        echo $post_title;
                                                        echo '"';
                                                        echo $get_pos==$post_title?' selected ':'';
                                                        echo '>';
                                                        echo $post_title;
                                                        echo '</option>';
                                                    }
                                                endwhile;
                                                echo '</select>';
                                            endif;
                                            ?>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-3 required form-select required">
                                                <select name="Branch_Apply" class="form-input form-control branch" id="branch_apply" required>
                                                    <option value=""​ disabled selected><?php _e("[:kh]សាខា[:en]Branch[:]");?></option>
                                                    <?php echo getlocation(5);?>
                                                </select>
                                            </div>

                                            <div class="form-group col-xs-6 col-sm-4 col-md-2">
                                                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/captcha/captcha_career.php?rand=<?php echo rand(); ?>" id="captcha_image_applynow" style="height:38px;"/>
                                            </div>

                                            <div class="form-group col-xs-6 col-sm-8 col-md-4 required">
                                                <input type="text" name="captcha_career" required id="captcha_career" class="form-input form-control" placeholder="<?php _e("[:kh]បញ្ចូលលេខ[:en]Enter Number[:]");?>"/>
                                            </div>
                                            
                                            <div class="col-xs-12 col-sm-8 col-md-6 pull-right text-center">
                                                <span class="info alert"><br />
                                                    <?= _e('[:kh]មិនអាចអានរួច មែនឬទេ? [:en]Can not read this Number?[:]');?>
                                                    <a href="javascript: refreshCaptcha();" style="color:#f0ad4e;text-decoration:underline;"><?php _e('[:kh]ចុចទីនេះ[:en] Click here[:]');?></a>
                                                    <?= _e('[:kh] ដើម្បីបង្ហាញលេខថ្មី![:en] for a new number![:]');?>
                                                </span>
                                                <p class="error_captcha_status_applynow"></p>
                                            </div>
                                        </div>
                                        <div class="form-group col-xs-12 col-sm-12 col-md-12 button-section">
                                                <input type="hidden" name="action" value="ApplynowCaptcha" />
                                                <button type="submit" class="btn btn-primary back-btn btn-submit"  href="javascript:void(0);" name="applynow-sumbit"><span>
                                                <i class="fa fa-paper-plane" aria-hidden="true"></i></span> <?php _e("[:kh]ដាក់ពាក្យ[:en]Apply[:]");?></button>
                                            </div>
                                    </div>
                                    <div class="row" style="margin-top:5rem;">
                                        <div class="col-xs-12 col-md-12">
                                            <h3 class="text-center green"><?php _e("[:kh]មុខដំណែងកំពុងជ្រើសរើស[:en]Available Positions[:]");?></h3>
                                        </div>
                                        <div class="col-xs-12 col-md-12">
                                            <?php
                                            echo '<ul class="position_list">';
                                            foreach($current_pos as $key=>$value){
                                                echo '<li data-title="';
                                                _e($value);
                                                echo '"';
                                                echo ' data-slug="'.$key.'">';
                                                _e($value);
                                                echo '</li>';
                                            }
                                            echo '</ul>';
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </fieldset>
                        
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <hr>
                                <div class="single-footer-share">
                                    <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                                    <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                                    <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo get_permalink();?>','twitter');"></a>
                                    <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo get_permalink();?>','linkedin');"></a>
                                    <div class="fb-like pull-right" data-href="<?php echo get_permalink();?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="false"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="loader"></div>
<div id="overlay">
    <div id="popup">
        <div id="close">X</div>
        <h2 class="green" style="font-weight:lighter;margin:0 0 10px;"><?php _e('[:kh]ជោគជ័យ![:en]Success![:]');?></h2>
        <p style="margin-bottom:0px;"><?php _e('[:kh]ពាក្យស្នើសុំបម្រើការងាររបស់អ្នកបានបញ្ជូនដោយជោគជ័យ។[:en]Your Application was Successfully Submitted.[:]');?></p>
        <p><?php _e('[:kh]សូមអរគុណ៕[:en]Thank You.[:]');?></p>
    </div>
</div>
<?php get_footer();?>


<script src="<?php echo get_template_directory_uri(); ?>/assets/datepicker/locale/bootstrap-datepicker.kh.min.js"></script>
<script>

    jQuery("#province_dob").change(function(e){
        var $province_id = jQuery(this).find("option:selected").val();
        var formURL = '/prasac/wp-admin/admin-ajax.php';
        var request_method = "POST";
        var postData = { 'provinceID':$province_id,'action': 'FilterBranch'}
        jQuery.ajax({
            url: formURL,
            type: request_method,
            data: postData,
            success: function(data, textStatus, jqXHR) {
                jQuery("#branch_apply").html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });
    jQuery("#date_of_birth").on('change', function(e) {
        jQuery("form#applynow").bootstrapValidator('revalidateField', 'DOB');
        
    });

    // jQuery('#account_number').keydown(function (e) {
    //     var key = e.charCode || e.keyCode || 0;
    //     $text = jQuery(this); 
    //     if (key !== 8 && key !== 9) {
    //         if ($text.val().length === 8) {
    //             $text.val($text.val() + '-');
    //         }

    //         if ($text.val().length === 12) {
    //             $text.val($text.val() + '-');
    //         }
    //     }
    //     return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
    // });

    jQuery(document).ready(function () {
        jQuery('#district').chained("#province_dob");
        var spinner = jQuery('#loader');

        jQuery('#close').click(function() {
            jQuery('#overlay').fadeOut(300);
            jQuery("form#applynow")[0].reset();
        });
        
        jQuery('.form-box fieldset:first-child').fadeIn('slow');
        // next step
        jQuery('div.alert-danger.step-verification').hide();

        jQuery("ul.position_list li").click(function(e){
            var $clicked_pos = jQuery(this).data('slug');
            window.location.href='/career/'+$clicked_pos;
        });

        jQuery('form#applynow').bootstrapValidator({
            fields: {
                Name: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Fullname is required.[:kh]ឈ្មោះ​ត្រូវតែ​បំពេញ[:]")?>'
                        },
                    }
                },
                Gender: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Gender is required.[:kh]ភេទ​ត្រូវតែ​ជ្រើសរើស[:]")?>'
                        },
                    }
                },
                DOB: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Date of Birth is required.[:kh]សូមជ្រើសរើសថ្ងៃខែឆ្នាំកំណើត​[:]")?>'
                        },
                    }
                },
                National_ID: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]National ID is required.[:kh]លេខអត្តសញ្ញាណបណ្ណ​ត្រូវតែ​បំពេញ[:]")?>'
                        },
                        stringLength: {
                            min:9,
                            max:10,
                            message: '<?php _e("[:en]National ID must be 9 digits[:kh]លេខអត្តសញ្ញាណបណ្ណគឺ 9 ខ្ទង់[:]")?>',
                        },
                    }
                },
                Phone_Numer:{
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Phone Number is required.[:kh]លេខទូរស័ព្ទ​ត្រូវតែ​បំពេញ[:]")?>'
                        },
                        stringLength: {
                            min:9,
                            max:10,
                            message: '<?php _e("[:en]Phone number from 9 to 10 digits[:kh]លេខទូរស័ព្ទគឺ ពី 9 ទៅ 10 ខ្ទង់ប៉ុណ្ណោះ[:]")?>',
                        },
                        regexp: {
                            regexp: /^[0-9_\.]+$/,
                            message: '<?php _e("[:en]Phone number must be numberic[:kh]លេខទូរស័ព្ទត្រូវតែជាលេខ[:]")?>'
                        },
                    }
                },
                Province:{
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Province is required.[:kh]សូមជ្រើសរើសខេត្ត/ក្រុង![:]")?>'
                        }
                    }
                },
                District:{
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]District is required.[:kh]សូមជ្រើសរើសស្រុក/ខណ្ឌ![:]")?>'
                        }
                    }
                },
                Degree: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Degree is required.[:kh]សូមជ្រើសរើសកម្រិតសិក្សា[:]")?>'
                        },
                    }
                },
                Major: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Major is required.[:kh]ជំនាញត្រូវតែបំពេញ[:]")?>'
                        },
                    }
                },
                Duration: {
                    validators: {
                        regexp: {
                            regexp: /^[0-9_\.]+$/,
                            message: '<?php _e("[:en]Duration must be numberic[:kh]រយៈពេលត្រូវតែជាលេខ[:]")?>'
                        },
                            between: {
                            min: 0.1,
                            max: 30,
                            message: '<?php _e("[:en]Duration must be from 0.3 to 30[:kh]រយៈពេលត្រូវតែចាប់ផ្ដើមពី ០.៨ ទៅ ៣0[:]")?>'
                        }
                    }
                },
                Position_Apply:{
                    validators:{
                        notEmpty:{
                            message:'<?php _e("[:en]Position is required![:kh]មុខតំណែងត្រូវតែបំពេញ[:]")?>'
                        }
                    }
                },
                Branch_Apply:{
                    validators:{
                        notEmpty:{
                            message:'<?php _e("[:en]Please select branch![:kh]សូមជ្រើសរើសសាខាដែលអ្នកចង់បម្រើការ![:]")?>'
                        }
                    }
                },
                captcha_career:{
                    validators:{
                        notEmpty:{
                            message:'<?php _e("[:en]Number is required![:kh]សូមវាយបញ្ចូលលេខខាងឆ្វេង[:]")?>'
                        },
                        regexp: {
                            regexp: /^[0-9_\.]+$/,
                            message: '<?php _e("[:en]Incorrect![:kh]មិនត្រឹមត្រូវទេ![:]")?>'
                        }
                    }
                }
            },
        }).on('success.form.bv', function(e, data){
            spinner.show();
            e.preventDefault();
            var $form = jQuery('form#applynow');
            var jqxhr = jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                method: "POST",
                data: $form.serializeArray(), 
                success: function(data) {
                    if(data =="Success"){
                        var $gender= jQuery("#application_gender option:selected").val();
                        var $name= jQuery("#application_name").val();
                        var $dob= jQuery("#date_of_birth").val();
                        var $phone_number="'" + jQuery("#phone_number").val();
                        var $national_id="'" + jQuery("#national_id").val();
                        var $province_final = jQuery("#province_dob option:selected").val() == 12?'Capital':'Province';
                        var $province_selected = jQuery("#province_dob option:selected").data('province') +' '+$province_final;
                        var $district_final = jQuery("#province_dob option:selected").val() == 12?'Khan '+jQuery("#district option:selected").data('district'):jQuery("#district option:selected").data('district')+' District';
                        var $permanent_address = $district_final+', '+ $province_selected;
                        var $degree= jQuery("#degree option:selected").val();
                        var $major= jQuery("#major").val();
                        var $position= jQuery("#position").val();
                        var $duration= jQuery("#duration").val();
                        var $company= jQuery("#company").val();
                        var $position_apply= jQuery("#position_apply option:selected").val();
                        var $branch_apply= jQuery("#branch_apply option:selected").data('branch');
                        var script_url = "https://script.google.com/macros/s/AKfycbx2G0ZnBigpdRB__PWWkF-MxzB50lXFM87EcqoJ1Q9HjXe1SU0/exec";
                        var url = script_url+"?action=insert&name="+$name+"&dob="+$dob+"&gender="+$gender+"&phone="+$phone_number+"&national_id="+$national_id+"&address="+$permanent_address+"&degree="+$degree+"&major="+$major+"&position="+$position+"&duration="+$duration+"&company="+$company+"&position_apply="+$position_apply+"&branch_apply="+$branch_apply;
                        var request = jQuery.ajax({
                            crossDomain: true,
                            url: url ,
                            method: "GET",
                            dataType: "jsonp",
                            success: function(data) {
                                spinner.hide();
                                jQuery('#overlay').fadeIn(300);
                            },
                            error: function(data) {
                                spinner.hide();
                            }
                        });
                    }else{
                        refreshCaptcha();
                        jQuery(".error_captcha_status_applynow").html("<div class='alert alert-danger' role='alert' style='padding:5px 10px;margin: 0 auto;vertical-align: middle;display: table;'><i class='fa fa-times-circle-o' aria-hidden='true'></i> <?php echo _e('[:kh]លេខដែលលោកអ្នកបញ្ចូលមិនត្រឹមត្រូវទេ[:en]Sorry! your entered number didn\'t match.[:]');?></div>");
                        spinner.hide();
                    }
                },
                error: function(data) {
                    jQuery(".loading-spinner-wrapper").css("opacity",0);
                    console.log('Error '+data);
                }
            });
        });
     
        jQuery("#nonmodal2").dialog({
            minWidth: 600,
            minHeight: 'auto',
            autoOpen: false,
            dialogClass: 'custom-ui-widget-header-accessible',
            position: {
                my: 'center',
                at: 'left+900'
            }
        });

        jQuery("input,textarea").focus(function() {
            jQuery(this)
                .parents(".form-group")
                .addClass("focused");
            });

            jQuery("input,textarea").blur(function() {
            var inputValue = jQuery(this).val();
            if (inputValue == "") {
                jQuery(this).removeClass("filled");
                jQuery(this)
                .parents(".form-group")
                .removeClass("focused");
            } else {
                jQuery(this).addClass("filled");
            }
        });
       
        jQuery("#applicant_dob").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            changeYear:false,
            changeMonth:true,
            clearBtn: true,
            language: "<?php echo _e('[:kh]kh[:en]en[:]');?>",
            todayHighlight: true,
            startDate: '-65y',
            endDate:'-18y',
            templates: {
                leftArrow: '<i class="fa fa-chevron-circle-left"></i>',
                rightArrow: '<i class="fa fa-chevron-circle-right"></i>'
            },
        });

        function ctrlq(e) {
            spinner.hide();
            alert("Successful");
        }
    });

    function refreshCaptcha(){
        var img = document.images['captcha_image_applynow'];
        img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
    }
</script>
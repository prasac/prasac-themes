<?php
/*
* template name: Online Request
*/

get_header();

?>
<style>
form.apply-form input[type=radio] {
    margin-top: 8px !important;
}
form.apply-form{
    font-size:16px;
}

form.apply-form textarea,
form.apply-form textarea.form.apply-form-control {
    padding-top: 10px;
    padding-bottom: 10px;
    line-height: 30px;
}
form.apply-form input[type="text"]:-moz-placeholder,
form.apply-form input[type="password"]:-moz-placeholder,
form.apply-form textarea:-moz-placeholder,
form.apply-form textarea.form.apply-form-control:-moz-placeholder {
    color: #888;
}

form.apply-form input[type="text"]:-ms-input-placeholder,
form.apply-form input[type="password"]:-ms-input-placeholder,
form.apply-form textarea:-ms-input-placeholder,
form.apply-form textarea.form.apply-form-control:-ms-input-placeholder {
    color: #888;
}

form.apply-form input[type="text"]::-webkit-input-placeholder,
form.apply-form input[type="password"]::-webkit-input-placeholder,
form.apply-form textarea::-webkit-input-placeholder,
form.apply-form textarea.form.apply-form-control::-webkit-input-placeholder {
    color: #888;
}

form.apply-form button.btn:hover {
    color: #fff;
}

form.apply-form button.btn:active {
    outline: 0;
    color: #fff;
    -moz-box-shadow: none;
    -webkit-box-shadow: none;
    box-shadow: none;
}

form.apply-form button.btn:focus {
    outline: 0;
    background: #4db848;
    color: #fff;
}

form.apply-form button.btn:active:focus,
form.apply-form button.btn.active:focus {
    outline: 0;
    background: #4db848;
    color: #fff;
}

form.apply-form strong {
    font-weight: 500;
}
form.apply-form ul.service-list{
    padding:10px 0;
    margin:0;
}

form.apply-form ul.service-list li a,
form.apply-form ul.service-list li  a:hover,
form.apply-form ul.service-list li  a:focus {
    color: #4db848;
    text-decoration: none;
    -o-transition: all .3s;
    -moz-transition: all .3s;
    -webkit-transition: all .3s;
    -ms-transition: all .3s;
    transition: all .3s;
}

form.apply-form h1,
form.apply-form h2 {
    margin-top: 10px;
    font-size: 38px;
    font-weight: 100;
    color: #ffffff;
    line-height: 50px;
}

form.apply-form h3 {
    font-size: 22px;
    font-weight: 300;
    color: #555;
    line-height: 30px;
}

form.apply-form .btn-link-1 {
    display: inline-block;
    height: 50px;
    margin: 0 5px;
    padding: 16px 20px 0 20px;
    background: #4db848;
    font-size: 16px;
    font-weight: 300;
    line-height: 16px;
    color: #fff;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
}

form.apply-form .btn-link-1:hover,
form.apply-form .btn-link-1:focus,
form.apply-form .btn-link-1:active {
    outline: 0;
    opacity: 0.6;
    color: #fff;
}

form.apply-form .btn-link-2 {
    display: inline-block;
    height: 50px;
    margin: 0 5px;
    padding: 15px 20px 0 20px;
    background: rgba(0, 0, 0, 0.3);
    border: 1px solid #fff;
    font-size: 16px;
    font-weight: 300;
    line-height: 16px;
    color: #fff;
    -moz-border-radius: 4px;
    -webkit-border-radius: 4px;
    border-radius: 4px;
}

form.apply-form .btn-link-2:hover,
form.apply-form .btn-link-2:focus,
form.apply-form .btn-link-2:active,
form.apply-form .btn-link-2:active:focus {
    outline: 0;
    opacity: 0.6;
    background: rgba(0, 0, 0, 0.3);
    color: #fff;
}


/***** Top content *****/

.assessment-container .form-box {
    padding:15px 0;
    overflow:hidden;
}

.assessment-container .form-box fieldset{font-size:16px;}

form.apply-form .form-top {
    overflow: hidden;
    padding-bottom: 10px;
    -moz-border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
    border-radius: 4px;
    text-align: left;
    transition: opacity .3s ease-in-out;
}

form.apply-form .form-bottom {
    padding: 25px 25px 30px 25px;
    background: #eee;
    -moz-border-radius: 0 0 4px 4px;
    -webkit-border-radius: 0 0 4px 4px;
    border-radius: 0 0 4px 4px;
    text-align: left;
    transition: all .4s ease-in-out;
}

form.apply-form .form-bottom:hover {
    -webkit-box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
}

form.apply-form .form-bottom button.btn {
    min-width: 105px;
}

form.apply-form .form-bottom .input-error {
    border-color: #d03e3e;
    color: #d03e3e;
}

.form-box fieldset {
    display: none;
    padding:0;
}
fieldset .chapterSteps ul li {
    float: left;
    padding: 0 5px;
    text-align: center;
    width:33.33%;
    margin:1rem 0;
    list-style: none;
}

fieldset#deposit .chapterSteps ul li {
    float: left;
    padding: 0 5px;
    text-align: center;
    width:25%;
    margin:1rem 0;
    list-style: none;
}
fieldset .chapterSteps ul.service-list li a span.chapterIcons {
    background:#ffffff;
    border-radius: 100%;
    color: #4db848;
    display: inline-block;
    font-size: 25px;
    height:100px;
    line-height: 50px;
    position: relative;
    text-align: center;
    width:100px;
    transition: all 400ms ease 0s;
}

fieldset .chapterSteps ul.service-list li a:hover span.chapterIcons {
    color:#4db848;
    transform: scale(0.9);
    transition-timing-function: cubic-bezier(0.47, 2.02, 0.31, -0.36);
}
fieldset .chapterSteps ul.service-list li a span.chapterIcons:before {
    content: "";
    height:100px;
    position: absolute;
    width:100px;
    -moz-border-radius: 100%;
    -webkit-border-radius: 100%;
    border-radius: 100%;
    left:0;
    top:-5px;
}


fieldset .chapterSteps ul.service-list li a span.chapterIcons:before{
    border:1px solid #4db848;
}
fieldset .chapterSteps ul.service-list li a:hover span.chapterIcons:before{
    box-shadow: 0 0 2px 10px #4db84833;
}
fieldset .chapterSteps ul.service-list li a span.chapterIcons img{
    width:90px;
}

fieldset .chapterSteps ul.service-list li a.active span.chapterIcons{
    transform: scale(0.9);
    transition-timing-function: cubic-bezier(0.47, 2.02, 0.31, -0.36);
}
fieldset .chapterSteps ul.service-list li a.active span.chapterIcons:before,
fieldset .chapterSteps ul.service-list li a:hover span.chapterIcons:before
{
    height: 105px;
    left: -3px;
    position: absolute;
    top: -7px;
    width: 105px;
    border: 1px solid #4db848;
    /* box-shadow: 0 0 2px 10px #4db84833; */
}
fieldset .chapterSteps ul.service-list li a.active span.chapterLink,
fieldset .chapterSteps ul.service-list li a:hover span.chapterLink
{
    transform: scale(1.1);
    transition: all .3s ease-in-out;
}

fieldset .chapterSteps ul li a span.chapterLink {
    color: #4db848;
    display: block;
    font-size: 16px;
    margin-top: 10px;
    text-align: center;
    line-height: 22px;
}


.bannerSection .upperBanner .newChapter .button-section {
    height: auto;
    margin-top: 20px;
    width: 100%;
    float: left;
    text-align: center;
}

form.apply-form a{
    color:#fff;
}

fieldset div.button-section {
    height: auto;
    width: 100%;
    float: left;
    text-align: center;
}
.button-section .btn-primary {
    background-color:#ffffff;
    border: 1px solid #4db848;
    -moz-border-radius:2rem;
    -webkit-border-radius: 2rem;
    color:#4db848;
    font-size: 20px;
    padding: 0.3rem 1rem;
    text-transform:capitalize;
    border-radius:0.3rem;
    margin:0 5px;
}
.button-section .btn-primary:after{
    border: 2px solid red;
}

.button-section .btn-primary:hover,.button-section .btn-primary:focus, .button-section .btn-primary.focus, .button-section .btn-primary:active:focus,
.btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active.focus, .btn-primary:active:focus,
.btn-primary:active:hover, .open>.dropdown-toggle.btn-primary.focus, .open>.dropdown-toggle.btn-primary:focus,
.open>.dropdown-toggle.btn-primary:hover{
    background:#4db848;
    color: #ffffff;
    box-shadow:0 2px 5px #616161;
    border-color:#4db848;
}

form.apply-form .chapterSteps h2{
    font-size:2em;
    padding-top: 1.2rem;
    padding-bottom: 0.8rem;
    color:#4db848;
    margin: 0;
    font-family:Hanuman,Roboto, sans-serif;
}
form.apply-form .alert-danger.step-verification{
    position:absolute;
    overflow:hidden;
    padding:5px;
    margin:0;
    border-radius:0;
    background:rgba(242, 176, 29,.7);
    border:rgb(242, 176, 29);
    bottom:5em;
}
form.apply-form input#amount{
    background: url("<?php echo get_template_directory_uri(); ?>/assets/icons/dollarIcon.png") no-repeat 10px 10px #fff;
    color: #848484 !important;
    cursor: default;
    display: inline-block;
    font-weight: 300;
    height:40px;
    line-height:2;
    padding: 0 20px 0 25px;
    text-align: left;
    border: none;
    width:200px;
    margin:15px 0;
    font-size:1.5em;
}
.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{
    background:#eea236;
}

form.apply-form .form-group{
    position: relative;
    margin:0;
    padding:10px;
}

form.apply-form .form-label{
  position: absolute;
  left:2em;
  top:1.5em;
  color: #151414;
  background-color:transparent;
  z-index: 10;
  transition: font-size 150ms ease-out, -webkit-transform 150ms ease-out;
  transition: transform 150ms ease-out, font-size 150ms ease-out;
  transition: transform 150ms ease-out, font-size 150ms ease-out, -webkit-transform 150ms ease-out;
  font-weight:lighter;
}
form.apply-form .form-group.form-select .form-label{
    left:1rem;
    top:-0.8rem;
    font-weight:lighter;
}
form.apply-form .focused .form-label {
  -webkit-transform: translateY(-125%);
   transform: translateY(-125%);
   left:15px;
   top:15px;
}

form.apply-form .form-input {
  position: relative;
  padding: 12px 0px 5px 0;
  width: 100%;
  outline: 0;
  border: 0;
}
input#service_name_other::placeholder{
    color: #ffffff;
}

form.apply-form input.form-input:focus,form.apply-form input.form-control:focus,form.apply-form textarea.form-control:focus,select.form-control,select.form-control:focus,.has-success .form-control:focus,form .input-group .form-control:focus,form .form-input.form-control:focus{
    box-shadow: inset 0 0 5px rgba(0,0,0,.3);
    border-color: #008329;
}
label.control-label{
    color:#ffffff;
}

form.apply-form input.form-control, form.apply-form select.form-control,form.apply-form span.input-group-addon{
    background:transparent;
    border-radius:0;
    border:0.5px solid #CCCCCC;
    padding:0 10px;
    box-shadow:none;
    -webkit-box-shadow:none;
    z-index:22;
    height:40px;
}
form.apply-form textarea.form-control{
    background:transparent;
    border-radius:0;
    border:0.5px solid #CCCCCC;
    padding:0 10px;
    box-shadow:none;
    -webkit-box-shadow:none;
    z-index:22;
}
input#captcha_deposit,input#captcha_loan{
    height:40px;
}
input#captcha_deposit+.form-label,input#captcha_loan+.form-label{
    top:2.3rem;
}

.form-group.focused .form-label,.form-group.focused .form-label,form.apply-form .form-group.form-select .form-label{
    color:#006917;
}

.form-group.focused input#captcha_deposit+.form-label,.form-group.focused input#captcha_loan+.form-label{
    top:1rem;
}

#loader {
  display: none;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  background: rgba(0,0,0,0.75) url(<?php echo get_template_directory_uri(); ?>/assets/images/prasac_loading.gif) no-repeat center center;
  z-index: 10000;
}
.has-success .control-label{
    color:#fff;
}

select.branch option,select.province option,select.district option,select.commune option,select.village option{
    color:#66686a;
}
h2.main-title{
    font-family: "Khmer OS Content", Roboto, sans-serif;
    position: relative;
    z-index: 1;
    text-align: center;
    padding-bottom:5px;
    line-height:30px !important;
}
.has-error .help-block{
    color:#a94442;
    position:absolute;
    right:1rem;
    bottom: -0.65rem;
}
fieldset#banking-service .form-group.required:after,
fieldset#loan-desc .form-group.required:after,
fieldset#bank-confirmation .form-group.required:after{
    content:" *";
    color:#a94442;
    position:absolute;
    right:0;
    top:40%;
    font-size:16px;
}
.st1{
    fill:red !important;
    color:red;
}
.loading-spinner-wrapper {
  position: relative;
  width: 100%;
  left: 0;
  text-align: center;
  z-index: 1000;
  margin:10px 0;
  opacity: 0;
  -webkit-transition: all 0.3s ease;
  -moz-transition: all 0.3s ease;
  -ms-transition: all 0.3s ease;
  -o-transition: all 0.3s ease;
  transition: all 0.3s ease; }

.loading-spinner {
  position: relative;
  vertical-align: middle;
  width: 50px;
  height: 12px;
  text-align: center;
  margin: 0 auto;
  display: block; }

.loading-spinner i {
  position: absolute;
  top: 0;
  width: 11px;
  height: 11px;
  border-radius: 50%;
  background: #f89f1b;
  -webkit-animation: pulse 1s infinite ease-in-out;
  -moz-animation: pulse 1s infinite ease-in-out;
  animation: pulse 1s infinite ease-in-out;
  -webkit-animation-fill-mode: both;
  -moz-animation-fill-mode: both;
  animation-fill-mode: both; }

.loading-spinner i.one {
  left: 0;
  -webkit-animation-delay: -.32s;
  -moz-animation-delay: -.32s;
  animation-delay: -.32s;
  }

.loading-spinner i.two {
  left: 50%;
  -webkit-transform: translateX(-50%);
  -moz-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  -o-transform: translateX(-50%);
  transform: translateX(-50%);
  -webkit-animation-delay: -.16s;
  -moz-animation-delay: -.16s;
  animation-delay: -.16s; }

.loading-spinner i.three {
  right: 0; }

.loading-spinner i {
  background: #f89f1b; 
}

#client_dob .input-group-addon{
    cursor:pointer;
}

/*Bootstrap Calendar*/
.datepicker {
  border-radius: 0;
  padding: 0;
}
.datepicker-days table thead,
.datepicker-days table tbody,
.datepicker-days table tfoot {
  padding: 10px;
  display: list-item;
  overflow:hidden;
}
.datepicker-days table thead,
.datepicker-months table thead,
.datepicker-years table thead,
.datepicker-decades table thead,
.datepicker-centuries table thead {
  background:#4cb849;
  color: #ffffff;
  border-radius: 0;
}

.datepicker .datepicker-days table tbody tr td.active{
    background:#4cb849;
    color: #ffffff;
}

.datepicker-days table thead tr:nth-child(2n + 0) td,
.datepicker-days table thead tr:nth-child(2n + 0) th {
  border-radius: 3px;
}
.datepicker-days table thead tr:nth-child(3n + 0) {
  text-transform: uppercase;
  font-weight: 300 !important;
  font-size: 12px;
  color: rgba(255, 255, 255, 0.85);
}
.table-condensed > thead > tr > td,
.table-condensed > thead > tr > th {
    padding:5px 9px;
    font-weight: lighter;
}

.table-condensed > tfoot tr:nth-child(1){
    float:left;
}
.table-condensed > tfoot tr:nth-child(1) th,.table-condensed > tfoot tr:nth-child(2) th{
    width: 50%;
    border-radius: 0;
    background-color:#FFFFFF;
    color:#4cb849;
    font-weight: lighter;
    border:1px solid #4cb849;
}
.table-condensed > tfoot tr:nth-child(1) th:hover,.table-condensed > tfoot tr:nth-child(2) th:hover{
    background-color:#4cb849;
    color:#FFFFFF;
}
.table-condensed > tfoot tr:nth-child(2){
    float:right;
}
.table-condensed > tbody > tr > td,
.table-condensed > tbody > tr > th,
.table-condensed > tfoot > tr > td,
.table-condensed > tfoot > tr > th{
  padding: 11px 13px;
}
.datepicker-months table thead td,
.datepicker-months table thead th,
.datepicker-years table thead td,
.datepicker-years table thead th,
.datepicker-decades table thead td,
.datepicker-decades table thead th,
.datepicker-centuries table thead td,
.datepicker-centuries table thead th {
  border-radius: 0;
}
.datepicker td,
.datepicker th {
  border-radius: 50%;
  padding: 0 12px;
}
.datepicker-days table thead,
.datepicker-months table thead,
.datepicker-years table thead,
.datepicker-decades table thead,
.datepicker-centuries table thead {
  background:#4cb849;
  color: #ffffff;
  border-radius: 0;
}
form.apply-form .has-error input.form-control,
form.apply-form .has-error select.form-control{
    border-color: #a94442;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
}
.datepicker table tr td.active,
.datepicker table tr td.active:hover,
.datepicker table tr td.active.disabled,
.datepicker table tr td.active.disabled:hover {
  background-image: none;
}
.datepicker .prev,
.datepicker .next {
  color: rgba(255, 255, 255, 0.5);
  transition: 0.3s;
  width: 37px;
  height: 37px;
}
.datepicker .prev:hover,
.datepicker .next:hover {
  background: transparent;
  color: rgba(255, 255, 255, 0.99);
  font-size: 21px;
}
.datepicker .datepicker-switch {
  font-size: 24px;
  font-weight: 400;
  transition: 0.3s;
}
.datepicker .datepicker-switch:hover {
  color: rgba(255, 255, 255, 0.7);
  background: transparent;
}
.datepicker table tr td span {
  border-radius: 2px;
  margin: 3%;
  width: 27%;
}
.datepicker table tr td span.active,
.datepicker table tr td span.active:hover,
.datepicker table tr td span.active.disabled,
.datepicker table tr td span.active.disabled:hover {
  background-color: #3546b3;
  background-image: none;
}
.dropdown-menu {
  border: 1px solid rgba(0, 0, 0, 0.1);
  box-shadow: 0 6px 12px rgba(0, 0, 0, 0.175);
}
.datepicker-dropdown.datepicker-orient-top:before {
  border-top: 7px solid rgba(0, 0, 0, 0.1);
}

#progress-bar-wrap {
  min-height: 20px;
  display: none;
  margin-bottom: 0;
}

#progress-bar-wrap .encouragement { display: none; }

#progress-bar-wrap .close:before {
  content: "\f0d7";
  display: inline-block;
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

#progress-bar-wrap span { line-height: 1.75em; }

#progress-bar-wrap.collapsed h4 {
  font-size: 14px;
  color: #999999;
}

#progress-bar-wrap.collapsed .encouragement { display: none; }

#progress-bar-wrap.collapsed .progress { display: none; }
#progress-bar-wrap .progress {
    height: 10px;
    margin-bottom: 0;
    overflow: hidden;
    background-color: #f5f5f5;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
    box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
}

#progress-bar-wrap.collapsed .close:before {
  content: "\f0d8";
  display: inline-block;
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
  line-height: .75em;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
.progress-bar-info{
    background-color:#4cb849;
}
.info.alert{
    font-size:14px;
}
.ui-dialog .ui-dialog-content,.ui-dialog-buttonset,.ui-dialog .ui-dialog-title,.ui-dialog .ui-dialog-buttonpane button{
    font-family:Hanuman,Roboto, sans-serif;
    font-weight:lighter;
}
.ui-dialog-buttonset>button,.ui-dialog-buttonset>button:focus,.ui-dialog-buttonset>button:visited{
    border:1px solid red;
    color:red;
}
.ui-dialog .ui-dialog-titlebar-close{
    visibility: hidden;
}
.ui-widget-overlay.ui-front{
    z-index:1000;
    opacity:0.8;
}
.ui-widget.ui-widget-content{
    z-index:1001;
}
.ui-draggable .ui-dialog-titlebar{
    background-color:#4cb8491f;
    color:#4cb849;
}
.datepicker-days table thead tr:nth-child(2n + 0) td, .datepicker-days table thead tr:nth-child(2n + 0) th{
    font-size:16px;
}

  
@-webkit-keyframes pulse {
    0%, 100%, 80% {
    opacity: 0; 
    }
    40% {
        opacity: 1;
    }
}

@-moz-keyframes pulse {
    0%, 100%, 80% {
        opacity: 0; 
    }
    40% {
        opacity: 1;
    }
}

@keyframes pulse {
    0%, 100%, 80% {
    opacity: 0;
    }
    40% {
        opacity: 1;
    }
}

/*==========  Mobile First Method  ==========*/

/* Custom, iPhone Retina */ 
@media only screen and (min-width : 320px) {
    fieldset .chapterSteps ul li{
        width:50%;
    }

    form.apply-form,fieldset .chapterSteps ul li a span.chapterLink,.form-control{
        font-size:14px;
    }

    fieldset .chapterSteps ul li {
        padding: 0 5px;
        text-align: center;
        width:50%;
        margin:1rem 0;
        list-style: none;
    }

    fieldset#deposit .chapterSteps ul li {
        padding: 0 5px;
        text-align: center;
        width:50%;
        margin:1rem 0;
        list-style: none;
    }
    fieldset .chapterSteps ul.service-list li a span.chapterIcons img{
        width:80px;
    }
    fieldset .chapterSteps ul.service-list li a span.chapterIcons:before,
    fieldset .chapterSteps ul.service-list li a.active span.chapterIcons:before,
    fieldset .chapterSteps ul.service-list li a:hover span.chapterIcons:before{
        height:95px;
        width:95px;
        left: 3px;
        top: -7px;
    }
}

/* Extra Small Devices, Phones */ 
@media only screen and (max-width : 480px) {
    
    form.apply-form .chapterSteps h2{
        font-size:1.8rem;
        padding:0.4rem;
    }
    form.apply-form .chapterSteps h3{
        font-size:1.3rem;
    }
}

/* Extra Small Devices, Phones */ 
@media only screen and (min-width : 480px) {
    fieldset .chapterSteps ul li{
        width:50%;
    }

    form.apply-form,fieldset .chapterSteps ul li a span.chapterLink,.form-control{
        font-size:14px;
    }

    fieldset .chapterSteps ul li {
        padding: 0 5px;
        text-align: center;
        width:50%;
        margin:1rem 0;
        list-style: none;
    }

    fieldset#deposit .chapterSteps ul li {
        padding: 0 5px;
        text-align: center;
        width:50%;
        margin:1rem 0;
        list-style: none;
    }
    fieldset .chapterSteps ul.service-list li a span.chapterIcons img{
        width:80px;
    }
    fieldset .chapterSteps ul.service-list li a span.chapterIcons:before,
    fieldset .chapterSteps ul.service-list li a.active span.chapterIcons:before,
    fieldset .chapterSteps ul.service-list li a:hover span.chapterIcons:before{
        height:95px;
        width:95px;
        left: 3px;
        top: -7px;
    }
    form.apply-form .chapterSteps h2{
        font-size:2rem;
        padding:0.4rem;
    }
    form.apply-form .chapterSteps h3{
        font-size:1.3rem;
    }
}

/* Small Devices, Tablets */
@media only screen and (min-width : 768px) {
    fieldset .chapterSteps ul li{
        width:50%;
    }

    form.apply-form,fieldset .chapterSteps ul li a span.chapterLink{
        font-size:14px;
    }

    fieldset .chapterSteps ul li {
        padding: 0 5px;
        text-align: center;
        width:33.33%;
        margin:1rem 0;
        list-style: none;
    }

    fieldset#deposit .chapterSteps ul li {
        padding: 0 5px;
        text-align: center;
        width:33.33%;
        margin:1rem 0;
        list-style: none;
    }
    fieldset .chapterSteps ul.service-list li a span.chapterIcons img{
        width:80px;
    }
    fieldset .chapterSteps ul.service-list li a span.chapterIcons:before,
    fieldset .chapterSteps ul.service-list li a.active span.chapterIcons:before,
    fieldset .chapterSteps ul.service-list li a:hover span.chapterIcons:before{
        height:95px;
        width:95px;
        left: 3px;
        top: -7px;
    }

}

/* Medium Devices, Desktops */
@media only screen and (min-width : 992px) {
    form.apply-form,fieldset .chapterSteps ul li a span.chapterLink,.form-control{
        font-size:14px;
    }

    fieldset .chapterSteps ul li{
        width:50%;
    }

    fieldset .chapterSteps ul li {
        padding: 0 5px;
        text-align: center;
        width:33.33%;
        margin:1rem 0;
        list-style: none;
    }

    fieldset#deposit .chapterSteps ul li {
        padding: 0 5px;
        text-align: center;
        width:25%;
        margin:1rem 0;
        list-style: none;
    }
}

/* Large Devices, Wide Screens */
@media only screen and (min-width : 1200px) {
    fieldset .chapterSteps ul li{
        width:50%;
    }

    form.apply-form,fieldset .chapterSteps ul li a span.chapterLink,.form-control{
        font-size:15px;
    }

    fieldset .chapterSteps ul li {
        padding: 0 5px;
        text-align: center;
        width:33.33%;
        margin:1rem 0;
        list-style: none;
    }

    fieldset#deposit .chapterSteps ul li {
        padding: 0 5px;
        text-align: center;
        width:25%;
        margin:1rem 0;
        list-style: none;
    }
    fieldset .chapterSteps ul.service-list li:nth-child(4){
        margin-left:11rem;
    }
    form.apply-form .chapterSteps h2{
        font-size:1.8rem;
        padding:0.4rem;
    }
    form.apply-form .chapterSteps h3{
        font-size:1.5rem;
    }
    
}
</style>
<div class="container-fluid">
    <div class="col-xs-12 col-md-12">
        <div class="container">
            <div class="assessment-container">
                <div class="row">
                    <div class="form-box">
                        <fieldset class="col-xs-12 col-md-10 col-md-offset-1" data-level="main-service" id="main-service" style="display:none;">
                            <form class="apply-form">
                                <div class="form-top chapterSteps">
                                    <div class="col-xs-12 col-md-12">
                                        <h2 class="main-title"><?php _e("[:kh]សូមស្វាគមន៍[:en]Welcome Online Request[:]");?></h2>
                                        <h3 class="text-center" style="margin:0;padding:0;color:#4db848;"><?php _e("[:kh]ការស្នើសុំប្រើប្រាស់សេវាហិរញ្ញវត្ថុតាមអនឡាញ[:en]for the Financial Services Usage[:]");?></h3>
                                        <p class="text-center" style="margin:10px 0;font-size:14px;"><?php _e("[:kh]សូមជ្រើសរើសសេវាហិរញ្ញវត្ថុណាមួយដែលលោកអ្នកត្រូវការ[:en]What would You Like to Do?[:]");?></p>
                                    </div>
                                    <ul class="service-list">
                                        <?php           
                                        global $wpdb;                         
                                        $main_service_query = "SELECT * FROM pr65ac2015_online_request_main_service ORDER BY created_date DESC ";
                                        $main_service_query_result = $wpdb->get_results($main_service_query, OBJECT);
                                        // var_dump($main_service_query_result);
                                        foreach($main_service_query_result as $lists):
                                            $data_next     = $lists->type;
                                            $data_core_service = $lists->core_service;
                                            $data_value = $lists->id;
                                            $title  = __('[:kh]title[:en]title_en[:]');
                                            $icon  = $lists->icon;
                                            $page = $lists->form_page;

                                            $form_head_title  = __('[:kh]form_head_text_kh[:en]form_head_text_en[:]');

                                            echo '<li>
                                                    <a class="'.$data_core_service.'" href="javascript:(0);" data-page="'.$page.'"  data-next="'.$data_next.'" data-value="'.$data_value.'" data-service="'.$data_core_service.'" data-formhead="'.$lists->$form_head_title.'">
                                                        <span class="chapterIcons prasac prasac-icon-loan">
                                                            <img src="'.get_template_directory_uri().'/layout/images/'. $icon .'"/>
                                                        </span>
                                                        <span class="chapterLink">';
                                                        echo $lists->$title;
                                                    echo '</span>
                                                    </a>
                                                </li>';
                                       endforeach;
                                        ?>
                                    </ul>
                                </div>
                            </form>
                        </fieldset>
                        <fieldset class="col-xs-12 col-md-10 col-md-offset-1" data-level="banking-service" id="banking-service">
                            <form role="form" class="apply-form" method="POST"  id="account_opening" enctype="multipart/form-data">
                                <div class="form-top chapterSteps">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="row" style="padding:0 10px;">
                                            <h2 class="main-title text-center"><?php _e("[:kh]បញ្ញើសន្សំ[:en]Deposit[:]");?></h2>
                                            <p class="text-center"><?php _e("[:kh]សូមបំពេញព័ត៌មានខាងក្រោមរួចចុចលើប៊ូតុងស្នើសុំ[:en]Please Fill below Information and Click Submit[:]");?></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-md-5 col-md-offset-1">
                                            <div class="form-group col-xs-12 col-md-12 form-select">
                                                <div id="banking_service">
                                                    <?php
                                                        global $wpdb;
                                                        $product_query = "SELECT * FROM pr65ac2015_online_request_product WHERE main_product_id = 1 ORDER BY id ASC ";
                                                        $product_query_result = $wpdb->get_results($product_query, OBJECT);
                                                        echo '<select name="deposit_service" class="form-input form-control" id="deposit_service" >';
                                                    echo '<option value="" disabled selected>';
                                                    _e("[:kh]សូមជ្រើសរើសប្រភេទគណនី[:en]Select Account Type[:]");
                                                    echo '</option>';
                                                        foreach($product_query_result as $lists):
                                                            $data_next     = $lists->type;
                                                            $data_value = $lists->id;
                                                            $titles  = __('[:kh]title[:en]title_en[:]');
                                                            $icon  = $lists->icon;
                                                            echo "<option value='$data_value'>".$lists->$titles."</option>";
                                                        endforeach;
                                                        echo '</select>';
                                                    ?>
                                                </div>
                                                <div id="feebase">
                                                    <input name="account_number" id="account_number" class="form-input form-control" type="text" placeholder="<?php _e("[:kh]សូមបញ្ចូលលេខគណនីសន្សំរបស់លោកអ្នក (១២ខ្ទង់)[:en]Please enter your account number (12 digits)[:]");?>"/>
                                                </div>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12 required">
                                                <input name="full_name" class="form-input form-control" type="text" placeholder="<?php _e("[:kh]ឈ្មោះ[:en]Name[:]");?>" required/>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12 required">
                                                <input type="text" name="phone_number" class="form-input form-control"  placeholder="<?php _e("[:kh]លេខទូរស័ព្ទ[:en]Phone Number[:]");?>" required/>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                                <input type="text" name="email_address" class="form-input form-control" placeholder="<?php _e("[:kh]អុីម៉ែល[:en]Email Address[:]");?>"/>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-5">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12 required form-select">
                                                <select name="province" class="form-input form-control province" id="province_deposit" required>
                                                    <option value="" disabled selected><?php _e("[:kh]ខេត្ត/ក្រុង[:en]Province/City[:]");?></option>
                                                    <?php echo getlocation(1); ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12 required form-select">
                                                <select name="branch" class="form-input form-control branch" id="branch_deposit" required>
                                                    <option value="" selected disabled><?php _e("[:kh]ជ្រើសរើសសាខា[:en]Select Branch[:]");?></option>
                                                    <?php echo getlocation(5); ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-xs-12 col-md-6 col-sm-6">
                                                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/captcha/captcha_deposit.php?rand=<?php echo rand();?>" id='captcha_image_deposit' style="height:40px;">
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-6 col-md-6 required">
                                                <input type="text" name="captcha_deposit" id="captcha_deposit" required class="form-input form-control" placeholder="<?php _e("[:kh]បញ្ចូលលេខ[:en]Enter Number[:]");?>"/>
                                            </div>
                                            <div class="col-xs-12 col-md-12 pull-right text-center">
                                                <span class="info alert"><br />
                                                    <?= _e('[:kh]មិនអាចអានរួច មែនឬទេ?[:en]Can not read this Number?[:]');?>
                                                    <a href="javascript: refreshCaptcha1();" style="color:red;text-decoration:underline;"><?php _e('[:kh]ចុចទីនេះ[:en] Click here[:]');?></a>
                                                    <?= _e('[:kh] ដើម្បីបង្ហាញលេខថ្មី[:en] for a new one[:]');?>
                                                </span>
                                                <p class="error_captcha_status_deposit"></p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12">
                                            <div class="loading-spinner-wrapper" id="app-loader">
                                                <span class="loading-spinner">
                                                    <i class="one"></i>
                                                    <i class="two"></i>
                                                    <i class="three"></i>
                                                </span>
                                            </div>
                                            <p class="message_status_deposit"></p>
                                        </div>
                                    </div>
                                    <div class="button-section">
                                        <input type="hidden" name="main_service" class="main_service"/>
                                        <input type="hidden" name="action" value="StoreOnlineRequest" />
                                        <input type="hidden" name="request_type" value="banking-service"/>
                                        <a class="btn btn-primary back-btn btn-previous"  href="javascript:void(0);" data-prev="deposit"​><span>
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i></span> <?php _e("[:kh]ត្រលប់[:en]Previous[:]");?></a>
                                        <button type="submit" class="btn btn-primary back-btn btn-submit"  href="javascript:void(0);" name="banking-service-request-sumbit"><span>
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i></span> <?php _e("[:kh]ស្នើសុំ[:en]Submit[:]");?></button>
                                    </div>
                                </div>
                            </form>
                        </fieldset>
                        <fieldset class="col-xs-12 col-md-10 col-md-offset-1" data-level="loan-desc" id="loan-desc">
                            <form role="form" class="apply-form" method="POST"  id="request_loan" enctype="multipart/form-data">
                                <div class="form-top chapterSteps">
                                    <div class="col-xs-12 col-md-12">
                                        <div class="row" style="padding:0 10px;">
                                            <h2 class="main-title text-center"><?php _e("[:kh]ឥណទាន[:en]Loan[:]");?></h2>
                                            <p class="text-center"><?php _e("[:kh]សូមបំពេញព័ត៌មានខាងក្រោមរួចចុចលើប៊ូតុងស្នើសុំ[:en]Please Fill below Information and Click Submit[:]");?></p>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="colxs-12 col-md-6">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12 required">
                                                <input name="full_name" class="form-input form-control" type="text" placeholder="<?php _e("[:kh]ឈ្មោះ[:en]Name[:]");?>" />
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12 required">
                                                <input type="text" name="phone_number"  class="form-input form-control" id="phone_number" placeholder="<?php _e("[:kh]លេខទូរស័ព្ទ[:en]Phone Number[:]");?>" required/>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                                <div class="input-group date"  id="client_dob">
                                                    <input type="text" name="client_dob" class="form-input form-control" readonly placeholder="<?php _e("[:kh]ថ្ងៃខែឆ្នាំកំណើត[:en]Date of Birth[:]");?>"/>
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar green" aria-hidden="true"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                                <input type="text" name="email_address" class="form-input form-control" placeholder="<?php _e("[:kh]អុីម៉ែល[:en]Email Address[:]");?>" />
                                            </div>
                                            <div class="form-group col-xs-6 col-sm-4 col-md-4 form-select">
                                                <select name="loan_currency" id="loan_currency" class="form-input form-control">
                                                    <option value="" disabled><?php _e("[:kh]រូបិយប័ណ្ណ[:en]Currency[:]");?></option>
                                                    <option value="KHR"><?php _e("[:kh]រៀល[:en]KHR[:]");?></option>
                                                    <option value="USD"><?php _e("[:kh]ដុល្លារ[:en]USD[:]");?></option>
                                                </select>
                                            </div>
                                            <div class="form-group col-xs-6 col-sm-8 col-md-8">
                                                <input type="text" name="loan_amount" class="form-input form-control currency" id="loan_amount" placeholder="<?php _e("[:kh]ទំហំកម្ចី[:en]Loan Amount[:]");?>" />
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                                <div class="input-group">
                                                    <select name="loan_term" id="loan_term" class="form-input form-control">
                                                        <option value="" disabled><?php _e("[:kh]រយៈពេលខ្ចីប្រាក់[:en]Loan Term[:]");?></option>
                                                        <?php 
                                                        $loan_term_max = 120;
                                                            for($i=2;$i<= $loan_term_max; $i+= 2){
                                                                echo "<option value='$i'>$i</option>";
                                                            }
                                                        ?>
                                                    </select>
                                                    <div class="input-group-addon"><?php _e("[:kh]ខែ[:en]Months[:]");?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="colxs-12 col-md-6">
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12">
                                                <textarea name="loan_purpose" id="" cols="30" rows="3" class="form-input form-control" id="loan_purpose" placeholder="<?php _e("[:kh]សូមបំពេញគោលបំណងខ្ចីប្រាក់[:en]Please enter your loan purpose[:]");?>"></textarea>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12 required form-select">
                                                <select autofocus name="province" class="form-input form-control province" id="province_loan" required>
                                                    <option value="" disabled selected><?php _e("[:kh]ខេត្ត/ក្រុង[:en]Province/City[:]");?></option>
                                                    <?php echo getlocation(1); ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-12 col-md-12 required form-select">
                                                <select name="branch" class="form-input form-control branch" id="branch_loan" required>
                                                    <option value="" selected disabled><?php _e("[:kh]ជ្រើសរើសសាខា[:en]Select Branch[:]");?></option>
                                                    <?php echo getlocation(5);?>
                                                </select>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-6 col-md-6">
                                                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/captcha/captcha_loan.php?rand=<?php echo rand(); ?>" id="captcha_image_loan" style="height:40px;"/>
                                            </div>
                                            <div class="form-group col-xs-12 col-sm-6 col-md-6 required">
                                                <input type="text" name="captcha_loan" required id="captcha_loan" class="form-input form-control" placeholder="<?php _e("[:kh]បញ្ចូលលេខ[:en]Enter Number[:]");?>"/>
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-12 pull-right text-center">
                                                <span class="info alert"><br />
                                                    <?= _e('[:kh]មិនអាចអានរួច មែនឬទេ? [:en]Can not read this Number?[:]');?>
                                                    <a href="javascript: refreshCaptcha2();" style="color:red;text-decoration:underline;"><?php _e('[:kh]ចុចទីនេះ[:en] Click here[:]');?></a>
                                                    <?= _e('[:kh] ដើម្បីបង្ហាញលេខថ្មី[:en] for a new one[:]');?>
                                                </span>
                                                <p class="error_captcha_status_loan"></p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-12">
                                            <div class="loading-spinner-wrapper" id="app-loader">
                                                <span class="loading-spinner">
                                                <i class="one"></i>
                                                <i class="two"></i>
                                                <i class="three"></i>
                                                </span>
                                            </div>
                                            <p class="message_status_loan"></p>
                                        </div>
                                    </div>
                                    <div class="button-section">
                                        <input type="hidden" name="main_service" class="main_service"/>
                                        <input type="hidden" name="secondary_service" class="secondary_service">
                                        <input type="hidden" name="action" value="StoreOnlineRequest" />
                                        <input type="hidden" name="request_type" value="Loan"/>
                                        <a class="btn btn-primary back-btn btn-previous"  href="javascript:void(0);" data-prev="main-service"><span>
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i></span> <?php _e("[:kh]ត្រលប់[:en]Previous[:]");?></a>
                                        <button type="submit" class="btn btn-primary back-btn btn-submit"  href="javascript:void(0);" name="loan-request-sumbit"><span>
                                        <i class="fa fa-file-text-o" aria-hidden="true"></i></span> <?php _e("[:kh]ស្នើសុំ[:en]Submit[:]");?></button>
                                    </div>
                                </div>
                            </form>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="loader"></div>
<div id="dialog" style="display: none;"><?php _e('[:kh]<span style="color:#4db848;">សូមអរគុណ! </span>សំណើរបស់លោកអ្នកត្រូវបានបញ្ជូនទៅកាន់ក្រុមការងារយើងខ្ញុំដោយជោគជ័យ[:en]<span style="color:#4db848;">Thank You! </span>Your request has ben sent successfully to our team[:]')?></div>

<?php get_footer();?>
<script src="<?php echo get_template_directory_uri(); ?>/assets/datepicker/locale/bootstrap-datepicker.kh.min.js"></script>
<script>

    jQuery('input.currency').number(true,0);
    jQuery("#loan_currency").change(function(){
        var $selected_currency = jQuery(this).val();
        if($selected_currency=='USD'){
            jQuery('input.currency').number(true,2);
        }else{
            jQuery('input.currency').number(true,0);
        }
    });

    jQuery("#province_deposit").change(function(e){
        var $province_id = jQuery(this).find("option:selected").val();
        var formURL = '/wp-admin/admin-ajax.php';
        var request_method = "POST";
        var postData = { 'provinceID':$province_id,'action': 'FilterBranch',}
        jQuery.ajax({
            url: formURL,
            type: request_method,
            data: postData,
            success: function(data, textStatus, jqXHR) {
                jQuery("#branch_deposit").html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    jQuery("#province_loan").change(function(e){
        var $province_id = jQuery(this).find("option:selected").val();
        var formURL = '/wp-admin/admin-ajax.php';
        var request_method = "POST";
        var postData = { 'provinceID':$province_id,'action': 'FilterBranch',}
        jQuery.ajax({
            url: formURL,
            type: request_method,
            data: postData,
            success: function(data, textStatus, jqXHR) {
                jQuery("#branch_loan").html(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    
    jQuery('#account_number').keydown(function (e) {
        var key = e.charCode || e.keyCode || 0;
        $text = jQuery(this); 
        if (key !== 8 && key !== 9) {
            if ($text.val().length === 8) {
                $text.val($text.val() + '-');
            }

            if ($text.val().length === 12) {
                $text.val($text.val() + '-');
            }
        }
        return (key == 8 || key == 9 || key == 46 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
    });

    jQuery(document).ready(function () {

        var spinner = jQuery('#loader');
        
        jQuery('.form-box fieldset:first-child').fadeIn('slow');
        // next step
        jQuery('div.alert-danger.step-verification').hide();

        jQuery("#dialog").dialog({
            modal: true,
            autoOpen: false,
            width: 350,
            height: 160,
            title:"<?php _e('[:kh]ជោគជ័យ![:en]Successfull![:]')?>",
            buttons: [
                {
                    id: "Yes",
                    text: "<?php _e('[:kh]ចាកចេញ[:en]Cancel[:]');?>",
                    click: function () {
                        jQuery("[id*=btnYes]").attr("rel", "Yes");
                        jQuery(this).dialog('close');
                        window.history.pushState({},'', "https://www.prasac.com.kh/online-request");
                        location.reload();
                    }
                }
            ]
        });
        jQuery("#dialog").effect('slide');

        
        //Trigger click by get parametter
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const product = urlParams.get('product');
        var $service_item_class = '';
        if(product){
            switch(product){
                case 'deposit':
                    $service_item_class = 'fieldset ul.service-list li:nth-child(1) a';
                    break;
                case 'loan':
                    $service_item_class = 'fieldset ul.service-list li:nth-child(2) a';
                    break;
                case 'atm':
                    $service_item_class = 'fieldset ul.service-list li:nth-child(3) a';
                    break;
                case 'mobile-banking':
                    $service_item_class = 'fieldset ul.service-list li:nth-child(4) a';
                    break;
                case 'bank-confirmation':
                    $service_item_class = 'fieldset ul.service-list li:nth-child(5) a';
                    jQuery("fieldset#banking-service input#account_number").parent().parent().addClass(' required');
                    break;
                default:
                    jQuery("fieldset#banking-service input#account_number").parent().parent().removeClass(' required');
                    break;
            }
            jQuery($service_item_class).addClass("active");
            spinner.show();
        }
        

        jQuery('fieldset ul.service-list li a').on('click', function (){
            var parent_fieldset = jQuery(this).parents('fieldset');
            var next_button = jQuery(this).data('next');
            var thisobj = jQuery(this);
            if(next_button){
                next_step = true;
            }else{
                next_step = false;
            }
           

            if (next_step) {
                parent_fieldset.fadeOut(400, function(){
                    var current_fieldset =  parent_fieldset.data("level");
                    if("main-service" == current_fieldset){
                        nextfieldset =  next_button;
                        var selected_service = jQuery(this).find('ul.service-list li a.active').data('formhead');

                        var main_service_selected = thisobj.data("value");
                        jQuery('input[type="hidden"].main_service').val(main_service_selected);

                        var $data_service = thisobj.data("service");

                        if(nextfieldset == "banking-service"){
                            var find_element =  jQuery('fieldset#banking-service').find('.btn-previous');
                            find_element.attr('data-prev','main-service');
                        }else{
                            var find_element =  jQuery('fieldset#banking-service').find('.btn-previous');
                            find_element.attr('data-prev','deposit');
                        }
                        
                        if($data_service=='banking-service'){
                            jQuery('form#account_opening div#banking_service').show();
                            jQuery('form#account_opening div#feebase').hide();
                        }else{
                            jQuery('form#account_opening div#banking_service').hide();
                            jQuery('form#account_opening div#feebase').show();
                        }

                    }else{
                        nextfieldset =  next_button;
                        var find_element =  jQuery('fieldset.'+nextfieldset).find('.btn-previous');
                        find_element.attr('data-prev','deposit');
                    }

                    // spinner.show();
                   
                    if(nextfieldset !='undefined'){
                        spinner.hide();
                        jQuery("fieldset#"+nextfieldset).fadeIn();
                        jQuery('fieldset#'+nextfieldset).find('.main-title').text(selected_service);
                    }else{
                        jQuery("fieldset#"+next_step).next().fadeIn();
                    }

                    var $page = thisobj.data("page");
                    if($page == 'bank-confirmation'){
                        window.history.pushState({},'', "?product="+$page);
                    }
                });
            }
            return false;
        });

        
        jQuery($service_item_class).trigger('click');
        var selected_service_load = jQuery($service_item_class).data('formhead');
        jQuery($service_item_class).parent().find('fieldset').find('h2.main-title').text(selected_service_load);
        // previous step
        jQuery('fieldset .button-section a.btn-previous').on('click', function () {
            var prevfieldset =  jQuery(this).attr("data-prev");
            var fieldset_object = jQuery(this).parents('fieldset');
            var old_active_item = fieldset_object.find('ul.service-list li a').hasClass('active');
            var current_form = fieldset_object.find("form").attr("id");
            if(old_active_item){
                jQuery(this).parents('fieldset').find('ul.service-list li a').removeClass('active');
            }
            fieldset_object.find('');
            fieldset_object.fadeOut(400, function () {
                if(prevfieldset){
                    jQuery("fieldset#"+prevfieldset).fadeIn();
                }else{
                    jQuery(this).prev().fadeIn();
                }
            });

            fieldset_object.find('form#'+current_form).bootstrapValidator('resetForm', true);
            window.history.pushState({},'', "https://www.prasac.com.kh/online-request");
        });

        
        //Active main menu
        jQuery("fieldset .chapterSteps ul.service-list li a").on("click", function() {
            var fieldset_object = jQuery(this).parents('fieldset');
            var old_active_item = fieldset_object.find('ul.service-list li a').hasClass('active');
            if(old_active_item){
                jQuery(this).parents('fieldset').find('ul.service-list li a').removeClass('active');
            }
            jQuery(this).addClass("active");
        });
        jQuery('form#account_opening').bootstrapValidator({
            fields: {
                account_number:{
                    validators:{
                        stringLength: {
                            min: 14,
                            max: 14,
                            message: '<?php _e("[:en]Account number must be 12 digits[:kh]លេខគណនីត្រូវតែ 12​ ខ្ទង់[:]")?>',
                        },
                        regexp: {
                            regexp: /^[0-9_ \-]+$/,
                            message: '<?php _e("[:en]Account number must be number 0-9[:kh]លេខគណនីត្រូវតែជាលេខពី 0 ដល់ 9[:]")?>'
                        },
                        callback: {
                            message: '<?php _e("[:en]Account number is required![:kh]លេខគណនីគឺចាំបាច់ត្រូវតែបំពេញ[:]")?>',
                            callback: function(value, validator) {
                                var accountNumber = jQuery("#account_number").val();
                                var valueMain =  jQuery('input[type="hidden"].main_service').val();
                                // console.log(valueMain);
                                if (valueMain != 5) {
                                    return true;
                                    jQuery("fieldset#banking-service input#account_number").parent().parent().addClass(' required');
                                }else {
                                    if(accountNumber !=''){
                                        return true;
                                    }else{
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                },
                full_name: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Your fullname is required.[:kh]ឈ្មោះលោកអ្នក​ត្រូវតែ​បំពេញ[:]")?>'
                        },
                    }
                },
                phone_number:{
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Phone number is required.[:kh]លេខទូរស័ព្ទលោកអ្នក​ត្រូវតែ​បំពេញ[:]")?>'
                        },
                        stringLength: {
                            min:9,
                            max:10,
                            message: '<?php _e("[:en]Phone number from 9 to 10 digits[:kh]លេខទូរស័ព្ទគឺ ពី 9 ទៅ 10 ប៉ណ្ណុះ[:]")?>',
                        },
                        regexp: {
                            regexp: /^[0-9_ \-]+$/,
                            message: '<?php _e("[:en]Phone number must be in latin[:kh]លេខទូរស័ព្ទត្រូវតែជាលេខឡាតាំង[:]")?>'
                        },
                    }
                },
                email_address:{
                    validators: {
                        emailAddress: {
                            message: '<?php _e("[:en]Email format is invalid[:kh]អុីម៉ែល​របស់លោកអ្នកមិនត្រូវតាមទម្រង់អុីម៉ែលនោះទេ![:]")?>'
                        }
                    }
                },
                province:{
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Province is required.[:kh]សូមជ្រើសរើសខេត្ត/ក្រុង![:]")?>'
                        }
                    }
                },
                branch:{
                    validators:{
                        notEmpty:{
                            message:'<?php _e("[:en]Branch is required[:kh]សូមជ្រើសរើសសាខាប្រាសាក់ដែលនៅជិតលោកអ្នក![:]")?>'
                        }
                    }
                },
                captcha_deposit:{
                    validators:{
                        notEmpty:{
                            message:'<?php _e("[:en]Number is required[:kh]សូមវាយបញ្ចូលលេខខាងឆ្វេង[:]")?>'
                        }
                    }
                }
            },
        }).on('success.form.bv', function(e, data){
            var $form = jQuery('form#account_opening'),
            url = '/wp-admin/admin-ajax.php';
            e.preventDefault();
            jQuery(".loading-spinner-wrapper").css("opacity",1);
            var jqxhr = jQuery.ajax({
                url: url,
                method: "POST",
                // dataType: "json",
                data: $form.serializeArray(), 
                success: function(data) {
                    console.log(data);
                    jQuery(".loading-spinner-wrapper").css("opacity",0);
                    jQuery(".message_status_deposit,.error_captcha_status_deposit").html("");
                    if(data=='Done'){
                        jQuery("#dialog").dialog("open");
                        jQuery("form#account_opening").bootstrapValidator('resetForm', true);
                        jQuery("form#account_opening")[0].reset();
                    }
                    
                    if(data=='Fail'){
                        jQuery(".message_status_loan").html("<div class='alert alert-danger' role='alert' style='padding:5px 10px;margin: 0 auto;vertical-align: middle;display: table;'><i class='fa fa-times-circle-o' aria-hidden='true'></i> <?php echo _e('[:kh]សំណើរបស់លោកអ្នកមិនត្រូវបានបញ្ជូនទេ[:en]Sorry! your reqeust hasn\'t been sent[:]');?></div>");
                    }

                    if(data=='Wrong'){
                        jQuery(".error_captcha_status_deposit").html("<div class='alert alert-danger' role='alert' style='padding:5px 10px;margin: 0 auto;vertical-align: middle;display: table;'><i class='fa fa-times-circle-o' aria-hidden='true'></i> <?php echo _e('[:kh]លេខដែលលោកអ្នកបញ្ចូលមិនត្រឹមត្រូវទេ[:en]Sorry! your entered number didn\'t match.[:]');?></div>");
                    }
                   
                },
                error: function(data) {
                    jQuery(".loading-spinner-wrapper").css("opacity",0);
                    console.log('Error '+data);
                }
            });
        });

        jQuery('form#request_loan').bootstrapValidator({
            fields: {
                full_name: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Your fullname is required.[:kh]ឈ្មោះលោកអ្នក​ត្រូវតែ​បំពេញ[:]")?>'
                        },
                    }
                },
                phone_number:{
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Your fullname is required.[:kh]ឈ្មោះលោកអ្នក​ត្រូវតែ​បំពេញ[:]")?>'
                        },
                        stringLength: {
                            min:9,
                            max:10,
                            message: '<?php _e("[:en]Phone number from 9 to 10 digits[:kh]លេខទូរស័ព្ទគឺ ពី 9 ទៅ 10 ប៉ណ្ណុះ[:]")?>',
                        },
                        regexp: {
                            regexp: /^[0-9_ \-]+$/,
                            message: '<?php _e("[:en]Phone number must be in latin[:kh]លេខទូរស័ព្ទត្រូវតែជាលេខឡាតាំង[:]")?>'
                        },
                    }
                },
                email_address:{
                    validators: {
                        emailAddress: {
                            message: '<?php _e("[:en]Email format is invalid[:kh]អុីម៉ែល​របស់លោកអ្នកមិនត្រូវតាមទម្រង់អុីម៉ែលនោះទេ![:]")?>'
                        }
                    }
                },
                province:{
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Province is required.[:kh]សូមជ្រើសរើសខេត្ត/ក្រុង![:]")?>'
                        }
                    }
                },
                branch:{
                    validators:{
                        notEmpty:{
                            message:'<?php _e("[:en]Branch is required[:kh]សូមជ្រើសរើសសាខាប្រាសាក់ដែលនៅជិតលោកអ្នក![:]")?>'
                        }
                    }
                },
                captcha_loan:{
                    validators:{
                        notEmpty:{
                            message:'<?php _e("[:en]Number is required![:kh]សូមវាយបញ្ចូលលេខបង្ហាញខាងឆ្វេង[:]")?>'
                        }
                    }
                }
            },
        }).on('success.form.bv', function(e, data){
            var $form = jQuery('form#request_loan'),
            url = '/wp-admin/admin-ajax.php';
            e.preventDefault();
            jQuery(".loading-spinner-wrapper").css("opacity",1);
            var jqxhr = jQuery.ajax({
                url: url,
                method: "POST",
                // dataType: "json",
                data: $form.serializeArray(), 
                success: function(data) {
                    console.log(data);
                    jQuery(".loading-spinner-wrapper").css("opacity",0);
                    jQuery(".error_captcha_status_loan,.message_status_loan").html("");
                    if(data =='Done'){
                        jQuery("#dialog").dialog("open");
                        jQuery("form#request_loan").bootstrapValidator('resetForm', true);
                        jQuery("form#request_loan")[0].reset();
                    }

                    if(data=='Fail'){
                        jQuery(".message_status_loan").html("<div class='alert alert-danger' role='alert' style='padding:5px 10px;margin: 0 auto;vertical-align: middle;display: table;'><i class='fa fa-times-circle-o' aria-hidden='true'></i> <?php echo _e('[:kh]សំណើរបស់លោកអ្នកមិនត្រូវបានបញ្ជូនទេ[:en]Sorry! your reqeust hasn\'t been sent[:]');?></div>");
                    }

                    if(data=='Wrong'){
                        jQuery(".error_captcha_status_loan").html("<div class='alert alert-danger' role='alert' style='padding:5px 10px;margin: 0 auto;vertical-align: middle;display: table;'><i class='fa fa-times-circle-o' aria-hidden='true'></i> <?php echo _e('[:kh]លេខដែលលោកអ្នកបញ្ចូលមិនត្រឹមត្រូវទេ[:en]Sorry! your entered number didn\'t match.[:]');?></div>");
                    }
                },
                error: function(data) {
                    jQuery(".loading-spinner-wrapper").css("opacity",0);
                    console.log('Error '+data);
                }
            });
        });

        jQuery("#nonmodal2").dialog({
            minWidth: 600,
            minHeight: 'auto',
            autoOpen: false,
            dialogClass: 'custom-ui-widget-header-accessible',
            position: {
                my: 'center',
                at: 'left+900'
            }
        });

        jQuery("input,textarea").focus(function() {
            jQuery(this)
                .parents(".form-group")
                .addClass("focused");
            });

            jQuery("input,textarea").blur(function() {
            var inputValue = jQuery(this).val();
            if (inputValue == "") {
                jQuery(this).removeClass("filled");
                jQuery(this)
                .parents(".form-group")
                .removeClass("focused");
            } else {
                jQuery(this).addClass("filled");
            }
        });
       
        jQuery("#client_dob").datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            changeYear:false,
            changeMonth:true,
            clearBtn: true,
            language: "<?php echo _e('[:kh]kh[:en]en[:]');?>",
            todayHighlight: true,
            startDate: '-65y',
            endDate:'-18y',
            templates: {
                leftArrow: '<i class="fa fa-chevron-circle-left"></i>',
                rightArrow: '<i class="fa fa-chevron-circle-right"></i>'
            },
        });
    });

    //Refresh Captcha
    function refreshCaptcha1(){
        var img = document.images['captcha_image_deposit'];
        img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
    }

    function refreshCaptcha2(){
        var img = document.images['captcha_image_loan'];
        img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
    }
</script>
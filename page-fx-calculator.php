<?php
// error_reporting(E_ALL);
/**
 * template name: Fx Calculator
 * Autor: Kimhim
 * Description: To calculate FX
 */

get_header();
?>
<style>
    h3{
        color:#4DB848;
    }
   .form-horizontal .control-label{
        text-align:left;
        margin-left:0px;;
        font-weight:normal;
    }
    .input-group form-control-placeholder{
        text-align:right;
    }
    .loan-calculator .form-group{
        /*padding:0;*/
    }
    .bt-layout{
        border:1px solid #dedede;
    }   
    #load_calculator_fieldset{
        border-top-right-radius: 0;
        border-top-left-radius: 0; 
    }
    .btn-warning{
        margin-right:10px;
    }
    table thead tr th{
        text-align:center;
    }
    input[readonly].generate-i{
        background-color:#ffffff;
    }
    .loan-calculator>h3{
        border:none;
    }
    .green-background{
        color:#ffffff;
        background-color:#4DB848;
        font-size:2rem;
        font-weight:500;
    }
    .row-title{
        /* color:#ffffff;
        background-color:#4DB848; */
    }
    .loan-calculator .has-feedback .form-control-feedback{
        top: 0;
        right: 15px;    
    }
    span#print_schedule,.generate_schedule_button{
        color:#4DB848;
        cursor:pointer;

    }
    
    .calculator_form{
        background-color:#f0f0f2;
        padding:10px 5px;
    }
    #btn-calculate,#btn-generate{
        border-radius:0;
        padding:0;
        border:none;
    }
    /* #btn-generate{
        margin-top:15.5rem;
    } */
    h2.note{
        color:#4DB848;
        font-weight:600;
    }
    .number-lg{
        border: none;
        text-align: center;
        font-size: 4rem;
        height: 4rem;
        background-color: transparent !important;
        box-shadow:none;
        color:#000000;
        font-weight:600;
    }
    .number-md{
        border: none;
        text-align: center;
        font-size: 2rem;
        background-color: transparent !important;
        box-shadow:none;
    }
    .div-number-md{
        display:block;
        margin-top:-1rem;
        text-align:right;
    }
    hr{
        padding:0;
        margin:0;
    }
    span.currency_type{
        position: absolute;
        right:0;
        top: 34%;
        font-size: 2rem;
        display:none;
    }
    span.currency_type_md{
        position: absolute;
        right: 0%;
        top: 40%;
        font-size: 1rem;
        display:none;
    }
    .sidebar {
        color: #333;
        text-align: left;
        line-height: 130%;
        padding: 0;
        margin: 0;
        clear: both;
        background-repeat: no-repeat;
        background-position: 0 0;
        box-sizing: border-box;
    }
    .form-control[disabled]{
        background-color:#4DB848;
    }
    .form-group .form-control{
        font-weight:600;
        font-size: 1.1rem;
        height:2.6rem;
    }
    div.print-title h3{
        font-family:Roboto,Khmer OS Muol Light;
        border:none;
        font-size:2rem;
        font-weight:lighter !important;
    }
    div.print-title h3.second-title{
        font-size:1.5rem;
    }
    .title-container{
        position:relative;
        padding:10px;
        overflow:hidden;
    }
    .title-container img.logo-image{
        position:absolute;
        left:0;
        top:0;
        width:100px;
    }
    .applicant-detail{
        background-color:#f0f0f2;
        padding:10px 0;
    }
    #printing-wrapper{
        overflow:hidden;
    }
    button#btn-calculate{
        font-family:Roboto,Khmer OS Muol Light;
        font-weight:lighter !important;
        font-size:1.3rem;
    }
    .cbc_fee_currency{
        font-size:12px;
    }
    span.currency-result{
        color:#4DB848;
    }
    #loader {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        background: rgba(0,0,0,0.75) url(<?php echo get_template_directory_uri(); ?>/assets/images/prasac_loading.gif) no-repeat center center;
        z-index: 10000;
    }
</style>
<div class="container-fluid">
    <main class="container">
        <section class="col-xs-12 col-sm-9 col-md-9 page-title">
            <div class="hidden-xs hidden-sm">
                <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                    <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                    <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                </ul>
            </div>
            <?php the_title('<h3 class="green">','</h3>') ?>
            <?php
                $args = array(
                'post_type'        => 'exchange',
                'showposts'    => 1,
                'post_status'      => 'publish',
                'suppress_filters' => true
                );
                $loop = new WP_Query( $args );
                if( $loop->have_posts() ):
                    while( $loop->have_posts() ): $loop->the_post();
                        $usd_buy  = get_field('usd_buy');
                        $usd_sell = get_field('usd_sell');
                        $thb_buy  = get_field('thb_buy');
                        $thb_sell = get_field('thb_sell');
                    ?>
                    <fieldset class="bt-layout">
                        <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6 calculator_form">
                            <div class="form-group">
                                <label for="amount" class="form-control-placeholder required control-label col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php _e('[:en]Amount[:kh]ចំនួន[:]')?></label>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <input type="text" name="amount" class="form-control input-md input value1" id="amount" placeholder="<?php _e('[:en]Amount[:kh]ចំនួន[:]')?>" value="100">
                                </div>
                            </div><!-- End Group Amount -->
                            <div class="row">
                                <section class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="currency-from" class="form-control-placeholder required control-label col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php _e('[:en]From[:kh]ពី​[:]')?></label>
                                        <div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
                                            <select class="form-control" id="currency-from">
                                                <option value="usd"><?php _e('[:en]USD[:kh]ដុល្លាអាមេរិក[:]') ?></option>
                                                <option value="khr"><?php _e('[:en]KHR[:kh]រៀល[:]') ?></option>
                                                <option value="thb"><?php _e('[:en]THB[:kh]បាត[:]') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </section>
                                <section class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label for="currency-to" class="form-control-placeholder required control-label col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php _e('[:en]To[:kh]ទៅ[:]')?></label>
                                        <div class="col-xs-12 col-sm-10 col-md-10 col-lg-10">
                                            <select class="form-control" id="currency-to">
                                                <option value="khr"><?php _e('[:en]KHR[:kh]រៀល[:]') ?></option>
                                                <option value="usd"><?php _e('[:en]USD[:kh]ដុល្លាអាមេរិក[:]') ?></option>
                                                <option value="thb"><?php _e('[:en]THB[:kh]បាត[:]') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <fieldset>
                                <div class="row">
                                    <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center payment_method_label"><?php _e('[:en]Calculation Result[:kh]លទ្ធផល[:]') ?></label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  text-center">
                                                <span id="result-amount" class="number-md">0.00</span>
                                                <span id="result-currency" class="number-md currency-result"></span>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                            </fieldset>
                        </div>
                    </fieldset>
                <?php 
                endwhile; 
            endif; 
            wp_reset_postdata();
            _e('<br />[:en]<strong style="padding:0;margin:0;">Note:</strong>[:kh]<strong> សម្គាល់៖</strong>​[:]');
            echo '<ul class="list">';
                    echo '<li>';
                        _e('[:en]The figure of this computation is for the indication purpose and subject to change.[:kh]តួលេខនៃការគណនានេះគ្រាន់តែជាព័ត៌មានបឋមប៉ុណ្ណោះ ហើយអាចមានការកែប្រែ។[:]');
                    echo '</li>';
                    echo '<li>';
                        _e('[:en]For any inquiry, please contact our nearest branch or call us at 023 999 911 or 086 999 911.[:kh]សម្រាប់ព័ត៌មានបន្ថែម សូមទាក់ទងសាខាប្រាសាក់ដែលនៅជិតលោកអ្នកបំផុត ឬ ទាក់ទងយើងខ្ញុំតាម 023 999 911 ឬ 086 999 911 ។[:]');
                    echo '</li>';
            echo '</ul>';
            ?>
        </section>
        <aside class="col-sm-3 col-md-3">
            <?= get_sidebar(); ?>
        </aside>
    </main><br />
<div>
<?php get_footer(); ?>
<div id="loader"></div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.js"></script>

<script>
    jQuery('#amount').number( true, 2 );//Convert

    function calculation() {
        var amount = document.getElementById('amount').value;
        amount = amount.replace(/,\s?/g, "");
        var currency_from = jQuery("#currency-from option:selected").val(); 
        var carrency_to = jQuery("#currency-to option:selected").val();

        var usd_buy = <?php echo $usd_buy; ?>;
        var usd_sell = <?php echo $usd_sell; ?>;
        var thb_buy = <?php echo $thb_buy; ?>;
        var thb_sell = <?php echo $thb_sell; ?>;
        switch (currency_from + ' ' + carrency_to) {
            case "khr khr":
                var y = amount * 1;
                jQuery('span#result-amount').number( true);
                jQuery("#result-currency").html(jQuery("#currency-to option:selected").html());
                jQuery("#result-amount").html(parseFloat(Math.round(y * 100) / 100));
            break;
            /* KHR to USD */
            case "khr usd":
                var x = carrency_to = usd_sell;
                var y = amount / x;
                jQuery('span#result-amount').number( true,2);
                jQuery("#result-currency").html(jQuery("#currency-to option:selected").html());
                jQuery("#result-amount").html(parseFloat(Math.round(y * 100) / 100));
            break;
                /* KHR to THB */
                case "khr thb":
                var x = carrency_to = thb_sell;
                var y = amount / x;
                jQuery('span#result-amount').number( true);
                jQuery("#result-currency").html(jQuery("#currency-to option:selected").html());
                jQuery("#result-amount").html(parseFloat(Math.round(y * 100) / 100));
            break;
                /* USD to USD */
                case "usd usd":
                var y = amount * 1;
                jQuery('span#result-amount').number( true);
                jQuery("#result-currency").html(jQuery("#currency-to option:selected").html());
                jQuery("#result-amount").html(parseFloat(Math.round(y * 100) / 100));
            break;
                /* USD to KHR */
            case "usd khr":
                var x = carrency_to = usd_buy;
                var y = amount * x;
                jQuery("#result-currency").html(jQuery("#currency-to option:selected").html());
                jQuery("#result-amount").html(parseFloat(Math.round(y * 100) / 100));
                jQuery('span#result-amount').number( true);
            break;
                /* USD to THB */
                case "usd thb":
                var x = carrency_to = usd_buy / thb_sell;
                var y = amount * x;
                jQuery('span#result-amount').number( true);
                jQuery("#result-currency").html(jQuery("#currency-to option:selected").html());
                jQuery("#result-amount").html(parseFloat(Math.round(y * 100) / 100));
            break;
            /* THB to THB */
            case "thb thb":
                var y = amount * 1;
                jQuery('span#result-amount').number( true);
                jQuery("#result-currency").html(jQuery("#currency-to option:selected").html());
                jQuery("#result-amount").html(parseFloat(Math.round(y * 100) / 100));
            break;
            /* THB to USD */
            case "thb usd":
                var x = carrency_to = usd_sell / thb_buy;
                var y = amount / x;
                jQuery('span#result-amount').number( true);
                jQuery("#result-currency").html(jQuery("#currency-to option:selected").html());
                jQuery("#result-amount").html(parseFloat(Math.round(y * 100) / 100));
            break;
            case "thb khr":
                var x = carrency_to = thb_buy;
                var y = amount * x;
                jQuery('span#result-amount').number( true);
                jQuery("#result-currency").html(jQuery("#currency-to option:selected").html());
                jQuery("#result-amount").html(parseFloat(Math.round(y * 100) / 100));
        }
    }

    jQuery(window).on("load",function(){
        calculation();
        jQuery("#amount").on('keyup',function(){
            calculation();
        });
        
        jQuery("#currency-from").on('change',function(){
            calculation();
        });

        jQuery("#currency-to").on('change',function(){
            calculation();
        });
    });
</script>
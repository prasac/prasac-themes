<div class="col-xs-12 col-md-12">
  <div class="page-header"><h3>ព័ត៌មាន​ផ្ទាល់ខ្លួន</h3></div>
</div>

<section class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="customer_name">ឈ្មោះ</label>
    <input type="text" class="form-control loan-form" id="borrowerName" name="customer_name" required>
    <div class="help-block with-errors"></div>
  </div>
</section>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label>ភេទ</label>
    <div class="loan-form">
    <label class="form-inline"><input type="radio" name="customer_gender" id="customer_gender" value="1" checked required><span class="gender-label">ប្រុស</span></label>
    <label class="form-inline"><input type="radio" name="customer_gender" id="customer_gender" value="2" required><span class="gender-label">ស្ត្រី</span></label>
    </div>
  </div>
</div>

<section class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="DOB">ថ្ងៃខែឆ្នាំកំណើត</label>
    <input type="text" id="customer_dob" class="form-control loan-form" name="customer_dob">
  </div>
</section>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="borrowerName">សញ្ជាតិ</label>
    <input type="text" class="form-control loan-form" id="borrowerNational" name="customer_nation" placeholder="">
  </div>
</div>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="borrowerIDType">ឯកសារបញ្ជាក់អត្តសញ្ញាណ</label>
    <select class="form-control loan-form" id="borrowerIDType" name="customer_personal_identify">
      <option value="">សូមជ្រើសរើស...</option>
      <option value="1">សៀវភៅគ្រួសារ</option>
      <option value="2">អត្តសញ្ញាណប័ណ្ណ</option>
      <option value="3">លិខិតឆ្លងដែន</option>
      <option value="4">សំបុត្រកំណើត</option>
      <option value="4">ប័ណ្ណបើកបរ</option>
    </select>
  </div>
</div>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="borrowerID">អត្តលេខ</label>
    <input type="text" class="form-control loan-form" id="borrowerIDNumber" name="customer_personal_number">
  </div>
</div>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="borrowerPhoneNumber">លេខទូរស័ព្ទ</label>
    <input type="number" class="form-control loan-form" id="borrowerPhoneNumber" name="customer_phone" placeholder="">
  </div>
</div>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="borrowerEmail">អ៊ីម៉ែល</label>
    <input type="email" class="form-control loan-form" id="borrowerEmail" name="customer_email" placeholder="">
  </div>
</div>

<div class="clearfixed"></div>

<div class="col-xs-12 col-md-12">
  <div class="page-header"><h3>អាសយដ្ឋានបច្ចុប្បន្ន</h3></div>
</div>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="customer_address">ផ្ទះលេខ</label>
    <input type="text" id="customer_address" class="form-control loan-form validate" name="customer_address">
  </div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="customer_street">ផ្លូវលេខ</label>
    <input type="text" id="customer_street" class="form-control loan-form validate" name="customer_street">
  </div>
</div>
<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="customer_group">ក្រុមទី</label>
    <input type="text" id="customer_group" class="form-control loan-form validate" name="customer_group">
  </div>
</div>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
  <label for="customer_province">ខេត្ត / រាជធានី</label>
  <?php
  global $wpdb;
  $table_name = 'provinces';
  $sql = "SELECT id,name_kh from provinces"; 
  $results = $wpdb->get_results($wpdb->prepare($sql));    
  ?>
  <select id="customer_province" class="form-control loan-form validate" name="customer_province">
  <option value="">Select an option</option>
  <?php foreach( $results as $res ) : ?>
      <option value="<?= $res->id; ?>"><?= $res->name_kh; ?></option>
  <?php endforeach; ?>
  </select>
  </div>
</div>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
  <label for="customer_district">ស្រុក / ខណ្ឌ / ក្រុង</label>
  <?php
  global $wpdb;
  $table_name = 'districts';
  $sql = "SELECT id,name,provinces_id from $table_name"; 
  $results = $wpdb->get_results($wpdb->prepare($sql));    
  ?>
  <select id="customer_district" class="form-control loan-form validate" name="customer_district">
  <option value="">Select an option</option>
  <?php foreach( $results as $res ) : ?>
      <option value="<?= $res->id; ?>" class="<?= $res->provinces_id; ?>"><?= $res->name; ?></option>
  <?php endforeach; ?>
  </select>
  </div>
</div>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
  <label for="customer_commune">ឃុំ / សង្កាត់</label>
  <?php
  global $wpdb;
  $table_name = 'communes';
  $sql = "SELECT id,name,district_id from $table_name"; 
  $results = $wpdb->get_results($wpdb->prepare($sql));    
  ?>
  <select id="customer_commune" class="form-control loan-form validate" name="customer_commune">
  <option value="">Select an option</option>
  <?php foreach( $results as $res ) : ?>
      <option value="<?= $res->id; ?>" class="<?= $res->district_id; ?>"><?= $res->name; ?></option>
  <?php endforeach; ?>
  </select>
  </div>
</div>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
  <label for="customer_village">ភូមិ</label>
  <?php
  global $wpdb;
  $table_name = 'villages';
  $sql = "SELECT id,name,commune_id from $table_name"; 
  $results = $wpdb->get_results($wpdb->prepare($sql));    
  ?>
  <select id="customer_village" class="form-control loan-form validate" name="customer_village">
  <option value="">Select an option</option>
  <?php foreach( $results as $res ) : ?>
      <option value="<?= $res->id; ?>" class="<?= $res->commune_id; ?>"><?= $res->name; ?></option>
  <?php endforeach; ?>
  </select>
  </div>
</div>

<div class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
  <label for="customer_branch">សាខាដែលនៅជិតលោកអ្នក</label>
  <?php
  global $wpdb;
  $table_name = 'branches';//$wpdb->prefix . 
  $sql = "SELECT id,name_kh,province_id from $table_name"; 
  $results = $wpdb->get_results($wpdb->prepare($sql));    
  ?>
  <select id="customer_branch" class="form-control loan-form validate" name="duty_station">
  <option value="">Select an option</option>
  <?php foreach( $results as $res ) : ?>
      <option value="<?= $res->id; ?>" class="<?= $res->province_id; ?>"><?= $res->name_kh; ?></option>
  <?php endforeach; ?>
  </select>
  </div>
</div>
<section class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    
    <label for="borrow_amount">ចំនួនទឹកប្រាក់កម្ចី</label>
    <div class="input-group">
      <div class="input-group-btn bs-dropdown-to-select-group">
        <button type="button" class="btn btn-default dropdown-toggle loan-form as-is bs-dropdown-to-select" data-toggle="dropdown">
          <span data-bind="bs-drp-sel-label">រូបិយប័ណ្ណ</span>
          <input type="hidden" name="borrow_currency_type" data-bind="bs-drp-sel-value" value="">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <li data-value="1"><a href="#">ដុល្លារអាមេរិក</a></li>
          <li data-value="2"><a href="#">រៀល</a></li>
          <li data-value="3"><a href="#">បាត</a></li>
        </ul>
      </div>
      <input type="text" class="form-control loan-form" id="borrowerName" name="borrow_amount" required>
    </div>

  </div>
</section>

<section class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="borrow_amount">ជាអក្សរ</label>
    <input type="text" class="form-control loan-form" id="borrow_amount_character" name="borrow_amount_character" required>
  </div>
</section>

<section class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="borrow_duration">រយៈពេលខ្ចី</label>
    <div class="input-group">
      <input type="text" class="form-control loan-form" id="borrow_duration" name="borrow_duration">
      <span class="input-group-addon">គិតជាខែ</span>
    </div>
  </div>
</section>

<section class="col-xs-12 col-sm-3 col-md-3">
  <div class="form-group">
    <label for="brrow_rate_request_permonth">អត្រាការប្រាក់ស្នើសុំក្នុងមួយខែ</label>
    <div class="input-group">
      <input type="number" class="form-control loan-form" id="brrow_rate_request_permonth" name="brrow_rate_request_permonth">
      <span class="input-group-addon">%</span>
    </div>
  </div>
</section>

<section class="col-xs-12 col-sm-12 col-md-12">
<div class="table-responsive">
  <table class="table table-bordered" id="data-purpose">
    <thead>
      <tr>
        <th>ល.រ</th>
        <th>គោលបំណងខ្ចី</th>
        <th>ឯកតា</th>
        <th>តម្លៃក្នុង 1 ឯកតា</th>
        <th>ចំនួនទឺកប្រាក់ដែលត្រូវចំណាយ</th>
        <th></th>
      </tr>
    </thead>
    <tbody id="dynamic_purpose">
      <tr>
        <td>1</td>
        <td class="form-group">
          <input type="text" id="borrow_purpose" class="form-control loan-form validate" name="borrow_purpose[]">
        </td>
        <td class="form-group">
          <input type="text" id="borrow_amount" class="form-control loan-form validate" name="borrow_amount[]">
        </td>
        <td class="form-group">
          <input type="text" id="borrow_unit_price" class="form-control loan-form validate" name="borrow_unit_price[]">
        </td>
        <td class="form-group">
          <input type="text" id="expence_amount" class="form-control loan-form validate" name="expence_amount[]" disabled>
        </td>
        <td class="form-group"><button class="form-control btn btn-sm btn-primary" type="button" name="add" id="addPurpose"><i class="fa fa-plus" aria-hidden="true"></i></button></td>
      </tr>
    </tbody>
  </table>
</div>
</section>

<section class="col-xs-12 col-sm-6 col-md-3">
  <div class="form-group">
    <label for="">ប្រភេទអាជីវកម្ម</label>
    <div class="checkbox"><label><input type="checkbox" value="1" name="borrow_purpose_type[]">កសិកម្ម</label></div>
    <div class="checkbox"><label><input type="checkbox" value="2" name="borrow_purpose_type[]">ពាណិជ្ជកម្ម / ជំនួញ</label></div>
    <div class="checkbox"><label><input type="checkbox" value="3" name="borrow_purpose_type[]">សេវាកម្ម</label></div>
    <div class="checkbox"><label><input type="checkbox" value="4" name="borrow_purpose_type[]">ដឹកជញ្ជូន</label></div>
    <div class="checkbox"><label><input type="checkbox" value="5" name="borrow_purpose_type[]">សាងសង</label></div>
    <div class="checkbox"><label><input type="checkbox" value="6" name="borrow_purpose_type[]">ប្រើប្រាស់ទូទៅក្នុងគ្រួសារ</label></div>
  </div>
</section>

<section class="col-xs-12 col-sm-6 col-md-3">
  <div class="form-group">
    <label for="">ទ្រង់ទ្រាយមុខជំនួញ</label>
    <div class="checkbox"><label><input type="checkbox" value="1" name="business_type[]">ឯកកម្មសិទ្ធិ</label></div>
    <div class="checkbox"><label><input type="checkbox" value="2" name="business_type[]">សហកម្មសិទ្ធិ</label></div>
    <div class="checkbox"><label><input type="checkbox" value="3" name="business_type[]">ពង្រីកមុខជំនួញ</label></div>
    <div class="checkbox"><label><input type="checkbox" value="4" name="business_type[]">ចាប់ផ្ដើមមុខជំនួញ</label></div>
  </div>
</section>

<section class="col-xs-12 col-sm-6 col-md-3">
  <div class="form-group">
    <label for="borrow_duration">ទីតាំងមុខជំនួញ</label>
    <input type="text" id="business_location" class="form-control loan-form validate" name="business_location">
  </div>
</section>
<section class="col-xs-12 col-sm-6 col-md-3">
  <div class="form-group">
    <label for="borrow_duration">របៀបសង</label>
    <select name="payment_method[]" id="payment_method" class="form-control loan-form validate">
      <option value="0">សូមជ្រើសរើស</option>
      <option value="1">ប្រចាំខែ</option>
      <option value="2">ប្រចាំត្រីមាស</option>
      <option value="3">ប្រចាំឆមាស</option>
      <option value="4">ប្រចាំឆ្នាំ</option>
    </select>
  </div>
</section>
<!-- Reference -->
<section class="col-xs-12 col-sm-12 col-md-12">
  <table class="table table-responsive table-bordered" id="dataTable">
    <thead>
    <tr>
      <th>ល.រ</th>
      <th>ឈ្មោះ</th>
      <th>ភេទ</th>
      <th>ថ្ងៃខែឆ្នាំកំណើត</th>
      <th>សញ្ជាតិ</th>
      <th>ឯកសារបញ្ជាក់អត្តសញ្ញាណ</th>
      <th>អត្តលេខ​</th>
      ​<th>​ទំនាក់ទំនង</th>
    </tr>
    </thead>

    <tbody id="p_scents">
    <tr>
      <td>1</td>
      <td class="form-group">
        <input type="text" id="reference_name" class="form-control loan-form validate" name="reference_name[]">
      </td>
      <td class="form-group">
        <div class="loan-form">
          <label class="form-inline"><input type="radio" name="reference_gender[]" id="customer_gender" value="1" checked><span class="gender-label">ប្រុស</span></label>
          <label class="form-inline"><input type="radio" name="reference_gender[]" id="customer_gender" value="2"><span class="gender-label">ស្ត្រី</span></label>
        </div>
      </td>
      <td class="form-group">
        <input id="ref_dob" type="text" class="form-control loan-form datepicke ref_dob" name="reference_dob[]" placeholder="ថ្ងៃខែឆ្នាំកំណើត">
      </td>
      <td class="form-group">
        <input type="text" id="reference_nation" class="form-control loan-form validate" name="reference_nation[]" placeholder="សញ្ជាតិ">
      </td>
      <td class="form-group">
        <select class="form-control" name="reference_personal_identify[]">
          <option value="" disabled selected>ជ្រើសរើស</option>
          <option value="1">សៀវភៅគ្រួសារ</option>
          <option value="2">អត្តសញ្ញាណប័ណ្ណ</option>
          <option value="3">លិខិតឆ្លងដែន</option>
          <option value="4">សំបុត្រកំណើត</option>
          <option value="5">ប័ណ្ណបើកបរ</option>
        </select>
      </td>
      <td class="form-group">
        <input type="text" id="reference_number" class="form-control loan-form validate" name="reference_number[]" placeholder="អត្តលេខ">
      </td>
      <td class="form-group">
      <input type="text" id="reference_relationship" class="form-control loan-form validate"​ name="reference_relationship[]" placeholder="ត្រូវជា">
      </td>
      <td class="form-group"><button class="form-control btn btn-sm btn-primary" type="button" name="add" id="addReference"><i class="fa fa-plus" aria-hidden="true"></i></button></td>
    </tr>
    </tbody>
  </table>

</section>
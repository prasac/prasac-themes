<?php
$rangeid=63;
$prevcid=64;
$prevwidth=1015;
$interval=false;
$range=array (
  32 => 
  array (
    0 => 278,
    1 => 426,
    2 => 355,
  ),
  35 => 
  array (
    0 => 556,
    1 => 556,
    'interval' => true,
  ),
  37 => 
  array (
    0 => 889,
    1 => 667,
    2 => 191,
  ),
  40 => 
  array (
    0 => 560,
    1 => 560,
    'interval' => true,
  ),
  42 => 
  array (
    0 => 645,
    1 => 584,
    2 => 278,
    3 => 333,
  ),
  46 => 
  array (
    0 => 278,
    1 => 278,
    'interval' => true,
  ),
  48 => 
  array (
    0 => 569,
    1 => 465,
    2 => 551,
    3 => 558,
    4 => 533,
    5 => 560,
    6 => 528,
    7 => 539,
    8 => 526,
    9 => 528,
  ),
  58 => 
  array (
    0 => 278,
    1 => 278,
    'interval' => true,
  ),
  60 => 
  array (
    0 => 584,
    1 => 584,
    'interval' => true,
    2 => 584,
  ),
  63 => 
  array (
    0 => 774,
    1 => 1015,
  ),
);
?>
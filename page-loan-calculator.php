<?php
error_reporting(E_ALL);
/*
* template name: Loan Calculation form
*/
get_header();
?>
<style>
    h3{
        color:#4DB848;
    }
   .form-horizontal .control-label{
        text-align:left;
        margin-left:0px;;
        font-weight:normal;
    }
    .input-group form-control-placeholder{
        text-align:right;
    }
    .loan-calculator .form-group{
        /*padding:0;*/
    }
    .bt-layout{
        border:1px solid #dedede;
    }   
    #load_calculator_fieldset{
        border-top-right-radius: 0;
        border-top-left-radius: 0; 
    }
    .btn-warning{
        margin-right:10px;
    }
    table thead tr th{
        text-align:center;
    }
    input[readonly].generate-i{
        background-color:#ffffff;
    }
    .loan-calculator>h3{
        border:none;
    }
    .green-background{
        color:#ffffff;
        background-color:#4DB848;
        font-size:2rem;
        font-weight:500;
    }
    .row-title{
        /* color:#ffffff;
        background-color:#4DB848; */
    }
    .loan-calculator .has-feedback .form-control-feedback{
        top: 0;
        right: 15px;    
    }
    span#print_schedule,.generate_schedule_button{
        color:#4DB848;
        cursor:pointer;

    }
    
    .calculator_form{
        background-color:#f0f0f2;
        padding:10px 5px;
    }
    #btn-calculate,#btn-generate{
        border-radius:0;
        padding:0;
        border:none;
    }
    /* #btn-generate{
        margin-top:15.5rem;
    } */
    h2.note{
        color:#4DB848;
        font-weight:600;
    }
    .number-lg{
        border: none;
        text-align: center;
        font-size: 4rem;
        height: 4rem;
        background-color: transparent !important;
        box-shadow:none;
        color:#000000;
        font-weight:600;
    }
    .number-md{
        border: none;
        text-align: center;
        font-size: 2rem;
        background-color: transparent !important;
        box-shadow:none;
    }
    .div-number-md{
        display:block;
        margin-top:-1rem;
        text-align:right;
    }
    hr{
        padding:0;
        margin:0;
    }
    span.currency_type{
        position: absolute;
        right:0;
        top: 34%;
        font-size: 2rem;
        display:none;
    }
    span.currency_type_md{
        position: absolute;
        right: 0%;
        top: 40%;
        font-size: 1rem;
        display:none;
    }
    .sidebar {
        color: #333;
        text-align: left;
        line-height: 130%;
        padding: 0;
        margin: 0;
        clear: both;
        background-repeat: no-repeat;
        background-position: 0 0;
        box-sizing: border-box;
    }
    .form-control[disabled]{
        background-color:#4DB848;
    }
    .form-group .form-control{
        font-weight:600;
        font-size: 1.1rem;
        height:2.6rem;
    }
    div.print-title h3{
        font-family:Roboto,Khmer OS Muol Light;
        border:none;
        font-size:2rem;
        font-weight:lighter !important;
    }
    div.print-title h3.second-title{
        font-size:1.5rem;
    }
    .title-container{
        position:relative;
        padding:10px;
        overflow:hidden;
    }
    .title-container img.logo-image{
        position:absolute;
        left:0;
        top:0;
        width:100px;
    }
    .applicant-detail{
        background-color:#f0f0f2;
        padding:10px 0;
    }
    #printing-wrapper{
        overflow:hidden;
    }
    button#btn-calculate{
        font-family:Roboto,Khmer OS Muol Light;
        font-weight:lighter !important;
        font-size:1.3rem;
    }
    .cbc_fee_currency{
        font-size:12px;
    }
</style>
<div class="container-fluid">
    <main class="container">
        <section <?= post_class('col-xs-12 col-sm-9 col-md-9 page-title'); ?> >
            <div class="hidden-xs hidden-sm">
                    <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                    <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                        <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                        <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                    </ul>
            </div>
            <?php the_title('<h3 class="green">','</h3>') ?>
                <fieldset class="bt-layout" id="load_calculator_fieldset">
                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6 calculator_form">
                        <form class="loan-calculator" action="" method="POST" id="calculate_form">
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label required col-lg-12 col-md-12 col-sm-12 col-xs-12" for="currency_type"><?php _e('[:en]Currency[:kh]រូបិយប័ណ្ណ​[:]') ?></label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <select class="form-control" name="currency_type" required id="currency_type">
                                                    <option value="USD" <?php echo !isset($_POST['currency_type'])?' selected="selected"':'';?>  <?php echo isset($_POST['currency_type']) && $_POST['currency_type']=='USD'?' selected="selected"':''?>><?php _e('[:en]USD[:kh]ប្រាក់​ដុល្លាអាមេរិក[:]') ?></option>
                                                    <option value="KHR" <?php echo isset($_POST['currency_type']) && $_POST['currency_type']=='KHR'?' selected="selected"':''?>><?php _e('[:en]KHR[:kh]ប្រាក់រៀល[:]') ?></option>
                                                    <option value="THB" <?php echo isset($_POST['currency_type']) && $_POST['currency_type']=='THB'?' selected="selected"':''?>><?php _e('[:en]THB[:kh]ប្រាក់​បាត[:]') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label required col-lg-12 col-md-12 col-sm-12 col-xs-12" for="loan_size"><?php _e('[:en]Loan amount[:kh]ទឹកប្រាក់កម្ចី[:]') ?></label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <input type="text" id="loan_size" min="100" value="<?php echo isset($_POST['loan_size'])?$_POST['loan_size']:50000;?>" name="loan_size" percentag="50" class="form-control currency"​ placeholder="00.00">
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <!--==== រយៈពេលខ្ចី ======-->
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label required col-lg-12 col-md-12 col-sm-12 col-xs-12" for="loan_term"><?php _e('[:en]Loan term in month[:kh]រយៈពេលខ្ចីជាខែ​[:]') ?></label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <input type="text" id="loan_term" value="<?php echo isset($_POST['loan_term'])?$_POST['loan_term']:12;?>" name="loan_term" class="form-control"  percentag="25"​ placeholder="00.00">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label required col-lg-12 col-md-12 col-sm-12 col-xs-12" for="interest_rate"><?php _e('[:en]Interest rate per month[:kh]អត្រាការប្រាក់ក្នុង 1 ខែ[:]') ?>(%)</label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <input type="text" id="interest_rate" value="<?php echo isset($_POST['interest_rate'])?$_POST['interest_rate']:1.2;?>" name="interest_rate" class="form-control"  percentag="25" placeholder="00.00">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--==== Loan fee and CBC fee ======-->
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label required col-lg-12 col-md-12 col-sm-12 col-xs-12" for="loan_fee"><?php _e('[:en]Loan Fee (%)[:kh]កម្រៃសេវា(%)​[:]') ?></label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <input type="text" id="loan_fee" value="<?php echo isset($_POST['loan_fee'])?$_POST['loan_fee']:'0.00';?>" name="loan_fee" class="form-control"  percentag="25"​ placeholder="0.00">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label required col-lg-12 col-md-12 col-sm-12 col-xs-12" for="cbc_fee"><?php _e('[:en]CBC Fee[:kh]កម្រៃ​ CBC[:]') ?>(<span><small class="cbc_fee_currency"><?php _e('[:en]USD[:kh]ប្រាក់​ដុល្លាអាមេរិក[:]') ?></small></span>)</label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <input type="text" id="cbc_fee" value="<?php echo isset($_POST['cbc_fee'])?$_POST['cbc_fee']:'0.00';?>" name="cbc_fee" class="form-control"  percentag="25" placeholder="0.00">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--==== ជម្រើសនៃការសង ======-->
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder required control-label col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php _e('[:en]Repayment option[:kh]ជម្រើសនៃការសងប្រាក់[:]') ?></label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 radio-box-payment-method">
                                                <input type="radio" id="depreciation" value="1" <?php echo !isset($_POST['payment-method'])?' checked ':''?> <?php echo isset($_POST['payment-method']) && $_POST['payment-method']==1?' checked ':''?> name="payment-method" data-label="<?php _e('[:en]First Installation Paid[:kh]ទឹកប្រាក់ដែលត្រូវសងនៅខែទី 1[:]') ?>">
                                                <label for="depreciation"> <?php _e('[:en]Declining[:kh]រំលស់ធម្មតា[:]') ?></label>&nbsp;&nbsp;
                                                <input type="radio" id="emi" value="2" <?php echo isset($_POST['payment-method']) && $_POST['payment-method']==2?'checked ':''?> name="payment-method" data-label="<?php _e('[:en]The amount to be paid monthly[:kh]ចំនួនទឹកប្រាក់ថេដែលត្រូវសងប្រចាំខែ[:]') ?>">
                                                <label for="emi" > <?php _e('[:en]EMI[:kh]រំលស់ថេរ[:]') ?></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12"><br />
                                    <button type="submit" name="btn_calculate_loan" class="btn btn-success btn-lg form-control" id="btn-calculate" data-loading-text="<i class='fa fa-spinner fa-spin '></i> <?php _e('[:en]In Processing...[:kh]កំពុង​គណនា...[:]') ?>"><?php _e('[:en]CALCULATE[:kh]គណនា[:]') ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <fieldset>
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-placeholder control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center payment_method_label"></label>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12  text-center">
                                            <span id="monthly_paid" class="monthly_paid number-lg">0.00</span>
                                        </div>
                                    </div>
                                </div>  
                            </div>

                            <!--==== ចំនួនទឹកប្រាក់សរុបដែលត្រូវសង​ ======-->
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-placeholder control-label col-lg-6 col-md-6 col-sm-6 col-xs-6" for="total_paid"><?php _e('[:en]Total Principal Paid[:kh]ប្រាក់ដើមត្រូវសងសរុប​[:]') ?></label>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 div-number-md">
                                            <span id="total_paid" class="number-md">0.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--==== ចំនួនការប្រាក់សរុប ======-->
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-placeholder control-label col-lg-6 col-md-6 col-sm-6 col-xs-6" for="total_interest"><?php _e('[:en]Total Interest Paid[:kh]ការប្រាក់សរុបត្រូវសង[:]') ?></label>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 div-number-md">
                                            <span id="total_interest"  class="number-md">0.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--==== Total Loan fee ======-->
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-placeholder control-label col-lg-6 col-md-6 col-sm-6 col-xs-6" for="total_interest"><?php _e('[:en]Total Loan Fee[:kh]កម្រៃសេវាសរុប[:]') ?></label>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 div-number-md">
                                            <span id="total_loan_fee"  class="number-md currency">0.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--==== CBC fee ======-->
                            <hr>
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="form-control-placeholder control-label col-lg-6 col-md-6 col-sm-6 col-xs-6" for="total_interest"><?php _e('[:en]CBC Fee[:kh]កម្រៃ CBC[:]') ?></label>
                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 div-number-md">
                                            <span id="total_cbc_fee"  class="number-md">0.00</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <hr ><br />
                                    <div class="text-center generate_schedule_button"><?php _e('[:en]Generate Repayment Schedule[:kh]បង្ហាញកាលវិភាគសងប្រាក់​[:]') ?></div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </fieldset>
            <script>
                function loan_fee_cal(){
                    var $selected_currency = jQuery("#currency_type option:selected").val();
                    var $loan_fee = jQuery("#loan_fee").val();
                    var $total_loan_fee = (jQuery("#loan_size").val() * $loan_fee / 100).toFixed(2);
                    if($selected_currency == 'KHR'){
                        $total_loan_fee = (jQuery("#loan_size").val() * $loan_fee / 100).toFixed();
                    }

                    if($selected_currency == 'THB'){
                        $total_loan_fee = (jQuery("#loan_size").val() * $loan_fee / 100).toFixed();
                    }
                    jQuery("#total_loan_fee").text($total_loan_fee);
                }
            </script>
            <br />
            <?php
            date_default_timezone_set("Asia/Phnom_Penh");
            $d1=$d2=0;   

            function dateDiff($date1, $date2){
                $date1_ts = strtotime($date1);
                $date2_ts = strtotime($date2);
                $diff = $date2_ts - $date1_ts;
                return round($diff / 86400);
            }

            //=================Calculate on submit==========

            if(isset($_POST['btn_calculate_loan'])){
                $capital_asset = floatval(str_replace(",","",$_POST['loan_size']));
                $currency       = isset($_POST['currency_type'])?$_POST['currency_type']:'';
                $loan_term      = isset($_POST['loan_term'])?$_POST['loan_term']:'';
                $interest_rate  = isset($_POST['interest_rate'])? ($_POST['interest_rate'] / 100) :'';
                $apply_date     = isset($_POST['apply_date']) && $_POST['apply_date'] !=''?$_POST['apply_date']:date("Y-m-d");
                $payment_menthod = $_POST["payment-method"];
                $loan_fee_percentage  = isset($_POST['loan_fee'])?$_POST['loan_fee']:'0.00';
                $cbc_fee    =   isset($_POST['cbc_fee'])?$_POST['cbc_fee']:0;
                $total_loan_fee = number_format($capital_asset * $loan_fee_percentage / 100,2);
                $total_cbc_fee  =   number_format($cbc_fee,2);
                if($currency == 'KHR' || $currency == 'THB'){
                    $total_loan_fee = number_format($capital_asset * $loan_fee_percentage / 100);
                    $total_cbc_fee  =   number_format($cbc_fee);
                }

                echo '<div id="printing-wrapper"> <div class="table-responsive table-schedule"  id="schedule_loan">
                            <div class="col-xs-12 col-md-12 text-center print-title title-container">';
                                echo '<img src="'.get_template_directory_uri().'/assets/images/logo-website.png" title="Loan Calculator" alt="Loan Calculator" class="img-responsive logo-image" />';
                                _e('[:en]<h3>PRASAC MFI</h3>[:kh]<h3>គ្រឹះស្ថានមីក្រូហិរញ្ញវត្ថុប្រាសាក់</h3>[:]');
                                _e('[:en]<h3 class="second-title">Repayment Schedule</h3>[:kh]<h3 class="second-title">កាលវិភាគសងប្រាក់</h3>[:]');
                        echo '</div>
                        <div class="col-md-12 applicant-detail">
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6 col-lg-6">';
                                _e('[:en]Loan Amount[:kh]ទឹកប្រាក់កម្ចី[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <span class="currency">:&nbsp;'.number_format($capital_asset,2).'</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Loan Term[:kh]រយៈពេលខ្ចី[:]');
                            echo '</label>';
                            echo '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">';
                            if($payment_menthod==1){
                                echo '<span id="loan_term_day">:&nbsp;'.$loan_term.'</span>';_e('[:en] &nbsp;Days[:kh]&nbsp;ថ្ងៃ[:]');
                            }else{
                                echo '<span id="loan_term_day">:&nbsp;'.$loan_term.'</span>';_e('[:en] &nbsp;Month(s)[:kh]&nbsp;ខែ[:]');
                            }
                            echo '</div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Total Interest Paid[:kh]ការប្រាក់សរុបត្រូវសង[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                :&nbsp;<span class="total-interest"></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Interest Rate Per Month[:kh]អត្រាការប្រាក់ក្នុង 1 ខែ[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <span>:&nbsp;'.$_POST['interest_rate'].'%</span>';
                            echo '</div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Total Loan Fee[:kh]កម្រៃសេវាសរុប[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                :&nbsp;<span class="total-loan-fee">'.$total_loan_fee.'</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Start Date[:kh]ថ្ងៃ​បើកប្រាក់[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <span>:&nbsp;'.date("d-m-Y",strtotime($apply_date)).'</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Loan Fee[:kh]កម្រៃ CBC[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                :&nbsp;<span class="total-cbc-fee">'.$total_cbc_fee.'</span>
                                </div>
                            </div>
                           
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]End Date[:kh]ថ្ងៃ​សងបញ្ចប់​​[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <span>:&nbsp;'.date("d-m-Y", strtotime("+$loan_term month",strtotime($apply_date))).'</span>
                                </div>
                            </div>
                           
                        </div>
                <table class="table table-condensed table-striped table-fixed">';
                echo '<thead><tr class="green row-title">';
                echo '<th>';
                    _e("[:en] Month[:kh] ខែ[:]");
                echo '</th>';
                if($payment_menthod==1){
                    echo '<th>';
                        _e("[:en] Repayment Date[:kh] ថ្ងៃសងប្រាក់[:]");
                    echo '</th>';
                    echo '<th>';
                        _e("[:en] Number of Days[:kh] ចំនួនថ្ងៃ[:]");
                    echo '</th>';
                }
                echo '<th>';
                    _e("[:en] Balance[:kh] ប្រាក់ដើមនៅជំពាក់[:]");
                echo '</th>';
                echo '<th>';
                    _e("[:en] Principal[:kh] ប្រាក់ដើមត្រូវសង[:]");
                echo '</th>';
                echo '<th>';
                    _e("[:en] Interest[:kh] ការប្រាក់ត្រូវសង[:]");
                echo '</th>';
                echo '<th>';
                    _e("[:en] Payment[:kh] ប្រាក់ត្រូវសងសរុប[:]");
                echo '</th>';
                echo '</tr></thead>';
                echo '<tbody>';

                if($payment_menthod==1){
                    $monthly_interest_rate = $capital_asset*$interest_rate;
                    $instalment = ($capital_asset/$loan_term);
                    $depreciation = number_format($instalment+$monthly_interest_rate,2);
                    if($currency == 'KHR' || $currency == 'THB'){
                        $depreciation = number_format($instalment+$monthly_interest_rate);
                    }

                    $total_instalment = 0;
                    $total_interest_general = 0;
                    $total_depreciation = 0;
                    $total_day = 0;
                    echo '<script>';
                    echo 'jQuery(document).ready(function(){';
                    echo    'jQuery(".monthly_paid").text("'.$depreciation.'");';
                    echo    'var selected_label = jQuery("div.radio-box-payment-method input[value='.$payment_menthod.'] ").data("label");';
                    echo    'jQuery("label.payment_method_label").html(selected_label);';
                    echo '});';
                    echo '</script>';
                        
                    $instalment = $capital_asset/$loan_term;
                    $depreciation = $instalment+$monthly_interest_rate;
                    $monthly_interest_rate = $capital_asset*$interest_rate;

                    for($i=1;$i<=$loan_term;$i++){
                        if($i==1):
                                $instalments=0;
                                $capital_assets =  $capital_asset;
                                $capitals = $capital_asset;//$capital_asset - $instalment;
                        else:
                                $instalments=$instalment;
                                $monthly_interest_rate = $capital_assets*$interest_rate;
                                $capital_assets  =  $capital_assets - $instalment;
                                $capitals = $capitals - $instalment;
                        endif;
                       
                        $total_instalment += $instalment;
                        echo "<tr class='text-center'>";
                            echo "<td>".$i."</td>";
                            $date1 = ($i==1)?$apply_date:date("d-m-Y", strtotime("+1 month",strtotime($date1)));
                            $date2 = ($i==1)?date("d-m-Y", strtotime("+1 month",strtotime($date1))):date("d-m-Y", strtotime("+1 month",strtotime($date2)));
                            
                            $dw = date( "w",strtotime($date2));
                            
                            if ($dw == 0) {
                                $date1 = date("d-m-Y", strtotime("-2 day",strtotime($date1)));
                                $date2 = date("d-m-Y", strtotime("-2 day",strtotime($date2)));
                                
                            }

                            if ($dw == 6) {
                                $date1 = date("d-m-Y", strtotime("-1 day",strtotime($date1)));
                                $date2 = date("d-m-Y", strtotime("-1 day",strtotime($date2)));
                            }
                            $total_day += dateDiff($date1,$date2);
                            $capital_amount = number_format(round($capital_assets,2),2);
                            $instalment_amount = number_format(round($instalment,2),2);
                            $interest_amount = number_format(round((($capital_assets*$interest_rate)/30)* dateDiff($date1,$date2),2),2);
                            $repayment_amount =  number_format(round($instalment+$monthly_interest_rate,2),2);
                            $captial = number_format(round($capitals,2),2);
                            $total_interest_general += $interest_amount;
                            if($currency == 'KHR'){
                                $capital_amount = number_format(floor(round($capital_assets,2)/100)*100);
                                $captial = number_format(floor(round($capitals)/100)*100);
                                $instalment_amount = number_format(floor($instalment/100)*100);
                                $interest_amount = number_format(floor(round(($capital_assets*$interest_rate)/30 * dateDiff($date1,$date2))/100)*100);
                                $repayment_amount =  number_format(floor(round($instalment+$monthly_interest_rate)/100)*100);
                            }

                            if($currency == 'THB'){
                                $capital_amount = number_format(round($capital_assets,2));
                                $captial = number_format(round($capitals,2));
                                $instalment_amount = number_format(round($instalment,2));
                                $interest_amount = number_format(round(($capital_assets*$interest_rate)/30 * dateDiff($date1,$date2),2));
                                $repayment_amount =  number_format(round($instalment+$monthly_interest_rate,2));
                            }

                            echo "<td>".$date2."</td>";
                            echo "<td>".dateDiff($date1,$date2)."</td>";
                            echo "<td class='currency'>".$captial."</td>";
                            echo "<td class='currency'>".$instalment_amount."</td>";
                            echo "<td class='currency'>".$interest_amount."</td>";
                            echo "<td class='currency'>".$repayment_amount."</td>";
                        echo "</tr>";
                    }

                  
                   
                    $total_interest_generals = number_format(round($total_interest_general,2),2);
                    $total_instalments = number_format(round($total_instalment,2),2);
                    $total_paid =  number_format(round($total_instalment+$total_interest_general,2),2);

                    if($currency == 'KHR'){
                        $total_interest_generals = number_format(floor(round($total_interest_general,2)/100)*100);
                        $total_instalments = number_format(floor(round($total_instalment,2)/100)*100);
                        $total_paid = number_format(floor(round($total_instalment+$total_interest_general,2)/100)*100);
                    }

                    if($currency == 'THB'){
                        $total_interest_generals = number_format(round($total_interest_general,2));
                        $total_instalments = number_format(round($total_instalment,2));
                        $total_paid = number_format(round($total_instalment+$total_interest_general,2));
                    }

                    echo "<tr class='text-center green total'>";
                        echo "<td colspan='2' class='text-right' style='text-align:right !important;'>";
                            _e("[:en] Total :[:kh] សរុប ៖[:]");
                        echo "</td>";
                        echo '<td>'.$total_day.'</td>';
                        echo '<td></td>';
                        echo "<td class='currency'>".$total_instalments."</td>";
                        echo "<td class='currency'>".$total_interest_generals."</td>";
                        echo "<td class='currency'>".$total_paid."</td>";
                    echo "</tr>";
                    
                    echo '<script>';
                    echo 'jQuery(document).ready(function(){';
                    echo    'jQuery("#total_paid").text("'.$total_instalments.'");';
                    echo    'jQuery("#loan_term_day").text(": '.$total_day.'");';
                    echo    'jQuery("#total_interest,.total-interest").text("'.$total_interest_generals.'");';
                    echo '});';
                    echo '</script>';
                }
               
                if($payment_menthod==2){
                    $monthly_interest_rate = $capital_asset*$interest_rate;
                    $emi    = $capital_asset * $interest_rate * pow(1 + $interest_rate, $loan_term) / (pow(1 + $interest_rate, $loan_term) - 1);
                    $instalment = $emi-$monthly_interest_rate;
                    $total_instalment = 0;
                    $total_interest = 0;
                    $interest_amount = 0;
                    $interest = 0;
                    $total_emi = 0;
                    $emi_monthly_paid=0;
                    $capitals = 0;
                    $total_day=0;

                    $emi_monthly_paid = number_format($capital_asset * $interest_rate * pow(1 + $interest_rate, $loan_term) / (pow(1 + $interest_rate, $loan_term) - 1),2);
                    if($currency == 'KHR' || $currency== 'THB'){
                        $emi_monthly_paid = number_format($capital_asset * $interest_rate * pow(1 + $interest_rate, $loan_term) / (pow(1 + $interest_rate, $loan_term) - 1));
                    }

                    echo '<script>';
                    echo 'jQuery(document).ready(function(){';
                    echo    'jQuery(".monthly_paid").text("'.$emi_monthly_paid.'");';
                    echo    'var selected_label = jQuery("div.radio-box-payment-method input[value='.$payment_menthod.'] ").data("label");';
                    echo    'jQuery("label.payment_method_label").html(selected_label);';
                    echo '});';
                    echo '</script>';
                    $instalment = $capital_asset/$loan_term;
                    for($i=1;$i<=$loan_term;$i++){
                        if($i==1):
                            $capital_assets =  $capital_asset;
                            $capitals = $capital_asset;//$capital_asset - $instalment;
                            //$instalment= $emi-($capital_assets*$interest_rate);
                        else:
                            //$instalment= $emi-($capital_assets*$interest_rate);
                            $capitals = $capitals - $instalment;
                            $capital_assets  =  $capital_assets - $instalment;
                        endif;
                        $instalment= $emi-($capital_assets*$interest_rate);
                        $total_instalment += $instalment;
                        $total_interest += ($capital_assets * $interest_rate);
                        $total_emi +=$emi;
                        
                        $captial = number_format(round($capitals,2),2);
                        $instalment_amount = number_format(round($instalment,2),2);
                        $interest_amount = number_format(round($capital_assets * $interest_rate,2),2);
                        $emi_amount = number_format(round($emi,2),2);

                        if($currency == 'KHR'){
                            $captial = number_format(floor(round($capital_assets)/100)*100);
                            $instalment_amount = number_format(floor(round($instalment)/100)*100);
                            $interest_amount = number_format(floor(round($capital_assets * $interest_rate)/100)*100);
                            $emi_amount = number_format(floor(round($emi)/100)*100);
                        }
    
                        if($currency == 'THB'){
                            $captial = number_format(round($capital_assets));
                            $instalment_amount = number_format(round($instalment));
                            $interest_amount = number_format(round($capital_assets * $interest_rate));
                            $emi_amount = number_format(round($emi));
                        }

                        echo "<tr class='text-center'>";
                            echo "<td>".$i."</td>";
                            $date1 = ($i==1)?$apply_date:date("d-m-Y", strtotime("+1 month",strtotime($date1)));
                            $date2 = ($i==1)?date("d-m-Y", strtotime("+1 month",strtotime($date1))):date("d-m-Y", strtotime("+1 month",strtotime($date2)));
                            
                            $dw = date( "w",strtotime($date2));
                            
                            if ($dw == 0) {
                                $date1 = date("d-m-Y", strtotime("-2 day",strtotime($date1)));
                                $date2 = date("d-m-Y", strtotime("-2 day",strtotime($date2)));
                                
                            }

                            if ($dw == 6) {
                                $date1 = date("d-m-Y", strtotime("-1 day",strtotime($date1)));
                                $date2 = date("d-m-Y", strtotime("-1 day",strtotime($date2)));
                            }                      
                            $total_day += dateDiff($date1,$date2);
                            // echo "<td>".$date2."</td>";
                            // echo "<td>".dateDiff($date1,$date2)."</td>";                         
                            echo "<td class='currency'>".$captial."</td>";
                            echo "<td class='currency'>".$instalment_amount."</td>";
                            echo "<td class='currency'>".$interest_amount."</td>";
                            echo "<td class='currency'>".$emi_amount."</td>";
                        echo "</tr>";
                    }

                    $instalment_total = number_format(round($total_instalment,2),2);
                    $interest_total = number_format(round($total_interest,2),2);
                    $emi_total = number_format(round($total_emi,2),2);

                    if($currency == 'KHR'){
                        $instalment_total = number_format(floor(round($total_instalment)/100)*100);
                        $interest_total = number_format(floor(round($total_interest)/100)*100);
                        $emi_total = number_format(floor(round($total_emi)/100)*100);
                    }

                    if($currency == 'THB'){
                        $instalment_total = number_format(round($total_instalment));
                        $interest_total = number_format(round($total_interest));
                        $emi_total = number_format(round($total_emi));
                    }

                    echo "<tr class='text-center green total'>";
                    echo "<td colspan='2' class='text-right'>";
                        _e("[:en] Total :[:kh] សរុប ៖[:]");
                    echo "</td>";
                        // echo "<td>".$total_day."</td>";
                        // echo "<td></td>";
                        echo "<td>".$instalment_total."</td>";
                        echo "<td class='currency'>".$interest_total."</td>";
                        echo "<td class='currency'>".$emi_total."</td>";
                    echo "</tr>";
                    echo '<script>';
                    echo 'jQuery(document).ready(function(){';
                    echo    'jQuery("#total_paid").text("'.$instalment_total.'");';
                    echo    'jQuery("#total_interest,.total-interest").text("'.$interest_total.'");';
                    echo '});';
                    echo '</script>';
                }
                echo '</tbody>';
                echo '</table>';
                echo '<p class="text-right hidden-print"><span id="print_schedule">';
                    _e('[:en]Print Repayment Schedule[:kh]បោះ​ពុម្ព​កាលវិភាគសងប្រាក់​[:]');
                echo '</span></p>';
                echo '</div>';
                _e('[:en]<strong style="padding:0;margin:0;">Note:</strong>[:kh]<strong> សម្គាល់៖</strong>​[:]');
                echo '<ul class="list">';
                        echo '<li>';
                            _e('[:en]The figure of this computation is for the indication purpose and subject to change.[:kh]តួលេខនៃការគណនានេះគ្រាន់តែជាព័ត៌មានបឋមប៉ុណ្ណោះ ហើយអាចមានការកែប្រែ។[:]');
                        echo '</li>';
                        echo '<li>';
                            _e('[:en]For any inquiry, please contact our nearest branch or call us at 023 999 911 or 086 999 911.[:kh]សម្រាប់ព័ត៌មានបន្ថែម សូមទាក់ទងសាខាប្រាសាក់ដែលនៅជិតលោកអ្នកបំផុត ឬ ទាក់ទងយើងខ្ញុំតាម 023 999 911 or 086 999 911 ។[:]');
                        echo '</li>';
                echo '</ul></div>';
                echo '<script>';
                echo 'jQuery(document).ready(function(){';
                echo 'loan_fee_cal();';
                echo 'jQuery("#total_cbc_fee").text(jQuery("#cbc_fee").val());';
                echo '});';
                echo '</script>';
            
            }else{//===========Default Calcuation=============
                $loan ='50,000.00';
                $capital_asset = floatval(str_replace(",","",$loan));
                $currency       = 'USD';
                $loan_term      = 12;
                $interest_rate  = (1.2 / 100);
                $apply_date     = date("Y-m-d");
                $payment_menthod = 1;
                $interest_rate_default = 1.2;
                $total_loan_fee =   '0.00';
                $total_cbc_fee  =   '0.00';
                echo '<div id="printing-wrapper"> <div class="table-responsive table-schedule"  id="schedule_loan">
                            <div class="col-xs-12 col-md-12 text-center print-title title-container">';
                                echo '<img src="'.get_template_directory_uri().'/assets/images/logo-website.png" title="Loan Calculator" alt="Loan Calculator" class="img-responsive logo-image" />';
                                _e('[:en]<h3>PRASAC MFI</h3>[:kh]<h3>គ្រឹះស្ថានមីក្រូហិរញ្ញវត្ថុប្រាសាក់</h3>[:]');
                                _e('[:en]<h3 class="second-title">Repayment Schedule</h3>[:kh]<h3 class="second-title">កាលវិភាគសងប្រាក់</h3>[:]');
                        echo '</div>
                        <div class="col-md-12 applicant-detail">
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6 col-lg-6">';
                                _e('[:en]Loan Amount[:kh]ទឹកប្រាក់កម្ចី[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <span class="currency">:&nbsp;'.number_format($capital_asset,2).'</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Loan Term[:kh]រយៈពេលខ្ចី[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <span id="loan_term_day">:&nbsp;'.$loan_term.'</span>';_e('[:en] &nbsp;Days[:kh]&nbsp;ថ្ងៃ[:]');
                            echo '</div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Total Interest Paid[:kh]ការប្រាក់សរុបត្រូវសង[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                :&nbsp;<span class="total-interest"></span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Interest Rate Per Month[:kh]អត្រាការប្រាក់ក្នុង 1 ខែ[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <span>:&nbsp;'.$interest_rate_default.'%</span>';
                            echo '</div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Total Loan Fee[:kh]កម្រៃសេវាសរុប[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                :&nbsp;<span class="total-loan-fee">'.$total_loan_fee.' </span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Start Date[:kh]ថ្ងៃ​បើកប្រាក់[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <span>:&nbsp;'.date("d-m-Y",strtotime($apply_date)).'</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]Loan Fee[:kh]កម្រៃ CBC[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                :&nbsp;<span class="total-cbc-fee">'.$total_cbc_fee.'</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                <label class="form-control-placeholder control-label col-sm-12 col-xs-12 col-md-6  col-lg-6">';
                                _e('[:en]End Date[:kh]ថ្ងៃ​សងបញ្ចប់​​[:]');
                            echo '</label>
                                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                    <span>:&nbsp;'.date("d-m-Y", strtotime("+$loan_term month",strtotime($apply_date))).'</span>
                                </div>
                            </div>
                           
                        </div>
                <table class="table table-condensed table-striped table-fixed">';
                echo '<thead><tr class="green row-title">';
                echo '<th>';
                    _e("[:en] Month[:kh] ខែ[:]");
                echo '</th>';
                echo '<th>';
                    _e("[:en] Repayment Date[:kh] ថ្ងៃសងប្រាក់[:]");
                echo '</th>';
                echo '<th>';
                    _e("[:en] Number of Days[:kh] ចំនួនថ្ងៃ[:]");
                echo '</th>';
                echo '<th>';
                    _e("[:en] Balance[:kh] ប្រាក់ដើមនៅជំពាក់[:]");
                echo '</th>';
                echo '<th>';
                    _e("[:en] Principal[:kh] ប្រាក់ដើមត្រូវសង[:]");
                echo '</th>';
                echo '<th>';
                    _e("[:en] Interest[:kh] ការប្រាក់ត្រូវសង[:]");
                echo '</th>';
                echo '<th>';
                    _e("[:en] Payment[:kh] ប្រាក់ត្រូវសងសរុប[:]");
                echo '</th>';
                echo '</tr></thead>';
                echo '<tbody>';

                if($payment_menthod==1){
                    $monthly_interest_rate = $capital_asset*$interest_rate;
                    $instalment = ($capital_asset/$loan_term);
                    $depreciation = number_format(round($instalment+$monthly_interest_rate,2),2);
                    if($currency == 'KHR'){
                        $depreciation = number_format(round($instalment+$monthly_interest_rate,2));
                    }
                    if($currency == 'THB'){
                        $depreciation = number_format(round($instalment+$monthly_interest_rate,2));
                    }
                    $total_instalment = 0;
                    $total_interest_general = 0;
                    $total_depreciation = 0;
                    $total_day = 0;
                    echo '<script>';
                    echo 'jQuery(document).ready(function(){';
                    echo    'jQuery(".monthly_paid").text("'.$depreciation.'");';
                    echo    'var selected_label = jQuery("div.radio-box-payment-method input[value='.$payment_menthod.'] ").data("label");';
                    echo    'jQuery("label.payment_method_label").html(selected_label);';
                    echo '});';
                    echo '</script>';
                        
                    $instalment = $capital_asset/$loan_term;
                    $depreciation = $instalment+$monthly_interest_rate;
                    $monthly_interest_rate = $capital_asset*$interest_rate;

                    for($i=1;$i<=$loan_term;$i++){
                        if($i==1):
                                $instalments=0;
                                $capital_assets =  $capital_asset;
                                $capitals = $capital_asset;//$capital_asset - $instalment;
                        else:
                                $instalments=$instalment;
                                $monthly_interest_rate = $capital_assets*$interest_rate;
                                $capital_assets  =  $capital_assets - $instalment;
                                $capitals = $capitals - $instalment;
                        endif;
                       
                        $total_instalment += $instalment;
                        
                       

                        echo "<tr class='text-center'>";
                            echo "<td>".$i."</td>";
                            $date1 = ($i==1)?$apply_date:date("d-m-Y", strtotime("+1 month",strtotime($date1)));
                            $date2 = ($i==1)?date("d-m-Y", strtotime("+1 month",strtotime($date1))):date("d-m-Y", strtotime("+1 month",strtotime($date2)));
                            
                            $dw = date( "w",strtotime($date2));
                            
                            if ($dw == 0) {
                                $date1 = date("d-m-Y", strtotime("-2 day",strtotime($date1)));
                                $date2 = date("d-m-Y", strtotime("-2 day",strtotime($date2)));
                                
                            }

                            if ($dw == 6) {
                                $date1 = date("d-m-Y", strtotime("-1 day",strtotime($date1)));
                                $date2 = date("d-m-Y", strtotime("-1 day",strtotime($date2)));
                            }
                            $total_day += dateDiff($date1,$date2);

                            $capital_amount = number_format(round($capital_assets,2),2);
                            $instalment_amount = number_format(round($instalment,2),2);
                            $interest_amount = number_format(round((($capital_assets*$interest_rate)/30)* dateDiff($date1,$date2),2),2);
                            $repayment_amount =  number_format(round($instalment+$monthly_interest_rate,2),2);
                            $captial = number_format(round($capitals,2),2);
                            $total_interest_general += $interest_amount;
                            if($currency == 'KHR'){
                                $capital_amount = number_format(floor(round($capital_assets,2)/100)*100);
                                $captial = number_format(floor(round($capitals)/100)*100);
                                $instalment_amount = number_format(floor($instalment/100)*100);
                                $interest_amount = number_format(floor(round(($capital_assets*$interest_rate)/30 * dateDiff($date1,$date2))/100)*100);
                                $repayment_amount =  number_format(floor(round($instalment+$monthly_interest_rate)/100)*100);
                            }

                            if($currency == 'THB'){
                                $capital_amount = number_format(round($capital_assets,2));
                                $captial = number_format(round($capitals,2));
                                $instalment_amount = number_format(round($instalment,2));
                                $interest_amount = number_format(round(($capital_assets*$interest_rate)/30 * dateDiff($date1,$date2),2));
                                $repayment_amount =  number_format(round($instalment+$monthly_interest_rate,2));
                            }

                            echo "<td>".$date2."</td>";
                            echo "<td>".dateDiff($date1,$date2)."</td>";
                            echo "<td class='currency'>".$captial."</td>";
                            echo "<td class='currency'>".$instalment_amount."</td>";
                            echo "<td class='currency'>".$interest_amount."</td>";
                            echo "<td class='currency'>".$repayment_amount."</td>";
                        echo "</tr>";
                    }

                  
                   
                    $total_interest_generals = number_format(round($total_interest_general,2),2);
                    $total_instalments = number_format(round($total_instalment,2),2);
                    $total_paid =  number_format(round($total_instalment+$total_interest_general,2),2);

                    if($currency == 'KHR'){
                        $total_interest_generals = number_format(floor(round($total_interest_general,2)/100)*100);
                        $total_instalments = number_format(floor(round($total_instalment,2)/100)*100);
                        $total_paid = number_format(floor(round($total_instalment+$total_interest_general,2)/100)*100);
                    }

                    if($currency == 'THB'){
                        $total_interest_generals = number_format(round($total_interest_general,2));
                        $total_instalments = number_format(round($total_instalment,2));
                        $total_paid = number_format(round($total_instalment+$total_interest_general,2));
                    }

                    echo "<tr class='text-center green total'>";
                        echo "<td colspan='2' class='text-right'>";
                            _e("[:en] Total :[:kh] សរុប ៖[:]");
                        echo "</td>";
                        echo "<td>".$total_day."</td>";
                        echo '<td></td>';
                        echo "<td class='currency'>".$total_instalments."</td>";
                        echo "<td class='currency'>".$total_interest_generals."</td>";
                        echo "<td class='currency'>".$total_paid."</td>";
                    echo "</tr>";
                    
                    echo '<script>';
                    echo 'jQuery(document).ready(function(){';
                    echo    'jQuery("#total_paid").text("'.$total_instalments.'");';
                    echo    'jQuery("#loan_term_day").text(": '.$total_day.'");';
                    echo    'jQuery("#total_interest,.total-interest").text("'.$total_interest_generals.'");';
                    echo '});';
                    echo '</script>';
                }
               
                if($payment_menthod==2){
                    $monthly_interest_rate = $capital_asset*$interest_rate;
                    $emi    = $capital_asset * $interest_rate * pow(1 + $interest_rate, $loan_term) / (pow(1 + $interest_rate, $loan_term) - 1);
                    $instalment = $emi-$monthly_interest_rate;
                    $total_instalment = 0;
                    $total_interest = 0;
                    $interest_amount = 0;
                    $interest = 0;
                    $total_emi = 0;
                    $emi_monthly_paid=0;
                    $capitals = 0;
                    $total_day=0;

                    $emi_monthly_paid = number_format($capital_asset * $interest_rate * pow(1 + $interest_rate, $loan_term) / (pow(1 + $interest_rate, $loan_term) - 1),2);
                    if($currency == 'KHR' || $currency== 'THB'){
                        $emi_monthly_paid = number_format($capital_asset * $interest_rate * pow(1 + $interest_rate, $loan_term) / (pow(1 + $interest_rate, $loan_term) - 1));
                    }

                    echo '<script>';
                    echo 'jQuery(document).ready(function(){';
                    echo    'jQuery(".monthly_paid").text("'.$emi_monthly_paid.'");';
                    echo    'var selected_label = jQuery("div.radio-box-payment-method input[value='.$payment_menthod.'] ").data("label");';
                    echo    'jQuery("label.payment_method_label").html(selected_label);';
                    echo '});';
                    echo '</script>';

                    for($i=1;$i<=$loan_term;$i++){
                        if($i==1):
                            $capital_assets =  $capital_asset;
                            $capitals = $capital_asset - $instalment;
                        else:
                            $capital_assets  =  $capital_assets - $instalment;
                            $instalment= $emi-($capital_assets*$interest_rate);
                            $capitals = $capital_assets - $instalment;
                        endif;
                        $total_instalment += $instalment;
                        $total_interest += ($capital_assets * $interest_rate);
                        $total_emi +=$emi;
                        
                        $captial = number_format(round($capitals,2),2);
                        $instalment_amount = number_format(round($instalment,2),2);
                        $interest_amount = number_format(round($capital_assets * $interest_rate,2),2);
                        $emi_amount = number_format(round($emi,2),2);

                        if($currency == 'KHR'){
                            $captial = number_format(floor(round($capitals)/100)*100);
                            $instalment_amount = number_format(floor(round($instalment)/100)*100);
                            $interest_amount = number_format(floor(round($capital_assets * $interest_rate)/100)*100);
                            $emi_amount = number_format(floor(round($emi)/100)*100);
                        }
    
                        if($currency == 'THB'){
                            $captial = number_format(round($capitals));
                            $instalment_amount = number_format(round($instalment));
                            $interest_amount = number_format(round($capital_assets * $interest_rate));
                            $emi_amount = number_format(round($emi));
                        }

                        echo "<tr class='text-center'>";
                            echo "<td>".$i."</td>";
                            $date1 = ($i==1)?$apply_date:date("d-m-Y", strtotime("+1 month",strtotime($date1)));
                            $date2 = ($i==1)?date("d-m-Y", strtotime("+1 month",strtotime($date1))):date("d-m-Y", strtotime("+1 month",strtotime($date2)));
                            
                            $dw = date( "w",strtotime($date2));
                            
                            if ($dw == 0) {
                                $date1 = date("d-m-Y", strtotime("-2 day",strtotime($date1)));
                                $date2 = date("d-m-Y", strtotime("-2 day",strtotime($date2)));
                                
                            }

                            if ($dw == 6) {
                                $date1 = date("d-m-Y", strtotime("-1 day",strtotime($date1)));
                                $date2 = date("d-m-Y", strtotime("-1 day",strtotime($date2)));
                            }                      
                            $total_day += dateDiff($date1,$date2);                       
                            echo "<td class='currency'>".$captial."</td>";
                            echo "<td class='currency'>".$instalment_amount."</td>";
                            echo "<td class='currency'>".$interest_amount."</td>";
                            echo "<td class='currency'>".$emi_amount."</td>";
                        echo "</tr>";
                    }

                    $instalment_total = number_format(round($total_instalment,2),2);
                    $interest_total = number_format(round($total_interest,2),2);
                    $emi_total = number_format(round($total_emi,2),2);

                    if($currency == 'KHR'){
                        $instalment_total = number_format(floor(round($total_instalment)/100)*100);
                        $interest_total = number_format(floor(round($total_interest)/100)*100);
                        $emi_total = number_format(floor(round($total_emi)/100)*100);
                    }

                    if($currency == 'THB'){
                        $instalment_total = number_format(round($total_instalment));
                        $interest_total = number_format(round($total_interest));
                        $emi_total = number_format(round($total_emi));
                    }

                    echo "<tr class='text-center green total'>";
                    echo "<td colspan='2' class='text-right'>";
                        _e("[:en] Total :[:kh] សរុប ៖[:]");
                    echo "</td>";
                        // echo "<td>".$total_day."</td>";
                    echo "<td></td>";
                    echo "<td>".$instalment_total."</td>";
                    echo "<td class='currency'>".$interest_total."</td>";
                    echo "<td class='currency'>".$emi_total."</td>";
                    echo "</tr>";
                    echo '<script>';
                    echo 'jQuery(document).ready(function(){';
                    echo    'jQuery("#total_paid").text("'.$instalment_total.'");';
                    echo    'jQuery("#total_interest,.total-interest").text("'.$interest_total.'");';
                    echo '});';
                    echo '</script>';
                }
                echo '</tbody>';
                echo '</table>';
                echo '<p class="text-right hidden-print"><span id="print_schedule">';
                    _e('[:en]Print Repayment Schedule[:kh]បោះ​ពុម្ព​កាលវិភាគសងប្រាក់​[:]');
                echo '</span></p>';
                echo '</div>';
                _e('[:en]<strong style="padding:0;margin:0;">Note:</strong>[:kh]<strong> សម្គាល់៖</strong>​[:]');
                echo '<ul class="list">';
                        echo '<li>';
                            _e('[:en]The figure of this computation is for the indication purpose and subject to change.[:kh]តួលេខនៃការគណនានេះគ្រាន់តែជាព័ត៌មានបឋមប៉ុណ្ណោះ ហើយអាចមានការកែប្រែ។[:]');
                        echo '</li>';
                        echo '<li>';
                            _e('[:en]For any inquiry, please contact our nearest branch or call us at 023 999 911 or 086 999 911.[:kh]សម្រាប់ព័ត៌មានបន្ថែម សូមទាក់ទងសាខាប្រាសាក់ដែលនៅជិតលោកអ្នកបំផុត ឬ ទាក់ទងយើងខ្ញុំតាម 023 999 911 ឬ 086 999 911 ។[:]');
                        echo '</li>';
                echo '</ul></div>';
            }
            ?>
        </section>
        <aside class="col-sm-3 col-md-3">
            <?= get_sidebar(); ?>
            <br />
            <?php 
            if ( have_rows('flexible_content') ) :
                //Check if flexible content exist
                while ( have_rows('flexible_content') ) : the_row();
                    if ( get_row_layout() == 'page_link' ) :
                      $post_id = get_sub_field_object('link')['value']->ID;
                      echo '<img class="img-responsive" src="'.get_sub_field('photo').'" alt="">';
                    endif;
                endwhile;
                wp_reset_postdata();
                //End flexible content
            endif;
            ?>
        </aside>
    </main>
</div>
<!-- <div class="laoding-main">
  <div class="loader">Loading...</div>
</div> -->
<?php get_footer(); ?>
<link rel="stylesheet" id="deposit-form-css" href="<?php echo get_template_directory_uri()."/layout/deposit-form.css" ?>" type="text/css" media="print">
<link rel="stylesheet" id="deposit-form-css" href="<?php echo get_template_directory_uri()."/assets/bootstrap-3.3.7/css/bootstrap.min.css" ?>" type="text/css" media="print">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.js"></script>
<script type="text/javascript">

    jQuery('input.currency').number(true,2);

    function printData(){
        var divToPrint=document.getElementById("printing-wrapper");
        newWin= window.open("");
        newWin.document.write('<html><head><style>body,html{font-family:Khmer OS Content,Roboto;} div.title-container h3{font-family:Khmer OS Muol Light,Roboto;color:#000000;font-weight:lighter !important;}table{width:100%;border-spacing:0;border-collapse: collapse;} table thead tr th{text-align:centert;font-weight:bold;color:#000000;font-size:12px;} table tbody tr td{text-align:right;font-size:12px;}div.logo-container{width:14%;float:left;}img.logo-image{width:100px;position:absolute;top:10px;left:10px;} div.title-container{width:100%;text-align:center;padding:10px 0;}div.applicant-detail{width:100%;margin-bottom:10px;overflow:hidden;padding:10px 0 !important;font-size:12px;}div.applicant-detail .col-lg-6{width: 50%; float: left;}table tr th,table tr td{border:0.15px solid #dedede;padding:1px 5px;} table tbody tr td:first-child{text-align:center;}table tbody tr:nth-of-type(odd) {background-color: #f9f9f9;} table tbody tr.total td{font-weight: bold;text-align:right;}strong,p,ul li{font-size:12px;}table tbody tr td{width:15.33%;}table tbody tr td:nth-child(1){width:8%;}#print_schedule{display:none;}</style></head>');
        newWin.document.write('<body>'+divToPrint.outerHTML+'</body></html>');
        newWin.print();
        newWin.close();
    }

    jQuery(document).ready(function(){
        jQuery('#print_schedule').on('click',function(){
            printData();
        });

        jQuery(".table-schedule").hide();
        jQuery(".generate_schedule_button").on("click",function(){
            var link = jQuery(this);
            jQuery('.table-schedule').slideToggle('fast', function() {
                if (jQuery(this).is(':visible')) {
                    link.text('<?php _e('[:en]Hide Autorized Schedule[:kh]បិទកាលវិភាគសងប្រាក់​[:]') ?>');                
                } else {
                    link.text('<?php _e('[:en]Show Autorized Schedule[:kh]បង្ហាញ​កាលវិភាគសងប្រាក់​[:]') ?>');                
                }        
            });     
        });
        
        // jQuery(".loading").hide();
        // jQuery('#btn-calculate').on('click', function() {
        //     var validator = jQuery('#calculate_form').data('bootstrapValidator');
        //     validator.validate();
        //     var allValid = true;
        //     if (validator.isValid()) {
        //     //     jQuery(".loading").show().fadeIn();
        //     //     window.setTimeout(function(){
        //     //         jQuery(".loading").hide().fadeOut();
        //     //     }, 12000);
        //     }
        // });


        jQuery("#currency_type").on('change',function(){
            var $select_currentcy = jQuery("option:selected").text();
            jQuery(".cbc_fee_currency").text($select_currentcy);
            loan_fee_cal();
        });
        //=========On keyup on Loan Fee =====
        jQuery("#loan_fee,#loan_size").on('keyup',function(){
            loan_fee_cal(this.value);
        });

        //=========On keyup on CBC Fee =====
        jQuery("#cbc_fee").on('keyup',function(){
            jQuery("#total_cbc_fee").text(this.value);
        });

        jQuery("ul li.page_item a").each(function(){
            var $selected_link = jQuery(this).attr("href").split('/').pop();
            if($selected_link=='currency-converter'){
                jQuery(this).attr("href", function(index, old) {
                    var $new_link = old.replace("calculators/currency-converter", "services/exchange");
                    jQuery(this).attr("href",$new_link );
                });
            }
        });


        //jQuery('#loan_size').tooltip({'trigger':'focus', 'title': '<?php //_e("[:en]Minimum loan size 100 USD or 400,000 Riel or 4000 THB[:kh]​ទំហំកម្ចីអប្បរមា 100 ដុល្លាអាមេរិក ឬ 400,000 រៀល ឬ 4000 បាត[:]")?>'});

        var label_default = "<?php _e('[:en]First Installation Paid[:kh]ទឹកប្រាក់ដែលត្រូវសងនៅខែទី 1[:]') ?>";
        var checked_label = jQuery('div.radio-box-payment-method input[type=radio]:checked').data('label');
        jQuery('label.payment_method_label').html(checked_label);
        jQuery('div.radio-box-payment-method input[type=radio]').click(function(){
            var checked_label = jQuery(this).data('label');
            jQuery('label.payment_method_label').html(checked_label);
        });
        
        jQuery(".btnapply_date").on('click',function(){
            jQuery("#apply_date").datepicker('show');
        });

        datepicker_application_date('apply_date');
        

        jQuery('#calculate_form').bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            // container: 'tooltip',
            // live: 'enabled',
            message: 'This value is not valid',
                fields: {
                    loan_size: {
                        validators: {
                            notEmpty: {
                                message: '<?php _e("[:en]Please input loan amount.[:kh]សូមបញ្ចូល​ទំហំ​កម្ចី[:]")?>'
                            },
                            regexp: {
                                regexp: /^[0-9-_ \.]+$/, 
                                message: '<?php _e("[:en]You Entered Invalid number![:kh]លោកអ្នក​បញ្ចូល​ទំហំកម្ចីមិនត្រឹមត្រូវ​ទេ[:]")?>' 
                            },
                        }
                    },
                    loan_term: {
                        validators: {
                            notEmpty: {
                                message: '<?php _e("[:en]Loan Term is required![:kh]រយៈពេលខ្ចីត្រូវតែបញ្ចូល[:]")?>'
                            },
                            regexp: {
                                regexp: /^[0-9-_ \.]+$/,  
                                message: '<?php _e("[:en]You Entered Invalid number![:kh]លោកអ្នក​បញ្ចូល​ទំហំកម្ចីមិនត្រឹមត្រូវ​ទេ[:]")?>' 
                            },
                            between: {
                                min: 1,
                                max: 120,
                                message: '<?php _e("[:en]Loan Term must be between 1 and 120 Months[:kh]រយៈពេលខ្ចីត្រូវតែ​នៅចន្លោះពី 1 ទៅ 120 ខែ[:]")?>'
                            },
                            // integer: {
                            //     message: '<?php _e("[:en]The value is not an integer[:kh]រយៈពេលខ្ចីត្រូវតែជាចំនួនគត់[:]")?>'
                            // }
                        }
                    },
                    interest_rate: {
                        validators: {
                            notEmpty: {
                                message: '<?php _e("[:en]Interest Rate is required![:kh]អត្រាការប្រាក់កម្ចីត្រូវតែបញ្ចូល[:]")?>'
                            },
                            regexp: {
                                regexp: /^[0-9-_ \.]+$/,  
                                message: '<?php _e("[:en]You Entered Invalid number![:kh]លោកអ្នក​បញ្ចូល​ទំហំកម្ចីមិនត្រឹមត្រូវ​ទេ[:]")?>' 
                            },
                            between: {
                                min:0.01,
                                max:18,
                                message: '<?php _e("[:en]Interest Rate must be between 0.01 and 18[:kh]អត្រាការប្រាក់កម្ចីត្រូវតែនៅចន្លោះពី 0.01 ទៅ 18 ភាគរយ[:]")?>'
                            },
                        }
                    },
            },
        });

        function datepicker_application_date(id){
            //Holiday date list here
            var holidays = [
                            "2018-01-01","2018-01-08","2018-01-31","2017-01-31","2018-03-08","2018-04-16",
                            "2018-05-01","2018-05-03","2018-05-14","2018-05-15","2018-06-01",
                            "2018-06-18","2018-09-24","2018-10-08",,"2018-10-09","2018-10-10","2018-10-15",
                            "2018-10-23","2018-10-29","2018-11-09","2018-11-21","2018-11-22",
                            ,"2018-11-23","2018-12-10"
                        ];
            jQuery("#"+id).datepicker({
                dateFormat: 'yy-mm-dd',
                autoclose: true,
                changeYear:true,
                changeMonth:true,
                // minDate: '-5m',
                // maxDate: '+5m',
                yearRange: "-5:+5",
                beforeShowDay: function(date){
                    var datestring = jQuery.datepicker.formatDate('yy-mm-dd', date);
                    var day = date.getDay();
                    if (day == 0 || day == 6) {
                        return [false, "weekendday"]
                    } else {
                        return [ holidays.indexOf(datestring) == -1 ]
                    }
                },
            }).datepicker("refresh");
        }
    });
</script>

<?php
 /*
* template name: Location Json Data
* Autor: Kimhim HOM
* Autor Contact: 093240717
*/
global $post;
query_posts(array(
    'post_type' => 'office', // You can add a custom post type if you like
    'post_status' => 'publish',
    'order'=> 'DESC',
    'posts_per_page' => -1, //limit of posts
    'tax_query' => array(
        array(
            'taxonomy' => 'office_categories',
            'field' => 'slug',
            'terms' =>'branch-location',
        ),
    ),
));

if (have_posts() ):
    $location_list = array();
    $arr_list = array();
    while(have_posts() ):the_post();
        $getLat = get_field('latitude');
        $getLng  = get_field('longitude');
        $post_title = get_the_title();
        $arr_list['title'] = get_the_title();
        $arr_list['lat'] = $getLat;
        $arr_list['lng'] = $getLng;
        $arr_list['permalink'] = get_permalink();
        array_push($location_list,$arr_list);
    endwhile;
    print_r(json_encode($location_list));
endif;
?>

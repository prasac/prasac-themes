<?php get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <section class="col-sm-12 col-md-12 text-center">
                <div class="no-content">
                    <h2 class="text-center"><i class="fa fa-frown-o fa-5x" aria-hidden="true"></i></h2>
                    <h1><?php _e('[:en]Oops! Page not found.[:kh]សូមអភ័យទោស! រកមិនឃើញអ្វីដែលលោកអ្នករកនោះទេ[:]');?></h1>
                    <p><?php _e('[:en]Please check your url and try it again latter[:kh]សូម​ត្រួតពិនិត្យមើល​ URL របស់​លោកអ្នក​ ហើយ​ព្យាយាមម្ដងទៀត[:]');?></p>
                    <h2><a class="btn btn-lg btn-golden" href="<?php echo esc_url( home_url() ); ?>"><?php _e('[:en]GO TO HOMEPAGE[:kh]ទៅកាន់ទំព័រដើម[:]');?></a></h2>
                </div>
            </section>
        </div>
    </div>
</div>
<?php get_footer(); ?>

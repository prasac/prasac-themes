
<?php
/**
 * Template Name: Single media
 *
 * @package 
 * @subpackage growing2gether
 * @since Growing2Gether 1.0.0
 */
get_header(); 

$prev_post = get_adjacent_post(false, '', true);
$next_post = get_adjacent_post(false, '', false);
$cats = [];
foreach ( get_the_category($post->ID) as $c ) {
    $cat = get_category($c);
    array_push($cats, $cat->name);
}
if ( sizeOf($cats) > 0 ) {
    $post_categories = implode(', ', $cats);
}else if(get_post_type()=='people'){
    $post_categories = '[:en]Corporate Social Responsibility[:kh] កិច្ចការសង្គម[:]';
    $post_categories = __($post_categories);
}else{
    $post_categories = 'Not Assigned';
}
$images = get_field('blog_gallery');
?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <section class="col-xs-12 col-sm-9 col-md-9">
                <div class="hidden-xs hidden-sm">
                        <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                        <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                            <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                            <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                        </ul>
                </div>

                <?php
                if( have_posts() ):
                    while( have_posts() ): the_post();?>
                        <header class="page-title">
                            <h3 class="green"><?= get_the_title(); ?></h3>
                            <ul class="single-post-meta list-unstyle">
                                <li class="single-post-date"><i class="fa fa-calendar"></i> <?php _e('[:en]Posted on&nbsp[:kh]ផ្សព្វផ្សាយ&nbsp[:]'.get_post_time('F j, Y')); ?></li>
                                <li class="single-post-tag"><?php _e('[:en]in&nbsp[:kh]នៅក្នុង&nbsp[:]'.$post_categories ); ?></li>
                            </ul>
                        </header>
                        
                        
                        <section class="single-content">
                            <?php 
                            if($images) : ?>
                                <!-- Slick slider carousel for gallery -->
                                <div id="single-content-slider" style="margin-bottom:25px;">
                                    <?php 
                                    foreach( $images as $image ):
                                    ?>
                                        <figure class="cap-left">
                                            <a href="<?= $image['url']; ?>" class="lightbox" >
                                                <img class="img-responsive img-lazy" src="<?= $image['url']; ?>" alt="">
                                            </a>
                                        </figure>
                                    <?php endforeach; ?>
                                </div>
                            <?php endif;
                            the_content(); ?>
                        </section>
                    <?php endwhile;?>
                    <div class="row">
                        <div class="col-xs-12 col-md-12">
                        <hr>
                            <div class="single-footer-share">
                                <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                                <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                                <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo get_permalink();?>','twitter');"></a>
                                <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo get_permalink();?>','linkedin');"></a>
                                <div class="fb-like pull-right" data-href="<?php echo get_permalink();?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="false"></div>
                            </div>
                        </div>
                    </div>
                <?php else :
                    echo 'Not found.';
                endif;
                ?>
            </section>
            <aside class="hidden-xs col-xs-12 col-sm-3 col-md-3">
                <div class="sidebar">
                    <div class="sidebar_title"><?php _e('[:en]Media[:kh]ព័ត៌មាន[:]'); ?></div>
                        <ul class="sidebar_content list" id="accordion">
                            <?php wp_list_pages('sort_column=menu_order&post_status=publish&title_li=&child_of=6796&echo=1'); ?>
                        </ul>
                </div>

                <div class="sidebar">
                    <div class="sidebar_title"><?php _e('[:en]Latest News and Event[:kh]ដំណឹងនិងព្រឹត្តិការណ៍ថ្មីៗ[:]'); ?></div>
                    <?php
                    query_posts(array(
                        'post_type' => 'post', // You can add a custom post type if you like
                        'paged' =>$paged,
                        'post_status' => 'publish',
                        'orderby' => 'post_date',
                        'order'=> 'DESC',
                        'posts_per_page' =>12 // limit of posts
                    ));
            
                    if ( have_posts() ) :
                        echo '<ul class="list">';
                            while ( have_posts() ) : the_post();?>
                                <li>
                                    <a class="text-center title-news" href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php echo get_the_title(); ?></a>
                                </li>
                            <?php
                            endwhile;
                        echo '</ul>';
                        ?>
                        
                        <style>
                            .padding-row{
                                margin:1rem 0;
                            }
                            .padding-row>div{
                                padding:0 1rem;
                            }
                            div.sidebar{
                                border:1px solid #4db848;
                            }
                            a.title-news{
                                color:#66686a;
                            }
                            a.title-news:hover{
                                color:#4db848;
                            }
                            .padding-row a.thumb{
                                border:1px solid #dedede;
                                border-radius:4px;
                                overflow:hidden;
                                display:block;
                                padding:5px;
                            }
                            .padding-row a.thumb:hover img{
                                -moz-transition: all 0.5s;
                                -webkit-transition: all 0.5s;
                                transition: all 0.5s;
                            }
                            .padding-row a.thumb:hover img{
                                -moz-transform: scale(1.5);
                                -webkit-transform: scale(1.5);
                                transform: scale(1.5);
                            }
                        </style>
                        <?php         
                        wp_reset_query();
                    else :
                        echo 'No';
                    endif;
                    ?>
                </div>
            </aside>
        </main>
    </div>
</div>
<?php get_footer(); ?>
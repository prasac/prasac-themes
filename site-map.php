<?php
/**
 * Template Name: Site Maps
 *
 * @package WordPress
 * @subpackage growing2gether
 * @since Growing2Gether 1.0.0
 */
get_header(); 
?>
<div id="content" class="col-sm-12 col-md-12 content">
<!-- Page title and content -->
<?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>

<?php if( have_posts() ): ?>
   <?php while( have_posts() ): the_post(); ?>
      <div class="page-title">
         <h3><?php the_title(); ?></h3>
         <?php the_content(); ?>
      </div>
   <?php endwhile; ?>
<?php endif ?>
<ul class="site-map test">
  <?php 
  wp_list_pages( array(
    'exclude'     => '',
    'post_status' => 'publish',
    'title_li'    => '',
    )
  ); 
  ?>
</ul>  
</div>
<style type="text/css">
  .site-map {
    list-style: none;
    margin: 0;
    float: left;
    padding-left: 0;
  }
  .site-map > li > .children,
    .site-map > li > .children > li > .children {
  padding-left: 20px;
  }
  .site-map > li {
    list-style-type: none;
    margin: 0;
    padding: 0px 15px 15px 0;
    font-weight: normal;
    background-position: left top;
    background-repeat: no-repeat;
    float: left;
  }
  .site-map > li a{
    color: #353535!important;
  }
  .site-map > li.page_item_has_children > a,
  .site-map > li.page_item > a {
    font-size: 16px;
    font-weight: 600;
    text-transform: uppercase;
  }
</style>
<?php get_footer(); ?>
<?php
/**
 * Template Name: Media
 * Created by PhpStorm.
 * User: trodeth.oulsarakham
 * Date: 22/12/2016
 * Time: 15:02
 */
get_header();
?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <aside class="hidden-xs col-sm-3 col-md-3">
                <?= get_sidebar(); ?>
            </aside>
            <section class="col-xs-12 col-sm-9 col-md-9 page-title">
                <div class="hidden-xs hidden-sm">
                        <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                </div>
                <h3 class="green"><?= _e('[:en]Media[:kh]ដំណឹងនិងព្រឹត្តិការណ៍[:]');?></h3>
            <?php
            // $query = new WP_Query( array(
            //     'post_type' => 'post',
            //     'posts_per_page' =>5,
            //     'post_status'   => 'publish'
            // ));
            if( $query->have_posts() ):
                while( $query->have_posts() ): $query->the_post();
                // get_template_part('template-parts/content', 'summary');
                endwhile;
                wp_reset_postdata();
            else :
                //get_template_part('template-parts/content', 'none');
            endif;
            //if ( function_exists('wp_bootstrap_pagination') ) wp_bootstrap_pagination();
            ?>
            </section>
        </main>
    </div>
</div>
<?php get_footer(); ?>

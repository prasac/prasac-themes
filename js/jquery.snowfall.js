/* jquery-Snowfall v1.0 | h4wldev@gmail.com | http://github.com/h4wldev */

(function($) {
    $.snowfall = {
        intervals: [],
        $wrapper: null,
        start: function(options, $wrapper) {
            var options = $.extend({}, {
                size: {
                    min: 30,
                    max: 60
                },
                interval: 450,
                color: '#fff',
                content: '&#10052;',
                disappear: 'linear'
            }, options);

            if ($wrapper == undefined) {
                $wrapper = $('#flower_falling');

                $wrapper.css({
                    'height': $(document).height() - 100,
                    'width': $(document).width() - 100,
                    'position': 'absolute',
                    'top': '0',
                    'left': '0',
                });
            }

            var $snowfall = $('<img src="https://www.prasac.com.kh/wp-content/themes/prasac-themes/assets/images/heart-theme.png"/>').css({ 'position': 'absolute', 'bottom': '0', 'z-index': '9999' }).html(options.content);
            $.snowfall.$wrapper = $wrapper;
            $.snowfall.$wrapper.show();

            $.snowfall.intervals.push(setInterval(function() {
                var wrapperWidth = $wrapper.width(),
                    wrapperHeight = $wrapper.height(),
                    flakeSize = options.size.min + (Math.random() * options.size.max),
                    duration = (wrapperHeight * 10) + (Math.random() * 2000),
                    startPosition = (Math.random() * wrapperWidth) - 100;

                $snowfall.clone().appendTo($wrapper).css({
                    'left': startPosition,
                    'opacity': 0.5 + Math.random(),
                    'font-size': flakeSize,
                    'color': options.color,
                    'width': randomIntFromInterval(10, 25),
                }).animate({
                    bottom: wrapperHeight,
                    left: (startPosition) + (Math.random() * 200),
                    opacity: 0.5
                }, duration, options.disappear, function() {
                    $(this).remove();
                });
            }, options.interval));
        },
        stop: function() {
            $.snowfall.intervals.forEach(function(interval) {
                $.snowfall.$wrapper.hide();
                $.snowfall.$wrapper.children('div').each(function() {
                    $(this).remove();
                });
                clearInterval(interval);
            });
        }
    };

    function randomIntFromInterval(min, max) { // min and max included 
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
})(jQuery);
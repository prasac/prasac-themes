<?php
 /*
* template name: Annual Workshop
*/
get_header();
error_reporting(0);
?>
<style>

h3.main-text{
    font-family:'Moul';
    font-size:2rem;
    line-height: 1.8em !important;
    /* text-shadow: 1px 5px 1px #dedede; */
    color:#FFFFFF;
}

.wrapper-container{
    padding:5px;
}
div.container-fluid.main{
    min-height:712px;
    /* background:url('<?php echo get_template_directory_uri(); ?>/assets/images/Banner-WorkShop_Bong-Him-For-Web_001.jpg') no-repeat center; */
    background: #000000;  /* fallback colour. Make sure this is just one solid colour. */
    background: -webkit-linear-gradient(rgba(75,184,72,0.6), rgba(195, 55, 100, 0.1)), url("<?php echo get_template_directory_uri(); ?>/assets/images/annual_meeting_2019.png") no-repeat center;
    background: linear-gradient(rgba(75,184,72,0.6), rgba(195, 55, 100, 0.1)), url("<?php echo get_template_directory_uri(); ?>/assets/images/annual_meeting_2019.png") no-repeat center; /* The least supported option. */
    background-size: cover;
}

#annual_workshop .items,
#annual_workshop .items :hover,
#annual_workshop .items:focus {
    color: #26A69A;
    text-decoration: none;
    -o-transition: all .3s;
    -moz-transition: all .3s;
    -webkit-transition: all .3s;
    -ms-transition: all .3s;
    transition: all .3s;
}

#annual_workshop div {
    padding:5px;
}
#annual_workshop .items span.chapterIcons {
    display:block;
    background-color: #fff;
    border-radius:15px;
    color: #4db848;
    font-size:18px;
    height:auto;
    padding:15px 8px;
    /* width:150px; */
    position: relative;
    text-align:left;
    transition: all 400ms ease 0s;
    box-shadow: 2px 2px 5px #dedede;
    margin:15px 0;
    border:2px solid #ffffff;
}

#annual_workshop .items:hover span.chapterIcons {
    border:2px solid  #ffd969;
}


#annual_workshop .items:hover span.chapterIcons {
    color: #ffd969;
    transform: scale(0.9);
    transition-timing-function: cubic-bezier(0.47, 2.02, 0.31, -0.36);
}
p.xs-small{
    font-size:14px;
    color:#ffffff;
}
p.xs-small i.red{
    color:#d9534f;
    font-size: 1.3rem;
}
@media (max-width: 768px) {
    
}

</style>
<div class="container-fluid main">
    <div class="container">
        <section class="wrapper-container">
            <div class="col-md-12">
                <div class="row">
                    <h3 class="text-center main-text">កិច្ច​ប្រជុំ​ប្រចាំឆ្នាំ</br>សូមស្វាគមន៍</h3>
                    <p class="text-center xs-small"><i class="fa fa-calendar red" aria-hidden="true"></i> ថ្ងៃចន្ទ ទី ៦ ខែមករា ឆ្នាំ ២០២០ ​| <i class="fa fa-map-marker red" aria-hidden="true"></i> ការិយាល័យកណ្ដាល</p>
                </div>
            </div>
            <div class="row" id="annual_workshop">
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <a class="items" href="https://www.prasac.com.kh/annual-meeting-2019/registration" alt="ចុះឈ្មោះ">
                        <span class="chapterIcons"><i class="fa fa-group" aria-hidden="true"></i> ចុះឈ្មោះ</span>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <a class="items" href="https://www.prasac.com.kh/annual-meeting-2019/annual-meeting-qa">
                        <span class="chapterIcons"><i class="fa fa-info" aria-hidden="true"></i> សំណួរ-ចម្លើយ</span>
                    </a>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <a class="items" href="https://vdr.prasac.com.kh/index.php/s/b3TNWPnZmzTMi6o">
                        <span class="chapterIcons"><i class="fa fa-photo" aria-hidden="true"></i> កម្រងអនុស្សាវរីយ៍</span>
                    </a>
                </div>
            </div>
        </section>
        <br />
    </div>
</div>
<div id="loading"></div>
<?php
get_footer();
?>

<script>
 jQuery(document).ready(function(){
    var spinner = jQuery('#loading');
    spinner.hide();
});
</script>
<?php
/**
 * Created by PhpStorm.
 * User: PRASAC
 * Date: 17/06/2020
 * Time: 13:40
 */
get_header();
?>
<?php 
    if ( have_rows('flexible_content') ) :
        while ( have_rows('flexible_content') ) : the_row();
            if ( get_row_layout() == 'page_link' ) :
            $post_id = get_sub_field_object('link')['value']->ID;
            echo '<div class="container-fluid">';?>
                        <img class="img-responsive" src="<?php echo get_sub_field('photo')?>" alt=""  onClick='window.open("<?php echo get_permalink($post_id)?>","_self");'>
                        <?php
            echo '</div>';
            endif;
        endwhile;
        wp_reset_postdata();
    endif;
?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <aside id="sidebar" class="col-sm-3 col-md-3 page-title">
            <div class="sidebar">
                    <div class="sidebar_title"><?= _e('[:en]Career[:kh]ឱកាសការងារ[:]');?></div>
                    <ul class="sidebar_content list" id="accordion">
                        <li class="page_item page-item-7910">
                            <a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/careers/job-opportunity/[:kh]/kh/careers/job-opportunity/[:]') ?>"><?php _e('[:en]Job Apportunity[:kh]ឱកាសការងារ[:]') ?></a>
                        </li>
                        <li class="page_item page-item-7913">
                            <a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/careers/why-choose-prasac/[:kh]/kh/careers/why-choose-prasac/[:]') ?>"><?php _e('[:en]Why Choose PRASAC?[:kh]ហេតុអ្វីជ្រើសរើសប្រាសាក់?[:]') ?></a>
                        </li>
                        <li class="page_item page-item-7908">
                            <a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/careers/online-application-form/[:kh]/kh/careers/online-application-form/[:]') ?>"><?php _e('[:en]Application for Employment[:kh]ស្នើសុំបម្រើការងារ[:]') ?></a>	
                        </li>
                    </ul>
                </div>
            </aside>
            <section class="col-sm-9 col-md-9">
                    <div class="hidden-xs hidden-sm">
                        <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                    </div>
                    <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                        <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                        <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                    </ul>
                    <?php
                    $job_image_thumbnail = get_field('job_thumbnail');
                    if($job_image_thumbnail){
                            echo '<img class="hidden-md hidden-lg hidden-sm" src="'.$job_image_thumbnail.'" alt="Job" title=""/>';
                    }
                    ?>
                <?php while( have_posts() ): the_post(); ?>
                        <?php
                        $post_title = get_the_title();
                        if( get_field('job_deadline_type') == 'yes' ):
                            $job_date = get_field('job_deadline');
                        endif;
                        $locations = get_field('job_location');
                        $open_position = get_field('job_open_post');
                        ?>
                        <header class="page-title">
                                <h3 class="green"><?= get_the_title(); ?></h1>
                        </header>
                        <article>
                        <p class="hidden-md hidden-lg hidden-sm">
                            <?php echo get_field('sharing_text');?>
                        </p>
                        </article>
                        <section <?= post_class(); ?> >
                            <div class="job-wrapper">
                                <span class="green"><i class="fa fa-calendar" aria-hidden="true"></i></span>&nbsp;<?php _e('[:en]Announced Date : [:kh]ថ្ងៃផ្សព្វផ្សាយ​ ៖ [:]') ?> <span class=""><?php echo get_the_date( 'F d, Y' );?></span><br />
                                <span class="green"><i class="fa fa-calendar" aria-hidden="true"></i></span>&nbsp;<?php _e('[:en]Deadline : [:kh]កាល​បរិច្ឆេទឈប់ទទួលពាក្យ ៖ [:]') ?> <span class=""><?php get_field('job_deadline_type') == false?_e('[:en]No Deadline[:kh]គ្មានកាលកំណត់[:]'):_e($job_date)?></span><br />
                                <span class="green"><i class="fa fa-user" aria-hidden="true"></i></span>&nbsp;<span class="">
                                <?php 
                                    _e('[:en]Number of applicants : [:kh]ចំនួនបេក្ខជនដែលត្រូវជ្រើសរើស ៖ [:]');
                                    if($open_position==0){
                                        _e('[:en]&nbsp;Many &nbsp;[:kh]&nbsp;ជាច្រើន[:]');
                                    }else{
                                        echo '&nbsp;'.$open_position.'&nbsp;&nbsp;';
                                    }
                                    '&nbsp;&nbsp;'._e('[:en]Positions[:kh]រូប[:]');
                                    ?>
                                </span>
                            </div>
                            <?php 
                                echo get_the_content();
                            ?>
                        </section>
                        <style>
                             .button{
                                color: #FFFFFF;
                                text-align: center;
                                transition: all 0.8s;
                                -webkit-transition:all 0.8s;
                                cursor: pointer;
                                border-radius:50px;
                                padding:4px 8px;
                                position:relative;
                                font-size:1rem;
                            }
                            .button span:after {
                                content: '\f0a9';
                                font-family:FontAwesome;
                                opacity:0;
                                transition: 0.8s;
                                -webkit-transition:all 0.8s;
                                font-size:1.5rem;
                                top:0;
                                position:absolute;
                                transition: all 0.5s;
                            }

                            .button:hover span {
                                padding-right:1rem;
                            }

                            .button:hover span:after {
                                transition: all 0.8s;
                                -webkit-transition:all 0.8s;
                                opacity: 1;
                                right:0.2rem;
                            }
                        </style>
                        <br>
                        <i class="fa fa-hand-o-right fa-4x pull-left faa-bounce animated green"></i>
                        <a class="button btn icon-btn btn-warning pull-right applynow-btn" target="_blank" href="/prasac/online-application-form?pos=<?php echo $post_title?>"><span><?php _e('[:kh]ដាក់ពាក្យឥលូវនេះ![:en]Apply Now![:]')?></span></a>
                <?php endwhile; ?>
                <br /> <br />
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                    <hr>
                        <div class="single-footer-share">
                            <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                            <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                            <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo get_permalink();?>','twitter');"></a>
                            <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo get_permalink();?>','linkedin');"></a>
                            <div class="fb-like pull-right" data-href="<?php echo get_permalink();?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="false"></div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div> 
</div>
<?php get_footer(); ?>
<form role="search" method="GET" class="navbar-form navbar-right" action="<?= esc_url(home_url('/')) ?>">
    <input type="text" id="search" name="search" placeholder="Search.." value="<?= get_search_query() ?>" />
</form>
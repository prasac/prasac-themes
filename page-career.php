<?php
/**
 * Template Name: Career
 *
 * @package WordPress
 * @subpackage growing_2gether
 * @since Growing2Gether 1.0.0
 */
get_header();
?>
<?php 
    if ( have_rows('flexible_content') ) :
        while ( have_rows('flexible_content') ) : the_row();
            if ( get_row_layout() == 'page_link' ) :
            $post_id = get_sub_field_object('link')['value']->ID;
            echo '<div class="container-fluid">';?>
                <img class="img-responsive" src="<?php echo get_sub_field('photo')?>" alt=""  onClick='window.open("<?php echo get_permalink($post_id)?>","_self");'>
                        <?php
            echo '</div>';
            endif;
        endwhile;
        wp_reset_postdata();
    endif;
?>
<div id="body" class="container-fluid">
    <div class="container">
        <aside id="sidebar" class="col-sm-3 col-md-3 page-title">
            <div class="sidebar">
                <?= do_shortcode('[wpb_childpages]'); ?>
            </div>
        </aside>
        <!-- Body -->
        <section id="content" class="col-sm-9 col-md-9 page">
            <div class="row">
                <div class="hidden-xs hidden-sm">
                <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                </div>
                <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                </ul>
                <?php
                if( have_posts() ): ?>
                <?php while( have_posts() ): the_post(); ?>
                <div class="page-title">
                    <h3 style="border-bottom:none;" class="green"><?php _e('[:en]Job Opportunity[:kh]ឱកាសការងារ[:]')?></h3>
                </div>
                <?= the_content(); ?>
                <?php endwhile; ?>
                <?php 
                endif 
                ?>
            </div>
        <?php
        global $post; 
            $current_date = current_time('Ymd'); 
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            query_posts(array(
                'post_type' => 'career', // You can add a custom post type if you like
                'paged' => $paged,
                'post_status' => 'publish',
                'meta_query'     => array(
                    'relation' => 'OR',
                    array(
                    'key'     => 'job_deadline',
                    'compare' => '>=',
                    'value'   => $current_date,
                    'type'    => 'DATE'
                    ),
                    array(
                    'key'     => 'job_dateType',
                    'compare' => '==',
                    'value'   => 'no',
                    'type'    => 'STRING'
                    )
                    ),
                'orderby'        =>'post_date',
                'order'				=> 'DESC',
                'posts_per_page' =>10 // limit of posts
            ));
        ?>
            <style>
                .button{
                    color: #FFFFFF;
                    text-align: center;
                    transition: all 0.8s;
                    -webkit-transition:all 0.8s;
                    cursor: pointer;
                    border-radius:50px;
                    padding:4px 8px;
                    font-size:1rem;
                }
                div.row-box{
                    background-color:#ffffff;
                    box-shadow:0.5px 0px 4px 0.5px #f2b01d40;
                    border:1px solid #dedede;
                    position:relative;
                    padding-bottom:15px;
                    margin-bottom:8px;
                }
                .button span:after {
                    content: '\f0a9';
                    font-family:FontAwesome;
                    opacity:0;
                    transition: 0.8s;
                    -webkit-transition:all 0.8s;
                    font-size:1.5rem;
                    top:0;
                    position:absolute;
                    transition: all 0.5s;
                }

                .button:hover span {
                    padding-right:1rem;
                }
                
                a.applynow-btn{
                    position:absolute;
                    right:15px;
                    top:37%;
                }
                div.row-box div.item{
                    padding-right:0;
                }
                .button:hover span:after {
                    transition: all 0.8s;
                    -webkit-transition:all 0.8s;
                    opacity: 1;
                    right:0.2rem;
                }
            </style>
        <?php if (have_posts() ): ?>
            <?php while(have_posts() ):the_post(); ?>
                    <?php
                    $post_title = get_the_title();
                    if( get_field('job_deadline_type')):
                    $job_date = get_field('job_deadline');
                    endif;
                    $job_posts = get_field('job_open_post');
                    $locations = get_field('job_location');
                    ?>
                    <div class="row  row-box">
                        <div class="col-xs-12 col-md-12">
                            <h4 class="green"><a href="<?= get_permalink();?>" target="_blank"><?= $post_title;?></a></h4>
                            <div class="row">
                                <div class="col-xs-12 col-md-3 item">
                                    <i class="fa fa-calendar green"></i> <?= _e('[:kh]ថ្ងៃផុតកំណត់៖[:en]Deadline : [:]');?> <i><?php get_field('job_deadline_type') == false?_e('[:en]No Deadline[:kh]គ្មានកាល​កំណត់[:]'):_e($job_date)?></i>
                                </div>
                                <div class="col-xs-12 col-md-4 item">
                                    <i class="fa fa-map-marker green"></i>
                                    <?= _e('[:kh]ទីតាំងការងារ៖[:en]Location : [:]');?> <i><?php
                                    if( $locations ) {
                                        $output = array();
                                    foreach( $locations as $location ): 
                                        $output[] = $location->name; 
                                    endforeach;
                                    $last  = array_slice($output, -1);
                                    $first = join(', ', array_slice($output, 0, -1));
                                    $both  = array_filter(array_merge(array($first), $last), 'strlen');
                                    _e(join('&nbsp;[:en]and[:kh]និង[:]&nbsp;', $both));
                                        } 
                                    ?></i>
                                </div>
                                <div class="col-xs-12 col-md-3 item">
                                    <i class="fa fa-user green"></i>
                                    <?= _e('[:kh]ចំនួនតម្រូវការ៖[:en]Vacancy : [:]');?>
                                    <i>
                                        <?php 
                                            if($job_posts==0){
                                                _e('[:en]&nbsp;Many &nbsp;[:kh]&nbsp;ជាច្រើន[:]');
                                            }else{
                                                echo '&nbsp;'.$job_posts.'&nbsp;&nbsp;';
                                            }
                                            '&nbsp;&nbsp;'._e('[:en]Positions[:kh]រូប[:]');
                                        ?>
                                    </i>
                                </div>
                            </div>
                        </div>
                        <a class="button btn icon-btn btn-warning pull-right applynow-btn" target="_blank" href="/prasac/online-application-form?pos=<?php echo $post_title;?>"><span><?php _e('[:kh]ដាក់ពាក្យឥឡូវនេះ[:en]Apply Now[:]')?></span></a>
                    </div>
            <?php 
                endwhile; 
                echo '<div class="col-xs-12 col-md-12 video-pagination text-center">';
                post_pagination();
                echo '</div>';
                wp_reset_query();
            ?>
        <?php else: ?>
                    <?php _e('[:en]Currently, there no job announcement[:kh]ពុំ​មាន​ការងារ​ផ្សព្វ​ផ្សាយ​នោះ​​ទេ!​[:]');?>
        <?php endif; ?>
            <div class="row">
                <div class="col-xs-12 col-md-12">
                <hr>
                    <div class="single-footer-share">
                        <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                        <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                        <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo get_permalink();?>','twitter');"></a>
                        <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo get_permalink();?>','linkedin');"></a>
                        <div class="fb-like pull-right" data-href="<?php echo get_permalink();?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="false"></div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php get_footer(); ?>

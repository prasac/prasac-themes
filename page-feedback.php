<?php 
/*
* template name: Feedback
*/
get_header(); ?>
<style>
#feedback_form .form-group {
    padding: 0;
}

#loader {
    display: none;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    background: rgba(0, 0, 0, 0.75) url(<?php echo get_template_directory_uri();?>/assets/images/prasac_loading.gif) no-repeat center center;
    z-index: 10000;
}
#captchaCalculation{
    background: #dedede;
    padding: 0.3em;
    font-size: 1.2em;
}
div.form-submit-error,div.form-submit-success{
    display:none;
}
</style>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <aside class="col-xs-12 col-sm-3 col-md-3 hidden-xs hidden-sm">
                <div class="sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </aside>
            <section class="col-xs-12 col-sm-9 col-md-9 post-558 page type-page status-publish hentry">
                <div class="row hidden-xs hidden-sm">
                    <div class="col-xs-12 col-md-12">
                        <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                        <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                            <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                            <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <h3 class="green"><?php _e('[:en]Feedback[:kh]មតិអតិថិជន[:]') ?></h3>
                        <form action="" method="post" enctype="multipart/form-data" id="feedback_form">
                            <div class="col-sm-5 col-md-5">
                                <!-- full_name-->
                                <div class="form-group">
                                    <span>
                                        <input type="text" name="full_name" class="form-control fullname" placeholder="<?php _e('[:en]Your Full Name[:kh]ឈ្មោះពេញរបស់អ្នក[:]') ?>">
                                    </span>
                                </div>

                                <!-- Email -->
                                <div class="form-group">
                                    <span class="wpcf7-form-control-wrap email">
                                        <input type="text" name="email" class="form-control"id="email" placeholder="Example@example.com">
                                    </span>
                                </div>

                                <!-- Phone-->
                                <div class="form-group">
                                    <span class="wpcf7-form-control-wrap phone_number">
                                        <input type="text" name="phone_number" class="form-control" id="phone_number"  placeholder="<?php _e('[:en]Your Phone Number[:kh]លេខទូរស័ព្ទរបស់អ្នក...[:]') ?>">
                                    </span>
                                </div>

                                <!-- Product -->
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="wpcf7-form-control-wrap SelectType">
                                                <select name="SelectType" class="form-control" id="product">
                                                    <option value="<?php _e('[:en]General[:kh]ទូទៅ[:]') ?>"><?php _e('[:en]General[:kh]ទូទៅ[:]') ?></option>
                                                    <option value="<?php _e('[:en]Corporate Bond[:kh]​សញ្ញាបណ្ណសាជីវកម្ម[:]') ?>"><?php _e('[:en]Corporate Bond [:kh]​សញ្ញាបណ្ណសាជីវកម្ម[:]') ?></option>
                                                    <option value="<?php _e('[:en]Loan[:kh]ឥណទាន[:]') ?>"><?php _e('[:en]Loan[:kh]ឥណទាន[:]') ?></option>
                                                    <option value="<?php _e('[:en]Saving[:kh]បញ្ញើសន្សំ [:]') ?>"><?php _e('[:en]Saving[:kh]បញ្ញើសន្សំ [:]') ?></option>
                                                    <option value="<?php _e('[:en]Local Transfer[:kh]ផ្ទេរប្រាក់ក្នុងស្រុក [:]') ?>"><?php _e('[:en]Local Transfer[:kh]ផ្ទេរប្រាក់ក្នុងស្រុក[:]') ?></option>
                                                    <option value="<?php _e('[:en]Cash-by-Code[:kh]ផ្ទេរប្រាក់តាមលេខសម្ងាត់[:]') ?>"><?php _e('[:en]Cash-by-Code[:kh]ផ្ទេរប្រាក់តាមលេខសម្ងាត់[:]') ?></option>
                                                    <option value="<?php _e('[:en]PPWSA Bill Payment[:kh]ទូទាត់វិក្កយបត្រទឹក [:]') ?>"><?php _e('[:en]PPWSA Bill Payment[:kh]ទូទាត់វិក្កយបត្រទឹក [:]') ?></option>
                                                    <option value="<?php _e('[:en]EDC Bill Payment[:kh]ទូទាត់វិក្កយបត្រភ្លើង[:]') ?>"><?php _e('[:en]EDC Bill Payment[:kh]ទូទាត់វិក្កយបត្រភ្លើង[:]') ?></option>
                                                    <option value="<?php _e('[:en]Phone Top-up[:kh]ទិញកាតទូរស័ព្ទ[:]') ?>"><?php _e('[:en]Phone Top-up[:kh]ទិញកាតទូរស័ព្ទ[:]') ?></option>
                                                    <option value="<?php _e('[:en]Foreign Exchange[:kh]ប្តូរប្រាក់[:]') ?>"><?php _e('[:en]Foreign Exchange[:kh]ប្តូរប្រាក់[:]') ?></option>
                                                    <option value="<?php _e('[:en]Payroll[:kh]សេវាបើកប្រាក់បៀវត្ស[:]') ?>"><?php _e('[:en]Payroll[:kh]សេវាបើកប្រាក់បៀវត្ស[:]') ?></option>
                                                    <option value="<?php _e('[:en]Mobile Banking[:kh]សេវាធនាគារចល័តតាមទូរសព្ទដៃ[:]') ?>"><?php _e('[:en]Mobile Banking[:kh]សេវាធនាគារចល័តតាមទូរសព្ទដៃ[:]') ?></option>
                                                    <option value="<?php _e('[:en]Internet Banking[:kh]សេវាធនាគារចល័តតាមកុំព្យូទ័រ[:]') ?>"><?php _e('[:en]Internet Banking[:kh]សេវាធនាគារចល័តតាមកុំព្យូទ័រ[:]') ?></option>
                                                    <option value="<?php _e('[:en]ATM[:kh]សេវាអេធីអឹម[:]') ?>"><?php _e('[:en]ATM[:kh]សេវាអេធីអឹម[:]') ?></option>
                                                    <option value="<?php _e('[:en]CDM[:kh]ម៉ាស៊ីនដក-ដាក់ប្រាក់[:]') ?>"><?php _e('[:en]CDM[:kh]ម៉ាស៊ីនដក-ដាក់ប្រាក់[:]') ?></option>
                                                    <option value="<?php _e('[:en]Fast Payment[:kh]សេវាទូទាត់រហ័ស[:]') ?>"><?php _e('[:en]Fast Payment[:kh]សេវាទូទាត់រហ័ស[:]') ?></option>
                                                    <option value="<?php _e('[:en]CSS[:kh]សេវា CSS[:]') ?>"><?php _e('[:en]CSS[:kh]សេវា CSS[:]') ?></option>
                                                    <option value="<?php _e('[:en]Payment Service via Bakong[:kh]សេវាទូទាត់តាមប្រព័ន្ធបាគង[:]') ?>"><?php _e('[:en]Payment Service via Bakong[:kh]សេវាទូទាត់តាមប្រព័ន្ធបាគង[:]') ?></option>
                                                    <option value="<?php _e('[:en]Real-Time Fund Transfer[:kh]សេវាផ្ទេរមូលនិធិភ្លាមៗ[:]') ?>"><?php _e('[:en]Real-Time Fund Transfer[:kh]សេវាផ្ទេរមូលនិធិភ្លាមៗ[:]') ?></option>
                                                    <option value="<?php _e('[:en]Other[:kh]សេវាផ្សេងៗ[:]') ?>"><?php _e('[:en]Other[:kh]សេវាផ្សេងៗ[:]') ?></option>
                                                </select>
                                            </span>
                                        </div>
                                        <div class="col-md-6">
                                            <span class="selectFeedback">
                                                <select name="selectFeedback"class="form-control" id="feedback">
                                                    <option value="<?php _e('[:en]Inquiry[:kh]សួរព័ត៌មាន[:]') ?>">
                                                        <?php _e('[:en]Inquiry[:kh]សួរព័ត៌មាន[:]') ?></option>
                                                    <option value="<?php _e('[:en]Suggestion[:kh]សំណើ[:]') ?>">
                                                        <?php _e('[:en]Suggestion[:kh]សំណើ[:]') ?></option>
                                                    <option value="<?php _e('[:en]Feedback[:kh]ផ្តល់យោបល់[:]') ?>">
                                                        <?php _e('[:en]Feedback[:kh]ផ្តល់យោបល់ [:]') ?></option>
                                                    <option value="<?php _e('[:en]Complaint[:kh]ការសរសើរ[:]') ?>">
                                                        <?php _e('[:en]Complaint[:kh]បណ្តឹង[:]') ?></option>
                                                </select>
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <!-- File upload -->
                                <div class="form-group">
                                    <label for="inputEmail3"
                                        class="control-label"><?php _e('[:en]Files Upload[:kh]ឯកសារភ្ជាប់ជាមួយ[:]') ?></label>
                                    <span class="wpcf7-form-control-wrap fileUpload">
                                        <input type="file" name="file_attach" size="40" class="file_upload" id="file_upload" accept=".pdf,.doc,.png,.jpg,.jpeg" />
                                    </span>
                                    <p class="help-block"><?php _e('[:en]File Type[:kh]ប្រភេទឯកសារ[:]') ?>: pdf, doc,
                                        docx, jpg, png, jpeg <br><?php _e('[:en]Max Size[:kh]ទំហំអតិបរមា​​[:]') ?>: 1.0
                                        MB</p>
                                </div>
                            </div>

                            <div class="col-sm-7 col-md-7">
                                <!-- Meesage -->
                                <div class="form-group">
                                    <span class="wpcf7-form-control-wrap txtMessage">
                                        <textarea name="txtmessage" cols="100" rows="6" class="form-control message" id="message" placeholder="<?php _e('[:en]Your Message...[:kh]សាររបស់អ្នក...[:]') ?>"></textarea>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-7 col-md-7">
                                <div class="form-group">
                                    <label class="form-control-placeholder control-label col-xs-12 col-sm-12 col-md-4 col-lg-4" id="captchaCalculation"></label>
                                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                                        <input type="text" class="form-control" name="captcha" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4">
                                <div class="form-group text-center">
                                    <input type="hidden" value="FeedbackMessege" name="action">
                                    <button type="submit" name="btn-submit" class="btn btn-md btn-success raised" id="btnsubmit"><?php _e('[:en]Send[:kh]បញ្ចូន[:]') ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-12 form-submit-error">
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-warning"></i> <strong> <?php _e('[:en]Sorry! your feedback is not submitted![:kh]សូមអភ័យទោស មតិរបស់លោកអ្នកមិន​ត្រូវបាន​ផ្ញើរ​​នោះទេ![:]') ?></strong>
                            <hr class="message-inner-separator">
                            <p class="text-center"><?php _e('[:en]Sorry to inform you that your feedback could not be submitted, any concerns please contact to our administrator[:kh]សូមអភ័តទោស មតិរបស់លោកអ្នក​មិន​ត្រូវ​បាន​បញ្ជូន​នោះទេ រាល់​ព័ត៌មាន​លម្អិត សូមទាក់ទងទៅកាន់​រមន្ត្រីរដ្ឋបាលរបស់​យើង​ខ្ញុំ[:]') ?></p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-12 form-submit-success">
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <i class="fa fa-check-circle"></i> <strong> <?php _e('[:en]Thanks You! your feedback has been submitted successfully![:kh]សូ​មអរគុណ! មតិរបស់លោកអ្នក​ត្រូវបានផ្ញើរ​ទៅកាន់​ប្រាសាក់​​យ៉ាងជោគជ័យ![:]') ?></strong>
                            <hr class="message-inner-separator">
                            <p class="text-center"> <?php _e('[:en]Thanks for your feeedback, our team will check and back you very soon[:kh]សូមអរគុណយ៉ាង​ជ្រាលជ្រៅ​ចំពោះ​មតិ​របស់​លោក​អ្នក​ ពួកយើងនឹង​ពិនិត្យមើល និង​ឆ្លើយតបទៅលោកអ្នកវិញក្នុងពេលឆាប់ៗនេះ[:]') ?></p>
                        </div>
                    </div>
                </div>
            </section>
            <aside class="col-xs-12 col-sm-3 col-md-3 visibility-xs">
                <div class="sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </aside>
        </main>
    </div>
</div>
 <!-- window.location.href = "/prasac"; -->
<div id="loader"></div>
<?php get_footer(); ?>

<?php
    $phpmailer_path =  get_template_directory().'/assets/phpmailer/PHPMailerAutoload.php';
    require_once($phpmailer_path);
    global $error;
    $mail = new PHPMailer();
    $mail->CharSet = "UTF-8";
    // $mail->IsSMTP();
    $mail->SMTPDebug =0;
    $mail->SMTPOptions = array(
        'ssl' => array(
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true
        )
        );
    $mail->Port = 25;
    $mail->Host = "mail.prasac.com.kh";
    $mail->Username = "kimhim.hom@prasac.com.kh";
    $mail->Password = '$PassWord@159753#';
    $mail->IsHTML(true);
    $mail_message_head ="<html>
                            <head></head>
                            <body style='marging:0;background-color:#F6F6F6;'>
                                <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#F6F6F6'>
                                    <tbody>
                                        <tr>
                                            <td align='center' valign='top'>
                                                <table href='https://fonts.googleapis.com/css?family=Content|Roboto' cellpadding='20' cellspacing='0' align='center' style='width:800px;heigh:auto;background-color:#ffffff'>
                                                    <tr>
                                                        <td colspan='2' align='left'>
                                                            <a href='www.prasac.com.kh'>
                                                                <img src='https://www.prasac.com.kh/wp-content/uploads/2017/12/Logo_50_Increased-03.png' style='width:100px;'>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr><td colspan='2' align='left'>";
                            $mail_message_footer = "</td></tr><tr>
                                                        <td colspan='2' align='center' style='padding:0;'>
                                                            <a href='#'><img src='https://www.prasac.com.kh/wp-content/uploads/2018/11/curse.png'></a>
                                                        </td>
                                                    </tr>
                                                    <tr style='background-color:#F6F6F6'>
                                                        <td style='padding:0;' colspan='2' align='center'>
                                                            <table cellpadding='0' cellspacing='0' align='center'>
                                                                <tr style='padding:10px 0 20px 0'>
                                                                    <td align='center' valign='middle' style='color:#707070;font-size:10px;line-height:10px;'>
                                                                        <a style='color:#F6F6F6;' href='https://www.facebook.com/prasacmfi/' rel='noopener' target='_blank'>
                                                                            <img alt='facebook' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/facebook.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;line-height:10px;display:inline-block;vertical-align:top'>
                                                                        </a>&nbsp;&nbsp;
                                                                        <a style='color:#F6F6F6;' href='https://www.youtube.com/user/PrasacMFI/featured' rel='noopener' target='_blank'>
                                                                            <img alt='youtube' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/youtube.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;display:inline-block;vertical-align:bottom'>
                                                                        </a>&nbsp;&nbsp;
                                                                        <a style='color:#F6F6F6;' href='https://twitter.com/prasaccambodia' rel='noopener' target='_blank'>
                                                                            <img alt='twitter' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/Twitter.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;display:inline-block;vertical-align:bottom'>
                                                                        </a>&nbsp;&nbsp;
                                                                        <a style='color:#F6F6F6;' href='https://www.linkedin.com/company/prasac/' rel='noopener' target='_blank'>
                                                                            <img alt='linkedin' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/linkedin.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;display:inline-block;vertical-align:bottom'>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </body>
                        </html>";

    if(isset($_POST['btn-submit'])){
        echo '<script>jQuery(document).ready(function (){';
        $attachment = $_FILES['file_attach']['tmp_name'];
        $attachment_file = $_FILES['file_attach']['name'];
        $upload_dir   = wp_upload_dir();
        $path = $upload_dir['basedir'].'/feedback/'; // Upload directory
        $files = glob($upload_dir['basedir'].'/feedback/*'); //get all file names
        $attach_file_path = $path.$attachment_file;
        if($attachment_file){
            foreach($files as $file){
                unlink($file); //delete file
            }
            move_uploaded_file($attachment, $path.$attachment_file);
        }

        
        if(!empty($_FILES['file_attach']['name'])){
            $mail->AddAttachment($upload_dir['basedir'].'/feedback/'.$attachment_file);
        }
        $font_family    =   '[:en]Roboto[:kh]Content';
        $font_family    =   __($font_family);
        $txtmessege     =   $_POST['txtmessage'];
        $username       =   $_POST['full_name'];
        $phone_umber    =   $_POST['phone_number'];
        $email          =   $_POST['email'];
        $feedback_about =   $_POST['SelectType'];
        $feedback_subject   =    $feedback_about.' | '.$_POST['selectFeedback'];

        // =======Send email====
        $mail->FromName = $username;
        $mail->Subject = "Client Feedback";
        $mail->setFrom('info@prasac.com.kh',$username);
        $mail->AddReplyTo( $email,$username);
        $mail->addBCC('kimhim.hom@prasac.com.kh','KIMIHIM HOM');
        $mail->AddAddress('info@prasac.com.kh','Client Feedback');


        $message = sprintf("
        <table cellpadding='20' cellspacing='0' align='center' font-family:'Helvetica,Arial,sans-serif' style='background-color:#ffffff;width:770px;padding:0 15px;'>
            <tr>
                <td height='5px' colspan='2' align='left'>
                    <p>Dear PRASAC,</p>
                    <p>$txtmessege</p>
                </td>
            </tr>
            <tr>
                <td height='5px' colspan='2' align='left'>
                    <h3 style='font-weight:bold;color:#4DB848;border-bottom:1px dashed #292f33;padding:5px 0;'>Client Info</h3>
                </td>
            </tr>
            <tr> 
                <td style='padding:0 0 10px 20px;' width='70' align='left'>
                    <span style='font-size:12px;color:#292f33;'>Username</span>
                </td>
                <td style='padding:0 20px 10px 0;border-collapse:collapse;color: #3c4043;font-family:$font_family,arial;font-size:14px;font-weight:normal;line-height:24px;' width='171' align='left'>
                    <span style='font-size:12px;font-weight:bold;color:#292f33;'>:&nbsp;&nbsp;$username</span>
                </td>
            </tr>
            <tr> 
                <td style='padding:0 0 10px 20px;' width='70' align='left'>
                    <span style='font-size:12px;color:#292f33;'>Phone Number</span>
                </td>
                <td style='padding:0 20px 10px 0;border-collapse:collapse;color: #3c4043;font-family:$font_family,arial;font-size:14px;font-weight:normal;line-height:24px;' width='171' align='left'>
                    <span style='font-size:12px;font-weight:bold;color:#292f33;'>:&nbsp;&nbsp;$phone_umber</span>
                </td>
            </tr>
            <tr> 
                <td style='padding:0 0 10px 20px;' width='70' align='left'>
                    <span style='font-size:12px;color:#292f33;'>E-mail</span>
                </td>
                <td style='padding:0 20px 10px 0;border-collapse:collapse;color: #3c4043;font-family:$font_family,arial;font-size:14px;font-weight:normal;line-height:24px;' width='171' align='left'>
                    <span style='font-size:12px;font-weight:bold;color:#292f33;'>:&nbsp;&nbsp;$email</span>
                </td>
            </tr>
        </table>");
        $messages = $mail_message_head;
        $messages .= $message;
        $messages .= $mail_message_footer;
        $mail->Body    = $messages;
        if(!$mail->Send())
        {
            echo 'jQuery(".form-submit-error").fadeIn(400);';
        }else{
            echo 'jQuery(".form-submit-success").fadeIn(400);';
        }
        echo '});</script>';
    }
?>
<script>
jQuery(document).on('load',function(){
    var spinner = jQuery("#loader");
    jQuery("form#feedback_form").on('submit',function(e){
        spinner.show();
    });
});

function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
};

jQuery('#captchaCalculation').html([randomNumber(90, 10), '+', randomNumber(90,10), '='].join(' '));

jQuery('form#feedback_form').bootstrapValidator({
    fields: {
        full_name: {
            validators: {
                notEmpty: {
                    message: '<?php _e("[:en]Your fullname is required.[:kh]ឈ្មោះលោកអ្នក​ត្រូវតែ​បំពេញ[:]")?>'
                }
            }
        },
        email: {
            validators: {
                notEmpty: {
                    message: '<?php _e("[:en]The value is not a valid email address.[:kh]សូមពិនិត្យមើលអ៊ីម៉ែលរបស់អ្នកម្តងទៀត[:]")?>'
                },
                emailAddress: {
                    message: 'The value is not a valid email address'
                }
            }
        },
        phone_number: {
            validators: {
                notEmpty: {
                    message: '<?php _e("[:en]Your fullname is required.[:kh]ឈ្មោះលោកអ្នក​ត្រូវតែ​បំពេញ[:]")?>'
                }
            }
        },
        SelectType: {
            validators: {
                notEmpty: {
                    message: '<?php _e("[:en]Please select one.[:kh]សូមធ្វើការជ្រើសរើស[:]")?>'
                }
            }
        },
        selectFeedback: {
            validators: {
                notEmpty: {
                    message: '<?php _e("[:en]Please select one.[:kh]សូមធ្វើការជ្រើសរើស[:]")?>'
                }
            }
        },
        file_attach: {
            id: 'txtFile',
            type: 'file',
            validators: {
                'file': {
                    extension: 'jpeg,png,pdf,jpg',
                    type: 'image/jpeg,image/png,image/jpg,application/pdf',
                    maxSize: 1024 * 1024,
                    message: 'The selected file is not valid'
                }
            }
        },
        txtmessage: {
            validators: {
                notEmpty: {
                    message: '<?php _e("[:en]Please input your messege.[:kh]សូមបំពេញ​មតិរបស់លោកអ្នក[:]")?>'
                }
            }
        },
        captcha: {
            validators: {
                callback: {
                    message: '<?php _e("[:en]Incorrect Number[:kh]ចម្លើយ​មិនត្រឹមត្រូវ[:]")?>',
                    callback: function(value, validator) {
                        var items = jQuery('#captchaCalculation').html().split(' '), sum = parseInt(items[0]) + parseInt(items[2]);
                        return value == sum;
                    }
                }
            }
        }
    },
});
</script>
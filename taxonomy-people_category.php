<?php get_header(); ?>
<style>
.slick-next, .slick-prev{
    /* top:50%; */
    z-index:2;
    background:#4db848;
    margin:-1px;
}
.slick-next:focus, .slick-next:hover, .slick-prev:focus, .slick-prev:hover{
    background:#f0ad4e;
}
.slick-prev:before,.slick-next:before{
    color:#ffffff;
}
.slick-next{
    right:0;
}
.slick-next:before{
    font-family:FontAwesome;
    content:'\f054';
    font-size:1rem;
}
.slick-prev{
    left:27.4rem;
}
.slick-prev:before{
    font-family:FontAwesome;
    content:'\f053';
    font-size:1rem;
}
.pw-widget.__pw-padding-true.__pw-layout-horizontal .pw-button, .pw-widget.__pw-padding-true.__pw-layout-horizontal .pw-native{
    padding-bottom:0;
}
.pw-button.__pw-size-32 .pw-button-icon,.pw-button.__pw-size-32 .pw-button__outer{
    width: 26px;
    min-height: 26px;
    line-height: 26px;
}
.pw-button-icon.__pw-size-32{
    font-size:20px;
}
.single-footer-share{
    margin:0;
}
.pw-button.__pw-size-32 .pw-button__outer{
    line-height:26px;
}
section.peoples>.row>.award-recognition:nth-child(3){
    float:right;
}
</style>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <aside class="hidden-xs col-xs-12 col-sm-3 col-md-3"><?= get_sidebar(); ?></aside>
            <section class="col-sm-9 col-md-9 peoples page-title">
                <?php
                    if(!is_tax('people_category','profile')){
                        echo '<div class="hidden-xs hidden-sm">';
                            if (function_exists('my_breadcrumbs')) my_breadcrumbs();
                        echo '</div>';
                    }
                ?>
                <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                    <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                    <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                </ul><br /><br />
                <div class="row">
                    <?php
                    if(is_tax('people_category','profile')){
                        $mypost =  get_page_by_path( 'about/profile/' );
                        echo _e($mypost->post_content);
                    }

                    if(is_tax('people_category','shareholders')){
                        echo category_description();
                    }
                if (!is_tax('people_category','awards-recognition') && !is_tax('people_category','funding-partner')):
                    while ( have_posts()) : the_post();
                        get_template_part('template-parts/content', 'people');
                    endwhile;
                ?>
                </div>
            <br />
            <?php
            elseif(is_tax('people_category','funding-partner')):
                wp_reset_postdata();
                $query = array(
                    'post_type' => 'page',
                    'post_status' => 'publish',
                    'pagename'    => 'about/funding-partner',
                    'orderby' => 'post_date',
                    'order'=> 'ASC',
                ); 
                
                $result = new WP_Query($query);
                if( $result->have_posts() ):
                    echo category_description();
                    while( $result->have_posts() ) : $result->the_post();
                        echo the_content();
                    endwhile;
                endif;

                /* 
                * Paginate Advanced Custom Field repeater
                */
                if( get_query_var('page') ) {
                    $page = get_query_var( 'page' );
                } else {
                    $page = 1;
                }
                
                // Variables
                $row              = 0;
                $conents_count    = get_field('award');
                $images_per_page  = count( $conents_count );//36; // How many images to display on each page
                $total            = count( $conents_count );
                $pages            = ceil( $total / $images_per_page );
                $min              = ( ( $page * $images_per_page ) - $images_per_page ) + 1;
                $max              = ( $min + $images_per_page ) - 1;
                //ACF Loop
                if( have_rows( 'award' ) ) :
                    echo '<div class="row">';
                        while( have_rows('award') ) : the_row();
                            $row++;
                            // Ignore this image if $row is lower than $min
                            if($row < $min) { continue; }
                            // Stop loop completely if $row is higher than $max
                            if($row > $max) { break; }
                            echo '<div class="col-xs-12 col-sm-4 col-md-4 text-center our-lender-wrapper">
                                <div class="img-thumbnail our-lender-content">
                                    <a href="'.get_sub_field('awards_thumbnail').'" title="'.get_sub_field('award_title').'"  class="lightbox">
                                        <img src="'.get_sub_field('awards_thumbnail').'" class="img-responsive lazy"/>
                                    </a>
                                    <h5>
                                        <a href="'.get_sub_field('award_link').'" title="'.get_sub_field('award_title').'">'.get_sub_field('award_title').'</a>
                                    </h5>
                                </div>
                            </div>';
                        endwhile;
                        wp_reset_postdata();
                            echo '<div class="col-xs-12 col-md-12 video-pagination text-center">';
                            echo paginate_links(array(
                                // 'base' =>get_permalink().'/%#%' . '/',
                                'format' => '?page=%#%',
                                'current' => $page,
                                'total' => $pages
                            ));
                        echo '</div></div>';
                else:
                    echo 'No data found!';
                endif;
                ?>
                <style>
                    .video-pagination .page-numbers:not(.next):not(.prev){
                        background-color:#4db848;
                        color:#ffffff;
                        padding: 7px 12px;
                        border-radius: 50%;
                    }
                    div.video-pagination span.page-numbers.current{
                        border:#4db848 1px solid !important;
                        background-color:transparent !important;
                        color:#4db848 !important;
                    }
                    div.video-pagination a.next,div.video-pagination a.prev{
                        padding: 7px 12px;
                        background-color:#4db848;
                        color:#ffffff;
                    }
                    div.video-pagination{
                        margin:2rem;
                    }
                </style>
                <?php
            elseif(is_tax('people_category','awards-recognition')):
                    wp_reset_postdata();
                    $query = array(
                        'post_type' => 'page',
                        'post_status' => 'publish',
                        'pagename'    => 'about/awards-recognition',
                        'orderby' => 'post_date',
                        'order'=> 'DESC',
                    ); 
                    
                    $result = new WP_Query($query);
                    if( $result->have_posts() ):
                        while( $result->have_posts() ) : $result->the_post();
                            echo the_content();
                        endwhile;
                    endif;

                    /* 
                    * Paginate Advanced Custom Field repeater
                    */
                    if( get_query_var('page') ) {
                        $page = get_query_var( 'page' );
                    } else {
                        $page = 1;
                    }
                    
                    // Variables
                    $row              = 0;
                    $images_per_page  = 24; // How many images to display on each page
                    $conents_count    = get_field('award');
                    $total            = count( $conents_count );
                    $pages            = ceil( $total / $images_per_page );
                    $min              = ( ( $page * $images_per_page ) - $images_per_page ) + 1;
                    $max              = ( $min + $images_per_page ) - 1;
                     //ACF Loop
                     if( have_rows( 'award' ) ) :
                        echo '<div class="row">';
                            while( have_rows('award') ) : the_row();
                                $row++;
                                // Ignore this image if $row is lower than $min
                                if($row < $min) { continue; }
                                // Stop loop completely if $row is higher than $max
                                if($row > $max) { break; }
                                echo '<a class="col-md-4 lightbox" href="'.get_sub_field('awards_thumbnail').'" style="margin:15px 0;" title="'.get_sub_field('award_title').'" alt="'.get_sub_field('award_title').'">
                                    <img src="'.get_sub_field('awards_thumbnail').'" class="img-responsive img-thumbnail post-thumbnail lazy" title="'.get_sub_field('award_title').'" />
                                </a>';
                            endwhile;
                            wp_reset_postdata();
                            echo '<div class="col-xs-12 col-md-12 video-pagination text-center">';
                            echo paginate_links(array(
                                // 'base' =>get_permalink().'/%#%' . '/',
                                'format' => '?page=%#%',
                                'current' => $page,
                                'total' => $pages
                            ));
                        echo '</div></div>';
                    else:
                        echo 'No data found!';
                    endif;
                ?>
                 <style>
                    .video-pagination .page-numbers:not(.next):not(.prev){
                        background-color:#4db848;
                        color:#ffffff;
                        padding: 7px 12px;
                        border-radius: 50%;
                    }
                    div.video-pagination span.page-numbers.current{
                        border:#4db848 1px solid !important;
                        background-color:transparent !important;
                        color:#4db848 !important;
                    }
                    div.video-pagination a.next,div.video-pagination a.prev{
                        padding: 7px 12px;
                        background-color:#4db848;
                        color:#ffffff;
                    }
                    div.video-pagination{
                        margin:2rem;
                    }
                </style>
            <?php else : ?>
                <?php get_template_part('template-parts/content', 'none'); ?>
            <?php endif; ?>
            </section>
        </main>
    </div>
</div>
<?php get_footer(); ?>
<script>
    jQuery(document).ready(function(){
        jQuery("#province_filter").on("change", function() {
            var value = jQuery(this).val().toLowerCase();
            jQuery(".csr-wrapper").filter(function(e) {
                jQuery('.filterTable_no_results').remove();
                jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1);
            });

            if(jQuery('.csr-wrapper:visible').length == 0){
                var no_results = jQuery('<div class="clearfix"></div><div class="filterTable_no_results"><div class="alert alert-danger" role="alert" style="padding:5px;"><i class="fa fa-exclamation-circle" aria-hidden="true"></i> <?php _e('[:en]No library founded![:kh]ពុំ​មាន​បណ្ណាល័យដែល​លោក​ស្វែងរក​នោះទេ[:]') ?></div></div>');
                jQuery(no_results).insertAfter('.province_filter');
            }else{
                jQuery('.filterTable_no_results').remove();
            }
        });
    });
</script>

<script>
    jQuery(document).ready(function() {

        jQuery(".nav-filter li a").on('click',function(){
            var term = jQuery(this).data("term");
            jQuery(".social-awards").show();
            // alert(term);
            if(term != 'all-terms'){
                jQuery(".social-awards").not("."+term).hide();
            }else{
                jQuery(".social-awards").show();
            }
            
        });
        jQuery('section.slick-slide').magnificPopup({
    		delegate: 'a',
            type: 'iframe',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            callbacks: {
                elementParse: function(item) {
                    console.log(item.el[0].className);
                    if(item.el[0].className == 'video') {
                        item.type = 'iframe',
                        item.iframe = {
                            patterns: {
                                youtube: {
                                    index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).
                                    id: 'v=',
                                    src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
                                },
                                vimeo: {
                                    index: 'vimeo.com/',
                                    id: '/',
                                    src: '//player.vimeo.com/video/%id%?autoplay=1'
                                },
                                gmaps: {
                                    index: '//maps.google.',
                                    src: '%id%&output=embed'
                                }
                            }
                        }
                    } else {
                        item.type = 'image',
                        item.tLoading = 'Loading image #%curr%...',
                        item.mainClass = 'mfp-img-mobile',
                        item.image = {
                            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                        }
                    }
                }
            }
        });


         /* Hero slider script config */
         jQuery('.library-item').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            cssEase: 'linear',
            fade: false,
            dots: false,
            arrows:true,
            slidesToShow: 1,
            slidesToScroll: 1,
            nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"></button>',
            prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;"></button>'
        });


    });
</script>

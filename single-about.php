<?php get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <aside class="hidden-xs col-xs-12 col-sm-3 col-md-3">
                <?= get_sidebar(); ?>
            </aside>
            <section class="col-xs-12 col-sm-9 col-md-9">
                <div class="hidden-xs hidden-sm">
                    <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                </div>
                <?php
                if( have_posts() ):
                    while( have_posts() ): the_post();
                        get_template_part('template-parts/content');
                    endwhile;
                else :
                        get_template_part('template-parts/content', 'none');
                endif;
                ?>
            </section>
            <aside class="visible-xs col-xs-12 col-sm-3 col-md-3" id="category_sidebar_bottom"></aside>
        </main>
    </div>
</div>
<?php get_footer(); ?>
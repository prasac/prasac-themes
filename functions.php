<?php

error_reporting(0);
error_reporting(E_ALL);
/****************************************
* Theme Setup
****************************************/
define('NCTM_DEBUG', false);
    function mail_message_head(){
        return  "<html>
                    <header></header>
                    <body style='marging:0;background-color:#F6F6F6;'>
                        <table cellpadding='0' cellspacing='0' style='width:100%;'>
                            <tbody>
                                <tr>
                                    <td  colspan='2'>
                                        <table href='https://fonts.googleapis.com/css?family=Hanuman|Roboto' cellpadding='20' cellspacing='0' align='center' style='width:800px;heigh:auto;background-color:#ffffff;'>
                                            <tbody>
                                                <tr>
                                                    <td align='left' valign='top' style='padding:40px 20px 20px 20px'>
                                                        <a href='#'><img src='https://www.prasac.com.kh/wp-content/uploads/2017/12/Logo_50_Increased-03.png' style='background-repeat:no-repeat;background-size:70px 70px; width:100px;'></a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                <td align='left' valign='top' style='padding:15px'>";
    }     

    function mail_message_footer(){            
                        return   "</td>
                                </tr>
                                <tr>
                                    <td colspan='2' align='center' style='padding:0;'>
                                        <a href='#'><img src='https://www.prasac.com.kh/wp-content/uploads/2018/11/curse.png' style='background-repeat:no-repeat;background-size:600px 92px; width:800px;padding:0;margin-bottom:-10px;'></a>
                                    </td>
                                </tr>
                                <tr style='background-color:#F6F6F6'>
                                    <td style='padding:0;' colspan='2' align='center'>
                                        <table cellpadding='0' cellspacing='0' align='center'>
                                            <tr style='padding:10px 0 20px 0'>
                                                <td align='center' valign='middle' style='color:#707070;font-size:10px;line-height:10px;'>
                                                    <a style='color:#F6F6F6;' href='https://www.facebook.com/prasacmfi/' rel='noopener' target='_blank'>
                                                        <img alt='facebook' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/facebook.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;line-height:10px;display:inline-block;vertical-align:top'>
                                                    </a>&nbsp;&nbsp;
                                                    <a style='color:#F6F6F6;' href='https://www.youtube.com/user/PrasacMFI/featured' rel='noopener' target='_blank'>
                                                        <img alt='youtube' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/youtube.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;display:inline-block;vertical-align:bottom'>
                                                    </a>&nbsp;&nbsp;
                                                    <a style='color:#F6F6F6;' href='https://twitter.com/prasaccambodia' rel='noopener' target='_blank'>
                                                        <img alt='twitter' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/Twitter.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;display:inline-block;vertical-align:bottom'>
                                                    </a>&nbsp;&nbsp;
                                                    <a style='color:#F6F6F6;' href='https://www.linkedin.com/company/prasac/' rel='noopener' target='_blank'>
                                                        <img alt='linkedin' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/linkedin.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;display:inline-block;vertical-align:bottom'>
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </body>
    </html>";
}

function my_login_logo() { ?>
  <style type="text/css">
      #login h1 a, .login h1 a {
        background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/login-logo.png);
        height:100px;
        width:320px;
        background-size: 320px 100px;
        background-repeat: no-repeat;
        padding-bottom: 30px;
      }
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function CheckuserRole(){
  var_dump(is_user_logged_in());
  var_dump(wp_get_current_user()->roles[0]);
}


            
function add_meta_tags() {
    global $post;
    $meta_desc = wp_strip_all_tags(trim(get_field('meta_description','option')));
    $meta_keyword = wp_strip_all_tags(trim(get_field('meta_keywords','option')));
    if (is_front_page()) {
        echo '<meta name="description" content="'.$meta_desc.'" />'. "\n";
        echo '<meta name="keywords" content="'.$meta_keyword.'" />'. "\n";
        echo '<meta property="fb:app_id" content="454287855022882" />';
        echo '<meta property="og:type" content="article">' . "\n";
        echo '<meta property="og:description" content="'.$meta_desc.'" />' . "\n";
        echo '<meta property="og:title" content="'.get_bloginfo('name').'"/>' . "\n";
        echo '<meta property="og:image" content="'.get_template_directory_uri().'/assets/images/logo_share.png"/>' . "\n";
    }
    if (!is_front_page() && $post) {
        $meta = strip_tags( $post->post_content );
        $meta = strip_shortcodes( $post->post_content );
        $meta = str_replace( array("\n", "\r", "\t"), ' ', $meta );
        $meta = wp_strip_all_tags(trim($meta));
        $posttags = get_the_tags();
        $tagName = '';
        if ($posttags){
            foreach($posttags as $tag) 
            { 
                $tagName .= $tag->name.',';
            }
        }
        $meta_keyword = rtrim($tagName,',');
        $meta_keyword_final = $meta_keyword?$meta_keyword:get_post()->post_title;
        echo '<meta name="description" content="' . wp_trim_words($meta,100,'...'). '" />' . "\n";
        echo '<meta name="keywords" content="' .$meta_keyword_final. '" />' . "\n";

      

        $f_title = get_post()?get_post()->post_title:'PRASAC MFI Title';
        $f_image = get_post()?get_post()->post_image:'PRASAC MFI Image';
        $f_description= get_post()?$meta:'PRASAC MFI Description';
        $f_url = get_the_permalink();
        //=============Facebook meta tag========
        echo '<meta property="og:site_name" content="PRASAC Microfinance Institution">' . "\n";
        echo '<meta property="og:type" content="article">' . "\n";
        echo '<meta property="fb:app_id" content="454287855022882" />';
        echo '<meta property="og:title"  name="title" content="'.$f_title.'"/>' . "\n"; 
        echo '<meta property="og:description" content="'.wp_trim_words($f_description,50,'...').'" />' . "\n";
       
        // exit;
        
        //============Twitter meta tag========
        echo '<meta name="twitter:card" content="summary_large_image">' . "\n";
        echo '<meta name="twitter:site" content="@prasaccambodia">' . "\n";
        echo '<meta name="twitter:title" content="'.$f_title.'">' . "\n";
        echo '<meta name="twitter:description" content="'.wp_trim_words($f_description,50,'...').'">' . "\n";
        //============Twitter meta tag========
        $sharing_image = get_field('social_sharing_image')?get_field('social_sharing_image'):'';
        $bakong_sharing_image = get_field('image_sharing_thumbnail')?get_field('image_sharing_thumbnail'):'';
        
        global $wpdb;
        $post_tag = isset($_GET['tag'])?$_GET['tag']:'prasac-scan-pay';
        $term = get_term_by('slug',$post_tag, 'merchant_tag');
        // var_dump((is_page_template('page-merchant.php') || is_singular('merchant')) && $term->slug == 'bakong-scan-pay');
        if((is_page_template('page-merchant.php') || is_singular('merchant')) && $term->slug == 'bakong-scan-pay'){
          if ($bakong_sharing_image):
              echo '<meta property="og:image" content="'.$bakong_sharing_image.'"/>' . "\n";
              echo '<meta name="twitter:image" content="'.$bakong_sharing_image.'">' . "\n";
              echo '<meta property="og:url" content="'.get_the_permalink().'?tag='.$term->slug .'" />' . "\n";
          else:
              echo '<meta property="og:image" content="'.get_field('image_sharing','option').'"/>' . "\n";
              echo '<meta name="twitter:image" content="'.get_field('image_sharing','option').'">' . "\n";
          endif;
        }else{
          if ($sharing_image) :
              echo '<meta property="og:image" content="'.$sharing_image.'"/>' . "\n";
              echo '<meta name="twitter:image" content="'.$sharing_image.'">' . "\n";
              echo '<meta property="og:url" content="'.$f_url.'" />' . "\n";
          else:
              echo '<meta property="og:image" content="'.get_field('image_sharing','option').'"/>' . "\n";
              echo '<meta name="twitter:image" content="'.get_field('image_sharing','option').'">' . "\n";
          endif;
        }
   }
}

add_action( 'wp_head', 'add_meta_tags' , 2 );

if ( !function_exists( 'theme_setup' ) ) :
  add_action( 'init', 'my_add_excerpts_to_pages' );
  function my_add_excerpts_to_pages() {
    add_post_type_support( 'page', 'excerpt','people');
  }
  add_action( 'after_setup_theme', 'theme_setup' );
  function theme_setup() {
    show_admin_bar( false );
    remove_action( 'wp_head', 'feed_links_extra', 3 ); // Display the links to the extra feeds such as category feeds
    remove_action( 'wp_head', 'feed_links', 2 ); // Display the links to the general feeds: Post and Comment Feed
    remove_action( 'wp_head', 'rsd_link' ); // Display the link to the Really Simple Discovery service endpoint, EditURI link
    remove_action( 'wp_head', 'wlwmanifest_link' ); // Display the link to the Windows Live Writer manifest file.
    remove_action( 'wp_head', 'index_rel_link' ); // index link
    remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // prev link
    remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
    remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 ); // Display relational links for the posts adjacent to the current post.
    remove_action( 'wp_head', 'wp_generator' ); // Display the XHTML generator that is generated on the wp_head hook, WP version
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    // remove_filter ('acf_the_content', 'wpautop');
    // remove_filter( 'the_content', 'wpautop' );
    // remove_filter( 'the_excerpt', 'wpautop' );
    load_theme_textdomain( 'Prasac Theme', get_template_directory() . '/languages' );
    add_theme_support( 'post-thumbnails' );
    register_nav_menus( array(
        'primary' => esc_html__( 'Primary', 'Prasac Theme' ),
        'secondary' => esc_html__( 'Secondary', 'Prasac Theme' ),
        'footer' => esc_html__( 'Footer', 'Prasac Theme' )
    ) );
  }
endif;
        
// add tag support to pages
function tags_support_all() {
    register_taxonomy_for_object_type('post_tag','page');
    register_taxonomy_for_object_type('post_tag','career');
    register_taxonomy_for_object_type('post_tag','office');
    register_taxonomy_for_object_type('post_tag','people');
    register_taxonomy_for_object_type('post_tag','document');
    register_taxonomy_for_object_type('post_tag','merchant');
    register_taxonomy_for_object_type('post_tag', 'exchange');
}
// ensure all tags are included in queries
function tags_support_query($wp_query) {
	if ($wp_query->get('tag')) $wp_query->set('post_type', 'any');
}
// tag hooks
add_action('init', 'tags_support_all');
add_action('pre_get_posts', 'tags_support_query');
add_action( 'admin_head', 'admin_head_example' );

function admin_head_example() { echo '<link rel="shortcut icon" type="image/x-icon" href="'.get_template_directory_uri().'/assets/images/favicon.ico" />'; }

/**********************************
# Include function
**********************************/
include( get_template_directory() . '/inc/bootstrap_navwalker.php' );
include( get_template_directory() . '/inc/wp_bootstrap_pagination.php' );
include( get_template_directory() . '/inc/cpt/cpt-document.php' );
include( get_template_directory() . '/inc/cf/cf-document.php' );

include( get_template_directory() . '/inc/cf/cf-career.php' );
include( get_template_directory() . '/inc/cpt/cpt-people.php' );
include( get_template_directory() . '/inc/cpt/cpt-merchant.php' );
include( get_template_directory() . '/inc/cpt/cpt-exchange.php' );
include( get_template_directory() . '/inc/cpt/cpt-office.php' );
include( get_template_directory() . '/inc/cf/cf-merchant.php' );
include( get_template_directory() . '/inc/cf/cf-library.php' );
include( get_template_directory() . '/inc/cf/cf-header.php' );


include( get_template_directory() . '/inc/cf/cf-general-setting.php' );
include( get_template_directory() . '/inc/cf/cf-office.php' );
include( get_template_directory() . '/inc/cpt/cpt-library.php' );
include( get_template_directory() . '/inc/cf/cf-feature-content.php' );
include( get_template_directory() . '/inc/cf/cf-flexible-content.php' );
include( get_template_directory() . '/inc/cf/cf-award-recognition.php' );
include( get_template_directory() . '/inc/cf/cf-gallery-slider.php' );
include( get_template_directory() . '/inc/cpt/cpt-locations.php' );
include( get_template_directory() . '/inc/cpt/cpt-years.php' );


include( get_template_directory() . '/inc/page-option.php' );
include( get_template_directory() . '/inc/breadcrumb.php' );
include( get_template_directory() . '/inc/template-tags.php' );
include( get_template_directory() . '/inc/filter-post-sc.php' );
/************************************
# Register and enque styles & scripts
************************************/
if ( !function_exists('add_theme_scripts') ):
  add_action('wp_enqueue_scripts', 'add_theme_scripts');
  function add_theme_scripts() {
    if ( !is_admin() ):
      /* Register script and style */
      wp_register_style('slick',get_template_directory_uri().'/layout/slick.css',array(), null, false);
      wp_register_style( 'slick-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css',array(), null, false);
      wp_register_style('popup-style',get_template_directory_uri().'/layout/magnific-popup.min.css', array(), '', false);
      wp_register_style('deposit-form', get_template_directory_uri().'/layout/deposit-form.min.css', array(), '', false);
      wp_register_style('jquery-ui-css', get_template_directory_uri().'/layout/jquery-ui.min.css', array(), '', false);
      wp_register_style('validator', get_template_directory_uri().'/assets/bootstrap-3.3.7/css/bootstrapValidator.min.css', array(), '', false);
      wp_register_style('popup-loader', get_template_directory_uri().'/layout/popup-loader.css', array(), '', false);

      wp_register_script( 'slick-script', get_template_directory_uri().'/js/slick.min.js', array(), null, false);
      wp_register_script( 'jquery-chained', get_template_directory_uri().'/forms/js/jquery-chained.min.js',array(), null, false);
      wp_register_script( 'jquery-ui', get_template_directory_uri().'/js/jquery-ui.min.js',array(), null, false);
      wp_register_script( 'validator',  get_template_directory_uri().'/js/bootstrapValidator.min.js',array(), null, false);
      wp_register_script( 'unveil',  get_template_directory_uri().'/js/jquery.unveil.js',array(), null, false);
      wp_register_script('popup-script', get_template_directory_uri().'/js/jquery.magnific-popup.min.js',array(), null, false);
      wp_register_script('map-script', 'https://maps.google.com/maps/api/js?key=AIzaSyDcF7zhxdfUqwRNBB6PZffyLOUvMpEPuZ8',array(), null, false);
      wp_register_script('number-script', get_template_directory_uri().'/js/jquery.number.min.js',array(), null, false);
      wp_register_script('bootstrap', get_template_directory_uri().'/assets/bootstrap-3.3.7/js/bootstrap.min.js',array(), null, false);
      wp_register_script('firemaks', get_template_directory_uri().'/js/firemaks.js',array(), null, false);
      wp_register_script('bootstrap-datepicker',get_template_directory_uri().'/js/bootstrap-datepicker.min.js',array(), null, false);

      /* Enqueue style for none admin page */
      wp_enqueue_style( 'bootstrap',get_template_directory_uri().'/assets/bootstrap-3.3.7/css/bootstrap.min.css',array(), null, false);
      wp_enqueue_style( 'style-navbar',get_template_directory_uri().'/layout/navbar.min.css',array(), null, false);
      wp_enqueue_style( 'style-include',get_template_directory_uri().'/layout/pagination.min.css',array(), null, false);
      wp_enqueue_style( 'bootstrap-datepicker-style',get_template_directory_uri().'/assets/bootstrap-3.3.7/css/bootstrap-datepicker.min.css',array(), null, false);

      wp_enqueue_style( 'style',get_template_directory_uri().'/style.min.css',array(), null, false);
      wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/assets/font-awesome/css/font-awesome.min.css',array(), null, false);
      wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/assets/font-awesome/css/font-awesome-animation.min.css',array(), null, false);

      wp_enqueue_style('style-include');
      wp_enqueue_style('style-navbar');
      wp_enqueue_script('jquery');
      wp_enqueue_style('popup-style');
      wp_enqueue_script('popup-script');
      wp_enqueue_style('jquery-ui-css');
      wp_enqueue_script('jquery-ui');
      if ( is_front_page()):
        wp_enqueue_style('popup-style');
        wp_enqueue_style('popup-loader');
        wp_enqueue_style('slick');
        wp_enqueue_style('slick-theme');
        wp_enqueue_script('popup-script');
        wp_enqueue_script('slick-script');
        wp_enqueue_script('unveil');
      elseif ( is_single() || is_singular('merchant') || is_tax()=='people'):
        wp_enqueue_style('slick');
        wp_enqueue_style('slick-theme');
        wp_enqueue_script('slick-script');
        wp_enqueue_script('map-script');
      elseif (is_page_template('page-term-deposit-calculator.php') || is_page_template('page-feedback.php') || is_page_template('page-merchant.php')):
        wp_enqueue_style('validator');
        wp_enqueue_style('jquery-ui-css');
        wp_enqueue_style('deposit-form');
        wp_enqueue_script('jquery-ui');
        wp_enqueue_script('jquery-chained');
        wp_enqueue_script('bootstrap');
        wp_enqueue_script('validator');
        wp_enqueue_style('slick');
        wp_enqueue_style('slick-theme');
        wp_enqueue_script('slick-script');
        wp_enqueue_script('map-script');
        wp_enqueue_style('font-awesome');
      elseif(is_page_template('page-online-request.php') || is_page_template('page-applynow.php')):
        wp_enqueue_style('validator');
        wp_enqueue_script('jquery-chained');
        wp_enqueue_script('bootstrap');
        wp_enqueue_script('validator');
        wp_enqueue_script('bootstrap-datepicker');
        wp_enqueue_style('bootstrap-datepicker-style');
        wp_enqueue_style('slick');
        wp_enqueue_style('slick-theme');
        wp_enqueue_script('slick-script');
        wp_enqueue_style('font-awesome');
        wp_enqueue_script('number-script');
      endif;
          wp_enqueue_script('mobile-detect');
          wp_enqueue_script('bootstrap');
          wp_enqueue_script('firemaks');
      endif;
  }
endif;


function assets() {
  wp_enqueue_script('term_ajax', get_template_directory_uri().'/js/filter-merchant-ajax.js', ['jquery'], null, false);
  wp_localize_script( 'term_ajax', 'bobz', array(
    'nonce'    => wp_create_nonce( 'bobz' ),
    'ajax_url' => admin_url('admin-ajax.php' )
  ));
}
add_action('wp_enqueue_scripts', 'assets', 100);

if (!function_exists('post_pagination')):
    function post_pagination(){
        global $wp_query;
        $pager = 999999999; // need an unlikely integer
        echo paginate_links(array('base' => str_replace($pager,'%#%',esc_url(get_pagenum_link($pager))),
            'format' => '?paged=%#%',
            'current' => max( 1, get_query_var('paged') ),
            'total' => $wp_query->max_num_pages
        ));
    }
endif;

/* Allow to upload SVG file */
function custom_mtypes($mTypes ){
  $mTypes['svg'] = 'image/svg+xml';
  $mTypes['svgz'] = 'image/svg+xml';
  return $mTypes;
}
add_filter( 'upload_mimes', 'custom_mtypes' );

/***************************************************
## Display list of child page
***************************************************/
//List Child Page Shortcode
function wpb_list_child_pages() {
  global $post;
    $parent = '';
    if ( is_page() && $post->post_parent ) {
        $childpages = wp_list_pages( 'post_status=publish&title_li=&child_of='.$post->post_parent.'&echo=0' );
        $parent =  get_the_title($post->post_parent);
    } else {
        $childpages = wp_list_pages( 'post_status=publish&title_li=&child_of='.$post->ID.'&echo=0' );
        $parent = $post->post_title;
    }

    $string = '';
    if ( $childpages ) {
      $string .= '<div class="sidebar"><div class="sidebar_title">'.$parent.' </div><ul class="sidebar_content list" id="accordion">'.$childpages.'</ul></div>';
    }
    return $string;
}
add_shortcode('wpb_childpages', 'wpb_list_child_pages');



/***************************************************
Display feature thumbnail from post and post type
$size : the size of image u want in feature post
$responsive : if you want responsive true
***************************************************/
function featured_image( $size, $responsive = true, $thumb = true, $class = '' ) {
  global $post;
  $post_tn_id = get_post_thumbnail_id( $post->ID );
  $image_responsive = 'img-responsive';//img-responsive
  $thumb_id =  get_post_thumbnail_id();
  $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
  if ( has_post_thumbnail() ) {
    $thumb_url = $thumb_url_array[0];
  } else{
    $thumb_url = 'https://dummyimage.com/350x200/f2f2f2/4aa818&text=loading';
  }
  
  if ( $responsive == false ) :
    $image_responsive = '';
  endif;

  if ( !empty($post_tn_id) && $thumb == true ) :
    $featured_image = wp_get_attachment_image_src($post_tn_id, $size);
    $result = '<img class="'.$class.' '.$image_responsive.'" src="'.$featured_image[0].'" alt="'.get_the_title().'">';
    
    if( $post->post_type == 'document' ) :
     // $result = '<img class="'.$class.' '.$image_responsive.'" src="'.$featured_image[0].'" alt="'.get_the_title().'" src="https://dummyimage.com/350x200/f2f2f2/4aa818&text=loading..." title="'.$post_title.'">';
      $result = '<img class="'.$class.' '.$image_responsive.'" data-src="'.$thumb_url.'" src="https://dummyimage.com/350x200/f2f2f2/4aa818&text=loading..." alt="'.$post_title.'">';
    endif;
  elseif( $thumb == true && $responsive == true ) :
    $thumbnail = get_template_directory_uri() . "/assets/thumbnail.jpg";
    $result = '<img class="'.$class.' '.$image_responsive.'" src="'.$thumbnail.'" alt="'.get_the_title().'">';
  
  endif;

  return $result;
}

/******************************************
## Check if any of ACF input value not empty
******************************************/
function check_acf_field($selector, $post_id = null, $format_value = false){
  $result = get_field($selector, $post_id, $format_value);
  if (empty($result)){
    return $selector . ' is empty';
  }
  return $result;
}

/*
* WPSE Excerpt Trim
*/
if ( ! function_exists( 'wpse_custom_wp_trim_excerpt' ) ) :
  function wpse_custom_wp_trim_excerpt($wpse_excerpt) {
    $raw_excerpt = $wpse_excerpt;
    if ( '' == $wpse_excerpt ) {

      $wpse_excerpt = get_the_content('');
      $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
      $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
      $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
      //$wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

      //Set the excerpt word count and only break after sentence is complete.
      $excerpt_word_count = 270;
      $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count);
      $tokens = array();
      $excerptOutput = '';
      $count = 0;

      // Divide the string into tokens; HTML tags, or words, followed by any whitespace
      preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

    foreach ($tokens[0] as $token) {

      if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) {
      // Limit reached, continue until , ; ? . or ! occur at the end
      $excerptOutput .= trim($token);
      break;
      }
      // Add words to complete sentence
      $count++;
      // Append what's left of the token
      $excerptOutput .= $token;
    }
    $read_more = '[:en]Read More About[:kh]ព័ត៌មានលម្អិតអំពី[:]';
    $read_more = __($read_more);
    $wpse_excerpt = mb_strimwidth(force_balance_tags($excerptOutput), 0,$excerpt_word_count, '...');//trim(force_balance_tags($excerptOutput));
    $excerpt_end = '<p><a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&raquo;&nbsp;' . sprintf(__($read_more.': %s &nbsp;&raquo;', 'wpse' ), get_the_title()) . '</a></p>';
    $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end);
    $wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph */
    return $wpse_excerpt;
  }
    return apply_filters('wpse_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
  }
endif;
remove_filter('get_the_excerpt', 'wp_trim_excerpt');

add_filter('get_the_excerpt', 'wpse_custom_wp_trim_excerpt');
function wpse_custom_excerpts($limit) {
    return wp_trim_words(get_the_excerpt(), $limit);
}



/************************
*Get social icon
************************/
function iconlist($icons) {
  foreach($icons as $icon=>$value) {
    if($value !='facebook is empty' || $value !='twitter is empty' || $value !='linkedin is empty' || $value !='you-tube is empty' || $value !='telegram is empty'){
      ?>
        <li><?= '<a href="'.$value.'" target="_blank">' ?><i class="fa fa-<?= $icon ?>"></i></a></li>
      <?php
    }
  }
}

function get_contacticons(){
  $icons = array(
    'facebook-square' => check_acf_field('facebook', 'option'),
    'twitter-square'=> check_acf_field('twitter', 'option'),
    'linkedin-square'=> check_acf_field('linkedin', 'option'),
    'youtube-square'=> check_acf_field('you-tube', 'option'),
    'telegram'=> check_acf_field('telegram', 'option')
  );
  return $icons;
}
/*
* Get only youTube ID from youTube URL
*/
function youtube_id_from_url($url) {
  $pattern = 
    '%^# Match any YouTube URL
    (?:https?://)?  # Optional scheme. Either http or https
    (?:www\.)?      # Optional www subdomain
    (?:             # Group host alternatives
      youtu\.be/    # Either youtu.be,
    | youtube\.com  # or youtube.com
      (?:           # Group path alternatives
        /embed/     # Either /embed/
      | /v/         # or /v/
      | /watch\?v=  # or /watch\?v=
      )             # End path alternatives.
    )               # End host alternatives.
    ([\w-]{10,12})  # Allow 10-12 for 11 char YouTube id.
    $%x';
  $result = preg_match($pattern, $url, $matches);
  if (false !== $result) {
    return $matches[1];
  }
  return false;
}


/*
* Customize archive post to display
*/
function customize_customtaxonomy_archive_display ( $query ) {
    if ( $query->is_main_query() && is_tax('document_category') ) {
        $query->set( 'post_type', 'document' );                 
        $query->set( 'posts_per_page', '8' );
    }

    if ( $query->is_main_query() && is_tax('post') ) {
        $query->set( 'post_type', 'post' );                 
        $query->set( 'posts_per_page', '10' );
    }

    if( $query->is_main_query() && is_tax('people_category') ){
        $query->set( 'post_type', 'people' );                 
        $query->set( 'posts_per_page', '50' );
    }

    if($query->is_main_query() && is_tax('people_category',array('our-lender')) ){
        $query->set('orderby', array('post_title'));
    }

    if( $query->is_main_query() && is_tax('office_categories') ){
        $query->set( 'post_type', 'office' );                 
        $query->set( 'posts_per_page', '10' );
    }
}
add_action( 'pre_get_posts', 'customize_customtaxonomy_archive_display' );

function prefix_ajax_exchange_search() {
  //  var_dump($_POST['d1']);
   wp_die();
}
add_action('wp_ajax_exchange_search', 'prefix_ajax_exchange_search');

/*
* Add categoriues filter to people post type admin dashboard
*/
function add_taxonomy_filters() {
	global $typenow;
	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
    // must set this to the post type you want the filter(s) displayed on
    $taxonomies = array();
	if( $typenow == 'people' ){
		$taxonomies = array('people_category');
    }elseif($typenow == 'office'){
        $taxonomies = array('office_categories'); 
    }
    foreach ($taxonomies as $tax_slug) {
        $tax_obj = get_taxonomy($tax_slug);
        $tax_name = $tax_obj->labels->name;
        $terms = get_terms($tax_slug);
        if(count($terms) > 0) {
            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
            echo "<option value=''>Show All $tax_name</option>";
            foreach ($terms as $term) { 
                echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>';
                _e($term->name);
                echo ' ('.$term->count .')';
                echo '</option>'; 
            }
            echo "</select>";
        }
    }
}
add_action( 'restrict_manage_posts', 'add_taxonomy_filters' );

/**
 * Function Name: addApplicant
 * Author: KIMHIM HOM
 * Description: To store applicantion which users apply for job announcement
 */
function addApplicant(){
  global $wpdb;
  date_default_timezone_set('Asia/Phnom_Penh');
  $date = new DateTime();
 
  if( $_POST['position'] != '' && $_POST['location'] != '' && $_POST['full_name_en'] !='' && $_POST['salary'] != ''){
    $current_date = $date->format("Y-m-d");
    $current_datetime = $date->format("Y-m-d H:i:s");
    $name_en = $_POST['full_name_en'];
    $name_kh = $_POST['full_name_kh'];
    $gender = $_POST['gender'];
    $dob = $date->format(date('Y-m-d',strtotime($_POST['dob'])));
    $diff = abs(strtotime($current_date) - strtotime($dob));
    $age = floor($diff / (365*60*60*24));
    $phone = $_POST['tel'];
    $email = $_POST['email'];
    $position = $_POST['position'];

    $address = 'Nº '.$_POST['permanent_address'].', Str.'. $_POST['permanent_street'].', '. $_POST['permanent_village'].', '. $_POST['permanent_commune'].', '.$_POST['permanent_district'].', '. $_POST['permanent_province_city'];
    
    $cust_array = array(
      'name_en'           => $name_en,
      'name_kh'           => $name_kh,
      'gender'  => $gender,
      'age'  => $age,
      'phone'  => $phone,
      'email'  => $email,
      'position_applied'  => $position,
      'applicant_address' => $address,
      'created_date' => $current_datetime,
      'modified_date'=> $current_datetime
    );
    if( $wpdb->insert('pr65ac2015_pdv_tbl_applicant', $cust_array) === FALSE) {
      echo "Error";
    } else {
      $last_id = $wpdb->insert_id;
      echo "Success ";
    }
  }else{
    echo 'Invalid data.';
  }
 die();
}
add_action('wp_ajax_addApplicant', 'addApplicant');
add_action('wp_ajax_nopriv_addApplicant', 'addApplicant');

function valid_email($email) {
    return !!filter_var($email, FILTER_VALIDATE_EMAIL);
}

function addSubscriber(){
  global $wpdb;
    date_default_timezone_set('Asia/Phnom_Penh');
    $date = new DateTime();
    if($_POST['subscribe-email'] !='' ){
        $current_date = $date->format("Y-m-d");
        $current_datetime = $date->format("Y-m-d H:i:s");
        $email = $_POST['subscribe-email'];
        $agr = array(
            'role' => 'Subscriber',
            'search'         =>  $email,
            'search_columns' => array( 'user_email' )
        );
        $user_query = new WP_User_Query($agr);//check email role as subscriber in database 
        $subject = "Sucessfull Subscribe";
        $messages = sprintf("Hello,<br/><br/>
                        You have successfully subscribed on our website so you will get notification when we have a new publication from now on,<br/><br/>
                        Best Regards<br/> 
                        PRASAC Admin");
        $message =  mail_message_head();
        $message .=  $messages;
        $message .=  mail_message_footer();
        if(count($user_query->results)==0){
            if(ps_mail('info@prasac.com.kh','PRASAC Admin',array($email),'Subscribers',$subject,$message)) {
                $data_array = array(
                    'user_email'  => $email
                );    
                $wpdb->insert('pr65ac2015_users', $data_array);
                $last_id = $wpdb->insert_id;
                // echo $last_id; die;
                $data_array1 = array(
                    'user_id'  => $last_id,
                    'meta_key'  => 'pr65ac2015_capabilities',
                    'meta_value'  => 'a:1:{s:10:"subscriber";b:1;}'
                );
                $wpdb->insert('pr65ac2015_usermeta', $data_array1);
                echo "Success, one subscriber have been saved into database";
            } else {
                echo "Sorry, subscriber could not saved into database.";
            }
        }else{
            echo 'Exist';
        }
    }else{
        echo 'Invalid data.';
    }
    die();
}
add_action('wp_ajax_addSubscriber', 'addSubscriber');
add_action('wp_ajax_nopriv_addSubscriber', 'addSubscriber');


/*=========Unsubscribe Emails Form===========*/
function Unsubscribe(){
     global $wpdb;
     // if($_POST['unsubscribe-email'] !=''){
     //   $email = 'SELECT * FROM `pr65ac2015_users` WHERE ID=5';
     //   echo $id; exit;
     // }
     if($_POST['unsubscribe-email']){ 
        $table_name = $wpdb->prefix . "users";
        $email = $_POST['unsubscribe-email'];
        $agr = array(
            'role' => 'Subscriber',
            'search'         =>  $email,
            'search_columns' => array( 'user_email' )
        );
        $user_query = new WP_User_Query($agr);
        // var_dump(count($user_query->results); exit;
        //$user = $wpdb->get_results( "SELECT * FROM $table_name WHERE user_email='$email' LIMIT 1" );
          if($user_query->results){
            //var_dump($user_query->results[0]->ID); exit;
            $id =  $user_query->results[0]->ID;
            $delete = $wpdb->get_results( "DELETE FROM $table_name WHERE ID=$id");
            if($delete){
              $usermeta_table = $wpdb->prefix . "usermeta";
              $wpdb->get_results( "DELETE FROM $usermeta_table WHERE user_id=$id");
            }
      
          }else{
            echo 'Not exist';
          } exit;
      }
    }
    add_action('wp_ajax_Unsubscribe', 'Unsubscribe');
    add_action('wp_ajax_nopriv_Unsubscribe', 'Unsubscribe');

/**
 * Function: ps_mail
 * Author: KIMHIM HOM
 * Description: Global email template to send our by using phpmailer
 */
function ps_mail($sender_mail = null,$sender_name,$reciever_mail=array(),$reciever_name,$subject,$message,$ccmail=null){
    $phpmailer_path = get_template_directory().'/assets/phpmailer/PHPMailerAutoload.php';
    
    require_once($phpmailer_path);
    global $error;
    $mail = new PHPMailer();
    try {
      $mail->CharSet = "UTF-8";
    //   $mail->IsSMTP();
      $mail->SMTPDebug = 2;
      $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
      );
      $mail->FromName = "$FromName";
      $mail->Port = 25;
      $mail->Host = "mail.prasac.com.kh";
      $mail->Username = "kimhim.hom@prasac.com.kh";
      $mail->Password = '$Prasac@159753!@#';
      $mail->IsHTML(true);
      // $mail->From = "info@prasac.com.kh";
      $mail->setFrom("info@prasac.com.kh","Online Request Form");
      $mail->AddReplyTo($sender_mail,'Online Request Form');

      // $mail->AddReplyTo($sender_mail,'Online Request Form');
      if($reciever_mail){
          $e=0;
          for($i=0;$i<count($reciever_mail);$i++){
            $mail->AddAddress($reciever_mail[$e],$reciever_name);
            $e++;
          }
      }
      //var_dump($e);exit;
      if($ccmail){
        $mail->addCC($ccmail);
      } 
    
      //$mail->addBCC('kimhim.hom@prasac.com.kh');
      // 	optional name
      $mail->Subject = $subject;
      $mail->Body    = $message;
    //   var_dump($mail);exit;
    if (!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
        return false;
    } else {
        return true;
    }
    } catch (phpmailerException $e) {
      echo "PHPMailer Error: ".$e->errorMessage(); //Pretty error messages from PHPMailer
    } catch (Exception $e) {
      echo "Other Error: ".$e->getMessage(); //Boring error messages from anything else!
    }
}

// if(get_field('pushing_notification_to_subscriber','option')){
//     function post_published_notification( $ID, $post ) {
//         $title = $post->post_title;
//         $title_test = str_replace('[:en]','',explode('[:kh]' ,$title)) ;
//         $final_title = $title_test[0];
//         $permalink = get_permalink( $ID );
//         $subject = "PRASAC NEW PUPLICATION";

//         $messages = "<tr><td style='font-family:Roboto, sans-serif;font-size:14px;font-weight:normal;line-height:24px;'>
//         Hello,<br/><br/>
//         We have published a new post in our website.<b> $final_title</b>,<br/>
//         You may view the latest post at. <a href='$permalink'>Link</a>,<br/><br/>
//         You received this e-mail because you asked to be notified when new updates are posted.<br/><br/>
//         Best Regards<br/> 
//         PRASAC Admin
//         </td></tr>";
//         $message =  mail_message_head();
//         $message .=  $messages;
//         $message .=  mail_message_footer();

//         $headers[] = '';
//         $subscribers = get_users( array ( 'role' => 'subscriber' ) );
//         foreach ( $subscribers as $subscriber ){
//             ps_mail('info@prasac.com.kh','PRASAC Admin',array($subscriber->user_email),'Subscribers',$subject,$message);
//         }
//     }
//     add_action('publish_post', 'post_published_notification', 10, 3 );
// }

// function checkroleloggedin_user(){
//     if(is_user_logged_in()){
//         $logged_role    =   wp_get_current_user()->roles[0];
//         if('applicant'==$logged_role){
//             header('Location:./applicant-profile');
//         }
//     }
// }
// add_action('init','checkroleloggedin_user');
  
function emailToBranch($request_type){
    global $wpdb;
    date_default_timezone_set('Asia/Phnom_Penh');
    $date = new DateTime();
    $current_datetime = $date->format("Y-m-d h:i:s A");
    
    $font_family    =   '[:en]Helvetica[:kh]Khmer OS Content[:]';
    $font_family    =   __($font_family);
    $full_name = $_POST['full_name'];
    $phone_number = $_POST['phone_number'];
    $client_dob = isset($_POST['client_dob'])?$_POST['client_dob']:'N/A';
    $client_email = isset($_POST['email_address']) && $_POST['email_address'] !=''?$_POST['email_address']:'N/A';
    $loan_amount  = isset($_POST['loan_amount'])?number_format_i18n($_POST['loan_amount'],2):'N/A';
    $account_number = isset($_POST['account_number']) && $_POST['account_number'] !=''?$_POST['account_number']:'N/A';
    $loan_purpose = isset($_POST['loan_purpose'])?$_POST['loan_purpose']:'N/A';
    $loan_term  = isset($_POST['loan_term'])?$_POST['loan_term']:'N/A';
    $province = $_POST['province'];
    $district = $_POST['district'];
    $commune = $_POST['commune'];
    $village = $_POST['village'];

    $house_number = $_POST['house_number']?$_POST['house_number']:'';
    $street_number = $_POST['street_number']?$_POST['street_number']:'';
    $group_number = $_POST['group_number']?$_POST['group_number']:'';
    $branch = $_POST['branch'];
    $main_service = $_POST['main_service'];
    $secondary_service = $_POST['deposit_service'];
    $request_type = $_POST['request_type'];
    $reciever_email = $_POST['reciever_email'];
    $loan_currency = $_POST['loan_currency'];
  

    //Get main product name
    $main_product_query = "SELECT title,title_en,form_head_text_en FROM pr65ac2015_online_request_main_service WHERE id = $main_service LIMIT 1";
    $main_product_query_result = $wpdb->get_results($main_product_query, OBJECT);
    // $main_product_title_lang = '[:en]title_en[:kh]title[:]';
    // $main_final_title = __($main_product_title_lang);
    $main_product_selected = $main_product_query_result?$main_product_query_result[0]->title_en:'N/A';


    //Get product name
    $product_query = "SELECT title,title_en FROM pr65ac2015_online_request_product WHERE id = $secondary_service LIMIT 1";
    $product_query_result = $wpdb->get_results($product_query, OBJECT);
    // $product_title_lang = '[:en]title_en[:kh]title[:]';
    // $final_title = __($product_title_lang);
    $product_selected = $product_query_result?$product_query_result[0]->title_en:'N/A';
    $product_for_subject = $product_query_result?$product_query_result[0]->title_en:'';

    if (qtranxf_getLanguage() == 'en') {
      $final_full_address = $fulladdress_en;
    } elseif (qtranxf_getLanguage() == 'kh') {
      $final_full_address = $fulladdress_kh;
    }
  
    $messages_loan= sprintf("<html>
    <header>
    </header>
    <body style='marging:0;padding:0;background-color:#F0F9F0;'>
        <table cellpadding='20' cellspacing='0' align='center' font-family:'Helvetica,Arial,sans-serif' style='background-color:#ffffff;font-size:14px;display:table;verticle-align:middle;'>
            <tr style='margin:0; padding:10px 5px;'>
              <td style='padding-bottom:10px;' colspan='2' align='left'>
                  <p>Dear Sir/Madam,<br /><br />
                  One client submitted her/his request by using our Online Request Form. For more details, please check client's information below</p>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span style='text-align:left;float:left;'>Client Name :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$full_name</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'> 
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Client Phone Number :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$phone_number</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Client Email :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$client_email</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Product & Service :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$main_product_selected</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span style=''>Currency:</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$loan_currency</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span style=''>Loan Amount:</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$loan_amount</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span style=''>Loan Purpose :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$loan_purpose</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Loan Term (Months) :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$loan_term</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'> 
              <td style='padding:5px;width:120px;' align='left'>
                  <span>To Branch  :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$branch</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'> 
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Date Time :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$current_datetime</span>
              </td>
            </tr>
        </table>
    </body>
    </html>");

    $messages_deposit= sprintf("<html>
    <header>
    </header>
    <body style='marging:0;padding:0;background-color:#F0F9F0;'>
        <table cellpadding='20' cellspacing='0' align='center' font-family:'Helvetica,Arial,sans-serif' style='background-color:#ffffff;font-size:14px;display:table;verticle-align:middle;'>
            <tr style='margin:0; padding:10px 5px;'>
              <td style='padding-bottom:10px;' colspan='2' align='left'>
                  <p>Dear Sir/Madam,<br /><br />
                  One client submitted her/his request by using our Online Request Form. For more details, please check client's information below</p>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span style='text-align:left;float:left;'>Client Name</span>
                  <span style='text-align:right;float:right;'>:</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$full_name</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'> 
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Client Phone Number :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$phone_number</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Client Email :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$client_email</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Product & Service :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$main_product_selected</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Financial Service :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$product_selected</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'>
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Account Number :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$account_number</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'> 
              <td style='padding:5px;width:120px;' align='left'>
                  <span>To Branch  :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$branch</span>
              </td>
            </tr>
            <tr style='margin:0; padding:0;'> 
              <td style='padding:5px;width:120px;' align='left'>
                  <span>Date Time :</span>
              </td>
              <td style='padding:5px;border-collapse:collapse;width:300px;' align='left'>
                  <span>&nbsp;&nbsp;$current_datetime</span>
              </td>
            </tr>
        </table>
    </body>
    </html>");

    $final_message = $request_type=='Loan'?$messages_loan:$messages_deposit;

    $message =  mail_message_head();
    $message .=  $final_message;
    $message .=  mail_message_footer();
    $subject = $main_product_selected;
    ps_mail('info@prasac.com.kh','Online Reqeust',array('callcenter@prasac.com.kh'),'PRASAC MFI Plc.','PRASAC Online Reqeust',$message,'kimhim.hom@prasac.com.kh');//,'raksmey.chheun@prasac.com.kh','suy.peang@prasac.com.kh'
    return true;
}

add_action('wp_ajax_emailToBranch', 'emailToBranch');
add_action('wp_ajax_nopriv_emailToBranch', 'emailToBranch');

function getLocationName($parent_id,$child_id){
    global $wpdb;
    // echo $parent_id;exit;
    if($parent_id ==1){ //Get province Name
      $query = "select name from provinces where id=$child_id limit 1";//Alias
      $list = $wpdb->get_results($query,OBJECT);
      echo $list[0]->name;
    }

    if($parent_id ==2 ){ //Get district Name
        $query = "select name from districts where id=$child_id";//Alias
        $list = $wpdb->get_results($query,OBJECT);
        echo $list[0]->name;
    }

    if($parent_id ==3 ){ //Get commune Name
        $query = "select name from communes where id=$child_id";//Alias
        $list = $wpdb->get_results($query,OBJECT);
        echo $list[0]->name;
    }
    if($parent_id ==4){ //Get village Name
        $query = "select name from villages where id=$child_id";//Alias
        $list = $wpdb->get_results($query,OBJECT);
        echo $list[0]->name;
    }
}
// add_action('wp_ajax_getLocationName', 'getLocationName');
// add_action('wp_ajax_nopriv_getLocationName', 'getLocationName');


function getlocation($level){
    global $wpdb;
    
    //=====
    /*$lelve 1: Provine, 2: District, 3:Commune, 4: Village
    */
    if($level==1){
        $query = "select id, name, name_kh from provinces order by id";//Alias
        $list = $wpdb->get_results($query,OBJECT);
        foreach($list as $lists):
            $name = $lists->name;
            $id = $lists->id;
            $name_kh = $lists->name_kh;
            _e("[:en]<option value='$id' data-province='$name'>".$name."</option>[:kh]<option value='$id' data-province='$name'>".$name_kh."</option>[:]");
        endforeach;
    }

    if($level==2){//get district name
        $query = "select DISTINCT d.province_id as p_id, d.id as d_id, d.name as d_name, d.name_kh as d_name_kh, p.name as p_name, p.name_kh as p_name_kh from provinces as p inner join districts as d on p.id=d.province_id order by d.id";//Alias
        $list = $wpdb->get_results($query,OBJECT);
        foreach($list as $lists):
            $distict_name     = $lists->d_name;
            $d_id = $lists->d_id;
            $distict_name_kh  = $lists->d_name_kh;
            $province_id    = $lists->p_id;
            $province_name_kh = $lists->p_name_kh;
            _e("[:en]<option value='$d_id' class='$province_id' data-district='$distict_name'>".$distict_name."</option>[:kh]<option value='$d_id' class='$province_id' data-district='$distict_name'>".$distict_name_kh."</option>");
       endforeach;
    }

    if($level==3){//Get commune name
        $query = "select DISTINCT c.district_id as d_id,c.name as c_name,c.id as c_id, c.name_kh as c_name_kh,d.name as d_name, d.name_kh as d_name_kh from districts as d inner join communes as c on d.id=c.district_id order by c.id";
        $list = $wpdb->get_results($query,OBJECT);
        foreach($list as $lists):
            $distict_name    = TRIM($lists->d_name);
            $distict_id    = TRIM($lists->d_id);
            $distict_name_kh = TRIM($lists->d_name_kh);
            $commune_name    = TRIM($lists->c_name);
            $commune_id   = TRIM($lists->c_id);
            $commune_name_kh = TRIM($lists->c_name_kh);
            _e("[:en]<option value='$commune_id' class='$distict_id'>".$commune_name."</option>[:kh]<option value='$commune_id' class='$distict_id'>".$commune_name."</option>");
        endforeach;
    }

    if($level==4){//Get village name
        $query = "select DISTINCT v.id as v_id,v.name as v_name,v.name_kh as v_name_kh,v.commune_id as c_id,c.name as c_name,c.name_kh as c_name_kh from communes as c inner join villages as v on c.id=v.commune_id where v.name !='' or v.name_kh !=''";
        $list = $wpdb->get_results($query,OBJECT);
        foreach($list as $lists):
            $c_name         = TRIM($lists->c_name);
            $c_id         = TRIM($lists->c_id);
            $c_name_kh      = TRIM($lists->c_name_kh);
            $v_name         = TRIM($lists->v_name);
            $v_id         = TRIM($lists->v_id);
            $v_name_kh      = TRIM($lists->v_name_kh);
            _e("[:en]<option value='$v_id' class='$c_id'>".$v_name."</option>[:kh]<option value='$v_id' class='$c_id'>".$v_name."</option>");
        endforeach;
    }

    if($level==5){//Get Branch Name
        $query = "select b.fc as branch_fc,b.id as b_id,b.name as b_name,b.name_kh as b_name_kh,p.id as p_id,p.name as p_name,p.name_kh as p_name_kh from branches as b, provinces as p where b.province_id=p.id order by b.province_id ASC";
        $list = $wpdb->get_results($query,OBJECT);
        foreach($list as $lists):
            $branch_name        = $lists->b_name;
            $branch_id          = $lists->b_id;
            $branch_name_kh     = $lists->b_name_kh;
            $province_name      = $lists->p_name;
            $province_id     = $lists->p_id;
            $province_name_kh   = $lists->p_name_kh;
            _e("[:en]<option value='$branch_name' class='$province_id' data-branch='$branch_name'>".$branch_name."</option>[:kh]<option data-branch='$branch_name' value='$branch_name_kh' class='$province_id'>".$branch_name_kh."</option>");
        endforeach;
    }
   
}

function getEvents(){
    // Our Start and End Dates
    global $wpdb;
    $data_events = '';
    $query = "select ec.event_title as title,CONCAT(c.year_num,'-',c.month_num,'-',c.day_num) as start,c.day_lunar as description from tbl_calendar as c left join tbl_event_calendar as ec ON c.id=ec.calendar_id ORDER BY c.id ASC";
    $lists = $wpdb->get_results($query,OBJECT);
    // foreach($lists as $row) {
    //      $data_events .= "{id:$row->day_num,title:$event_title,start:new Date($row->year_num,$row->month_num,$row->day_num),allDay:true,className:'important'}";
    // }
    // $events = array();
    // foreach($lists as $row) {
    //     $event_lists = array();
    //     $event_lists['title'] = $row->event_title;
    //     $date_time = $row->year_num.'-'.$row->month_num.'-'.$row->day_num;
    //     $event_lists['start'] = 'new Date()';
    //     $event_lists['description'] = $row->day_lunar;
    //     $event_lists['allDay'] = true;
    //     $event_lists['className'] = 'important';
    //     array_push($events, $event_lists);
    // }
    return $lists;
 }

//  add_action( 'wp_ajax_UserRegistration', 'UserRegistration' );
//  do_action( 'wp_ajax_UserRegistration', 'UserRegistration' );
 function user_registration(){
    global $wpdb;
    $id = isset($_POST['user_id'])?$_POST['user_id']:false;
    $data = '';
    if($id){
        $query = "select user_id,user_name,IF(user_gender='F','Female','Male') as user_gender,user_position,user_regional,user_branch,fcsite from registration_list where user_id=$id limit 1";
        $selected_user = $wpdb->get_results($query,OBJECT);
        // var_dump($selected_user);
        if($selected_user){
            $postData = array(
                    "user_id" => $selected_user[0]->user_id,
                    "user_name" => $selected_user[0]->user_name,
                    "user_gender" => $selected_user[0]->user_gender,
                    "user_position" => $selected_user[0]->user_position,
                    "user_regional" => $selected_user[0]->user_regional,
                    "user_branch" => $selected_user[0]->user_branch,
                    "fcsite" => $selected_user[0]->fcsite,
            );
            echo json_encode($postData);
        }
        
    }
    die();
 }
add_action('wp_ajax_user_registration', 'user_registration');
add_action('wp_ajax_nopriv_user_registration', 'user_registration');


function StoreOnlineRequest(){
  global $wpdb;
  date_default_timezone_set('Asia/Phnom_Penh');
  $date = new DateTime();
  $current_datetime = $date->format("Y-m-d H:i:s");
  session_start();
  $status = '';
  if($_POST['request_type']=='Loan'){
    $enterd_captcha_loan = trim($_POST['captcha_loan']);
    if(strcasecmp($_SESSION['captcha_loan'], $enterd_captcha_loan) != 0){
      $status = "Wrong";
    }else{
      $full_name = $_POST['full_name'];
      $phone_number = $_POST['phone_number'];
      $client_dob = $_POST['client_dob'];
      $client_email = $_POST['email_address'];
      $loan_amount  = $_POST['loan_amount'];
      $loan_term  = $_POST['loan_term'];
      $loan_purpose  = $_POST['loan_purpose'];
      $branch = $_POST['branch'];
      $main_service = $_POST['main_service'];
      $request_type = $_POST['request_type'];

      $online_request =  $wpdb->prefix . 'online_request';
      $loan_inserted_query = $wpdb->insert($online_request , array(
        'client_name' => "$full_name",
        'client_phone' => "$phone_number",
        'client_email' => "$client_email",
        'client_dob' => "$client_dob",
        'loan_amount' => "$loan_amount",
        'loan_term' => "$loan_term",
        'loan_purpose' => "$loan_purpose",
        'province' => $province,
        'branch' => "$branch",
        'main_product' => $main_service,
        'created_date' =>"$current_datetime",
        'modified_date' =>"$current_datetime"
        )
      );
      if($loan_inserted_query){
        if(emailToBranch($request_type)){
          $status = "Done";
        }else{
          $status = "Fail";
        }
      }else{
        $status = "Fail";
      }
    }
  }

  if($_POST['request_type']=='banking-service'){
    $enterd_captcha = trim($_POST['captcha_deposit']);
    if(strcasecmp($_SESSION['captcha_deposit'], $enterd_captcha) != 0){
        $status = "Wrong";
    }else{
      $full_name = $_POST['full_name'];
      $phone_number = $_POST['phone_number'];
      $client_email = $_POST['client_email'];
      $branch = $_POST['branch'];
      $main_service = $_POST['main_service'];
      $deposit_service = $_POST['deposit_service'];
      $request_type = $_POST['request_type'];
      $online_request =  $wpdb->prefix . 'online_request';
      $account_number = isset($_POST['account_number']) && $_POST['account_number'] !=''?$_POST['account_number']:'N/A';

      $banking_inserted_query = $wpdb->insert($online_request , array(
        'client_name' => "$full_name",
        'client_phone' => "$phone_number",
        'client_email' => "$client_email",
        'branch' => "$branch",
        'main_product' => $main_service,
        'product' => $deposit_service,
        'account_number'=>$account_number,
        'created_date' =>"$current_datetime",
        'modified_date' =>"$current_datetime"
        )
      );
      if($banking_inserted_query){
        if(emailToBranch($request_type)){
          $status = "Done";
        }else{
          $status = "Fail";
        }
      }else{
        $status = "Fail";
      }
    }
  }
  echo $status;
  die();
}
add_action('wp_ajax_StoreOnlineRequest', 'StoreOnlineRequest');
add_action('wp_ajax_nopriv_StoreOnlineRequest', 'StoreOnlineRequest');


function FilterBranch(){
  $provinceID = isset($_POST['provinceID'])?$_POST['provinceID']:12;
  $provinceID = $provinceID==23?7:$provinceID;
  global $wpdb;
  date_default_timezone_set('Asia/Phnom_Penh');
  $date = new DateTime();
  $current_datetime = $date->format("Y-m-d H:i:s");

  $query = "SELECT b.fc AS branch_fc,b.id AS b_id,b.name AS b_name,b.name_kh AS b_name_kh,p.id AS p_id,p.name AS p_name,p.name_kh AS p_name_kh FROM branches AS b, provinces AS p WHERE b.province_id=p.id ORDER BY FIELD(b.province_id,$provinceID) DESC";
  $list = $wpdb->get_results($query,OBJECT);
  echo '<option value="" selected disabled>';
    _e("[:kh]សាខា[:en]Branch[:]");
  echo '</option>';
  foreach($list as $lists):
      $branch_name        = $lists->b_name;
      $branch_id          = $lists->b_id;
      $branch_name_kh     = $lists->b_name_kh;
      $province_name      = $lists->p_name;
      $province_id        = $lists->p_id;
      $province_name_kh   = $lists->p_name_kh;
      _e("[:en]<option value='$branch_name' class='$province_id' data-branch='$branch_name'>".$branch_name."</option>[:kh]<option data-branch='$branch_name' value='$branch_name' class='$province_id'>".$branch_name_kh."</option>");
  endforeach;
}
add_action('wp_ajax_FilterBranch', 'FilterBranch');
add_action('wp_ajax_nopriv_FilterBranch', 'FilterBranch');

function ApplynowCaptcha(){
    session_start();
    $status = '';
    $captcha_applynow = trim($_POST['captcha_career']);
    // var_dump(strcasecmp($_SESSION['captcha_careers'], $enterd_captcha_applynow));
    // exit;
    if(strcasecmp($_SESSION['applyjob'],$captcha_applynow) == 0){
      $status = "Success";
    }else{
        $status = "Wrong";
    }
    echo $status;
    die();
}
add_action('wp_ajax_ApplynowCaptcha', 'ApplynowCaptcha');
add_action('wp_ajax_nopriv_ApplynowCaptcha', 'ApplynowCaptcha');
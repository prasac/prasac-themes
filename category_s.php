<?php get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <aside class="hidden-xs col-xs-12 col-sm-3 col-md-3"><?= get_sidebar(); ?></aside>
            <section class="col-xs-12 col-sm-9 col-md-9">
                <div class="hidden-xs hidden-sm">
                    <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                </div>
                <div class="page-title">
                <h3 class="green"><?php single_cat_title(); ?></h3>
                </div>
                <?php
                if( have_posts() ) {
                    while( have_posts()): the_post();
                        get_template_part('template-parts/content', 'summary');
                    endwhile;
                    if ( function_exists('wp_bootstrap_pagination') ) wp_bootstrap_pagination();
                } else {
                    get_template_part('template-parts/content', 'none');
                }
                ?>
            </section>
            <div class="clear"></div>
        </main>
    </div>
</div>
<?php get_footer(); ?>

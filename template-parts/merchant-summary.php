<?php
$title = get_the_title();
$website = get_field('website');
?>
<div id="post-<?php the_ID(); ?>" class="col-md-12 merchant-content">
  <address class="flex-content">
    <section class="poster-card_body">
        <div class="col-md-6 col-sm-12">
            <div class="row">
                <a class="lightbox" href="<?php the_post_thumbnail_url(); ?>" title="<?php the_title(); ?>" >
                    <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive" title="<?php the_title(); ?>"/>
                </a>
            </div>
        </div>
        <div class="col-md-6 col-sm-12" style="margin-bottom:67px;">
            <h4 class="green" style="margin-top:10px;">
                <?php the_title(); ?>
            </h4>
            <table class="flex-address">
                <tr>
                    <td class="title"><i class="fa fa-check-circle-o" aria-hidden="true"></i></td>
                    <td class="text"><?= get_field('discount_amount'); ?></td>
                </tr>
                <tr>
                    <td class="title"><i class="fa fa-home" aria-hidden="true"></i></td>
                    <td class="text"><?= get_field('address'); ?></td>
                </tr>
                <tr>
                    <td class="title"><i class="fa fa-phone" aria-hidden="true"></i></td>
                    <td class="text"><?= get_field('phone'); ?></td>
                </tr>
                <?php if( $website ) : ?>
                <tr>
                    <td class="title"><i class="fa fa-globe" aria-hidden="true"></i></td>
                    <td class="text"><a href="<?= $website ?>" target="_blank"><?= $website ?></a></td>
                </tr>
                <?php endif; ?>
            </table>
        </div>
    </section>
    <div class="single-footer-share" style="margin-bottom:0;position:absolute;bottom:0;">
        <div class="row">
            <div class="col-xs-12 col-md-12">
            <hr>
                <div class="single-footer-share">
                    <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                    <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                    <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo get_permalink();?>','twitter');"></a>
                    <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo get_permalink();?>','linkedin');"></a>
                    <div class="fb-like pull-right" data-href="<?php echo get_permalink();?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="false"></div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script>
        jQuery(document).ready(function(){
            jQuery('.lightbox').magnificPopup({
                type: 'image',
                gallery: {
                    enabled: true
                },
            });

            jQuery('#list').click(function(event){
                event.preventDefault();
                if(!jQuery('#wpas-results-inner .merchant-content').hasClass('col-md-12') || jQuery('#wpas-results-inner .merchant-content').hasClass('column') || jQuery('#wpas-results-inner').hasClass('display-grid') || jQuery('.single-footer-share .col-xs-12').hasClass('text-center')){
                    jQuery('#wpas-results-inner').removeClass('display-grid');
                    jQuery('#wpas-results-inner .merchant-content').removeClass('column').addClass(' col-md-12');
                    jQuery('#wpas-results-inner .merchant-content address').addClass(' flex-content');
                    jQuery('.single-footer-share .col-xs-12').removeClass('text-center').addClass(' text-right');
                };
                if(jQuery('.poster-card_body .col-sm-12').hasClass('col-md-12')){
                    jQuery('.poster-card_body .col-sm-12').removeClass('col-md-12').addClass('col-md-6');
                }
            });

            jQuery('#grid').click(function(event){
                event.preventDefault();
                 if(jQuery('#wpas-results-inner .merchant-content').hasClass('col-md-12') || jQuery('.single-footer-share .col-xs-12').hasClass('text-right')){
                    jQuery('#wpas-results-inner').addClass('display-grid');
                    jQuery('#wpas-results-inner .merchant-content address').removeClass('flex-content');
                    jQuery('#wpas-results-inner .merchant-content ').removeClass('col-md-12').addClass('column');
                    jQuery('.single-footer-share .col-xs-12').removeClass('text-right').addClass(' text-center');
                };
                if(jQuery('.poster-card_body .col-sm-12').hasClass('col-md-6')){
                    jQuery('.poster-card_body .col-sm-12').removeClass('col-md-6').addClass(' col-md-12');
                }
            });
        });
    </script>
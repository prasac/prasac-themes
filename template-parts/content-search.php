<?php
/**
 * Created by Visual Studio Code.
 * User: HOM KIMHIM
 * Date: 19/12/2017
 * Time: 11:27
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>

        <?php if ( 'post' === get_post_type() ) : ?>
            <div class="entry-meta">
                <span class="entry-date">
                    <?php echo '<i class="fa fa-calendar danger"></i><span style="color:#f0ad4e;"> Posted on '.get_the_date('M d, Y').'</span>'; ?>
                </span>
            </div><!-- .entry-meta -->
        <?php endif; ?>
    </header><!-- .entry-header -->

    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div><!-- .entry-summary -->
</article><!-- #post-## -->

<?php
/*********************
# Global Configuration
 *********************/
$post_title = get_the_title();
$website = '#';
if ( is_tax('people_category')  ) {
    $website = get_field('url');
} else {
    $website = '#';
}
?>
<?php if( is_tax('people_category' ,'vision-mission') ): ?>
    <article id ="people-<?= $post->ID; ?>" <?php post_class('content-summary') ?> >
        <?php echo the_content(); ?>
    </article>

<?php elseif( is_tax('people_category' ,array('shareholders', 'management', 'board-of-directors') ) ): ?>
    <!-- Only appear with shareholders, management, boards of director cateogry -->
    <article id="people-<?= $post->ID; ?>" <?php post_class('content-summary col-xs-12 col-md-12') ?> >
        <div class="col-xs-12 col-sm-6 col-md-3 text-center block-image">
            <a href="<?php the_post_thumbnail_url(); ?>" title="<?= $post_title; ?>" class="lightbox">
                <img src="<?php the_post_thumbnail_url(); ?>" class="img-responsive img-thumbnail">
            </a>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-9 block-content">
            <h4 class="media-heading green" style="margin:0;"><?= $post_title; ?></h4>
            <?php if ( is_tax('people_category', array('board-of-directors', 'management') ) ): ?>
                <h5 class="green" style="margin-top:0;"><?= get_field('main_header'); ?></h5>
            <?php endif; ?>
            <?php echo the_content(); ?>
        </div>
    </article>
    <?php elseif( is_tax('people_category' ,array('license') ) ): ?>
        <div class="col-xs-12 col-sm-6 col-md-4 award-recognition text-left" style="margin:5px 0;display:block;">
            <a class="group1 cboxElement lightbox" href="<?= get_field('image')?>" title="<?= $post_title; ?>" >
                <img src='<?= get_field('image')?>' class='img-responsive img-thumbnail post-thumbnail lazy' />
            </a>
        </div>
    <?php 
    elseif( is_tax('people_category' ,array('milestones'))):
    ?>
     <div class="col-sm-12">
        <ul class="timeline">
            <li>
            <div class="timeline-badge success"><i class="fa fa-calendar"></i></div>
            <div class="timeline-panel">
                <div class="timeline-heading page-title">
                <h4 class="timeline-title"><?= $post_title; ?></h4>
                </div>
                <div class="timeline-body">
                    <?php echo the_content(); ?>
                </div>
            </div>
            </li>
        </ul>
    </div>
    <?php 
    elseif( is_tax('people_category','social-awards')):
    ?>
        <div class=" col-xs-12 col-md-4 social-awards <?= get_the_terms($post->ID,'years')[0]->name;?>">
            <div class="box14">
                <img src="<?= get_field('image');?>" alt="" class="img-responsive">
                <div class="box-content">
                    <p class="title"><?= the_title();?></p>
                    <ul class="icon">
                        <li><a href="<?= get_field('image');?>" class="lightbox" title="<?= the_title();?>"><i class="fa fa-search-plus"></i></a></li>
                        <li><a href="<?= get_field('url');?>" title="<?= get_field('url');?>" target="_blank"><i class="fa fa-link"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    <?php elseif(is_tax('people_category','awards-recognition')):?>
        <a class="col-md-4 lightbox" href="<?= get_field('image')?>">
            <img src='<?= get_field('image')?>' class='img-responsive img-thumbnail post-thumbnail lazy' />
        </a>
            <style>
            .video-pagination .page-numbers:not(.next):not(.prev){
                background-color:#4db848;
                color:#ffffff;
                padding: 7px 12px;
                border-radius: 50%;
            }
            div.video-pagination span.page-numbers.current{
                border:#4db848 1px solid !important;
                background-color:transparent !important;
                color:#4db848 !important;
            }
            div.video-pagination a.next,div.video-pagination a.prev{
                padding: 7px 12px;
                background-color:#4db848;
                color:#ffffff;
            }
            div.video-pagination{
                margin-bottom:2rem;
            }
        </style>
<?php elseif(is_tax('people_category',array('organizational-chart', 'message-from-ceo') ) ): ?>
     <article id="people-<?= $post->ID; ?>" <?php post_class('content-summary') ?> >
        <?php echo the_content(); ?>
    </article>
<?php endif; ?>



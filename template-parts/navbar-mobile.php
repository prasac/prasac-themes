<header id="mobile-header" class="navbar-static-top">
  <nav class="mobile-navbar navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a href="<?php echo esc_url( home_url() ); ?>" class="navbar-brand"><img src="<?php echo get_field('mobi_logo', 'option') ?>" alt="<?php bloginfo('name'); ?>" class="img-responsive" width="50"/></a>
    <?php qtranxf_generateLanguageSelectCode('text'); ?>
    <span class="search_click"><i class="fa fa-search" aria-hidden="true" style="vertical-align:middle;"></i>&nbsp;</span>
    </div>
    <div id="primary-navbar-collapse" class="navbar-collapse collapse ">
    <?php
    wp_nav_menu( array(
            'menu'            => 'MainMenu',
            'depth'           => 3,
            'container'       => false,
            'menu_class'      => 'nav navbar-nav dropdown',
            'menu_id'         => 'mobile-menu',
            'fallback_cb'     => 'wp_bootstrap_navwalker::e',
            'walker'          => new wp_bootstrap_navwalker())
    );
    ?>
        <!-- <div class="row">
            <div class="col-xs-12 text-left" id="internet_backing_row">
                <a href="https://ibanking.prasac.com.kh/" target="_blank"><i class="fa fa-internet-explorer"></i> Internet Banking</a></li>
                <a href="<?php //echo get_page_link(576); ?>"><i class="fa fa-mobile-phone"></i> Mobile Banking</a>
            </div>
        </div> -->
    </div>
    <div class="search-wrapper-mobile">
        <form action="<?= esc_url(home_url('/')) ?>" class="search-form">
          <div class="form-group has-feedback">
            <label for="search" class="sr-only">Search</label>
            <input type="text" class="form-control" name="s" id="search" placeholder="search..." value="<?= get_search_query(); ?>">
          </div>
        </form>
      </div>
  </nav>
</header>
 <script>    
    jQuery(document).ready(function(){
      jQuery(".search_click").click(function(){
            jQuery("div.search-wrapper-mobile").slideToggle('fast');
      });
      
      jQuery('.navbar a.dropdown-toggle').on('click', function(e) {
        var $el = jQuery(this);
        var $parent = jQuery(this).offsetParent(".dropdown-menu");
        jQuery(this).parent("li").toggleClass('open');

        if(!$parent.parent().hasClass('nav')) {
            $el.next().css({"top": $el[0].offsetTop, "left": $parent.outerWidth() - 4});
        }

        jQuery('.nav li.open').not(jQuery(this).parents("li")).removeClass("open");

        return false;
      });
    });
</script>
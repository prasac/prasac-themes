<?php if ( have_rows('product_repeater') ):
    $count = 0;?>
    <?php while ( have_rows( 'product_repeater' ) ) : the_row(); ?>
    <?php
    $post_objects = get_sub_field('product_link');
    $image = get_sub_field('product_image');
    $feature_image = "feature-image";
    $feature_content = "";
    ?>
        <?php if ( $post_objects ): ?>
            <?php
            $post = $post_objects;
            setup_postdata($post);
            $title = get_the_title($post->ID);
            if ( $count % 2 == 1 ) {
                $feature_content = "feature_bg";
                $feature_image = "feature-image pull-right";
            }
            ?>
            <section id="feature-content" class="col-sm-12 col-md-12">
                <div class="row no-gutter">
                    <!-- Feature Image -->
                    <section class="col-sm-6 col-md-6 <?= $feature_image; ?>">
                        <a href="<?= get_permalink(); ?>" title="<?= $title; ?>"><img class="lazy img-responsive" src="<?= $image['url']; ?>" alt="<?= $title; ?>"></a>
                    </section>
                    <!-- Feature Content -->
                    <section class="col-sm-6 col-md-6 <?= $feature_content; ?>">
                        <div class="feature-desc">
                            <h4 class="green"><?= $title; ?></h4>
                            <p class="text-justify"><?= get_the_excerpt(); ?></p>
                            <a class="btn btn-sm btn-default" href="<?= get_permalink(); ?>">learn more <i class="fa fa-chevron-right" style="vertical-align:middle;"></i></a>
                        </div>
                    </section>
                </div>
            </section>
            <?php
            $count++;
            wp_reset_postdata();?>
        <?php endif; ?>
    <?php endwhile;?>
<?php endif; ?>

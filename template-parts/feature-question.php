<?php if ( have_rows('question_repeater') ): $num_id = 0; ?>
<h4>FAQs</h4>
<section id="accordion" class="panel-group">
    <?php
    while ( have_rows('question_repeater') ): the_row();
        $question = get_sub_field('question');
        $answer = get_sub_field('answer');
    ?>
    <div class="panel panel-default">
      <div id="headingOne" class="panel-heading">
          <a href="#collapse-<?= $num_id; ?>" data-toggle="collapse" data-parent="#accordion">
            <div class="panel-title"><?=$question; ?></div>
          </a>
      </div>
      <div id="collapse-<?= $num_id; ?>" class="panel-collapse collapse">
        <div class="panel-body page"><?=$answer; ?></div>
      </div>
    </div>
    <?php
    $num_id++;
    endwhile;
    ?>
</section>
<?php endif; ?>

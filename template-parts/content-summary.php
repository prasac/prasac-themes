<?php
/*********************
# Global Configuration
 *********************/
$post_title = get_the_title();
if( is_category() ) :
$category = get_the_category($post->ID);
?>
<!-- Content summary list on front page and media page as latest post -->
<article id="<?php echo $post->ID; ?>" <?php post_class('c-hub-river__entry') ?> >
    <div class="c-hub-entry PP">
        <a class="c-hub-entry__image-wrapper" href="<?php echo get_permalink($post->ID); ?>" title="<?php echo $post_title ?>">
            <!-- Header -->
            <div class="c-hub-entry__image">
                <?php if( function_exists('featured_image') ) echo featured_image('medium', true, 'img-lazy c-dynamic-image'); ?>
            </div>
        </a>
        <!-- Body -->
        <div class="c-hub-entry__body">
            <div class="c-hub-entry__title green">
                <a href="<?php echo get_permalink($post->ID); ?>" title="<?php echo $post_title ?>"><?php echo $post_title; ?></a>
            </div>
            <p class="article_date">
                <i class="fa fa-calendar danger"></i>&nbsp;&nbsp;<?php _e(get_post_time('F j, Y - g:i a')); ?>
            </p>
            <p class="hidden-xs"><?= mb_strimwidth(get_the_excerpt(), 0,201, '...'); ?></p>
            <p class="visible-xs"><?= mb_strimwidth(get_the_excerpt(), 0,50, '...'); ?></p>
            <div class="c-content hidden-xs text-right"><a class="btn btn-sm btn-default" href="<?= get_permalink($post->ID); ?>"><?php _e('[:en]read more[:kh]អានបន្ត[:]'); ?>&nbsp<i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </div>
</article>
<?php endif; ?>
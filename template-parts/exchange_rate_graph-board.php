<style>
.highcharts-credits{
    opacity:0;
}
</style>
<div class="col-xs-12 col-sm-12 col-md-12">
  <!-- Nav tabs -->
  <div class="row">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#foreign-exchange_rates" aria-controls="foreign-exchange_rates" role="tab" data-toggle="tab">
                <?php _e('[:en]Daily Exchange Rates[:kh]តារាង​អត្រា​​ប្ដូរ​​ប្រាក់​​ប្រចាំ​​ថ្ងៃ​ [:]');?>
            </a>
        </li>
        <li role="presentation">
            <a href="#graph" aria-controls="graph" role="tab" data-toggle="tab">
                <?php _e('[:en]Graph[:kh]​ក្រាហ្វ[:]');?>
            </a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="foreign-exchange_rates">
            <br />
            <?php
            if(isset($_POST['graph'])){
                ?>
                    <script>
                        jQuery(function () {
                            jQuery('a[href="#graph"]').tab('show');
                        });
                    </script>
                <?php
            }
            $query = array(
                'post_type' => 'exchange',
                'post_status' => 'publish',
                'orderby' => 'post_date',
                'order'=> 'DESC',
                'posts_per_page' =>2
            ); 
            ?>
            <div class="row">
                <form action="" method="post" id="exchange_search">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Date</label>
                            <div class='input-group date'>
                                <input  id='date' type='text' class="form-control" name="date" value="<?php echo isset($_POST['date'])?$_POST['date']:''?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                <div class="col-md-4">
                        <div class="form-group">
                            <label>Update</label>
                            <div class='input-group' id='update'>
                                 <select class="form-control" name="time" id="time" required>
                                      <option value="0" date="0" <?php echo isset($_POST['time']) && $_POST['time']==''?' selected="selected"':'';?>>All</option>
                                    <?php
                                        $query_date = array(
                                            'post_type' => 'exchange',
                                            'post_status' => 'publish',
                                            'orderby' => 'post_date',
                                            'order'=> 'DESC',
                                            'posts_per_page' =>-1
                                        ); 

                                    $result_date = new WP_Query($query_date);

                                        if( $result_date->have_posts() ):
                                            while( $result_date->have_posts() ) : $result_date->the_post();
                                               ?>
                                               <option value="<?php echo get_post_time('G:i:s');?>" date="<?php echo get_post_time('Y-m-d');?>" <?php echo isset($_POST['time']) && get_post_time('G:i:s')==$_POST['time']?' selected="selected"':'';?> >
                                                    <?php echo get_post_time('G:i:s a');?>
                                               </option>
                                               <?php
                                            endwhile;
                                        endif;
                                    ?>
                                </select>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <div class='input-group go'>
                                    <span class="btn btn-default btn-sm glyphicon glyphicon-search go" id="search-button"></span>
                                    &nbsp;<span class="btn btn-default btn-sm glyphicon glyphicon-refresh go" id="refresh-button"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <hr>
            <div class="content">
                <?php 
                //AND post_date LIKE '2017-06-29%' AND post_date LIKE "%09:54:43"
                if(isset($_POST['date']) && isset($_POST['time']) && $_POST['time'] !='0'){
                        //Variable statment here
                        $year = date("Y",strtotime($_POST['date']));
                        $month = date("m",strtotime($_POST['date']));
                        $day = date("d",strtotime($_POST['date']));
                        $hour = date("G",strtotime($_POST['time']));
                        $minute = date("i",strtotime($_POST['time']));
                        $second = date("s",strtotime($_POST['time']));
                        $query = array(
                            'post_type' => 'exchange',
                            'post_status' => 'publish',
                            'orderby' => 'post_date',
                            'order'=> 'DESC',
                            'date_query' => array(
                                array(
                                    'year'  =>$year,
                                    'month' =>$month,
                                    'day'   =>$day
                                ),
                                array(
                                    'hour'      =>$hour,
                                    'compare'   => '==',
                                ),
                                array(
                                    'minute'      =>$minute,
                                    'compare'   => '==',
                                ),
                                array(
                                    'second'      =>$second,
                                    'compare'   => '==',
                                ),
                            )
                        ); 
                }

                if(isset($_POST['date']) && isset($_POST['time']) && $_POST['time'] =='0'){
                        //Variable statment here
                        $year = date("Y",strtotime($_POST['date']));
                        $month = date("m",strtotime($_POST['date']));
                        $day = date("d",strtotime($_POST['date']));
                        $query = array(
                            'post_type' => 'exchange',
                            'post_status' => 'publish',
                            'orderby' => 'post_date',
                            'order'=> 'DESC',
                            'date_query' => array(
                                array(
                                    'year'  =>$year,
                                    'month' =>$month,
                                    'day'   =>$day
                                )
                            )
                        ); 
                }

                $result = new WP_Query($query);
                ?>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><?php _e('[:en]Currency[:kh]រូបិយប័ណ្ណ[:]')?></th>
                            <th><?php _e('[:en]Buying[:kh]ទិញ​[:]')?></th>
                            <th><?php _e('[:en]Selling[:kh]លក់[:]')?></th>
                            <th><?php _e('[:en]Posted Date[:kh]ថ្ងៃ​បង្កើត[:]')?></th>
                            <th><?php _e('[:en]Posted Time[:kh]ម៉ោងបង្កើត[:]')?></th>
                        </tr>
                    </head>
                    <tbody>
                        <?php
                        if( $result->have_posts() ):
                            while( $result->have_posts() ) : $result->the_post();
                                $usd_buy  = get_field('usd_buy');
                                $usd_sell = get_field('usd_sell');
                                $thb_buy  = get_field('thb_buy');
                                $thb_sell = get_field('thb_sell');
                                $post_date = get_post_time('d-M-Y');
                                $post_time = get_post_time('G:i:s a');
                            ?>
                            <tr>    
                                <td>
                                    <div class="flag">
                                    <img class="flag_usd" src="<?php bloginfo('template_url'); ?>/assets/icons/all_flags.png" alt="" title=""/>
                                    </div>
                                    USD
                                </td>
                                <td class="text-right"><?php echo $usd_buy;?></td>
                                <td class="text-right"><?php echo $usd_sell;?></td>
                                <td class="text-center"><?php echo $post_date;?></td>
                                <td class="text-center"><?php echo $post_time;?></td>
                            </tr>
                            <tr>    
                                <td>
                                    <div class="flag">
                                    <img class="flag_thb" src="<?php bloginfo('template_url'); ?>/assets/icons/thb_flag.png" alt="" title=""/>
                                    </div>
                                    THB
                                    </td>
                                <td class="text-right"><?php echo $thb_buy;?></td>
                                <td class="text-right"><?php echo $thb_sell;?></td>
                                <td class="text-center"><?php echo $post_date;?></td>
                                <td class="text-center"><?php echo $post_time;?></td>
                            </tr>
                    <?php 
                            endwhile;
                            wp_reset_postdata();
                        else:
                            echo '<tr>';
                                echo '<td colspan="5" class="text-center">';
                                    echo '<span style="color:red;">No data found.</span>';
                                echo '</td>';
                            echo '</tr>';
                        endif;
                        ?>
                    </tbody>
                </table>
            </div>

            <style>
                span.glyphicon.glyphicon-calendar,span.glyphicon-time,span.glyphicon-search{
                    color:#F2B01D;
                    cursor:pointer;
                }
                span.go, span.go:hover,span.go:active:focus{font-weight:bold;color:#FD7A03;}
            table.table>tbody>tr>td{
                    /*padding:3px 8px;*/
                    font-size:12px;
                }
                .flag {
                    position: relative;
                    display: inline-block;
                    width: 20px;
                    height: 14px;
                    overflow: hidden;
                    margin: 0 auto;
                }
                img.flag_usd{
                    left: -41px;
                    position: relative;
                }
                img.flag_thb{
                    left:0;
                    position: relative;
                    top:-3px;
                }
            </style>
        </div>
        <div role="tabpanel" class="tab-pane" id="graph">
            <script src="https://code.highcharts.com/highcharts.js"></script>
            <script src="https://code.highcharts.com/modules/exporting.js"></script>

            <!-- Additional files for the Highslide popup effect -->
            <script src="https://www.highcharts.com/media/com_demo/js/highslide-full.min.js"></script>
            <script src="https://www.highcharts.com/media/com_demo/js/highslide.config.js" charset="utf-8"></script>
            <link rel="stylesheet" type="text/css" href="https://www.highcharts.com/media/com_demo/css/highslide.css" />
            <script src="https://highcharts.github.io/export-csv/export-csv.js"></script>
            <br />
            <div class="row">
                <form action="#" method="post" id="exchange_search_graph" name="search_graph">
                    <div class="col-xs-6 col-md-4">
                        <div class="form-group">
                            <label>From</label>
                            <div class='input-group date_from'>
                                <input id='date_from' type='text' class="form-control" name="date_from"  value="<?php echo isset($_POST['date_from'])?$_POST['date_from']:''?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6  col-md-4">
                        <div class="form-group">
                            <label>To</label>
                            <div class='input-group date_to'>
                                <input  id='date_to' type='text' class="form-control" name="date_to" value="<?php echo isset($_POST['date_to'])?$_POST['date_to']:''?>"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3 col-md-2">
                        <div class="form-group">
                            <label>Currency</label>
                            <div class='input-group'>
                                <select class="form-control" name="currency" style="padding:0;margin:0;">
                                    <option value="USD" <?php echo isset($_POST['currency']) && $_POST['currency']=='USD' ?' selected="selected"':''?>>USD</option>
                                    <option value="THB" <?php echo isset($_POST['currency']) && $_POST['currency']=='THB' ?' selected="selected"':''?>>THB</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 col-md-2" style="padding-left:0;">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <div class='input-group go'>
                                    <input type="hidden" name="graph" value="1"/>
                                    <span class="btn btn-default btn-sm glyphicon glyphicon-search go" id="graph-search-button"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div id="container" style="min-width:400px; height: 400px; margin: 0 auto"></div>
            
            <script>
            Highcharts.setOptions({
                global: {
                    useUTC: false
                }
            });
            var maxval="4500";
            Highcharts.chart('container', {
                    chart: {
                        type: 'spline',//spline
                        zoomType: 'xy'
                    },
                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        x: 0,
                        y: 0
                    },
                    title: {
                        text:null //'Graph displaying PRASAC rates noted in <?php //echo isset($_POST['currency'])?$_POST['currency']:'USD'?>'
                    },
                    subtitle: {
                        text: null //'from  <?php //echo isset($_POST['date_from'])?$_POST['date_from']:date('Y-m-d'); echo isset($_POST['date_to'])?' to '.$_POST['date_to']:date('Y-m-d');?>'
                    },
                    xAxis: {
                        labels: {
                            formatter: function () {
                                return Highcharts.dateFormat('%d/%m/%y', this.value);
                            }
                        },
                        tickInterval:1 * 24 * 3600000, // 1 days

                    },
                    yAxis: {
                        title: {
                            text: '<b>KHR</b>'
                        },
                        min:<?php echo isset($_POST['currency']) && $_POST['currency']=='THB'?120:3950?>
                    },
                    tooltip: {
                        headerFormat: '<b>{series.name}</b><br>',
                        pointFormat: '{point.x:%d/%m/%Y}: {point.y:.2f} KHR'
                    },

                    plotOptions: {
                        spline: {
                            marker: {
                                enabled: true
                            }
                        },
                        series: {
                            point: {
                                events: {
                                    click: function (e) {
                                        hs.htmlExpand(null, {
                                            pageOrigin: {
                                                x: e.pageX || e.clientX,
                                                y: e.pageY || e.clientY
                                            },
                                            headingText: this.series.name+"<br />",
                                            maincontentText: Highcharts.dateFormat('%e/%m/%Y', this.x) + " : " + this.y + ' KHR',
                                            width: 200
                                        });
                                    }
                                }
                            }
                        }
                    },
                     exporting: {
                        enabled: false
                    },
                    //Date.UTC(year, month, day, hours, minutes, seconds, millisec)
                    series: [{
                        name:"<?php echo isset($_POST['currency']) && $_POST['currency']=='THB'?'THB Rates':'USD Rates';?>",
                        color:"<?php echo isset($_POST['currency']) && $_POST['currency']=='THB'?'#F2B01D':'#4DBB48';?>",
                        data: [
                            <?php 
                                $query_graph_buy = array(
                                    'post_type' => 'exchange',
                                    'post_status' => 'publish',
                                    'orderby' => 'post_date',
                                    'order'=> 'ASC',
                                    'posts_per_page'=>-1
                                ); 
                                if(isset($_POST['date_from']) && isset($_POST['date_to'])){
                                    $date_from = date("Y-m-d",strtotime($_POST['date_from']));
                                    $year = date("Y",strtotime($_POST['date_to']));
                                    $month = date("m",strtotime($_POST['date_to']));
                                    $day = date("d",strtotime($_POST['date_to']));
                                    $query_graph_buy = array(
                                        'post_type' => 'exchange',
                                        'post_status' => 'publish',
                                        'orderby' => 'post_date',
                                        'order'=> 'ASC',
                                        'posts_per_page'=>-1,
                                        'date_query' => array(
                                            'after'     =>$date_from,
                                            'before'    => array(
                                                'year'  =>$year,
                                                'month' =>$month,
                                                'day'   =>$day,
                                            ),
                                            'inclusive' => true
                                        )
                                    ); 
                                }

                                $result_graph_buy = new WP_Query($query_graph_buy);
                                if( $result_graph_buy->have_posts()):
                                    while( $result_graph_buy->have_posts() ) : $result_graph_buy->the_post();
                                         if(isset($_POST['currency'])){
                                            if($_POST['currency']=='USD'){
                                            ?>
                                              [Date.UTC(<?php echo get_post_time('Y')?>,<?php echo get_post_time('m')-1?>,<?php echo get_post_time('d')?>),<?php echo (get_field('usd_buy') + get_field('usd_sell'))/2;?>],
                                            <?php
                                            }else{
                                            ?>
                                              [Date.UTC(<?php echo get_post_time('Y')?>,<?php echo get_post_time('m')-1?>,<?php echo get_post_time('d')?>),<?php echo (get_field('thb_buy')+get_field('thb_sell'))/2;?>],
                                            <?php
                                            }
                                        }else{?>
                                            [Date.UTC(<?php echo get_post_time('Y')?>,<?php echo get_post_time('m')-1?>,<?php echo get_post_time('d')?>),<?php echo (get_field('usd_buy') + get_field('usd_sell'))/2;?>],
                                        <?php
                                        }
                                    endwhile;
                                endif;
                            ?>
                            ]
                    }]
                    // {
                    //      name: 'THB Rates',
                    //         color: '#F2B01D',
                    //         data: [
                    //         <?php 
                    //             $query_graph_sell = array(
                    //                 'post_type' => 'exchange',
                    //                 'post_status' => 'publish',
                    //                 'orderby' => 'post_date',
                    //                 'order'=> 'ASC',
                    //                 'posts_per_page'=>-1
                    //             ); 
                    //             if(isset($_POST['date_from']) && isset($_POST['date_to'])){
                    //                 $date_from = date("Y-m-d",strtotime($_POST['date_from']));
                    //                 $year = date("Y",strtotime($_POST['date_to']));
                    //                 $month = date("m",strtotime($_POST['date_to']));
                    //                 $day = date("d",strtotime($_POST['date_to']));
                    //                 $query_graph_sell = array(
                    //                     'post_type' => 'exchange',
                    //                     'post_status' => 'publish',
                    //                     'orderby' => 'post_date',
                    //                     'order'=> 'ASC',
                    //                     'posts_per_page'=>-1,
                    //                     'date_query' => array(
                    //                         'after'     =>$date_from,
                    //                         'before'    => array(
                    //                             'year'  =>$year,
                    //                             'month' =>$month,
                    //                             'day'   =>$day,
                    //                         ),
                    //                         'inclusive' => true
                    //                     )
                    //                 ); 
                    //             }
                    //             $result_graph_sell = new WP_Query($query_graph_sell);
                    //             if( $result_graph_sell->have_posts()):
                    //                 while( $result_graph_sell->have_posts() ) : $result_graph_sell->the_post();
                    //                     if(isset($_POST['currency'])){
                    //                         if($_POST['currency']=='USD'){
                    //                         ?>
                    //                             [Date.UTC(<?php //echo get_post_time('Y')?>,<?php //echo get_post_time('m')-1?>,<?php //echo get_post_time('d')?>),<?php //echo  get_field('usd_sell');?>],
                    //                         <?php
                    //                         }else{
                    //                         ?>
                    //                             [Date.UTC(<?php //echo get_post_time('Y')?>,<?php //echo get_post_time('m')-1?>,<?php //echo get_post_time('d')?>),<?php //echo get_field('thb_sell');?>],
                    //                         <?php
                    //                         }
                    //                     }else{?>
                    //                         [Date.UTC(<?php //echo get_post_time('Y')?>,<?php //echo get_post_time('m')-1?>,<?php //echo get_post_time('d')?>),<?php //echo  (get_field('thb_buy') + get_field('thb_sell'))/2;?>],
                    //                     <?php
                    //                     }
                    //                 endwhile;
                    //             endif;
                    //         ?>
                    //         ]
                    // }]
                });
            </script>
            <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.0/themes/base/jquery-ui.css" />
            <style>
                #ui-datepicker-div div.ui-datepicker-header{
                    background-image:none;
                    background-color:#F2B01D;
                    color:#555555;
                    font-size:12px;
                }
                .ui-datepicker table{
                    font-size:0.7em;
                }
                .ui-widget-header .ui-icon,.ui-icon, .ui-widget-content .ui-icon{
                    background-image:url("<?php bloginfo('template_url'); ?>/assets/icons/ui-icons_ffffff_256x240.png");
                }
                .ui-datepicker td span, .ui-datepicker td a{
                    text-align:center;
                    padding:0;
                }
                .ui-datepicker{
                    width:12em;
                }
                a.ui-state-default.ui-state-active{
                    background: #F2B01D;
                    color: #fff;
                }
                .draggable-header .highslide-heading{
                        position: absolute;
                        margin: -3px 0.4em;
                }
                
                .modal {
                    position: fixed;
                    width: 100%;
                    height: 100%;
                    top: 0;
                    left: 0;
                    background: rgba(0, 0, 0, 0.7);
                }
                .modal .chart {
                    height: 90%;
                    width: 90%;
                    max-width: none;
                }
                .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus{
                    color:#F2B01D;
                    border:1px solid #F2B01D;
                }
                .input-group-addon{
                    font-size:13px;
                }
                .highcharts-reset-zoom,.highcharts-button-box{
                    background-color:transparent;
                    fill:transparent;
                    stroke:none;
                }
                .highcharts-reset-zoom text{
                    color:#F2B01D !important;
                    fill:#F2B01D !important;
                }
                .highcharts-reset-zoom text :hover{
                    color:#4DB848 !important;
                    fill:#4DB848 !important;
                }
                /*button.ui-datepicker-current.ui-state-default{
                    font-size:13px;
                    background: #F2B01D;
                    color: #fff;
                    padding:0 2px;
                }*/
            </style>
            <script src="https://code.jquery.com/ui/1.10.0/jquery-ui.js"></script>
            <script>
                jQuery(function() {
                   datepickerSetting();
                   datepicker_date_time_Setting();
                   datepicker_graph();
                   
                });

                function datepickerSetting(){
                     //Holiday date list here
                    var holidays = [
                                    "2017-01-01","2017-01-07","2017-02-11","2017-03-08","2017-01-01",
                                    "2017-04-14","2017-04-15","2017-04-16","2017-05-01","2017-05-13",
                                    "2017-05-14","2017-05-15","2017-06-01",,"2017-06-18","2017-09-19","2017-09-20",
                                    "2017-09-21","2017-09-24","2017-15-15","2017-10-23","2017-10-29",
                                    ,"2017-11-02","2017-11-03","2017-11-04","2017-11-09","2017-12-10"
                                ];
                    jQuery("#date").datepicker({
                        dateFormat: 'yy-mm-dd',
                        autoclose: true,
                        changeYear:true,
                        changeMonth:true,
                        maxDate: '+0m',
                        yearRange: "-22:+0",
                        beforeShowDay: function(date){
                            var datestring = jQuery.datepicker.formatDate('yy-mm-dd', date);
                            var day = date.getDay();
                            if (day == 0 || day == 6) {
                                return [false, "weekendday"]
                            } else {
                                return [ holidays.indexOf(datestring) == -1 ]
                            }
                        }
                    });
                }

                function datepicker_date_time_Setting(){
                    if(jQuery("#date").val()==''){
                        jQuery("#date").datepicker('setDate',new Date());
                    }
                    //Click go 
                    jQuery("#search-button").click(function(e){
                        jQuery("form#exchange_search").submit();
                        return true;
                    });

                    jQuery( "#refresh-button" ).click(function() {
                        jQuery("form#exchange_search").trigger("reset");
                        location.reload(true);
                    });


                    //check time if date is not null
                    if(jQuery("#date").val()!=''){
                        var dateselected = jQuery("#date").val();
                        jQuery("#time option").each(function(){
                            var dateoftime  = jQuery(this).attr('date');
                            if(dateoftime != dateselected && dateoftime != '0'){
                                jQuery(this).hide();
                            }
                        });
                    }
                    //check time for date
                    jQuery("#date").change(function(){
                        var changedate = jQuery(this).val();
                        jQuery("#time option").each(function(){
                            var dateoftime  = jQuery(this).attr('date');
                            if(dateoftime == changedate || dateoftime == '0'){
                                jQuery(this).show();
                            }else{
                                jQuery(this).hide();
                            }
                        });
                    });
                }

                function datepicker_graph(){
                    //Holiday date list here
                    var holidays = [
                                    "2017-01-01","2017-01-07","2017-02-11","2017-03-08","2017-01-01",
                                    "2017-04-14","2017-04-15","2017-04-16","2017-05-01","2017-05-13",
                                    "2017-05-14","2017-05-15","2017-06-01",,"2017-06-18","2017-09-19","2017-09-20",
                                    "2017-09-21","2017-09-24","2017-15-15","2017-10-23","2017-10-29",
                                    ,"2017-11-02","2017-11-03","2017-11-04","2017-11-09","2017-12-10"
                                ];
                    jQuery("#date_from,#date_to").datepicker({
                        dateFormat: 'yy-mm-dd',
                        autoclose: true,
                        changeYear:true,
                        changeMonth:true,
                        maxDate: '+0m',
                        yearRange: "-22:+0",
                        beforeShowDay: function(date){
                            var datestring = jQuery.datepicker.formatDate('yy-mm-dd', date);
                            var day = date.getDay();
                            if (day == 0 || day == 6) {
                                return [false, "weekendday"]
                            } else {
                                //eturn [true, "businessday"]
                                return [ holidays.indexOf(datestring) == -1 ]
                            }
                        },
                    });

                    jQuery("#graph-search-button").click(function(e){
                        jQuery("#exchange_search_graph").submit();
                    });

                    //Set default date to text box
                    if(jQuery("#date_from").val()==''){
                        jQuery("#date_from").datepicker('setDate',new Date());
                    }
                    if(jQuery("#date_to").val()==''){
                        jQuery("#date_to").datepicker('setDate',new Date());
                    }
                }
            </script>
        </div>
    </div>
 </div>
        <div class="row">
            <h4 class="green"><?php _e('[:en]Remark :[:kh]សម្គាល់៖[:]')?></h4>
            <p>
                <?php _e('[:en]The above rates are subject to change. Please contact our branches for applicable rates when making a transaction.[:kh]អត្រា​ប្ដូរប្រាក់​ខាងលើ​អាច​មានការ​ប្រែប្រួល​គ្រប់ពេលវេលា​។​ ​សូម​ទាក់ទង​មក​សាខា​របស់​យើងខ្ញុំ​ ​ដើម្បី​ទទួល​បាន​អត្រា​ពិតប្រាកដ​ពេល​លោកអ្នក​ធ្វើ​ប្រតិបត្តិ​ការ​។​[:]')?>
            </p>
        </div>
    </div>
<div>

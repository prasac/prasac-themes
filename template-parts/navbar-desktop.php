<header id="desktop-header" style="background: #fff url(<?php echo get_field('header', 'option') ?>) top center no-repeat;">
    <div class="alert alert-warning" id="alert-wanring" role="alert" style="padding:5px;margin-bottom:0;border-radius:0;">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Warning!</strong> you are using internet explorer. please consider to change.
    </div>

    <section id="menu">
        <nav id="header" class="navbar fadeIn animated">
            <div id="header-container" class="navbar-container">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a id="brand" class="navbar-brand" href="<?php echo esc_url( home_url() ); ?>"></a>
                    </div>
                    <!-- <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs  header">
                        <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                        <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                    </ul> -->
                    <?php qtranxf_generateLanguageSelectCode(array('type'   => 'image')); ?>
                    <div id="navbar" class="collapse navbar-collapse">
                        <?php
                            wp_nav_menu( array(
                                    'menu'            => 'Header Menu', 
                                    'depth'           =>1,
                                    'container'       => '',
                                    'post_status' => 'publish',
                                    'menu_class'      => 'nav navbar-nav pull-right',
                                    'menu_id'         => 'header_menu')
                            );
                        ?>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container-fluid menu" id="menu-container">
            <div class="container">
                <a class="text-center prasac-logo" href="<?php echo esc_url( home_url() ); ?>" title="<?php bloginfo('name'); ?>">
                    <img src="<?php echo get_field('desk_logo', 'option') ?>" alt="<?php bloginfo('name'); ?>">
                </a>
                <ul id="desktop-menu" class="main-menu-wrapper">
                    <li class="drop-down services"><a href="#"><?php _e('[:en]Products & Services[:kh]ផលិតផលនិងសេវា[:]'); ?></a>
                        <div class="container-fluid sub-menu mega-menu fadeIn animated">
                            <div class="container ">
                                <div class="col-sm-3 col-md-3 service-item">
                                    <h3><a href="#"><?php _e('[:en]Loans[:kh]ឥណទាន[:]') ?></a></h3>
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/micro-loans/[:kh]/kh/services/loans/micro-loans/[:]') ?>"><?php _e('[:en]Micro Loan[:kh]ឥណ​ទាន​ខ្នាត​តូច​បំផុត[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/small-and-medium-loans/[:kh]/kh/services/loans/small-and-medium-loans/[:]') ?>"><?php _e('[:en]SME Loan[:kh]ឥណទានសហគ្រាស​ធុន​តូចនិងមធ្យម[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/personal-loans/[:kh]/kh/services/loans/personal-loans/[:]') ?>"><?php _e('[:en]Personal Loan[:kh]ឥណទានប្រើប្រាស់ផ្ទាល់ខ្លួន[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/motorbike-loans/[:kh]/kh/services/loans/motorbike-loans/[:]') ?>"><?php _e('[:en]Motorbike Loan[:kh]ឥណទានម៉ូតូ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/car-loans/[:kh]/kh/services/loans/car-loans/[:]') ?>"><?php _e('[:en]Car Loan[:kh]ឥណទានរថយន្ត​[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/home-improvement-loan/[:kh]/kh/services/loans/home-improvement-loan/[:]') ?>"><?php _e('[:en]Home Improvement Loan [:kh]ឥណទានកែលម្អគេហដ្ឋាន[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/housing-loan/[:kh]/kh/services/loans/housing-loan/[:]') ?>"><?php _e('[:en]Housing Loan [:kh]ឥណទានគេហដ្ឋាន[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/education-loans/[:kh]/kh/services/loans/education-loans/[:]') ?>"><?php _e('[:en]Education Loan[:kh]ឥណទានសិក្សាអប់រំ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/latrine-loans/[:kh]/kh/services/loans/latrine-loans/[:]') ?>"><?php _e('[:en]Latrine Loan[:kh]ឥណទានសាងសង់បង្គន់[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans//bio-gas-loans/[:kh]/kh/services/loans//bio-gas-loans/[:]') ?>"><?php _e('[:en]Biogas Loan[:kh]ឥណទានជីវឧស្ម័ន[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/emergency-loans/[:kh]/kh/services/loans/emergency-loans/[:]') ?>"><?php _e('[:en]Emergency Loan[:kh]ឥណទានបន្ទាន់[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/loans/group-loans/[:kh]/kh/services/loans/group-loans/[:]') ?>"><?php _e('[:en]Group Loan[:kh]ឥណទានក្រុម[:]') ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-3 col-md-3 service-item">
                                    <h3><a href="#"><?php _e('[:en]Deposits[:kh]សេវាបញ្ញើសន្សំ[:]') ?></a></h3>
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/deposits/savings-account/[:kh]/kh/services/deposits/savings-account/[:]') ?>"><?php _e('[:en]Savings Account[:kh]គណនីសន្សំ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/deposits/unfixed-deposit-account/[:kh]/kh/services/deposits/unfixed-deposit-account/[:]') ?>"><?php _e('[:en]Unfixed Deposit Account[:kh]គណនីចម្រើនគ្មានកាលកំណត់[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/deposits/flexi-growth-savings-account/[:kh]/kh/services/deposits/flexi-growth-savings-account/[:]') ?>"><?php _e('[:en]Flexi Growth Savings Account[:kh]គណនីចម្រើនតាមលំដាប់[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/deposits/savings-plan-account/[:kh]/kh/services/deposits/savings-plan-account/[:]') ?>"><?php _e('[:en]Savings Plan Account[:kh]គណនីសន្សំតាមផែនការ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/deposits/kid-growth-savings-account/[:kh]/kh/services/deposits/kid-growth-savings-account/[:]') ?>"><?php _e('[:en]Kid Growth Savings Account[:kh]គណនីកូនចម្រើន[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/deposits/term-deposit-account/[:kh]/kh/services/deposits/term-deposit-account/[:]') ?>"><?php _e('[:en]Term Deposit Account[:kh]គណនីបញ្ញើមានកាលកំណត់[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/deposits/employee-savings-account/[:kh]/kh/services/deposits/employee-savings-account/[:]') ?>"><?php _e('[:en]Employee Savings Account[:kh]គណនីសន្សំនិយោជិត[:]') ?></a></li>
                                    </ul>
                                    <h3><a href="#"><?php _e('[:en]Mobile &amp; Internet Banking[:kh]សេវាធនាគារចល័ត​[:]') ?></a></h3>
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/mobile-and-internet-banking/mobile-banking/[:kh]/kh/services/mobile-internet-banking/mobile-banking[:]') ?>"><?php _e('[:en]PRASAC Mobile Banking[:kh]ប្រាសាក់ម៉ូបាលប៊ែងឃីង[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/mobile-and-internet-banking/internet-banking/[:kh]/kh/services/mobile-internet-banking/internet-banking[:]') ?>"><?php _e('[:en]PRASAC Internet Banking[:kh]ប្រាសាក់អុីនធឺណិតប៊ែងឃីង[:]') ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-3 col-md-3 service-item">
                                    <h3><a href="#"><?php _e('[:en]Bill Payments[:kh]សេវាទូទាត់វិក្កយបត្រ[:]') ?></a></h3>
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/bill-payment/ppwsa-bill-payment/[:kh]/kh/services/bill-payment/ppwsa-bill-payment/[:]') ?>"><?php _e('[:en]PPWSA Bill Payment[:kh]ទូទាត់វិក្កយបត្រទឹក[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/bill-payment/edc-bill-payment/[:kh]/kh/services/bill-payment/edc-bill-payment/[:]') ?>"><?php _e('[:en]EDC Bill Payment[:kh]ទូទាត់វិក្កយបត្រភ្លើង[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/bill-payment/ppswma-bill-payment/[:kh]/kh/services/bill-payment/ppswma-bill-payment/[:]') ?>"><?php _e('[:en]PPSWMA Bill Payment[:kh]ទូទាត់វិក្កយបត្រសំរាមនិងសំណល់រឹង[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/bill-payment/westline-school/[:kh]/kh/services/bill-payment/westline-school/[:]') ?>"><?php _e('[:en]Westline School[:kh]សាលារៀនវ៉េស្ទឡាញន៍[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/bill-payment/northline-school/[:kh]/kh/services/bill-payment/northline-school/[:]') ?>"><?php _e('[:en]Northline School[:kh]សាលារៀនណត្សឡាញន៍[:]') ?></a></li>
                                    </ul>
                                    <h3><a href="#"><?php _e('[:en]Interbank Fund Transfer[:kh]សេវាផ្ទេរប្រាក់ឆ្លងធនាគារ[:]') ?></a></h3>
                                    <ul class="sub-menu">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/interbank-fund-transfer/real-time-fund-transfer/[:kh]/kh/services/interbank-fund-transfer/real-time-fund-transfer/[:]') ?>"><?php _e('[:en]Real Time Fund Transfer (RFT)[:kh]សេវាផ្ទេរមូលនិធិភ្លាមៗ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/interbank-fund-transfer/bakong/[:kh]/kh/services/interbank-fund-transfer/bakong/[:]') ?>"><?php _e('[:en]Bakong Payment Service[:kh]សេវា​ទូទាត់​បាគង[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/interbank-fund-transfer/fast-payment/[:kh]/kh/services/interbank-fund-transfer/fast-payment/[:]') ?>"><?php _e('[:en]Fast Payment[:kh]សេវាទូទាត់រហ័ស[:]') ?></a></li>
                                    </ul>
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/interbranch-fund-transfer/[:kh]/kh/services/interbranch-fund-transfer/[:]') ?>"><?php _e('[:en]Interbranch Fund Transfer[:kh]ផ្ទេរប្រាក់ឆ្លងសាខា[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/cash-by-code/[:kh]/kh/services/cash-by-code[:]') ?>"><?php _e('[:en]Cash-by-Code[:kh]ផ្ទេរប្រាក់តាមលេខសម្ងាត់​[:]') ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-3 col-md-3 service-item">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/css/[:kh]/kh/services/css/[:]') ?>"><?php _e('[:en]CSS[:kh]សេវា CSS[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/bank-confirmation/[:kh]/kh/services/bank-confirmation/[:]') ?>"><?php _e('[:en]Bank Confirmation[:kh]សេវា​បញ្ជាក់​សមតុល្យគណនី​[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/phone-top-up/[:kh]/kh/services/phone-top-up/[:]') ?>"><?php _e('[:en]Phone Top-Up[:kh]សេវាទិញកាតទូរស័ព្ទ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/exchange/[:kh]/kh/services/exchange/[:]') ?>"><?php _e('[:en]Foreign Exchange [:kh]សេវាប្តូរប្រាក់[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/standing-instruction/[:kh]/kh/services/standing-instruction/[:]') ?>"><?php _e('[:en]Standing Instruction[:kh]​សេវាបញ្ជា​ការ​ទូទាត់​អចិន្ត្រៃយ៍[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/payroll/[:kh]/kh/services/payroll/[:]') ?>"><?php _e('[:en]Payroll[:kh]សេវាបើកប្រាក់បៀវត្ស[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/atm/[:kh]/kh/services/atm/[:]') ?>"><?php _e('[:en]ATM[:kh]សេវាអេធីអឹម[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/cash-deposit-machine/[:kh]/kh/services/cash-deposit-machine/[:]') ?>"><?php _e('[:en]Cash Deposit Machine[:kh]ម៉ាស៊ីនដាក់ប្រាក់[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/pos/[:kh]/kh/services/pos/[:]') ?>"><?php _e('[:en]POS[:kh]ម៉ាស៊ីនឆូតកាត[:]') ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="drop-down"><a href="#"><?php _e('[:en]Reports[:kh]របាយការណ៍[:]') ?></a>
                        <div class="container-fluid sub-menu mega-menu fadeIn animated">
                            <div class="container sub-menu-ungrid-3-col">
                            <div class="col-sm-4 col-md-3">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/reports/social-performance-dashboard/[:kh]/kh/reports/social-performance-dashboard/[:]') ?>"><?php _e('[:en]Social Performance Dashboard [:kh]របាយការណ៍សង្ខេបលទ្ធផលសង្គម[:]') ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 col-md-3">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/reports/annual-report/[:kh]/kh/reports/annual-report/[:]') ?>"><?php _e('[:en]Annual Report [:kh]របាយការណ៍ប្រចាំឆ្នាំ[:]') ?></a></li>
                                    </ul>
                                </div>
                                 <div class="col-sm-4 col-md-3">
                                    <ul class="sub-menu-style-none">
                                         <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/reports/independence-report/[:kh]/kh/reports/independence-report/[:]') ?>"><?php _e('[:en]Independence Auditor\'s Report[:kh]របាយការណ៍របស់សវនករឯករាជ្យ[:]') ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="drop-down"><a href="#"><?php _e('[:en]Media[:kh]ព័ត៌មាន​[:]') ?></a>
                        <div class="container-fluid sub-menu mega-menu fadeIn animated">
                             <div class="container sub-menu-ungrid-4-col">
                                <div class="col-sm-2 col-md-2 media-item">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/media/news-and-event/[:kh]/kh/media/news-and-event/[:]') ?>"><?php _e('[:en]News &amp; Event[:kh]ដំណឹងនិងព្រឹត្តិការណ៍[:]') ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-2 col-md-2 media-item">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/media/sound-music/[:kh]/kh/media/sound-music/[:]') ?>"><?php _e('[:en]Sound &amp; Music[:kh]សំឡេងនិងតន្ត្រី[:]') ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-2 col-md-2 media-item">
                                    <ul class="sub-menu-style-none">
                                            <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/media/video/[:kh]/kh/media/video/[:]') ?>"><?php _e('[:en]Video[:kh]វីដេអូ[:]') ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-2 col-md-2 media-item">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/media/public-holiday/[:kh]/kh/media/public-holiday/[:]') ?>"><?php _e('[:en]Public Holiday[:kh]ថ្ងៃឈប់សម្រាក[:]') ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="drop-down "><a href="#"><?php _e('[:en]About PRASAC[:kh]ព័ត៌មានពីប្រាសាក់[:]') ?></a>
                        <div class="container-fluid sub-menu mega-menu fadeIn animated">
                            <div class="container sub-menu-ungrid-4-col">
                                <div class="col-sm-4 col-md-3 service-item">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/message-from-ceo/[:kh]/kh/about/message-from-ceo/[:]') ?>"><?php _e('[:en]Message From CEO[:kh]ចំណាប់អារម្មណ៍ប្រធាននាយកប្រតិបត្តិ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/vision-mission/[:kh]/kh/about/vision-mission/[:]') ?>"><?php _e('[:en]Vision &amp; Mission[:kh]ទស្សនវិស័យនិងបេសកកម្ម[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/profile/[:kh]/kh/about/profile/[:]') ?>"><?php _e('[:en]Profile [:kh]សាវតារ[:]') ?></a></li>
                                    </ul>
                                </div>

                                <div class="col-sm-3 col-md-3 service-item">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/milestones/[:kh]/kh/about/milestones/[:]') ?>"><?php _e('[:en]Key Milestones [:kh]ដំណាក់កាលសំខាន់នៃការអភិវឌ្ឍ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/organizational-chart/[:kh]/kh/about/organizational-chart/[:]') ?>"><?php _e('[:en]Organizational Chart[:kh]រចនាសម្ព័ន្ធ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/shareholders/[:kh]/kh/about/shareholders/[:]') ?>"><?php _e('[:en]Shareholders[:kh]ភាគទុនិក[:]') ?></a></li>
                                    </ul>
                                </div>

                                <div class="col-sm-3 col-md-3 service-item">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/board-of-directors/[:kh]/kh/about/board-of-directors/[:]') ?>"><?php _e('[:en]Board of Directors[:kh]ក្រុមប្រឹក្សាភិបាល[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/management/[:kh]/kh/about/management/[:]') ?>"><?php _e('[:en]Executive Committee [:kh]គណៈគ្រប់គ្រង[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/awards-recognition/[:kh]/kh/about/awards-recognition/[:]') ?>"><?php _e('[:en]Awards &amp;  Recognition[:kh]ពានរង្វាន់និងស្នាដៃ[:]') ?></a></li>
                                        
                                    </ul>
                                </div>
                                
                                <div class="col-sm-3 col-md-3 service-item">
                                    <ul class="sub-menu-style-none">
                                    <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/license/[:kh]/kh/about/license/[:]') ?>"><?php _e('[:en]License [:kh]អាជ្ញាប័ណ្ណ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/about/funding-partner/[:kh]/kh/about/funding-partner/[:]') ?>"><?php _e('[:en]Funding Partners [:kh]ដៃគូវិនិយោគ[:]') ?></a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>
                    </li>
                    <li class="drop-down"><a href="#"><?php _e('[:en]Contact us[:kh]ទំនាក់ទំនង[:]') ?></a>
                        <div class="container-fluid sub-menu mega-menu fadeIn animated">
                             <div class="container services">
                                <div class="col-sm-3 col-md-3 service-item">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/contact-us/head-office/[:kh]/kh/contact-us/head-office/[:]') ?>"><?php _e('[:en]Head Office[:kh]ការិយាល័យកណ្តាល[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/contact-us/customer-service/[:kh]/kh/contact-us/customer-service/[:]') ?>"><?php _e('[:en]Hotline Number[:kh]សេវាបម្រើអតិថិជន[:]') ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-3 col-md-3 service-item">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/contact-us/investor-relations/[:kh]/kh/contact-us/investor-relations/[:]') ?>"><?php _e('[:en]Investor Relations[:kh]​​ទំនាក់ទំនង​វិនិយោគិន[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/contact-us/branch-location/[:kh]/kh/contact-us/branch-location/[:]') ?>"><?php _e('[:en]Branch Locator[:kh]បណ្តាញការិយាល័យ[:]') ?></a></li>    
                                    </ul>
                                </div>
                                <div class="col-sm-3 col-md-3 service-item">
                                    <ul class="sub-menu-style-none">
                                    <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/contact-us/atm-location/[:kh]/kh/contact-us/atm-location/[:]') ?>"><?php _e('[:en]ATM Locator[:kh]បណ្តាញអេធីអឹម[:]') ?></a></li>
                                    <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/contact-us/cdm-location/[:kh]/kh/contact-us/cdm-location/[:]') ?>"><?php _e('[:en]CDM Locator[:kh]បណ្តាញម៉ាស៊ីនដាក់ប្រាក់[:]') ?></a></li>
                                    </ul>
                                </div>
                                <div class="col-sm-3 col-md-3 service-item">
                                    <ul class="sub-menu-style-none">
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/contact-us/library-location/[:kh]/kh/contact-us/library-location/[:]') ?>"><?php _e('[:en]Library Locator[:kh]ទីតាំង​បណ្ណាល័យ[:]') ?></a></li>
                                        <li><a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/contact-us/feedback/[:kh]/kh/contact-us/feedback/[:]') ?>"><?php _e('[:en]Feedback[:kh]មតិអតិថិជន[:]') ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="search-wraper">
                            <form class="navbar-form navbar-right " role="search" method="GET" action="<?= esc_url(home_url('/')) ?>">
                                <div id="custom-search-input">
                                    <div class="input-group col-md-12">
                                        <input id="search" type="text" name="s"  class="search-query form-control" placeholder="<?php _e('[:en]Search[:kh]ស្វែងរក[:]'); ?>" value="<?= get_search_query() ?>" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-danger submit-search" type="submit">
                                                <span class=" glyphicon glyphicon-search"></span>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
            <?php
            // wp_nav_menu( array(
            //         'menu'            => 'MainMenu', 
            //         'depth'           =>3,
            //         'container'       => '',
            //         'post_status' => 'publish',
            //         'menu_class'      => 'main-menu-wrapper',
            //         'menu_id'         => 'desktop-menu')
            // );
            ?>
        </div>

        
    </section>
</header>
<script> 
        jQuery(document).ready(function(){

                /**
                * This object controls the nav bar. Implement the add and remove
                * action over the elements of the nav bar that we want to change.
                *
                * @type {{flagAdd: boolean, elements: string[], add: Function, remove: Function}}
                */
                var myNavBar = {

                    flagAdd: true,

                    elements: [],

                    init: function (elements) {
                        this.elements = elements;
                    },

                    add : function() {
                        if(this.flagAdd) {
                            for(var i=0; i < this.elements.length; i++) {
                                document.getElementById(this.elements[i]).className += " fixed-theme";
                            }
                            this.flagAdd = false;
                        }
                    },

                    remove: function() {
                        for(var i=0; i < this.elements.length; i++) {
                            document.getElementById(this.elements[i]).className =
                                    document.getElementById(this.elements[i]).className.replace( /(?:^|\s)fixed-theme(?!\S)/g , '' );
                        }
                        this.flagAdd = true;
                    }

                };

                /**
                * Init the object. Pass the object the array of elements
                * that we want to change when the scroll goes down
                */
                myNavBar.init(  [
                    "header",
                    "header-container",
                    "brand"
                ]);

                /**
                * Function that manage the direction
                * of the scroll
                */
                function offSetManager(){

                    var yOffset = 0;
                    var currYOffSet = window.pageYOffset;

                    if(yOffset < currYOffSet) {
                        myNavBar.add();
                    }
                    else if(currYOffSet == yOffset){
                        myNavBar.remove();
                    }

                }

                /**
                * bind to the document scroll detection
                */
                window.onscroll = function(e) {
                    offSetManager();
                }

                /**
                * We have to do a first detectation of offset because the page
                * could be load with scroll down set.
                */
                offSetManager();

                var nav = jQuery('#menu-container');
                jQuery(window).scroll(function () {
                    if (jQuery(this).scrollTop() >90) {
                        nav.addClass(" fixed-menu");
                    } else {
                        nav.removeClass("fixed-menu");
                    }
                });
                
        });
</script>

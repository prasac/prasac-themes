<?php
$prev_post = get_adjacent_post(false, '', true);
$next_post = get_adjacent_post(false, '', false);
$cats = [];
foreach ( get_the_category($post->ID) as $c ) {
    $cat = get_category($c);
    array_push($cats, $cat->name);
}
if ( sizeOf($cats) > 0 ) {
    $post_categories = implode(', ', $cats);
}else if(get_post_type()=='people'){
    $post_categories = '[:en]Corporate Social Responsibility[:kh] កិច្ចការសង្គម[:]';
    $post_categories = __($post_categories);
}else{
    $post_categories = 'Not Assigned';
}

$images = get_field('blog_gallery');
?>


<article id="post-<?= $post->ID; ?>" <?php post_class('single-post'); ?> >
<?php if( get_post_type() == 'post'): ?>
        <header class="page-title">
            <h3 class="green"><?= get_the_title(); ?></h3>
            <ul class="single-post-meta list-unstyle">
                <li class="single-post-date"><i class="fa fa-calendar"></i> <?php _e('[:en]Posted on&nbsp[:kh]ផ្សព្វផ្សាយ&nbsp[:]'.get_post_time('F j, Y')); ?></li>
                <li class="single-post-tag"><?php _e('[:en]in&nbsp[:kh]នៅក្នុង&nbsp[:]'.$post_categories ); ?></li>
            </ul>
        </header>
        <section class="single-content">
            <?php 
            if($images) : ?>
                <div id="single-content-slider" style="margin-bottom:25px;">
                    <?php 
                    foreach( $images as $image ):
                    ?>
                        <figure class="cap-left">
                            <a href="<?= $image['url']; ?>" class="lightbox" >
                                <img class="img-responsive img-lazy" src="<?= $image['url']; ?>" alt="">
                            </a>
                        </figure>
                    <?php endforeach; ?>
                </div>
            <?php endif;
            the_content(); ?>
        </section>
    <?php 
    elseif( get_post_type() == 'merchant' ) : ?>
    <?php 
        echo get_field('address');
    ?>
<!-- Single Office -->
<?php elseif( get_post_type() == 'office' ) : ?>
        <header class="page-title">
            <h3><?= get_the_title(); ?></h3>
        </header>
        <?php
        $taxonomy = get_the_terms( $post->ID, 'office_categories' );
        $marker_map = '';
        if(count($taxonomy)==1 && $taxonomy[0]->slug=='atm-location'){// One Category only
            $marker_map = 'atm-location';
        }

        if(count($taxonomy)==1 && $taxonomy[0]->slug=='branch-location'){// One Category only
            $marker_map = 'atm-branch';
        }


        if(count($taxonomy)==2){//2 Categories
            $marker_map = 'atm-branch';
        }

        if(count($taxonomy) == 3){//3 Categories
            $marker_map = 'cdm-atm-branch';
        }

        if('library-location'== $taxonomy[0]->slug):
            echo get_the_content();
            if ( have_rows('gallery_slider') ):
            ?>
                <!-- Slick slider carousel for gallery -->
                <div class="row">
                    <div id="single-content" class="col-md-12">
                        <?php 
                            echo '<section class="library-item" id="single-content-slider">';
                            while ( have_rows('gallery_slider') ) : the_row();?>
                                    <figure class="cap-left">
                                        <a title="<?= get_sub_field('gallery_slider_caption') ?>" href="<?= get_sub_field('gallery_slider_image') ?>" class="lightbox" style="display:block;overflow:hidden;height:450px; background-image:url(<?= get_sub_field('gallery_slider_image')?>); background-position:center;background-size: cover;">
                                        </a>
                                    </figure>
                            <?php 
                            endwhile;
                            echo '</section>';
                        ?>
                    </div>
                </div>
                <br />
            <?php endif;
            
            ?>
            <span  id="latlng" data-latlng="<?php echo get_field('latitude').','.get_field('longitude'); ?>" data-title="<?= the_title(); ?>" data-desc='<?= the_content();?>'></span>
            <style>
                #single-content .slick-next, .slick-prev{
                    width:35px;
                    height:35px;
                    z-index:1;
                }
                #single-content .slick-next:before, .slick-prev:before{
                    font-size:2.5rem;
                }
               
                #single-content .slick-prev{
                    left:0;
                }

                #single-content .slick-next{
                    right:0;
                }
            </style>
            <?php 
        else:
        ?>
            <ul class="list-unstyle">
                <li><i class="fa fa-home" aria-hidden="true"></i>&nbsp<?= get_field('address'); ?></li>
                <?php if(get_field('phone')):?>
                    <li><i class="fa fa-phone" aria-hidden="true"></i>&nbsp<?= get_field('phone'); ?></li>
                <?php endif;?>
                <?php if(get_field('email')):?>
                    <li><i class="fa fa-envelope" aria-hidden="true"></i>&nbsp<a href="mailto:<?= get_field('email'); ?>"><?= get_field('email'); ?></a></li>
                <?php endif;?>
            </ul>
            <span data-slug="<?php echo $marker_map;?>" id="latlng" data-latlng="<?php echo get_field('latitude').','.get_field('longitude'); ?>" data-title="<?= the_title(); ?>" data-desc='<?php echo get_field('address');?>'></span>
        <?php 
        endif;
        ?>
        <div id="map-canvas" class="" style="width:auto; height: 500px;"></div>
    <?php endif; ?>
    <footer class="single-footer">
        <div class="row">
            <div class="col-xs-12 col-md-12">
            <hr>
                <div class="single-footer-share">
                    <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                    <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                    <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo get_permalink();?>','twitter');"></a>
                    <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo get_permalink();?>','linkedin');"></a>
                    <div class="fb-like pull-right" data-href="<?php echo get_permalink();?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="false"></div>
                </div>
            </div>
        </div>
    </footer>
</article>
<?php if( is_page('exchange')) :
     ?>
     </section>
     <section class="col-xs-12 col-sm-9 col-md-7">
       <?php the_title('<h3 class="green">','</h3>') ?>
       <?php  echo the_content();?>
        <div class="wrapper" style="background-color: rgba(234, 234, 234, 0.79); padding:10px 10px;overflow:hidden;">
            <?php get_template_part('template-parts/exchange', 'board'); ?>
            <?php get_template_part('template-parts/exchange_rate_graph', 'board'); ?>
        </div>
      </section>
      <section class="col-xs-12 col-sm-6 col-md-2">
        <?php  get_template_part('page-template/currency', 'convertor'); ?>
     </section> 
      <section <?= post_class('col-xs-12 col-sm-12 col-md-9'); ?> >
     <?php
    elseif(is_page('feedback')):
          ?>
          <?php the_title('<h3 class="green">','</h3>') ?>
          <div class="row">
            <?= do_shortcode('[contact-form-7 id="3953" title="Contact form"]'); ?>
        </div>
    <?php
    elseif(is_page('head-office')):
    $images = get_field('side_bar_banner');
        ?>
        <?php the_title('<h3 class="green">','</h3>') ?>
           <section class="single-content">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <?php echo the_content();?>
                    </div>
                </div>
            </section>
             <div class="clearfix"></div>
            <h3 class="green"><?php _e('[:en]Find us on Map[:kh]ស្វែង​រក​យើង​​នៅលើ​ផែន​ទី[:]');?></h3>
            <div class="map-responsive">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3909.1778282941887!2d104.90778731533491!3d11.539097991806779!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x310950e1582d4d57%3A0x82e73de59efb4c26!2sPRASAC+%7C+Head+Office!5e0!3m2!1sen!2skh!4v1494479605193" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div> 
        <?php
    elseif( is_page('sound-music') ):
    ?>
    <?php the_title('<h3 class="green">','</h3>') ?>
            <iframe width="100%" height="450" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/199402825&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
    <?php elseif( is_page('video') ): ?>
    <?php the_title('<h3 class="green">','</h3>') ?>
        <div class="row">
        <?php 
        /* 
        * Paginate Advanced Custom Field repeater
        */
        if( get_query_var('page') ) {
            $page = get_query_var( 'page' );
        } else {
            $page = 1;
        }
        
        // Variables
        $row              = 0;
        $images_per_page  = 15; // How many images to display on each page
        $images           = get_field('flexible_content');
        $total            = count( $images );
        $pages            = ceil( $total / $images_per_page );
        $min              = ( ( $page * $images_per_page ) - $images_per_page ) + 1;
        $max              = ( $min + $images_per_page ) - 1;
        //ACF Loop
        if( have_rows( 'flexible_content' ) ) :
            while( have_rows('flexible_content') ) : the_row();
                $url = get_sub_field('url');
                $title = get_sub_field('title');
                $row++;

                // Ignore this image if $row is lower than $min
                if($row < $min) { continue; }
            
                // Stop loop completely if $row is higher than $max
                if($row > $max) { break; }

                echo '<div class="accordion-'.get_row_index().' col-xs-12 col-sm-6 col-md-4 post-title" style="margin-bottom:15px;">';
                    echo '<div class="thumbnail">';
                        echo '<a class="video" href="https://www.youtube.com/watch?v='.youtube_id_from_url($url).'?autoplay=1&controls=1&showinfo=0">
                                    <img src="https://img.youtube.com/vi/'.youtube_id_from_url($url).'/mqdefault.jpg" alt="YouTube" width="100%">
                                    <i class="fa fa-3x fa-youtube-play text-center"></i>
                                </a>';
                        echo '<div class="caption">
                                    <h5 class="text-center youtube-title">
                                        <a href="https://www.youtube.com/embed/'.youtube_id_from_url($url).'?autoplay=1&amp;controls=1&amp;showinfo=0">'.mb_strimwidth($title, 0,30, '...').'</a>
                                    </h5>
                                </div>';
                    echo '</div>';
                echo '</div>';
            endwhile;
        endif;
        wp_reset_postdata();
        // Pagination
        echo '<div class="col-xs-12 col-md-12 video-pagination text-center">';
            echo paginate_links(array(
                'base' => get_permalink() . '/%#%' . '/',
                'format' => '?page=%#%',
                'current' => $page,
                'total' => $pages
            ));
        echo '</div>';
       ?>
      </div>
      <style>
        .video-pagination .page-numbers:not(.next):not(.prev){
            background-color:#4db848;
            color:#ffffff;
            padding: 7px 12px;
            border-radius: 50%;
        }
        div.video-pagination span.page-numbers.current{
            border:#4db848 1px solid !important;
            background-color:transparent !important;
            color:#4db848 !important;
        }
        div.video-pagination a.next,div.video-pagination a.prev{
            padding: 7px 12px;
            background-color:#4db848;
            color:#ffffff;
        }
        div.video-pagination{
            margin-bottom:2rem;
        }
    </style>
   <?php
   elseif(is_page('site-map')):
        the_title('<h3 class="green text-left">','</h3>');
        ?>
        <?php
        wp_nav_menu( array(
                'menu'            => 'site-map',
                'depth'           =>4,
                'container'       => false,
                'menu_class'      => 'site-map list',
                'menu_id'         => 'menu-sitemap')
        );
    ?>
            <style type="text/css">
                ul#menu-sitemap>li:nth-child(1), ul#menu-sitemap>li:nth-child(2), ul#menu-sitemap>li:nth-child(3), ul#menu-sitemap>li:nth-child(4), ul#menu-sitemap>li:nth-child(5), ul#menu-sitemap>li:nth-child(6){
                    padding:0;
                    margin:0;
                    width:33.33%;
                }
                a{
                    color:#333;
                }
                a:hover{
                    color:#4DB848;
                }
                ul#menu-sitemap>li{
                    float:left;
                }
                ul#menu-sitemap>li .sub-menu{
                    padding:5px 0;
                    margin:0;
                    padding-left:20px !important;
                }
                ul#menu-sitemap > li.menu-item-has-children >a{
                    font-size:1.1em;
                    pointer-events: none;
                    cursor: default;
                    font-weight:600;
                    color: #5cb85c;
                }
                ul#menu-sitemap > li>a{
                    font-size:1.1em;
                    font-weight:600;
                }                
            </style>
        <?php
    elseif(is_page('news-and-event'))://Page list news and events
        the_title('<h3 class="green text-left">','</h3>');

        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
        query_posts(array(
            'post_type' => 'post', // You can add a custom post type if you like
            'paged' => $paged,
            'post_status' => 'publish',
            'orderby' => 'post_date',
            'order'=> 'DESC',
            'posts_per_page' =>5 // limit of posts
        ));

        if ( have_posts() ) :
            while ( have_posts() ) : the_post();?>
                <article class="col-xs-12 col-md-12">
                    <div class="c-hub-entry PP">
                        <a class="c-hub-entry__image-wrapper" href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title();?>">
                            <!-- Header -->
                            <div class="c-hub-entry__image">
                                <?php if( function_exists('featured_image') ) echo featured_image('medium', true, 'img-lazy c-dynamic-image'); ?>
                            </div>
                        </a>
                        <!-- Body -->
                        <div class="c-hub-entry__body">
                            <div class="c-hub-entry__title green">
                                <a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php echo get_the_title(); ?></a>
                            </div>
                            <p class="article_date">
                                <i class="fa fa-calendar danger"></i>&nbsp;&nbsp;<?php _e(get_post_time('F j, Y - g:i a')); ?>
                            </p>
                            <p class="hidden-xs"><?= mb_strimwidth(get_the_excerpt(), 0,201, '...'); ?></p>
                            <p class="visible-xs"><?= mb_strimwidth(get_the_excerpt(), 0,50, '...'); ?></p>
                            <div class="c-content hidden-xs text-right"><a class="btn btn-sm btn-default" href="<?= get_permalink(); ?>"><?php _e('[:en]read more[:kh]អានបន្ត[:]'); ?>&nbsp<i class="fa fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </article>
            <?php
            endwhile; 
            echo '<div class="col-xs-12 col-md-12 video-pagination text-center">';
            post_pagination();
            echo '</div>';
            wp_reset_query();
        else :
            echo 'No Data';
        endif;
        ?>
        <style>
            .video-pagination .page-numbers:not(.next):not(.prev){
                background-color:#4db848;
                color:#ffffff;
                padding: 7px 12px;
                border:#4db848 1px solid !important;
            }
            div.video-pagination span.page-numbers.current,.video-pagination .page-numbers:hover{
                border:#4db848 1px solid !important;
                background-color:transparent !important;
                color:#4db848 !important;
            }
            div.video-pagination a.next,div.video-pagination a.prev{
                padding: 7px 12px;
                background-color:#4db848;
                color:#ffffff;
            }
            .video-pagination{
                margin-bottom:2rem;
            }
        </style>
        <?php
    else:
        echo the_title( sprintf( '<h3 class="green">', esc_url( get_permalink() ) ), '</h3>' );
        echo the_content();
    endif;
?>
    <div class="row">
        <div class="col-xs-12 col-md-12">
        <hr>
            <div class="single-footer-share">
                <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo get_permalink();?>','twitter');"></a>
                <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo get_permalink();?>','linkedin');"></a>
                <div class="fb-like pull-right" data-href="<?php echo get_permalink();?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="false"></div>
            </div>
        </div>
    </div>
</div>
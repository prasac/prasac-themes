<div class="no-content col-sm-12 col-md-12">
  <h2 class="text-center"><i class="fa fa-frown-o fa-5x" aria-hidden="true"></i></h2>
  <h2 class="text-center"><?php _e('[:en]Sorry, We couldn\'t find any content here.[:kh]សូមអភ័យ យើង​ខ្ញុំ​រក​មិន​ឃើញអត្ថបទទីនេះទេ!​[:]');?></h2>
</div>
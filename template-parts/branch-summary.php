<?php
  $field_name = "type";
  $values = get_field($field_name);
  $field = get_field_object($field_name);
  $terms = get_the_terms( get_the_ID(), 'office_categories' );
  foreach( $terms as $term ) {
    $atm = $term->slug == 'atm-location';
    $branch = $term->slug == 'branch-location';
  }
  $phone_number = get_field('phone');
?>

<li id="post-<?php the_ID(); ?>" class="flex-item">
  <address class="flex-content">
    <section class="poster-card_body">
      
      <?php if( has_term( 'pos-location', 'office_categories' ) ): ?>
      <a href="<?= get_permalink(); ?>"><?php echo featured_image('medium' ,true ,true ,''); ?></a>
      <?php else : ?>
      <a href="<?= get_permalink(); ?>">
        <img class="img-responsive" src="https://maps.googleapis.com/maps/api/staticmap?center=<?= get_field('latitude'); ?>,<?= get_field('longitude'); ?>&zoom=18&size=350x200&markers=color:green%7CLabel:%7C<?= get_field('latitude'); ?>,<?= get_field('longitude'); ?>&key=AIzaSyBfp_TqwVnb9olDkRP7BxL6738pR-kcnLE">
      </a>
      <?php endif; ?>

      <h4 class="green" style="margin-top:10px;">
        <a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
      </h4>

      <table class="flex-address">
        <tr>
          <td class="title"><i class="fa fa-lg fa-map-marker" aria-hidden="true"></i></td>
          <td class="text"><?= get_field('address'); ?></td>
        </tr>
        <?php if( $phone_number ) : ?>
        <tr>
          <td class="title"><i class="fa fa-lg fa-phone" aria-hidden="true"></i></td>
          <td class="text"><?= $phone_number; ?></td>
        </tr> 
        <?php endif; ?>     
      </table>
    </section>

    <footer class="poster-card_footer">
<?php if(get_field('shorturl')):?><a href="<?= get_field('shorturl'); ?>" class="pull-left" target="_bank"><?php _e('[:en]View On Map[:kh]មើលផែនទី[:]') ?></a><?php endif;?>
        <a href="<?php echo get_permalink(); ?>"  class="pull-right"><?php _e('[:en]View Detail[:kh]ព័ត៌មានលម្អិត[:]') ?></a>
    </footer>
  </address>
</li>

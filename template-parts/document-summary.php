<?php
error_reporting(0);
$file_kh = get_field('khmer_doc');
$file_en = get_field('english_doc');

$kh_doc = $file_kh['url'];
$en_doc = $file_en['url'];

$filesize_en = filesize( get_attached_file( $file_en['ID'] ) );
$en_doc_size = size_format($filesize_en, 2);

$filesize_kh = filesize( get_attached_file( $file_kh['ID'] ) );
$kh_doc_size = $filesize_kh?size_format($filesize_kh, 2):$en_doc_size;

$thumbnail = 'thumbnail_kh';
if ( qtranxf_getLanguage() == 'en' ) {
  $file_size = "File size:&nbsp;$en_doc_size";
  $thumbnail = 'thumbnail_en';
} else {
  $file_size = "ទំហំឯកសារ៖&nbsp;$kh_doc_size";
}
$imag_path = get_field($thumbnail);
global $post;
if($imag_path){
  $cover = '<a href="'.$imag_path.'" class="lightbox"><img src="'.$imag_path.'" class="img-responsive img-thumbnail"/></a>';
}else{
  $cover = get_the_post_thumbnail($post->ID, 'medium', array( 'class' => 'img-responsive img-thumbnail' ) );
}
?>
  <div class="col-sm-3 col-md-3 text-center report-cover">
    <?php echo $cover; ?>
    <h5><?= get_the_title(); ?></h5>
    <span>
    <?= $file_size; ?>
    </span>
    <ul class="list-inline">
    <?php if( $file_kh  ) { ?>
    <li><a class="pdf" href="<?= $kh_doc; ?>"><i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp;<?php _e('[:en]Khmer[:kh]ភាសាខ្មែរ[:]'); ?></a></li>
    <?php } ?>
    <?php if( $file_en  ) { ?>
    <li><a class="pdf" href="<?= $en_doc; ?>"><i class="fa fa-cloud-download" aria-hidden="true"></i>&nbsp;<?php _e('[:en]English[:kh]អង់គ្លេស[:]'); ?></a></li>
    <?php } ?>
    </ul>          
  </div>
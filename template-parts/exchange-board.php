<?php
$args = array(
    'post_type'        => 'exchange',
    'posts_per_page'    => 1,
    'post_status'      => 'publish',
    'suppress_filters' => true
);
$loop = new WP_Query( $args );
if( $loop->have_posts() ):
    while( $loop->have_posts() ): $loop->the_post();
        $usd_buy  = number_format(get_field('usd_buy'));
        $usd_sell = number_format(get_field('usd_sell'));
        $thb_buy  = get_field('thb_buy');
        $thb_sell = get_field('thb_sell');
        $post_date = get_post_time('j F Y - g:i a');
    endwhile;
    $buy = get_field('usd_buy') / get_field('thb_sell');
    $sell = get_field('usd_sell') / get_field('thb_buy');
    ?>
    <table class="table custom-table table-bordered table-condensed" style="margin-bottom:5px;">
        <thead>
            <tr>
                <th class="text-left"><?php _e('[:en]Currency[:kh]រូបិយប័ណ្ណ[:]');?></th>
                <th class="text-center"><?php _e('[:en]Buy[:kh]ទិញ[:]');?></th>
                <th class="text-center"><?php _e('[:en]Sell[:kh]លក់[:]');?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-left">USD/KHR</td>
                <td class="text-center"><?= $usd_buy; ?></td>
                <td class="text-center" class="text-right"><?= $usd_sell; ?></td>
            </tr>
            <tr>
                <td class="text-left">THB/KHR</td>
                <td class="text-center"><?= $thb_buy; ?></td>
                <td class="text-center"><?= $thb_sell; ?></td>
            </tr>
            <tr>
                <td class="text-left">USD/THB</td>
                <td class="text-center"><?php printf("%.2f", $buy); ?></td>
                <td class="text-center"><?php printf("%.2f", $sell) ?></td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="text-left"><i class="fa fa-calendar"></i> <?= $post_date; ?></td>
            </tr>
        </tfoot>
    </table> <!-- Exchange Rate Table -->
<?php endif; ?>
<!-- <?php if( get_locale() == 'en_US' ):?>
    <p>Please <a href="/prasac/services/exchange/">click here</a> to check currency convertor.</p>
<?php else: ?>
    <p>សូម<a href="/prasac/services/exchange/">ចុចទីនេះ​</a>​សម្រាប់​ចូលប្រើប្រាស់កម្មវិធីគណនាប្រាក់​ប្ដូរ</p>
<?php endif;?> -->

<?php
/**
 * Template Name: Merchant
 *
 * @package WordPress
 * @subpackage growing2gether
 * @since Growing2Gether 1.0.0
 */
error_reporting(0);
error_reporting(E_ALL);
 get_header();
    if ( have_rows('flexible_content') ) :
        while ( have_rows('flexible_content') ) : the_row();
            if ( get_row_layout() == 'page_link' ) :
                $post_id = get_sub_field_object('link')['value']->ID;
                echo '<div class="container-fluid">';?>
                            <img class="img-responsive" src="<?php echo get_sub_field('photo')?>" alt=""  onClick='window.open("<?php echo get_permalink($post_id)?>","_self");'>
                            <?php
                echo '</div>';
            endif;
        endwhile;
       wp_reset_postdata();
    endif;
?>
<div id="body" class="container-fluid">
    <div class="container">
        <!-- Body -->
        <?php             
            global $post;
            $post_tag = isset($_GET['tag'])?$_GET['tag']:'prasac-scan-pay';
            $location_tag = isset($_GET['province'])?$_GET['province']:false;
            $categories = isset($_GET['category'])?$_GET['category']:false;
            global $paged;
            $paged = (get_query_var('paged')) ? get_query_var('paged'):1;
        ?>

        <section id="content" class="col-sm-12 col-md-9 content">
            <div class="clearfix"></div>
                <?php             
                if(!$location_tag || $location_tag =='0'){
                    $args = array(
                        'post_type' => 'merchant', // You can add a custom post type if you like
                        'paged' => $paged,
                        'post_status' => 'publish',
                        'orderby' => 'rand',
                        'order'=> 'DESC',
                        'posts_per_page' =>30, //limit of posts
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'merchant_tag',
                                'field' => 'slug',
                                'terms' =>$post_tag
                            ),
                        ),
                    );
                }
                if($location_tag !='0'){
                    $args = array(
                        'post_type' => 'merchant', // You can add a custom post type if you like
                        'paged' => $paged,
                        'post_status' => 'publish',
                        'orderby' => 'rand',
                        'order'=> 'DESC',
                        'posts_per_page' =>30, //limit of posts
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'merchant_tag',
                                'field' => 'slug',
                                'terms' =>$post_tag
                            ),
                            array(
                                'taxonomy' => 'locations',
                                'field' => 'slug',
                                'terms' => $location_tag
                            ),
                        ),
                    );
                }

                // $loop = new WP_Query($args);
                query_posts($args);
                ?>
                    <?php
                        $term = get_term_by('slug',$post_tag, 'merchant_tag'); 
                        echo '<div class="row" style="padding:5px;">';
                        echo '<h3 class="green">'.$term->name.'</h3>';
                        echo term_description( $post->id, 'document_category');
                        echo '</div>';
                    ?>
                    <div class="row" style="padding:5px;">
                        <div class="col-xs-12 col-md-12 search-partner-box">
                            <form action="" method="get">
                                <div class="col-xs-8 col-md-4">
                                    <?php
                                    $terms  = array(
                                        'taxonomy'     =>'locations',
                                        'post_type'    => 'merchant',
                                        'order_by' => 'name',
                                        'order' => 'ASC',
                                        'tax_query'=> array(
                                            array(
                                                'taxonomy'     =>'locations',
                                                'hide_empty'   => 1,
                                            )
                                        )
                                    );
                                    $taxonomies = get_terms($terms);
                                    if ( !empty($taxonomies) ) :
                                        echo '<select class="form-control" name="province">';
                                        echo '<option value="0">';
                                        _e('[:kh]ទាំងអស់[:en]All[:]');
                                        echo '</option>';
                                        foreach( $taxonomies as $location ) {
                                            if($location->count>1){
                                                echo '<option';
                                                echo isset($_GET['province'])  && $_GET['province']==$location->slug?' selected':'';
                                                echo ' value="'.esc_attr($location->slug).'">'.$location->name.'</option>';
                                            }
                                        }
                                        echo '</select>';
                                    endif;
                                    ?>
                                </div>
                                <!-- <div class="col-xs-4 col-md-4">
                                    <?php 
                                        // $terms  = array(
                                        //     'taxonomy'     =>'merchant_categories',
                                        //     'post_type'    => 'merchant',
                                        //     'order_by' => 'name',
                                        //     'order' => 'ASC',
                                        //     'tax_query'=> array(
                                        //         array(
                                        //             'taxonomy'     =>'merchant_categories',
                                        //         )
                                        //     )
                                        // );
                                        // $taxonomies = get_terms($terms);
                                        // if ( !empty($taxonomies) ) :
                                        //     echo '<select class="form-control" name="category">';
                                        //     echo '<option value="0">';
                                        //     _e('[:kh]ទាំងអស់[:en]All[:]');
                                        //     echo '</option>';
                                        //     foreach( $taxonomies as $category_list) {
                                        //         echo '<option';
                                        //         echo isset($_GET['category'])  && $_GET['category']==$category_list->slug?' selected':'';
                                        //         echo ' value="'.esc_attr($category_list->slug).'">'.$category_list->name.'</option>';
                                        //     }
                                        //     echo '</select>';
                                        // endif;
                                    ?>
                                </div> -->
                                <div class="col-xs-4 col-md-4">
                                    <input type="hidden" name="tag" value="<?php echo isset($_GET['tag'])?$_GET['tag']:'';?>"/>
                                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> <?= _e('[:kh]ស្វែងរក[:en]Search[:]');?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php
                if (have_posts() ):
                    echo '<div class="row">';
                    $count =1;
                    $i=0;
                    while(have_posts() ):the_post();
                        global $post;
                        $terms = get_terms('merchant_tag');
                        $post_tag = isset($_GET['tag'])?$_GET['tag']:'prasac-scan-pay';
                        $share_url = get_permalink().'?tag='.$post_tag;
                        if($terms){
                            $share_url =  get_permalink().'?tag='.$term->slug;
                        }
                        
                        $getLat = get_field('latitude');
                        $getLng  = get_field('longitude');
                        $post_title = get_the_title(); 
                        $thumb_id =  get_post_thumbnail_id();
                        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
                        if ( has_post_thumbnail() ) {
                            $fealture_image = '<img src="'.$thumb_url_array[0].'" class="img-lazy img-responsive" alt="" title=""/>';
                        } else{
                            $fealture_image = '';
                        }
                        $image_link = $thumb_url_array[0]?$thumb_url_array[0]:'';

                        echo '<article class="col-xs-12 col-sm-6 col-md-4 col-lg-4 fadeIn animated animate-item scan-pay-item">
                                <div class="box-content">
                                    <div class="item">
                                        <a href="'.$image_link.'" title="'.get_the_title().'" class="merchant-image lightbox">
                                            '.$fealture_image.'
                                        </a>
                                    </div>
                                    <h4 class="indent-text">
                                        <a href="'.get_permalink().'?tag='.$term->slug.'" class="title-two-row-text" title="'.get_the_title().'">'.get_the_title().'</a>
                                    </h4>
                                    <p class="indent-text address des-two-row-text">'.get_the_content().'</p>';
                                    echo '<p class="indent-text" style="padding:14px 10px;height:3rem;color:#1A73E8;">';
                                    ?>
                                    <a href="javascript:shareSocial('<?php echo $share_url;?>','facebook');" style="color:#333333;"><?= _e('[:en]Share[:kh]ចែករំលែក[:]')?></a> <a class="fa fa-facebook single-share-fb-icon" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                                    <?php
                                    if($getLat !='' && $getLng !=''):
                                        echo '&nbsp;&nbsp;<a class="direction" style="color:#333333;" href="https://www.google.com/maps/dir/?api=1&destination='.$getLat.','.$getLng.'&travelmode=driving" target="_blank"" >';
                                        echo _e('[:en]Direction[:kh]ទិសដៅ[:]').'&nbsp;';    
                                        echo '<img src="'.get_template_directory_uri().'/assets/images/direction-icon.png" width="22" />';
                                        echo '</a>';
                                    endif;
                                    echo '</p>';?>
                                    <?php
                            echo '
                                </div>
                            </article>';
                        $count++;
                        $i++;
                    endwhile;
                    echo '</div><div class="row">';
                    echo '<div class="video-pagination text-center">';
                        post_pagination();
                    echo '</div>';
                    wp_reset_query();
                else :
                    echo '<div class="col-xs-12 col-md-12"><p style="color:red;">No Partner found!</p></div>';
                endif;
            ?>
        </section>
        <aside class="col-xs-12 col-sm-3 hidden-xs">
            <div class="sidebar">
                <div class="sidebar" style="text-align:center !important;">
                    <div class="sidebar_title"><?= _e('[:en]Scan-Pay Partner[:kh]​ដៃគូ​ស្កេន​-​ទូទាត់[:]');?></div>
                    <?php 
                    global $post;
                    $terms = get_terms('merchant_tag');
                    $post_tag = isset($_GET['tag'])?$_GET['tag']:'prasac-scan-pay';
                    $share_url = get_permalink().'?tag='.$post_tag;
                    if($terms){
                        echo '<ul class="sidebar_content list text-left">';
                            foreach ($terms as $term){
                                $active_class = $post_tag==$term->slug? ' active':'';
                                echo "<li class='page_item text-left'><a class='$active_class' href='/prasac/scan-pay-partner?tag=$term->slug'>$term->name</a></li>";
                            }
                        echo '</ul>';
                       $share_url =  get_permalink().'?tag='.$term->slug;
                    }
                    ?>
                    <br />
                    <a href="https://www.prasac.com.kh/services/loans/small-and-medium-loans">
                        <embed class="html5-content" src="<?php echo site_url();?>/wp-content/uploads/html5/250x600/index.html" width="300" height="650" style="border:none;"></embed>
                        <style>
                            .html5-container {
                                background:transparent;
                                border:none;
                                text-align:center;
                                margin:15px 0;
                            }
                        </style>
                    </a>
                </div>
            </div>
        </aside>
        <div class="row">
            <div class="col-xs-12 col-md-9">
            <hr>
                <div class="single-footer-share">
                            
                    <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                    <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo  $share_url; ?>','facebook');"></a>
                    <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo  $share_url;?>','twitter');"></a>
                    <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo  $share_url;?>','linkedin');"></a>
                    <div class="fb-like pull-right" data-href="<?php echo $share_url;?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="false"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
<style>
    .title-two-row-text {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp:1;
        /* number of lines to show */
        -webkit-box-orient: vertical;
        line-height:2;
    }
    .des-two-row-text {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp:2;
        /* number of lines to show */
        -webkit-box-orient: vertical;
        line-height:1.5;
    }
    .box-content{
        border:1px solid rgba(75, 184, 72, 0.25)
    }
    .indent-text{
        padding:0 10px;
        margin:0;
    }
    .indent-text a.single-share-fb-icon{
        padding: 3px 6px;
        background:#1a73e8a8;
        color: #fff;
        border-radius: 50%;
    }
    .indent-text a.single-share-fb-icon:hover{
        background:#1a73e8;
    }
    
    .box-content .item{
        left: 0;
        top: 0;
        position: relative;
        overflow: hidden;
        -webkit-transition: .8s;
    }
    .box-content .item img{
        -webkit-transition: 0.6s ease;
        transition: 0.6s ease;
        
    }
    .box-content .item img:hover{
        -webkit-transform: scale(1.2);
        transform: scale(1.2);

    }
    h4.indent-text{
        font-weight:bold;
        font-size:14px;
        color:#1A73E8;
    }
    h4.indent-text a:hover{
        text-decoration:underline;
    }
    .direction{
        color:#1A73E8;
    }
    ul#merchant_category{ 
        height: 30px;
        width: 200px;
        border: 1px #4db848 solid;
        background-color:#ffffff;
        display: list-item; 
    }
    ul#merchant_category li.init{
        position:relative;
    }
    ul#merchant_category li { width:200px; padding: 5px 10px; z-index: 2; float:left;}
    ul#merchant_category li:not(.init) {width: 200px; display: list-item; color: #4db848;background:#ffffff;margin:2px; padding:2px;cursor:pointer;}
    ul#merchant_category li:not(.init):hover, ul#merchant_category li.selected:not(.init) {color: #4db848;}
    li.init { cursor: pointer; }

        .item-list p{
            font-size:14px;
            line-height:1.5;
        }
        .merchant-title {
            font-size:16px;
            margin:0;
            padding:0;
        }
        .slick-next:before, .slick-prev:before{
            color:#f2b01d;
        }

		.clickable{
		    cursor: pointer;   
		}
        .panel-warning>.panel-heading{
            position:relative;
        }

		.panel-heading div {
			top:0;
            position: absolute;
            right: 12px;
            line-height:0;
		}
		.panel-heading div span{
			margin:0;
		}
        .panel-body a{
            color:#393318;
        }
        .panel-body a:before{
            font-size:15px;
            color:red;
        }
        .panel-body a:hover{
            color:#4db848;
        }

        .clickable {
            display: inline-block;
            padding: 0;
            border-radius:0;
            cursor: pointer;
            font-size:2.5rem;
        }
        table#dev-table tbody tr, table#dev-table tbody tr td,table{
            border:0;
        }
        /* table tbody tr:nth-child(even) {background: #f2b01d0d;} */
        table tbody tr:nth-child(odd) {background: #4bb8480f;}
        .panel-primary{
            border:0;
        }
        table tbody tr td p{
            margin:0;
        }
        .panel-primary>.panel-heading{
            background-color:#ffffff;
            border:0;
        }
        .glyphicon{
            color:#4db848;
        }

        
        .radio input,
        .checkbox input {
            opacity: 0;
            position: absolute;
            z-index: 1;
            cursor: pointer;
            margin-left: -20px;
        }
        .radio input:checked + label::before,
        .checkbox input:checked + label::before {
        border-color: #9575cd;
        }
        .radio input:checked + label::after,
        .checkbox input:checked + label::after {
            content: '';
            display: inline-block;
            position: absolute;
            width: 13px;
            height: 13px;
            left: 2px;
            top: 4px;
            margin-left: -20px;
            border: 1px solid #9575cd;
            border-radius: 50%;
            background-color: #9575cd;
        }
        .radio label,
        .checkbox label {
            display: inline-block;
            position: relative;
            padding-left: 5px;
        }
        .radio label::before,
        .checkbox label::before {
            content: '';
            display: inline-block;
            position: absolute;
            width: 17px;
            height: 17px;
            left: 0;
            top: 2px;
            margin-left: -20px;
            border: 1px solid #ccc;
            border-radius: 50%;
            background-color: #fff;
        }
        .radio.disabled label,
        .checkbox.disabled label {
        color: #ccc;
        }
        .radio.disabled label::before,
        .checkbox.disabled label::before {
            opacity: 0.54;
            border-color: #ccc;
        }
        .checkbox input:checked + label::before {
            border-color: #d58512;
            background-color: #d58512;
        }
        .checkbox input:checked + label::after {
            content: "\f00c";
            font-family: FontAwesome;
            font-size: 13px;
            color: #fff;
            top: 0;
            left: 1px;
            border-color: transparent;
            background-color: transparent;
        }
        .checkbox label::before {
            border-radius: 2px;
        }
        .merchant-category{
            margin:2px 0;
        }

</style>
<script>
    jQuery(window).scroll(function(){
        if (jQuery(window).scrollTop() == jQuery(document).height() - jQuery(window).height()){
            if(jQuery(".pagenum:last").val() <= jQuery(".rowcount").val()) {
                var pagenum = parseInt(jQuery(".pagenum:last").val()) + 1;
                getresult('getresult.php?page='+pagenum);
            }
        }
    });

    function getresult(url) {
        jQuery.ajax({
            url: url,
            type: "GET",
            data:  {rowcount:$("#rowcount").val()},
            beforeSend: function(){
                $('#loader-icon').show();
            },
            complete: function(){
                $('#loader-icon').hide();
            },
            success: function(data){
                $("#faq-result").append(data);
            },
            error: function(){} 	        
        });
    }
</script>
<script>
    jQuery(document).ready(function(e){
        jQuery('select#merchant_category').on('change', function() {
            var merchantID = this.value;
            if(0==merchantID){
                jQuery("table#qr-code-merchant tbody tr").fadeIn('slow');
            }else{
                jQuery("table#qr-code-merchant tbody tr").css('display','none');
                jQuery("table#qr-code-merchant tbody tr."+merchantID).fadeIn('slow');
            }
        });
        jQuery('table.table tr td:nth-child(2)').each(function () {
            var rowID = jQuery(this).attr('class');
            if(jQuery(this).find('p').length <= 1){
                jQuery('table tbody tr td.'+rowID+' p.mainaddress a.moreaddress').hide();
            }
        });

        jQuery('.mainaddress .moreaddress').on('click',function(e){
            e.preventDefault();
            var rowID = jQuery(this).parents().parents().attr('class');
            var moreaddressClass = jQuery(this).data('moreaddclass');
            var thisrow = jQuery(this);
            jQuery('p.'+moreaddressClass).fadeToggle('slow', function() {
                if (jQuery(this).is(":visible")) {
                    jQuery(thisrow).text('<?php echo _e($less_address);?>');                
                } else {
                    jQuery(thisrow).text('<?php echo _e($more_address);?>');                
                }        
            });
        });

        jQuery("ul#merchant_category").on("click", ".init", function() {
            jQuery(this).closest("ul#merchant_category").children('li:not(.init)').toggle();
        });

        var allOptions = jQuery("ul#merchant_category").children('li:not(.init)');
        jQuery("ul#merchant_category").on("click", "li:not(.init)", function() {
            allOptions.removeClass('selected');
            jQuery(this).addClass('selected');
            jQuery("ul#merchant_category").children('.init').html(jQuery(this).html());
            allOptions.toggle();
        });


    });
</script>
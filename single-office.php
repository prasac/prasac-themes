<?php get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <aside class="hidden-xs col-xs-12 col-sm-3 col-md-3" id="category_sidebar_top">
                <div class="sidebar hidden-xs">
                    <div class="sidebar_title"><?php _e('[:en]Contact Us[:kh]ទំនាក់ទំនង[:]'); ?></div>
                        <ul class="sidebar_content list" id="accordion">
                            <?php wp_list_pages('sort_column=menu_order&post_status=publish&title_li=&child_of=428&echo=1'); ?>
                        </ul>
                </div>
            </aside>
            <section class="col-xs-12 col-sm-9 col-md-9">
                <div class="hidden-xs hidden-sm">
                    <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                </div>
                <?php
                if( have_posts() ):
                    while( have_posts() ): the_post();
                        get_template_part('template-parts/content');
                    endwhile;
                else :
                    get_template_part('template-parts/content', 'none');
                endif;
                ?>
            </section>
            <aside class="visible-xs col-xs-12 col-sm-3 col-md-3" id="category_sidebar_bottom"></aside>
        </main>
    </div>
</div>
<?php get_footer();
?>

<script>
    jQuery(document).ready(function(){
        var element = jQuery(this);
        var map, infowindow;
        function initialize(myCenter) {
            var single_page_slug = jQuery("#latlng").data('slug');
            var imgPoint = "https://www.prasac.com.kh/wp-content/uploads/2021/02/marker-atm.png";
            console.log(single_page_slug);
            if(single_page_slug=='atm'){
                imgPoint = "https://www.prasac.com.kh/wp-content/uploads/2021/02/marker-atm.png";
            }

            if(single_page_slug=='atm-branch' || single_page_slug=='cdm-atm-branch'){
                imgPoint = "https://www.prasac.com.kh/wp-content/uploads/2021/02/marker-branchatm.png";
            }

            var image = new google.maps.MarkerImage(imgPoint);
            var marker = new google.maps.Marker({
                position: myCenter,
                icon: image,
                animation: google.maps.Animation.DROP,
        });

        var mapProp = {
            center: myCenter,
            zoom:16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);
        marker.setMap(map);
        var infoWindow = new google.maps.InfoWindow({maxWidth:500}), marker, i;
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker) {
                return function() {
                    if (marker.getAnimation() != null) {
                        marker.setAnimation(null);
                    } else {
                        marker.setAnimation(google.maps.Animation.BOUNCE);
                    }
                    var datalatlng = jQuery("#latlng").data('latlng');
                    var contentString = '<b class="green" style="font-size:16px;">'+ jQuery("#latlng").data('title') +'</b><br /><br /><p>'+ jQuery("#latlng").data('desc')+'</p><br /><a class="infowindow_link" href="https://www.google.com/maps/dir/?api=1&destination='+datalatlng+'&travelmode=driving" target="_blank" alt="Get direction from your current location" title="Get direction from your current location"><img src="<?php echo get_template_directory_uri() ?>/assets/images/direction-icon.png" width="25" /> ';
                    contentString += "<?php echo _e('[:en]  Get Direction[:kh]  ទិសដៅ[:]');?>";
                    contentString += '</a>';
                    infoWindow.setContent(contentString);
                    infoWindow.open(map, marker);
                    map.setZoom(18);
                    map.setCenter(marker.getPosition());
                }
            })(marker));
        };
        var data = jQuery("#latlng").data('latlng').split(',');
        initialize(new google.maps.LatLng(data[0], data[1]));

        // jQuery(".infowindow_link").live('click',function(e){
        //     window.location.href=jQuery(this).attr('href');
        // });
    });
</script>
<style>
.gm-style{
    font-family:Hanuman,Roboto;
}
.infowindow_link:hover{
    /* text-decoration:underline; */
    color:red;
}
<style>
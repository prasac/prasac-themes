<?php
/*
* template name: Construction
*/
get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <section id="content" class="col-xs-12 col-md-12 content">
                <article class="text-center under-construction jumbotron">
                    <?php
                        if (have_posts()):
                        while (have_posts()): the_post();
                            the_content();
                        endwhile;
                        endif;
                    ?>
                </article>
            </section>
        </main>
    </div>
</div>
<?php get_footer(); ?>

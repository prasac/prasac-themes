<?php 
error_reporting(0);
/*
* template name: Deposit Calculation form
*/
get_header();
?>
<style>
    h3{
        color:#4DB848;
    }
   .form-horizontal .control-label{
        text-align:left;
        margin-left:0px;;
        font-weight:normal;
    }
    .input-group form-control-placeholder{
        text-align:right;
    }
    .form-group .form-control{
        font-weight:600;
        font-size: 1.1rem;
        height:2.6rem;
    }
    .bt-layout{
        border:1px solid #dedede;
        border-radius:3px;
        border-top-right-radius: 0;
        border-top-left-radius: 0;
        /* background-color: #f0f0f2; */
    }
    input[readonly].generate-i{
        background-color:#ffffff;
    }
    .deposit-calculator>h3{
        border:none;
    }
    button#btn-calculate {
        margin-top:15px;
        border-radius:0;
        border:none;
    }
    .green-background{
        color:#ffffff;
        background-color:#4DB848;
        font-size:2rem;
        font-weight:500;
    }
    .loan-calculator .has-feedback .form-control-feedback{
        top: 0;
        right: 15px;    
    }
    .green-back{
        color:#ffffff;
        background-color:#4DB848;
        font-size:1.3rem;
        font-weight:500;
        padding:5px 0;
        margin-bottom:10px;
    }
    .green-back p{
        font-size:1.2rem;
        font-weight:normal;
        color:#ffea00;
        margin-bottom:0px;
    }
    select:invalid{ 
        color: gray;
        font-style:italic; 
    }
    .bt-layout .sub-layout{
        background-color:#f0f0f2;
        padding:0 15px 15px;
    }
    .capital-asset{
        font-size:4rem;
        line-height: 2.1;
        text-align:center;
        margin-bottom:3rem;
    }
    .capital-asset .currency-type{
        font-size:2rem;
    }
    .text-center>p{
        margin:10px 0;
    }
    hr{
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .number-lg {
        border: none;
        text-align: center;
        font-size: 4rem;
        height: 4rem;
        background-color: transparent !important;
        box-shadow: none;
        color: #000000;
        font-weight: 600;
    }
    .number-md {
        border: none;
        text-align: center;
        font-size: 2rem;
        background-color: transparent !important;
        box-shadow: none;
    }
    .label-md{
        line-height:2.5rem;
    }
    button#btn-calculate{
        font-family:Roboto,Khmer OS Muol Light;
        font-weight:lighter !important;
        font-size:1.3rem;
    }
    .result-box{
        padding:15px;
        /* background-color:#ffffff; */
    }
    table.table{
        margin-bottom:0 !important;
    }
</style>
<div class="col-xs-12 col-md-12 col-lg-12 visible-xs"></div>
<div class="container-fluid">
    <main class="container">
        <section <?= post_class('col-xs-12 col-sm-9 col-md-9 page-title'); ?> >
            <div class="hidden-xs hidden-sm">
                    <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                    <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                        <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                        <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                    </ul>
            </div>
            <?php the_title('<h3 class="green">','</h3>') ?>
                <fieldset class="bt-layout">
                    <form class="deposit-calculator" action="" method="post" id="calculate_form">
                        <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6 sub-layout">
                            <!--==== ប្រភេទ​រូបិយប័ណ្ណ ​======-->
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label required col-lg-12 col-md-12 col-sm-12 col-xs-12" for="currency_type"><?php _e('[:en]Currency[:kh]រូបិយប័ណ្ណ​[:]') ?></label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <select class="form-control" name="currency_type" required id="currency_type">
                                                    <option value="KHR" <?php echo isset($_POST['currency_type']) && $_POST['currency_type']=='KHR'?' selected="selected"':''?>><?php _e('[:en]KHR[:kh]ប្រាក់រៀល[:]') ?></option>
                                                    <option value="USD" <?php echo !isset($_POST['currency_type'])?' selected="selected"':'';?>  <?php echo isset($_POST['currency_type']) && $_POST['currency_type']=='USD'?' selected="selected"':''?>><?php _e('[:en]USD[:kh]ប្រាក់​ដុល្លាអាមេរិក[:]') ?></option>
                                                    <option value="THB" <?php echo isset($_POST['currency_type']) && $_POST['currency_type']=='THB'?' selected="selected"':''?>  data-label="<?php _e('[:en]Average Monthly Net Interest[:kh]ការប្រាក់ប្រចាំខែជាមធ្យមបន្ទាប់ពីបង់ពន្ធរួច[:]') ?>"><?php _e('[:en]THB[:kh]ប្រាក់​បាត[:]') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label required col-lg-12 col-md-12 col-sm-12 col-xs-12" for="deposit_size"><?php _e('[:en]Deposit  amount[:kh]ទឹកប្រាក់បញ្ញើ[:]') ?></label>
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                <input type="text" id="deposit_size" min="100" value="<?php echo isset($_POST['deposit_size'])?$_POST['deposit_size']:100000?>" name="deposit_size" percentag="50" class="form-control currency"​ placeholder="00.00">
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 required" for="deposit_option"><?php _e('[:en]Interest withdrawal option[:kh]ជម្រើសនៃការទទួលបានការប្រាក់[:]') ?></label>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <select id="deposit_opt" name="deposit_option" class="form-control" required>
                                                    <option value="1" <?php echo isset($_POST['deposit_option']) && $_POST['deposit_option']==1?' selected="selected"':'';?> 
                                                    data-label1="<?php _e('[:en]Total Interest[:kh]ការប្រាក់សរុប[:]');?>" data-label2="<?php  _e('[:en]Total Withholding Tax [:kh]ពន្ធកាត់ទុកសរុប[:]');?>" data-label3="<?php _e('[:en]Total Net Interest[:kh]ការប្រាក់សុទ្ធសរុប[:]') ?>"><?php _e('[:en]Maturity[:kh]បញ្ចប់កិច្ចសន្យា[:]') ?></option>
                                                    <option value="2" <?php echo isset($_POST['deposit_option']) && $_POST['deposit_option']==2?' selected="selected"':'';?>
                                                    data-label1="<?php _e('[:en]Average Monthly Interest[:kh]ការប្រាក់ប្រចាំខែជាមធ្យម[:]') ?>" data-label2="<?php _e('[:en]Average Monthly Withholding tax[:kh]ពន្ធកាត់ទុកប្រចាំខែជាមធ្យម[:]') ?>" data-label3="<?php _e('[:en]Net Monthly Interest in Average[:kh]ការប្រាក់សុទ្ធប្រចាំខែជាមធ្យម[:]') ?>"><?php _e('[:en]Monthly[:kh]ប្រចាំខែ​[:]') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 required" for="deposit_term"><?php _e('[:en]Deposit term in month[:kh]រយៈពេលបញ្ញើសន្សំជាខែ[:]') ?></label>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <select id="td_term" class="form-control" name="deposit_term">
                                                    <option value="1,4.00,4.00" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='1,4.00,4.00'?' selected="selected"':'';?>><?php _e('[:en]1 Month[:kh]1 ខែ[:]') ?></option>
                                                    <option value="2,4.25,4.15" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='2,4.25,4.15'?' selected="selected"':'';?>><?php _e('[:en]2 Months[:kh]2 ខែ[:]') ?></option>
                                                    <option value="3,5.50,5.25" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='3,5.50,5.25'?' selected="selected"':'';?>><?php _e('[:en]3 Months[:kh]3 ខែ[:]') ?></option>
                                                    <option value="4,5.75,5.50" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='4,5.75,5.50'?' selected="selected"':'';?>><?php _e('[:en]4 Months[:kh]4 ខែ[:]') ?></option>
                                                    <option value="5,6.00,5.75" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='5,6.00,5.75'?' selected="selected"':'';?>><?php _e('[:en]5 Months[:kh]5 ខែ[:]') ?></option>
                                                    <option value="6,6.25,6.00" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='6,6.25,6.00'?' selected="selected"':'';?>><?php _e('[:en]6 Months[:kh]6 ខែ[:]') ?></option>
                                                    <option value="7,6.30,6.10" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='7,6.30,6.10'?' selected="selected"':'';?>><?php _e('[:en]7 Months[:kh]7 ខែ[:]') ?></option>
                                                    <option value="8,6.40,6.15" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='8,6.40,6.15'?' selected="selected"':'';?>><?php _e('[:en]8 Months[:kh]8 ខែ[:]') ?></option>
                                                    <option value="9,6.50,6.25" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='9,6.50,6.25'?' selected="selected"':'';?>><?php _e('[:en]9 Months[:kh]9 ខែ[:]') ?></option>
                                                    <option value="10,6.60,6.35" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='10,6.60,6.35'?' selected="selected"':'';?>><?php _e('[:en]10 Months[:kh]10 ខែ[:]') ?></option>
                                                    <option value="11,6.70,6.45" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='11,6.70,6.45'?' selected="selected"':'';?>><?php _e('[:en]11 Months[:kh]11 ខែ[:]') ?></option>
                                                    <option value="12,8.00,7.75" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='12,8.00,7.75'?' selected="selected"':'';?>><?php _e('[:en]12 Months[:kh]12 ខែ[:]') ?></option>
                                                    <option value="18,8.00,7.75" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='18,8.00,7.75'?' selected="selected"':'';?>><?php _e('[:en]18 Months[:kh]18 ខែ[:]') ?></option>
                                                    <option value="24,8.00,7.75" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='24,8.00,7.75'?' selected="selected"':'';?>><?php _e('[:en]24 Months[:kh]24 ខែ[:]') ?></option>
                                                    <option value="36,8.00,7.75" class="1" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='36,8.00,7.75'?' selected="selected"':'';?>><?php _e('[:en]36 Months[:kh]36 ខែ[:]') ?></option>

                                                    <option value="1,4.00,4.00" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='1,4.00,4.00'?' selected="selected"':'';?>><?php _e('[:en]1 Month[:kh]1 ខែ[:]') ?></option>
                                                    <option value="2,4.15,4.25" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='2,4.15,4.25'?' selected="selected"':'';?>><?php _e('[:en]2 Months[:kh]2 ខែ[:]') ?></option>
                                                    <option value="3,5.25,5.50" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='3,5.25,5.50'?' selected="selected"':'';?>><?php _e('[:en]3 Months[:kh]3 ខែ[:]') ?></option>
                                                    <option value="4,5.50,5.75" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='4,5.50,5.75'?' selected="selected"':'';?>><?php _e('[:en]4 Months[:kh]4 ខែ[:]') ?></option>
                                                    <option value="5,5.75,6.00" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='5,5.75,6.00'?' selected="selected"':'';?>><?php _e('[:en]5 Months[:kh]5 ខែ[:]') ?></option>
                                                    <option value="6,6.00,6.25" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='6,6.00,6.25'?' selected="selected"':'';?>><?php _e('[:en]6 Months[:kh]6 ខែ[:]') ?></option>
                                                    <option value="7,6.10,6.30" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='7,6.10,6.30'?' selected="selected"':'';?>><?php _e('[:en]7 Months[:kh]7 ខែ[:]') ?></option>
                                                    <option value="8,6.15,6.40" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='8,6.15,6.40'?' selected="selected"':'';?>><?php _e('[:en]8 Months[:kh]8 ខែ[:]') ?></option>
                                                    <option value="9,6.25,6.50" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='9,6.25,6.50'?' selected="selected"':'';?>><?php _e('[:en]9 Months[:kh]9 ខែ[:]') ?></option>
                                                    <option value="10,6.35,6.60" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='10,6.35,6.60'?' selected="selected"':'';?>><?php _e('[:en]10 Months[:kh]10 ខែ[:]') ?></option>
                                                    <option value="11,6.45,6.70" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='11,6.45,6.70'?' selected="selected"':'';?>><?php _e('[:en]11 Months[:kh]11 ខែ[:]') ?></option>
                                                    <option value="12,7.75,8.00" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='12,7.75,8.00'?' selected="selected"':'';?>><?php _e('[:en]12 Months[:kh]12 ខែ[:]') ?></option>
                                                    <option value="18,7.75,8.00" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='18,7.75,8.00'?' selected="selected"':'';?>><?php _e('[:en]18 Months[:kh]18 ខែ[:]') ?></option>
                                                    <option value="24,7.75,8.00" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='24,7.75,8.00'?' selected="selected"':'';?>><?php _e('[:en]24 Months[:kh]24 ខែ[:]') ?></option>
                                                    <option value="36,7.75,8.00" class="2" <?php echo isset($_POST['deposit_term'])&&$_POST['deposit_term']=='36,7.75,8.00'?' selected="selected"':'';?>><?php _e('[:en]36 Months[:kh]36 ខែ[:]') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <!--==== អាត្រាការប្រាក់ ======-->
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 required" for="interest_rate"><?php _e('[:en]Interest rate per year[:kh]អត្រាការប្រាក់ប្រចាំឆ្នាំ[:]') ?></label>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" id="interest_rate" required value="<?php echo isset($_POST['interest_rate'])?$_POST['interest_rate']:''?>" name="interest_rate" class="form-control generate-i"  percentag="25" placeholder="00.00">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="form-control-placeholder control-label col-lg-12 col-md-12 col-sm-12 col-xs-12 required" for="tax" ><?php _e('[:en]Withholding tax[:kh]ពន្ធលើការប្រាក់[:]') ?></label>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <select class="form-control" name="tax" required >
                                                    <!--<option></option>-->
                                                    <option value="6" <?php echo isset($_POST['tax']) && $_POST['tax']=='6'?' selected="selected"':''?>><?php _e('[:en]Resident[:kh]និវាសនជន[:]') ?></option>
                                                    <option value="14" <?php echo isset($_POST['tax']) && $_POST['tax']=='14'?' selected="selected"':''?>><?php _e('[:en]Non-resident[:kh]អនិវាសនជន[:]') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                    <button type="submit" name="btn-submit" class="btn btn-success  btn-lg btn-block" id="btn-calculate" data-lable="<?php _e('[:en]CALCULATE[:kh]គណនា[:]') ?>" data-loading-text="<i class='fa fa-spinner fa-spin '></i> <?php _e('[:en]In Processing...[:kh]កំពុងគណនា...[:]') ?>" ><?php _e('[:en]CALCULATE[:kh]គណនា[:]') ?></button>
                                </div>
                            </div>
                        </div>
                     </form>
                    <div class="col-xs-12 col-ms-12 col-md-6 col-lg-6">
                        <div class="row">
                            <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12 result-box">
                                <div class="row maturity-row">
                                    <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12 text-center label-1">
                                        <?php _e('[:en]Total Interest[:kh]ការប្រាក់សរុប[:]');?>
                                    </div>
                                    <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12 text-center">
                                        <span id='total-label1' class='number-md'>0.00</span>
                                    </div>  
                                </div>
                                <div class="row maturity-row">
                                    <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12 text-center label-2">
                                        <?php  _e('[:en]Total Withholding Tax [:kh]ពន្ធកាត់ទុកសរុប[:]');?>
                                    </div>
                                    <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12 text-center">
                                        <span id='total-label2' class='number-md'>0.00</span>
                                    </div>  
                                </div>
                                <div class="row maturity-row">
                                    <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12">
                                        <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12 text-center label-3">
                                            <?php _e('[:en]Total Net Interest[:kh]ការប្រាក់សុទ្ធសរុប[:]') ?>
                                        </div>
                                        <div class="col-xs-12 col-ms-12 col-md-12 col-lg-12 text-center">
                                            <span id='total-label3' class='number-lg'>0.00</span>
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" value="" name="maturity_value_1" id="maturity_value_1" />
                                <input type="hidden" value="" name="maturity_value_2" id="maturity_value_2" />
                                <input type="hidden" value="" name="maturity_value_3" id="maturity_value_3" />

                                <input type="hidden" value="" name="monthly_value_1" id="monthly_value_1" />
                                <input type="hidden" value="" name="monthly_value_2" id="monthly_value_2" />
                                <input type="hidden" value="" name="monthly_value_3" id="monthly_value_3" />

                            </div>  
                        </div>
                    </div>
                </fieldset><br />
                <?php
                    date_default_timezone_set("Asia/Phnom_Penh");
                    $d1=$d2=0;   
        
                    function dateDiff($date1, $date2){
                        $date1_ts = strtotime($date1);
                        $date2_ts = strtotime($date2);
                        $diff = $date2_ts - $date1_ts;
                        return round($diff / 86400);
                    }

                    function cal_days_in_year($year){
                        $days=0; 
                        for($month=1;$month<=12;$month++){ 
                            $days = $days + cal_days_in_month(CAL_GREGORIAN,$month,$year);
                         }
                     return $days;
                    }

                    $number_of_day_in_year = cal_days_in_year(date("Y"));
                    
                    if(isset($_POST['btn-submit'])){
                        $total_day                  =   0;
                        $maturity_total_interests   =   0;
                        $maturity_taxibilitys       =   0;
                        $maturity_interest_earneds  =   0;
                        $av_monthly_interest        =   0;
                        $av_monthly_wht             =   0;
                        $av_net_monthly_interest    =   0;
                        $maturity_rate=0;
                        $monthly_rate=0;

                        $capital_asset = floatval(str_replace(",","",$_POST['deposit_size']));
                        $term   = $_POST['deposit_term'];
                        $tax    = $_POST['tax']/100;
                        $rate   = $_POST['interest_rate']/100;
                        $result_term    = explode(',', $term);
                        $term_result    = $result_term[0];
                        $term_rate    = $result_term[1]/100;
                        $second_rate =  $result_term[2]/100;
                        $deposit_option = $_POST['deposit_option'];

                        $maturity_rate = $term_rate;
                        $monthly_rate  = $second_rate;

                        if($deposit_option==2){
                            $maturity_rate = $second_rate;
                            $monthly_rate  = $term_rate;
                        }

                        $date2 = date("d-m-Y", strtotime("+$term_result month",strtotime($start_date)));
                        $day = dateDiff($start_date,$date2);

                        $currency_selected = isset($_POST['currency_type'])?$_POST['currency_type']:'USD';
                        $start_date = isset($_POST['starting_date']) && $_POST['starting_date']?$_POST['starting_date']:date("Y-m-d");
                        $total_interest_m  = ($capital_asset*$day*$maturity_rate)/$number_of_day_in_year;
                        $taxibility_m = $total_interest_m*$tax;
                        $interest_earned_m = $total_interest_m-$taxibility_m;
                       

                        $maturity_total_interests    = number_format($total_interest_m,2);
                        $maturity_taxibilitys      = number_format($taxibility_m,2);
                        $maturity_interest_earneds   = number_format($interest_earned_m,2);

                        // for($i=1;$i<=$term_result;$i++){
                        //     $date1 = ($i==1)?$start_date:date("d-m-Y", strtotime("+1 month",strtotime($date1)));
                        //     $date2 = ($i==1)?date("d-m-Y", strtotime("+1 month",strtotime($date1))):date("d-m-Y", strtotime("+1 month",strtotime($date2)));
                        //     $day = dateDiff($date1,$date2);
                        //     $net_interest = number_format(round(($capital_asset*$day*$monthly_rate)/$number_of_day_in_year,2),2);
                        //     $net = $capital_asset*$day*$monthly_rate/$number_of_day_in_year;

                        //     $total_day += dateDiff($date1,$date2);
                        //     $wht = $net*$tax;
                        //     $monthly_net_interest = number_format(round($net-($net*$tax),2),2);
                        //     $total_net +=   $net;
                        //     $total_wht +=   $wht;
                        // }

                        // $date1 = ($i==1)?$start_date:date("d-m-Y", strtotime("+1 month",strtotime($date1)));
                        // $date2 = ($i==1)?date("d-m-Y", strtotime("+$term_result month",strtotime($date1))):date("d-m-Y", strtotime("+1 month",strtotime($date2)));

                      
                        $net_interest = number_format(round(($capital_asset*$day*$monthly_rate)/$number_of_day_in_year,2),2);
                        $net = $capital_asset*$day*$monthly_rate/$number_of_day_in_year;
                        $wht = $net*$tax;
                        $monthly_net_interest = number_format(round($net-($net*$tax),2),2);
                        $total_net +=   $net;
                        $total_wht +=   $wht;

                        $av_monthly_interest        = number_format($total_net/$term_result,2);
                        $av_monthly_wht             = number_format($total_wht/$term_result,2);
                        $av_net_monthly_interest    = number_format(($total_net/$term_result)-($total_wht/$term_result),2);

                        if($currency_selected != 'USD'){
                            $maturity_total_interests    = number_format($total_interest_m);
                            $maturity_taxibilitys      = number_format($taxibility_m);
                            $maturity_interest_earneds   = number_format($interest_earned_m);

                            $av_monthly_interest        = number_format(($total_net)/$term_result);
                            $av_monthly_wht             = number_format($total_wht/$term_result);
                            $av_net_monthly_interest    = number_format(($total_net/$term_result)-($total_wht/$term_result));
                        }


                        
                        echo '<script>';
                        echo 'jQuery(document).ready(function(){';
                            //===========Monthly============
                            echo    'jQuery("#monthly_value_1").val("'.$av_monthly_interest.'");';
                            echo    'jQuery("#monthly_value_2").val("'.$av_monthly_wht.'");';
                            echo    'jQuery("#monthly_value_3").val("'.$av_net_monthly_interest.'");';

                            //=============Maturity=========
                            echo    'jQuery("#maturity_value_1").val("'.$maturity_total_interests.'");';
                            echo    'jQuery("#maturity_value_2").val("'.$maturity_taxibilitys.'");';
                            echo    'jQuery("#maturity_value_3").val("'.$maturity_interest_earneds.'");';
                        
                            if($deposit_option==1){
                                echo    'jQuery("#total-label1").text("'.$maturity_total_interests.'");';
                                echo    'jQuery("#total-label2").text("'.$maturity_taxibilitys.'");';
                                echo    'jQuery("#total-label3").text("'.$maturity_interest_earneds.'");';
                                
                            }else{
                                echo    'jQuery("#total-label1").text("'.$av_monthly_interest.'");';
                                echo    'jQuery("#total-label2").text("'.$av_monthly_wht.'");';
                                echo    'jQuery("#total-label3").text("'.$av_net_monthly_interest.'");';
                            }
                        echo '});';
                        echo '</script>';
                        
                        
                    }else{
                        $deposit_amount = '100,000.00';
                        $capital_asset = floatval(str_replace(",","",$deposit_amount));
                        $term   = '12,8.00';
                        $tax    = 6/100;
                        $rate   = 8.00/100;
                        $start_date = date("Y-m-d");

                       
                        $result_term    = explode(',', $term);
                        $term_result    = $result_term[0];
                        $term_rate    = $result_term[1]/100;
                        $total_interests = number_format($total_interest,2);
                        $start_date = date("Y-m-d");
                        $interest_earned  = round(($capital_asset*$rate*$term_result)/12,2);
                        $taxibility = $interest_earned*$tax;
                        $total_interest = $interest_earned-$taxibility;

                        $total_interests    = number_format($total_interest,2);
                        $taxibilitys      = number_format($taxibility,2);
                        $interest_earneds   = number_format($interest_earned,2);
                        $total_day = 0;
                        $av_monthly_interest=0;
                        $av_monthly_wht=0;
                        $av_net_monthly_interest =0;

                        


                        for($i=1;$i<=$term_result;$i++){
                            $date1 = ($i==1)?$start_date:date("d-m-Y", strtotime("+1 month",strtotime($date1)));
                            $date2 = ($i==1)?date("d-m-Y", strtotime("+1 month",strtotime($date1))):date("d-m-Y", strtotime("+1 month",strtotime($date2)));
                            $day = dateDiff($date1,$date2);
                            $net_interest = number_format(round($capital_asset*(7.75/100)*$day/$number_of_day_in_year,2),2);
                            $net = $capital_asset*(7.75/100)*$day/$number_of_day_in_year;
                            $total_day += dateDiff($date1,$date2);
                            $wht = number_format(round($net*$tax,2),2);
                            $monthly_net_interest = number_format(round($net-($net*$tax),2),2);
                            $total_net +=   $net;
                            $total_wht +=   $wht;
                        }

                        $av_monthly_interest        = number_format(($total_net)/12,2);
                        $av_monthly_wht             = number_format($total_wht/12,2);
                        $av_net_monthly_interest    = number_format(($total_net/12)-($total_wht/12),2);

                        echo '<script>';
                        echo 'jQuery(document).ready(function(){';
                        //===========Monthly============
                        echo    'jQuery("#monthly_value_1").val("'.$av_monthly_interest.'");';
                        echo    'jQuery("#monthly_value_2").val("'.$av_monthly_wht.'");';
                        echo    'jQuery("#monthly_value_3").val("'.$av_net_monthly_interest.'");';

                        //=============Maturity=========
                        echo    'jQuery("#total-label1").text("'.$interest_earneds.'");';
                        echo    'jQuery("#total-label2").text("'.$taxibilitys.'");';
                        echo    'jQuery("#total-label3").text("'.$total_interests.'");';

                        echo    'jQuery("#maturity_value_1").val("'.$interest_earneds.'");';
                        echo    'jQuery("#maturity_value_2").val("'.$taxibilitys.'");';
                        echo    'jQuery("#maturity_value_3").val("'.$total_interests.'");';

                        echo '});';
                        echo '</script>';
                    }
                    ?>
                <?php 
                _e('[:en]<strong style="padding:0;margin:0;">Note:</strong>[:kh]<strong> សម្គាល់៖</strong>​[:]');
                echo '<ul class="list">';
                        echo '<li>';
                            _e('[:en]The figure of this computation is for the indication purpose and subject to change.[:kh]តួលេខនៃការគណនានេះគ្រាន់តែជាព័ត៌មានបឋមប៉ុណ្ណោះ ហើយអាចមានការកែប្រែ។[:]');
                        echo '</li>';
                        echo '<li>';
                            _e('[:en]For any inquiry, please contact our nearest branch or call us at 023 999 911 or 086 999 911.[:kh]សម្រាប់ព័ត៌មានបន្ថែម សូមទាក់ទងសាខាប្រាសាក់ដែលនៅជិតលោកអ្នកបំផុត ឬ ទាក់ទងយើងខ្ញុំតាម 023 999 911 ឬ 086 999 911 ។[:]');
                        echo '</li>';
                echo '</ul>';
                ?>
        </section>
        <aside class="col-sm-3 col-md-3">
            <?= get_sidebar(); ?>
            <?php 
            if ( have_rows('flexible_content') ) :
                //Check if flexible content exist
                while ( have_rows('flexible_content') ) : the_row();
                    if ( get_row_layout() == 'page_link' ) :
                      $post_id = get_sub_field_object('link')['value']->ID;
                      echo '<img class="img-responsive" src="'.get_sub_field('photo').'" alt="">';
                    endif;
                endwhile;
                wp_reset_postdata();
                //End flexible content
            endif;
            ?>
        </aside>
    </main>
</div>
<?php get_footer(); ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.js"></script>
<script type="text/javascript">
jQuery('input.currency').number( true, 2 );
jQuery(document).ready(function(){

    jQuery("#td_term").chained("#deposit_opt");

    datepicker_application_date('starting_date');
    if(jQuery("#interest_rate").val()==''){
        jQuery("#interest_rate").val('8.00'); 
    }

    //console.log(jQuery("#td_term").attr("selectedIndex"));
    
    
    if (jQuery("#td_term").attr("selectedIndex") == 0) {
        var $deposit_option = jQuery("#deposit_opt option:selected").val();
        console.log($deposit_option);
        jQuery('#td_term option[value="12,8.00,7.75"]').prop('selected','selected');
    }

    jQuery('#td_term').on('change',function(){
        var optionSelected = jQuery("option:selected", this);
        var valueSelected = this.value;
        var numbersArray = valueSelected.split(',');
        jQuery("#interest_rate").val(numbersArray[1]);
    });

    //========Show label onload after submit======
        var checked_label1 = jQuery("#deposit_opt option:selected", this).data('label1');
        var checked_label2 = jQuery("#deposit_opt option:selected", this).data('label2');
        var checked_label3 = jQuery("#deposit_opt option:selected", this).data('label3');

        jQuery('div.label-1').html(checked_label1);
        jQuery('div.label-2').html(checked_label2);
        jQuery('div.label-3').html(checked_label3);


    // jQuery('.monthly-row').hide();
    jQuery('#deposit_opt').on('change',function(){
        jQuery("#total-interest,#final_balance,#total_net_interest").text('0.00');
        var optionSelected = jQuery("option:selected", this);

        var checked_label1 = jQuery("option:selected", this).data('label1');
        var checked_label2 = jQuery("option:selected", this).data('label2');
        var checked_label3 = jQuery("option:selected", this).data('label3');

        jQuery('div.label-1').html(checked_label1);
        jQuery('div.label-2').html(checked_label2);
        jQuery('div.label-3').html(checked_label3);

        var valueSelected = this.value;
        jQuery('#td_term option[value="12,8.00,7.75"]').prop('selected','selected');
        jQuery("#interest_rate").val('8.00');

        jQuery('#total-label1').text(jQuery("#maturity_value_1").val());
        jQuery('#total-label2').text(jQuery("#maturity_value_2").val());
        jQuery('#total-label3').text(jQuery("#maturity_value_3").val());

        if(valueSelected == 2){
            jQuery('#td_term option[value="12,7.75,8.00"]').prop('selected','selected');
            jQuery("#interest_rate").val('7.75');
            
            jQuery('#total-label1').text(jQuery("#monthly_value_1").val());
            jQuery('#total-label2').text(jQuery("#monthly_value_2").val());
            jQuery('#total-label3').text(jQuery("#monthly_value_3").val());
        }
    });

    var label_default = "<?php _e('[:en]Amount due in the first Month[:kh]ចំនួនទឹកប្រាក់ដែលត្រូវសងក្នុងខែដំបូង[:]') ?>";    
    jQuery('#calculate_form').bootstrapValidator({
        // feedbackIcons: {
        //     valid: 'glyphicon glyphicon-ok',
        //     invalid: 'glyphicon glyphicon-remove',
        //     validating: 'glyphicon glyphicon-refresh'
        // },
        // container: 'tooltip',
        live: 'enabled',
        message: 'This value is not valid',
        fields: {
            deposit_size: {
                validators: {
                    notEmpty: {
                        message: '<?php _e('[:en]Please input deposit amount![:kh]សូមធ្វើការបញ្ចូលចំនួននៃប្រាក់បញ្ញើ![:]') ?>'
                    },
                    regexp: {
                        regexp: /^[0-9-_ \.]+$/, 
                        message: '<?php _e('[:en]You entered wrong number![:kh]លេខដែលអ្នកបញ្ចូលមិនត្រឹមត្រូវទេ![:]') ?>' 
                    }
                }
            },
            deposit_term: {
                validators: {
                    notEmpty: {
                        message: '<?php _e('[:en]Please input deposit amount![:kh]សូមធ្វើការបញ្ចូលចំនួននៃប្រាក់បញ្ញើ![:]') ?>'
                    }
                }
            }
        },
    });
   
    //Change sidebar URL for converter calculator page to Forieng Exchange Page
    jQuery("ul li.page_item a").each(function(){
        var $selected_link = jQuery(this).attr("href").split('/').pop();
        if($selected_link=='currency-converter'){
            jQuery(this).attr("href", function(index, old) {
                var $new_link = old.replace("calculators/currency-converter", "services/exchange");
                jQuery(this).attr("href",$new_link );
            });
        }
    });
});

function datepicker_application_date(id){
        //Holiday date list here
    var holidays = [
                    "2018-01-01","2018-01-08","2018-01-31","2017-01-31","2018-03-08","2018-04-16",
                    "2018-05-01","2018-05-03","2018-05-14","2018-05-15","2018-06-01",
                    "2018-06-18","2018-09-24","2018-10-08",,"2018-10-09","2018-10-10","2018-10-15",
                    "2018-10-23","2018-10-29","2018-11-09","2018-11-21","2018-11-22",
                    ,"2018-11-23","2018-12-10"
                ];
    jQuery("#"+id).datepicker({
        dateFormat: 'yy-mm-dd',
        autoclose: true,
        changeYear:true,
        changeMonth:true,
        // minDate: '-1m',
        // maxDate: '+1m',
        yearRange: "-5:+5",
        beforeShowDay: function(date){
            var datestring = jQuery.datepicker.formatDate('yy-mm-dd', date);
            var day = date.getDay();
            if (day == 0 || day == 6) {
                return [false, "weekendday"]
            } else {
                return [ holidays.indexOf(datestring) == -1 ]
            }
        },
    });
    jQuery(".starting_button").on('click',function(){
        jQuery("#starting_date").datepicker('show');
    });
};
</script>

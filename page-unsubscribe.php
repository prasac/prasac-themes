<?php 
/*
* template name: Unsubscribe form
*/
get_header();
// error_reporting(0);
?>
<style>
.panel{box-shadow:none;}
.panel h2{ color:#444444; font-size:18px; margin:0 0 8px 0;}
.panel p { color:#777777; font-size:14px; margin-bottom:30px; line-height:24px;}
.login-form .form-control {
  background: #f7f7f7 none repeat scroll 0 0;
  border: 1px solid #d4d4d4;
  border-radius: 4px;
  font-size: 14px;
  height: 50px;
  line-height: 50px;
}
.main-div {
  background: #ffffff none repeat scroll 0 0;
  border-radius: 2px;
  border: #4DB848 1px solid ;
  margin: 10px auto 30px;
  max-width: 38%;
  padding: 50px 70px 70px 71px;
}

.login-form .form-group {
  margin-bottom:10px;
}
.textarea{height:120px;}
.login-form{ text-align:center;}
.forgot a {
  color: #777777;
  font-size: 14px;
  text-decoration: underline;
}
.login-form  .btn.btn-primary {
  background: #f0ad4e none repeat scroll 0 0;
  border-color: #f0ad4e;
  color: #ffffff;
  font-size: 14px;
  width: 100%;
  height: 50px;
  line-height: 50px;
  padding: 0;
}
.forgot {
  text-align: left; margin-bottom:30px;
}
.botto-text {
  color: #ffffff;
  font-size: 14px;
  margin: auto;
}
.login-form .btn.btn-primary.reset {
  background: #ff9900 none repeat scroll 0 0;
}
.back { text-align: left; margin-top:10px;}
.back a {color: #444444; font-size: 13px;text-decoration: none;}
 
</style>

<div id="body" class="container-fluid">
    <div class="container">
        <div class="login-form">
            <div class="main-div">
                <div class="panel">
                <h3 class="green"><?php _e('[:en]Unsubscribe[:kh]ស្នើសុំចាកចេញ')?></h3>
                <p><?php _e('[:en]Please enter your email to be unsubscribed![:kh]សូមបញ្ចូលអុីម៉ែលរបស់លោកអ្នកដើម្បីស្នើសុំចាកចេញ!')?></p>
                </div>
                <form action="" method="POST" id="unsubscribe">
                    <div class="form-group">
                        <input type="email" name="unsubscribe-email" id="unsubscribe-email" class="form-control" value="" placeholder="<?php _e('[:en]Email Address[:kh]អុីម៉ែល')?>">
                    </div>
                    <input type="hidden" name="action" value="Unsubscribe">
                    <button type="submit" id="btn-unsubscribe" name="unsubscribe" value="Unsubscribe" class="btn btn-primary"><?php _e('[:en]Unsubscribe[:kh]ស្នើសុំចាកចេញ')?></button>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
    
<?php
get_footer();
?>

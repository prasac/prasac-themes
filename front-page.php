<?php get_header(); ?>
<!--======Feature Product Random blog=======-->
<?php 
   $rows = get_field('feature', 'option'); 
  if($rows){ 
?>
    <div class="container-fluid">
        <section id="feature-content_wrapper" class="section-shaded section-feature">
            <div class="container">
                <div class="row">
                    <header class="col-sm-12">
                        <h2 class="h2 green text-center"><?php _e('[:en]What is most important for you?[:kh]តើ​សេវា​កម្ម​មួយ​ណា​ដែល​ជា​ជម្រើស​របស់​លោក​អ្នក![:]');?></h2>
                    </header>
                    <div class="col-sm-12">
                        <div id="product-feature">
                        <?php 
                            shuffle( $rows ); 
                            foreach($rows as $row) {
                                $image = $row['feature_image'];
                                $title = $row['feature_title'];
                                $description = $row['feature_desc'];
                                $url = $row['feature_url'];
                                ?>
                                <article class="important-product-feature col-sm-4">
                                    <div class="feature_wrapper">
                                        <header class="feature-thumb">
                                        <img alt="<?= $url; ?>" class="img-responsive" src="<?= $image; ?>" style="width:100px;">
                                        </header>
                                        <h3><?= $title; ?></h3>
                                        <div class="feature_desc">
                                        <?php if( get_locale()=='en_US' ): ?>
                                            <p><?php echo wp_trim_words($description ,11, '...');?></p>
                                        <?php else : ?>
                                            <p><?php echo mb_strimwidth($description ,0,85,'...');//mb_strimwidth ?></p>
                                        <?php endif; ?>
                                        </div>
                                        <p class="feature_footer">
                                        <a class="btn btn-warning btn-outline btn-md" href="<?= $url; ?>" title="<?= $title; ?>"><?php _e('[:en]Read More[:kh]អានបន្ត[:]'); ?></a>
                                        </p>
                                    </div>
                                </article>
                            <?php 
                                }//==End foreach
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        }//===End if
        ?>
        <!-- latest News -->
        <section id="lastest-post_wrapper">
        <div class="container">
            <header class="col-sm-12 col-md-12">
            <h2 class="h2 green text-center"><?php _e('[:en]Latest News[:kh]ព័ត៌មាន និង​ព្រឹត្តិការណ៍​សំខាន់ៗ[:]')?></h2>
            </header>
            <?php
            $query = new WP_Query( array(
                'post_type'      => 'post',
                'posts_per_page' => 6,
                'post_status'    => 'publish'
            ));
            if( $query->have_posts() ): 
            ?>
            <?php while( $query->have_posts() ): $query->the_post(); 
                    $post_title = get_the_title(); 
                    $thumb_id =  get_post_thumbnail_id();
                    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
                    if ( has_post_thumbnail() ) {
                        $thumb_url = $thumb_url_array[0];
                    } else{
                        $thumb_url = get_template_directory_uri().'/assets/images/image_not_found.png';
                    }
            ?>
                <article <?php post_class('col-xs-12 col-sm-6 col-md-4 news-wrapper'); ?>>
                <div class="article_wrapper">
                    <header class="article_headers col-xs-5 col-sm-12 col-md-12 col-lg-12">
                    <div>
                        <a href="<?= get_permalink($post->ID); ?>" title="<?= $post_title ?>">
                        <?php //if( function_exists('featured_image') ) echo featured_image('full', true, true, ''); ?>
                        <img class="img-lazy img-responsive" src="<?php echo $thumb_url; ?>" alt="<?php echo $post_title; ?>">
                        </a>
                    </div>
                    </header>
                    <div class="article_contents content  col-xs-7 col-sm-12 col-md-12 col-lg-12">
                    <h4 title="<?= $post_title; ?>">
                        <a href="<?= get_permalink($post->ID); ?>">
                        <?php 
                            if( get_locale() == 'en_US' ) {
                                echo mb_strimwidth( $post_title, 0, 50, '...');
                            } else {
                                echo mb_strimwidth( $post_title, 0, 80, '...');
                            }
                            ?>
                        </a>
                    </h4>
                    <!-- <a href="<?= get_permalink($post->ID); ?>" title="<?= $post_title ?>">
                            <h4 title="<?= $post_title; ?>">
                            <?php 
                            if( get_locale() == 'en_US' ) {
                                echo mb_strimwidth( $post_title, 0, 50, '...');
                            } else {
                                echo mb_strimwidth( $post_title, 0, 80, '...');
                            }
                            ?>
                            </h4>
                        </a>   -->
                        <p class="article_date">
                        <i class="fa fa-calendar danger"></i>&nbsp;&nbsp;<?php _e(get_post_time('F j, Y')); ?>
                        </p>
                    <?php 
                        if( get_locale() == 'en_US' ) {
                            echo '<p class="hidden-xs">'.mb_strimwidth( get_the_excerpt(), 0, 110, '...').'</p>';
                        } else {
                            echo '<p class="hidden-xs">'.mb_strimwidth( get_the_excerpt(), 0, 100, '...').'</p>';
                        }
                        ?>
                    </div>
                    <p class="article_footer hidden-xs">
                    <a class="article_footer-readmore" href="<?= get_permalink($post->ID); ?>" title="<?= $post_title ?>"><?php _e('[:en]Read More[:kh]អានបន្ត[:]'); ?></a>
                    </p>
                </div>
                </article>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <footer class="col-sm-12 col-md-12" style="z-index:999;">
            <h3 class="text-center"><a class="btn btn-warning btn-outline" href="<?php echo get_site_url(); ?>/media/news-and-event/"><?php _e('[:en]Show More[:kh]មើល​បន្ដ​[:]');?></a></h3>
        </footer>
        </section>

        <?php if(get_field('banner_large2', 'option')):?>
        <section id="video-wrapper" class="hidden-xs">
        <div class="block1">
            <img src="<?php echo get_field('banner_large2', 'option') ?>" class="img-responsive" alt="" title=""/>
            <div class="container description">
            <div class="col-sm-6 col-md-6">
            <h3 style="line-height: 40px;background-color: #4db84894;padding: 1px 4px;color: #ffffff;font-size: 1.5rem;display:table;"><?php echo get_field('banner_title2', 'option') ?></h3><br/>
            <a class="btn btn-success btn-md" href="<?php echo get_field('banner_url2', 'option') ?>"><?php _e('[:en]Read More[:kh]ព័ត៌មានបន្ថែម[:]');?></a>
            </div>
            </div>
        </div>
        </section>
        <?php endif;?>
        <section id="useful-information" class="container text-center">
            <div class="row">
            <header class="col-sm-12 col-md-12">
                <h2 class="h2 green text-center"><?php _e('[:en]You can find further information or feedback here![:kh]លោក​អ្នក​អាច​ស្វែង​រក​ព័ត៌​មាន​បន្ថែម​នៅទី​នេះ​![:]')?></h2>
            </header>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-md-12 block-container">
                <section class="col-xs-12 col-sm-6 col-md-3 userful-block">
                        <h4 class="h4"><?php _e('[:en]Call Center[:kh]​ផ្នែក​បម្រើ​អតិថិជន[:]')?></h4>
                        <img class="img-responsive img-thumbnail" src="<?= get_field('call_center', 'option'); ?>" alt="Image Ads">
                </section>
                <section class="col-xs-12 col-sm-6 col-md-3 userful-block">
                    <h4 class="h4"><?php _e('[:en]Mobile Banking App[:kh]កម្មវិធី​ធនាគារចល័ត[:]')?></h4>
                    <div class="img-thumbnail">
                        <p><?php _e('[:en]You can download Mobile Banking App from:[:kh]លោក​អ្នក​អាច​ទាញ​យក​កម្មវិធី​ធនាគារចល័ត​ពី​តំណរ​ភ្ជាប់​ខាង​ក្រោម​៖[:]')?></p>
                        <div class="row">
                            <section class="col-md-12 app-download">
                                <div class="row">
                                    <a href="https://itunes.apple.com/app/id971426760" title="Download for Android" target="_blank">
                                        <img src="<?php bloginfo('template_url')?>/assets/app_store.png"  alt="" title="" style="" data-pin-nopin="true">
                                    </a>
                                </div>
                            </section>
                            <section class="col-md-12 app-download">
                                <div class="row">
                                    <a href="https://play.google.com/store/apps/details?id=com.prasac.ibanking.prasacmbanking" title="Download for iPhone" target="_blank">
                                    <img src="<?php bloginfo('template_url')?>/assets/play_store.png" alt="" title="" style="" data-pin-nopin="true">
                                    </a>
                                </div>
                            </section>
                        </div>
                    </div>
                </section>

                <section class="col-xs-12 col-sm-6 col-md-3 userful-block">
                        <h4 class="h4"><?php _e('[:en]ATM Network[:kh]​បណ្ដាញ​ម៉ាសុី​ន​អេធី​អឹម​[:]')?></h4>
                        <a href="<?php get_bloginfo('url')?>/contact-us/atm-location/">
                        <img class="img-responsive img-thumbnail" src="<?= get_field('atm_network', 'option'); ?>" alt="Image Ads">
                        </a>
                </section>
                <section class="col-xs-12 col-sm-6 col-md-3 userful-block">
                    <h4  class="h4"><?php _e('[:en]Exchange Rate[:kh]តារាងអត្រាប្ដូរប្រាក់​[:]') ?></h4>
                    <?php get_template_part('template-parts/exchange', 'board'); ?>
                    <a href="<?php echo get_site_url(); ?><?php _e('[:en]/en/services/exchange/[:kh]/kh/services/exchange/[:]') ?>"><?php _e('[:en]<p >Please click here</a> to use our Currency Converter.</p>[:kh]<p class="text-left"
                    >ចុចទីនេះ​</a>ដើម្បី​ប្រើប្រាស់​កម្មវិធីប្ដូរ​ប្រាក់​</p>[:]') ?></a>
                </section>
                </div>
            </section>

            </div>
        </div>
        </section>
        <?php
        $images = get_field('lender_carousel', 'option');
        if( $images ): ?>
        <section id="lender-wrapper" class="hidden-xs">
            <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                <div id="lender-carousel">
                <?php foreach( $images as $image ): ?>
                <a class="item" href="#"><img class="img-responsive" src="<?php echo $image['sizes']['large']; ?>" alt=""></a>
                <?php endforeach; ?>
                </div>
                </div>
            </div>
            </div>
        </section>
        <?php endif; ?>
    </div>
    <?php get_footer(); ?>

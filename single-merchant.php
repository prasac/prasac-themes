<?php get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <section <?php post_class('col-sm-9 col-md-9 page-title'); ?>>
                <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                    <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                    <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                </ul>
                <div class="clearfix"></div>
                <h3 class="green"><?php the_title(); ?></h3>
                <div class="row">
                    <?php
                        while( have_posts() ): the_post();
                            // while( have_rows('merchant_detail') ): the_row();
                                echo '<div class="col-xs-12 col-md-12">'.get_the_content().'</div>';
                                $thumb_id =  get_post_thumbnail_id();
                                $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
                                $image_url = '';
                                if ( has_post_thumbnail() ) {
                                    $fealture_image = '<img src="'.$thumb_url_array[0].'" class="img-lazy img-responsive" alt="" title=""/>';
                                    $image_url = $thumb_url_array[0];
                                } else{
                                    $fealture_image = '';
                                    $image_url = '';
                                }
                                echo '<span id="latlng" data-thumbnail="'.$image_url.'" data-latlng="'.get_field('latitude').','.get_field('longitude').'" data-title="'.get_the_title().'" data-desc="'.get_the_content().'"></span>';
                            // endwhile;
                       endwhile;
                    ?>
                    <div class="col-xs-12 col-md-12">
                        <br />
                        <div id="map-canvas" class="" style="width:auto; height: 500px;"></div>
                    </div>
                </div>
            </section>
            <aside class="col-xs-12 col-sm-3 hidden-xs">
                <div class="sidebar">
                    <div class="sidebar" style="text-align:center !important;">
                        <div class="sidebar_title"><?= _e('[:en]Scan-Pay Partner[:kh]​ដៃគូ​ស្កេន​-​ទូទាត់[:]');?></div>
                        <?php 
                        global $post;
                        $terms = get_terms('merchant_tag');
                        $post_tag = isset($_GET['tag'])?$_GET['tag']:'prasac-scan-pay';
                        if($terms){
                            echo '<ul class="sidebar_content list text-left">';
                                foreach ($terms as $term){
                                    $active_class = $post_tag==$term->slug? ' active':'';
                                    echo "<li class='page_item text-left'><a class='$active_class' href='/prasac/scan-pay-partner?tag=$term->slug'>$term->name</a></li>";
                                }
                            echo '</ul>';
                        }
                        ?>
                        <br />
                        <a href="https://www.prasac.com.kh/services/loans/small-and-medium-loans">
                            <embed class="html5-content" src="<?php echo site_url();?>/wp-content/uploads/html5/250x600/index.html" width="300" height="650" style="border:none;"></embed>
                            <style>
                                .html5-container {
                                    background:transparent;
                                    border:none;
                                    text-align:center;
                                    margin:15px 0;
                                }
                            </style>
                        </a>
                    </div>
                </div>
            </aside>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-9">
            <hr>
                <div class="single-footer-share">
                    <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                    <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                    <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo get_permalink();?>','twitter');"></a>
                    <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo get_permalink();?>','linkedin');"></a>
                    <div class="fb-like pull-right" data-href="<?php echo get_permalink();?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="false"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
get_footer();
?>
<script>
    jQuery(document).ready(function(){
        var element = jQuery(this);
        var map, infowindow;
        function initialize(myCenter) {
            var imgPoint = "<?= esc_url( get_template_directory_uri() ); ?>/assets/images/pages/pin-point.png";
            var image = new google.maps.MarkerImage(imgPoint);
            var marker = new google.maps.Marker({
                position: myCenter,
                icon: image,
                animation: google.maps.Animation.DROP,
        });

        var mapProp = {
            center: myCenter,
            zoom:16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);
        marker.setMap(map);
        var infoWindow = new google.maps.InfoWindow({maxWidth:500}), marker, i;
        // Add info window to marker    
        google.maps.event.addListener(marker, 'click', (function(marker) {
                return function() {
                    if (marker.getAnimation() != null) {
                        marker.setAnimation(null);
                    } else {
                        marker.setAnimation(google.maps.Animation.BOUNCE);
                    }
                    var datalatlng = jQuery("#latlng").data('latlng');
                    var $image_thumbnail = jQuery("#latlng").data('thumbnail')!=''?'<img src="'+jQuery("#latlng").data('thumbnail')+'" width="150" style="text-align:left;float:left;margin-right:15px;"/>':'';
                    var contentString = $image_thumbnail+'<b class="green" style="font-size:16px;">'+ jQuery("#latlng").data('title') +'</b><br /><br /><p>'+ jQuery("#latlng").data('desc')+'</p><br /><a class="infowindow_link" style="font-weight:bold;cursor:pointer;" alt="Get direction from your current location" title="Get direction from your current location" href="https://www.google.com/maps/dir/?api=1&destination='+datalatlng+'&travelmode=driving" target="_blank"><img src="<?php echo get_template_directory_uri() ?>/assets/images/direction-icon.png" width="25" /> ';
                    contentString += "<?php echo _e('[:en]  Get Direction[:kh]  ទិសដៅ[:]');?>";
                    contentString += '</a>';
                    infoWindow.setContent(contentString);
                    infoWindow.open(map, marker);
                    map.setZoom(18);
                    map.setCenter(marker.getPosition());
                }
            })(marker));
        };
        var data = jQuery("#latlng").data('latlng').split(',');
        initialize(new google.maps.LatLng(data[0], data[1]));

        // jQuery(".infowindow_link").live('click', function(){
        //     window.location.href=jQuery(this).attr('href');
        // });
    });
</script>
<style>
.gm-style{
    font-family:Hanuman,Roboto;
}
.infowindow_link:hover{
    /* text-decoration:underline; */
    color:red;
}

.infowindow_link{
    color:#1A73E8;
}

.gm-style .gm-style-iw-c{
    border-radius:0;
}
</style>

<?php
 /*
* template name: Annual Workshop Registration
*/
get_header();
error_reporting(0);
?>
<style>
.btn-bs-file {
    position: relative;
}
div.container-fluid.main{
    min-height:980px;
    /* background:url('<?php echo get_template_directory_uri(); ?>/assets/images/Banner-WorkShop_Bong-Him-For-Web_001.jpg') no-repeat center; */
    background: #000000;  /* fallback colour. Make sure this is just one solid colour. */
    background: -webkit-linear-gradient(rgba(75,184,72,0.6), rgba(195, 55, 100, 0.1)), url("<?php echo get_template_directory_uri(); ?>/assets/images/annual_meeting_2019.png") no-repeat center;
    background: linear-gradient(rgba(75,184,72,0.6), rgba(195, 55, 100, 0.1)), url("<?php echo get_template_directory_uri(); ?>/assets/images/annual_meeting_2019.png") no-repeat center; /* The least supported option. */
    background-size: cover;
}
.btn-bs-file input[type="file"] {
    position: absolute;
    top: -9999999;
    filter: alpha(opacity=0);
    opacity: 0;
    width: 0;
    height: 0;
    outline: none;
    cursor: inherit;
}
label{
    color:#3c763d;
}
.green {
    font-weight: 600 !important;
    text-transform: capitalize;
}
.text-white{
    color:#FFFFFF;
}
form#user_id_form input,form#annual_meeting_registration input, textarea,textarea.form-control{
    background:transparent;
    border:1px solid #3c763d;
    color:#3c763d;
}

[type=radio]:checked+label:after{
    background-color:#ffd969;
}
h3.main-text{
    font-family:"Moul";
    color:#3c763d;
    font-size:2rem;
    font-weight:500 !important;
    line-height: 1.8em !important;
}
div.alert-success{
    border-radius:0;
    background: rgba(255,255,255,0.5);
}
button.btn-registration{
    border: none;
    background: rgba(75,184,72,0.7);
    color: rgba(75,184,72,1);
    text-align: center;
    font-size: 1.5rem;
    color: #FFFFFF;
    width: 100%;
    border-radius: 5px;
    padding: 1rem 0;
}
.h1,
.h2,
.h3,
h1,
h2,
h3 {
    margin-top: 20px;
    margin-bottom: 10px;
}

.wrapper-container{
    /* background-color:#eeeeee; */
    padding:5px;
}
@media (max-width: 768px) {
    section.wrapper-container{
        padding:10px 0;
    }
    form .form-group {
        padding:0;
        margin-bottom: 0.8rem;
    }
}
</style>
<div class="container-fluid main">
    <div class="container">
        <br />
        <section class="wrapper-container">
            <div class="success-wrapper">
                <div class="col-md-12 col-lg-12 alert alert-success text-center" role="alert">
                    <p class="text-center">សូមអរគុណសម្រាប់ការចុះឈ្មោះរបស់លោកអ្នក</p>
                    <p><i class="fa fa-spinner fa-long-arrow-right"></i> <a href="">ចុះឈ្មោះម្ដងទៀត</a></p>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="row" id="form-container">
                        <legend style="border:none;">
                            <div class="col-md-12">
                                <h3 class="text-center main-text">ចុះឈ្មោះវត្តមាន</h3>
                            </div>
                        </legend>
                        <form id="user_id_form" method="POST" enctype="multipart/form-data">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-12 control-label col-form-label" for="ID">អត្តលេខ <span class="required_field">*</span></label>
                                    <div class="col-xs-12 col-md-12">
                                        <input name="ID" placeholder="" class="form-control" type="text" id="user_id">
                                        <input type="hidden" name="action" value="user_registration" />
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form id="annual_meeting_registration" method="POST" enctype="multipart/form-data">
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-12 control-label col-form-label" for="Name">ឈ្មោះ <span class="required_field">*</span></label>
                                    <div class="col-xs-12 col-md-12">
                                        <input name="Name" placeholder="" class="form-control" type="text" id="Name">
                                        <input name="ID" placeholder="" class="form-control" type="hidden" id="user_id_result">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-12 control-label col-form-label" for="Name">ភេទ <span class="required_field">*</span></label>
                                    <div class="col-xs-12 col-md-12">
                                        <input name="Gender" placeholder="" class="form-control" type="text" id="Gender">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-12 control-label col-form-label" for="Position">តួនាទី <span class="required_field">*</span></label>
                                    <div class="col-xs-12 col-md-12">
                                        <input name="Position" class="form-control" id="Position" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-12 control-label col-form-label" for="Regional">ភូមិភាគ <span class="required_field">*</span></label>
                                    <div class="col-xs-12 col-md-12">
                                        <input name="Regional" class="form-control" id="Regional" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-12 control-label col-form-label" for="Branch">សាខា<span class="required_field">*</span></label>
                                    <div class="col-xs-12 col-md-12">
                                        <input name="Branch" class="form-control" id="Branch" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-12 control-label col-form-label" for="FCSite">លេខ FC<span class="required_field">*</span></label>
                                    <div class="col-xs-12 col-md-12">
                                        <input name="FCSite" class="form-control" id="FCSite" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-12 control-label col-form-label" for="WhatsApp">លេខទូរស័ព្ទ WhatsApp<span class="required_field">*</span></label>
                                    <div class="col-xs-12 col-md-12">
                                        <input name="WhatsAppNumber" class="form-control" id="WhatsApp" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-12 control-label col-form-label" for="Facebook">ឈ្មោះគណនីហ្វេសបុក<span class="required_field">*</span></label>
                                    <div class="col-xs-12 col-md-12">
                                        <input name="FacebookUser" class="form-control" id="Facebook" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-12 control-label col-form-label"></label>
                                    <div class="col-xs-12 col-md-12">
                                        <button type="submit" class="btn-registration" name="btn-registration"><i class="fa fa-save"></i> រក្សាទុក</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <br />
    </div>
</div>
<div id="loader"></div>
<?php
get_footer();
?>
<style>
#loader {
  display: none;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  background: rgba(0,0,0,0.75) url(<?php echo get_template_directory_uri(); ?>/assets/images/prasac_loading.gif) no-repeat center center;
  z-index: 10000;
}
.has-feedback .form-control {
    padding-right: 0;
}

div.form-group.has-error>.form-control,
div.form-group.has-error .input-group .form-control,
div.form-group.has-error div>.form-control {
    border-color: #a94442;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
}

#ab_row2 div.form-group .form-control,
#ab_row3 div.form-group .form-control,
#ab_row4 div.form-group .form-control,
#ab_row5 div.form-group .form-control {
    border: 1px solid #ccc;
}

span.required_field {
    color: red;
}

.form-group {
    margin-bottom: 0;
    padding: 4px 8px;
}

.panel.panel-horizontal {
    display: table;
    width: 100%;
}

.panel.panel-horizontal>.panel-heading,
.panel.panel-horizontal>.panel-body,
.panel.panel-horizontal>.panel-footer {
    display: table-cell;
}

.panel.panel-horizontal>.panel-heading,
.panel.panel-horizontal>.panel-footer {
    border: 0;
    vertical-align: middle;
}

.panel.panel-horizontal>.panel-heading {
    border-right: 1px solid #ddd;
    border-top-right-radius: 0;
    border-bottom-left-radius: 4px;
}

.panel.panel-horizontal>.panel-footer {
    border-left: 1px solid #ddd;
    border-top-left-radius: 0;
    border-bottom-right-radius: 4px;
}

label {
    font-weight: lighter;

}

.form-horizontal .radio-inline {
    padding-top: 0;
}

.form-group input[type="checkbox"] {
    /* display:block; */
}

.checkbox-danger input[type="checkbox"]:checked+label::before {
    background-color: #d9534f;
    border-color: #d9534f;
}

.checkbox-danger input[type="checkbox"]:checked+label::after {
    color: #fff;
}

input[type="radio"]:checked+label {
    color: #4DB848;
    font-weight: lighter;
}

.col-md-1>input {
    padding: 2px;
}

#success_message {
    display: none;
}

.panel-heading .accordion-toggle:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';
    /* essential for enabling glyphicon */
    content: "\e114";
    /* adjust as needed, taken from bootstrap.css */
    float: right;
    /* adjust as needed */
    color: grey;
    /* adjust as needed */
}

.panel-heading .accordion-toggle.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\e080";
    /* adjust as needed, taken from bootstrap.css */
}

h4.panel-title {
    /* font-family:"Moul"; */
    font-size: 1.6em;
    font-weight: light;
}

.btn-bs-file {
    position: relative;
}

.btn-bs-file input[type="file"] {
    position: absolute;
    top: -9999999;
    filter: alpha(opacity=0);
    opacity: 0;
    width: 0;
    height: 0;
    outline: none;
    cursor: inherit;
}

.checkbox-inline,
.radio-inline {
    padding-left: 5px;
}
</style>
<script>
//Show image profile immediately
jQuery("#filebrowse").change(function() {
    readURL(this);
    readURLPDF(this);
});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            jQuery('#img-upload').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURLPDF(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            jQuery('#img-upload-pdf').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
</script>

<style>
div#ui-datepicker-div.ui-datepicker {
    z-index: 2 !important;
}

#ui-datepicker-div div.ui-datepicker-header {
    background-image: none;
    background-color: #F2B01D;
    color: #555555;
    font-size: 12px;
}

.ui-datepicker table {
    font-size: 0.7em;
}

.ui-widget-header .ui-icon,
.ui-icon,
.ui-widget-content .ui-icon {
    background-image: url("<?php bloginfo('template_url');?>/assets/images/ui-icons_ffffff_256x240.png");
}

.ui-datepicker td span,
.ui-datepicker td a {
    text-align: center;
}

.ui-datepicker {
    width: 14em;
}

a.ui-state-default.ui-state-active {
    background: #F2B01D;
    color: #fff;
}

.draggable-header .highslide-heading {
    position: absolute;
    margin: -3px 0.4em;
}

.modal {
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: rgba(0, 0, 0, 0.7);
}

.modal .chart {
    height: 90%;
    width: 100%;
    max-width: none;
}

.ui-state-hover,
.ui-widget-content .ui-state-hover,
.ui-widget-header .ui-state-hover,
.ui-state-focus,
.ui-widget-content .ui-state-focus,
.ui-widget-header .ui-state-focus {
    color: #F2B01D;
    border: 1px solid #F2B01D;
}

.input-group-addon {
    font-size: 13px;
}

.highcharts-reset-zoom,
.highcharts-button-box {
    background-color: transparent;
    fill: transparent;
    stroke: none;
}

.highcharts-reset-zoom text {
    color: #F2B01D !important;
    fill: #F2B01D !important;
}

.highcharts-reset-zoom text :hover {
    color: #4DB848 !important;
    fill: #4DB848 !important;
}
</style>
<script>
jQuery(document).ready(function() {
    var spinner = jQuery('#loader');
    var messagebox  =   jQuery('.success-wrapper');
    var form_container  =   jQuery('#form-container');
    spinner.hide();
    messagebox.hide();
    form_container.css('opacity',1);
    jQuery(".not-signle-row,.only-for-married").hide();
    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 18);
    var endyearemployment = new Date();
    endyearemployment.setFullYear(endyearemployment.getFullYear());
   
    
    jQuery("#dateofbirth").datepicker({
        dateFormat: 'dd/mm/yy',
        autoclose: true,
        changeYear: true,
        changeMonth: true,
        minDate: start,
        maxDate: end,
        yearRange: start.getFullYear() + ':' + end.getFullYear()
    });
    jQuery('.calendar-dateofbirth').click(function() {
        jQuery("#dateofbirth").focus();
    });

    
    jQuery("#dateofemployment,.experience_date_employfrom0,.experience_date_employto0,.experience_date_employfrom1,.experience_date_employto1,.experience_date_employfrom2,.experience_date_employto2,.experience_date_employfrom3,.experience_date_employto3,.experience_date_employfrom4,.experience_date_employto4").datepicker({
        dateFormat: 'dd/mm/yy',
        autoclose: true,
        changeYear: true,
        changeMonth: true,
        minDate: start,
        maxDate: endyearemployment,
        yearRange: start.getFullYear() + ':' + endyearemployment.getFullYear()
    });
    jQuery('.calendar-ex-date-from0').click(function() {
        jQuery(".experience_date_employfrom0").focus();
    });
    jQuery('.calendar-ex-date-to0').click(function() {
        jQuery(".experience_date_employto0").focus();
    });

    jQuery('.calendar-ex-date-from1').click(function() {
        jQuery(".experience_date_employfrom1").focus();
    });
    jQuery('.calendar-ex-date-to1').click(function() {
        jQuery(".experience_date_employto1").focus();
    });

    jQuery("#comfirm_date,#apply_date").datepicker({
        dateFormat: 'dd/mm/yy'
    }).datepicker('setDate', new Date()).attr('readonly', 'readonly');

    jQuery('#user_id').keyup(function(e){
        // e.preventDefault();
        var id_check_length =   jQuery(this).val().length;
        var user_id = jQuery(this).val();
        if(id_check_length==5){
            spinner.show();
            jQuery("form#user_id_result").val(user_id);
            var $form = jQuery('form#user_id_form');
            var formURL = '/wp-admin/admin-ajax.php';
            jQuery.ajax({
                url: formURL,
                type: "POST",
                data:{'user_id':user_id,'action':'user_registration'},
                success: function(data, textStatus, jqXHR) {
                    spinner.hide();
                    var json_data   =   JSON.parse(data);

                    var user_id =   json_data['user_id'];
                    var user_name =   json_data['user_name'];
                    var user_gender =   json_data['user_gender'];
                    var user_position =   json_data['user_position'];
                    var user_regional =   json_data['user_regional'];
                    var user_branch =   json_data['user_branch'];
                    var user_fcsite =   json_data['fcsite'];

                    jQuery("#user_id_result").val(user_id);
                    jQuery("#Name").val(user_name);
                    jQuery("#Position").val(user_position);
                    jQuery("#Gender").val(user_gender);
                    jQuery("#Regional").val(user_regional);
                    jQuery("#Branch").val(user_branch);
                    jQuery("#FCSite").val(user_fcsite);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            });
        }
    });

    jQuery('#user_id_form').bootstrapValidator({
        fields: {
            ID: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញអត្តលេខ​របស់លោកអ្នក'
                    },
                }
            },
        }
    });


    jQuery('form#annual_meeting_registration').bootstrapValidator({
        fields: {
            ID: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញអត្តលេខ​របស់លោកអ្នក'
                    },
                }
            },
            Name: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញអត្តឈ្មោះ​របស់លោកអ្នក'
                    },
                }
            },
            Gender: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញអត្តលេខ​របស់លោកអ្នក'
                    },
                }
            },
            Position: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញអត្តលេខ​របស់លោកអ្នក'
                    },
                }
            },
            Regional: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញអត្តលេខ​របស់លោកអ្នក'
                    },
                }
            },
            Branch: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញអត្តលេខ​របស់លោកអ្នក'
                    },
                }
            },
            FCSite: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញអត្តលេខ​របស់លោកអ្នក'
                    },
                }
            },
            WhatsAppNumber: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញអត្តលេខ​របស់លោកអ្នក'
                    },
                }
            },
            FacebookUser: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញអត្តលេខ​របស់លោកអ្នក'
                    },
                }
            }
        }
    }).on('success.form.bv', function(e, data){
        spinner.show();
        var $form = jQuery('form#annual_meeting_registration'),
        url = 'https://script.google.com/macros/s/AKfycbx2G0ZnBigpdRB__PWWkF-MxzB50lXFM87EcqoJ1Q9HjXe1SU0/exec';
        e.preventDefault();
       
        var jqxhr = jQuery.ajax({
            url: url,
            method: "GET",
            dataType: "json",
            data: $form.serialize(),
            success: function(data) {
                spinner.hide();
                form_container.css('opacity',0);
                messagebox.show(500).css('opacity',0);
                console.log('Good');
            },
            error: function(data) {
                console.log(data);
                console.log('Bad');
            }
        });
    });
});
</script>
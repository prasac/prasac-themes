<?php
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
  'page_title' => 'Option',
  'menu_title' => 'Theme Option',
  'menu_slug'  => 'theme-option',
  'capability' => 'edit_posts',
  'redirect'   => false
  ));
  acf_add_options_sub_page(array(
    'page_title'    => 'General Setting',
    'menu_title'    => 'General',
    'parent_slug'   => 'theme-option',
  ));
  acf_add_options_sub_page(array(
    'page_title'    => 'Slider Setting',
    'menu_title'    => 'Slider',
    'parent_slug'   => 'theme-option',
  ));
  acf_add_options_sub_page(array(
    'page_title'    => 'Header Setting',
    'menu_title'    => 'Header',
    'parent_slug'   => 'theme-option',
  ));
  acf_add_options_sub_page(array(
    'page_title'    => 'Footer Settings',
    'menu_title'    => 'Footer',
    'parent_slug'   => 'theme-option',
  ));

  acf_add_options_page(array(
    'page_title'    => 'Page Setting',
    'menu_title'    => 'Page Setting',
    'menu_slug'     => 'page-settings',
    'capability'    => 'edit_posts',
    'redirect'      =>  true
  ));
  acf_add_options_sub_page(array(
    'page_title'    => 'Front Page',
    'menu_title'    => 'Front Page',
    'parent_slug'   => 'page-settings',
  ));

}

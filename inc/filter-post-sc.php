<?php
/**
 * Shortocde for displaying terms filter and results on page
 */
function merchant_post_sc($atts) {

  $a = shortcode_atts( array(
    'post'        => 'posts',
    'tax'         => 'post_tag', // Taxonomy
    'second_tax'  => 'category',
    'second_term' => 'news-and-media',
    'terms'       => false, // Get specific taxonomy terms only
    'active'      => false, // Set active term by ID
    'per_page'    => 12, // How many posts per page,
    'pager'       => 'pager' // 'pager' to use numbered pagination || 'infscr' to use infinite scroll
  ), $atts );

  $result = NULL;
  $post_type = $a['post'];
  $tax = $a['tax'];
  
  $second_tax = $a['second_tax'];
  $second_term = $a['second_term'];
  
  $terms  = get_terms( array(
    'taxonomy'     => $tax,
    'post_type'    => $post_type,
    'hide_empty'   => 1,
    'tax_query'    => array(
        array(
          'taxonomy' => $second_tax,
          'field'    => 'slug',
          'terms'    => $second_term,
          'operator' => 'IN'
        ),
      ),
    )
  );
  if (count($terms)) :
    ob_start(); ?>
    <div id="container-async" data-paged="<?= $a['per_page']; ?>" class="sc-ajax-filter" style="position:relative;">
        <!-- Filter -->
        <dl id="filter-dropdown" class="btn-group dropdown" data-value="all-terms">
        <dt><a href="#"><span><?php _e('[:en]Select the province[:kh]ជ្រើសរើសខេត្ត'); ?></span></a></dt>
        <dd>
        <ul class="nav-filter">
            <li>
            <a href="#" data-value="all-terms" data-post="<?= $post_type; ?>" data-filter="<?= $terms[0]->taxonomy; ?>" data-term="all-terms" data-page="1" data-second-tax="<?= $second_tax; ?>" data-second-term="<?= $second_term; ?>" ><?php _e('[:en]All[:kh]ទាំងអស់[:]'); ?><span class="value">all-term</span></a>
            </li>
            <?php
            foreach ($terms as $term ) :
                $args = array(
                    'fields' =>'id',
                    'posts_per_page' => -1,
                    'post_type' => $post_type,
                    'hide_empty'   => 1,
                    'tax_query' => array(
                    'relation' => 'AND',
                    array(
                        'taxonomy' => $tax,
                        'field'    => 'slug',
                        'terms'    => $term->slug,
                        'operator' => 'IN'
                    ),
                    array(
                        'taxonomy' => $second_tax,
                        'field'    => 'slug',
                        'terms'    => $second_term,
                        'operator' => 'IN'
                    )
                    )
                );
                $ps = get_posts( $args );
                $post_count = count($ps);
                $query = new WP_Query($args);
                if( $query->have_posts() ) :
                    ?>
                        <li <?php if ( $term->term_id == $a['active'] ) :?> class="active" <?php endif; ?> >
                            <a href="<?= get_term_link( $term, $term->taxonomy ); ?>" data-post="<?= $post_type; ?>" data-filter="<?= $term->taxonomy; ?>" data-term="<?php echo $term->slug; ?>" data-page="1" data-second-tax="<?= $second_tax; ?>" data-second-term="<?= $second_term; ?>" >
                            <?= $term->name; ?>&nbsp(<?= $post_count; ?>)
                            <span class="value"><?= $term->slug; ?></span>
                            </a>
                        </li>
                    <?php
                endif;
            endforeach;
            ?>
        </ul>
        </dd>
        </dl>
        <?php
            if( $second_tax == 'merchant_tag' ) :
        ?>
            <div class="btn-menu" style="float: right;">
                <a href="#" id="list" class="btn btn-default btn-sm">
                    <span class="fa fa-th-list"></span>
                </a>
                <a href="#" id="grid" class="btn btn-default btn-sm">
                    <span class="fa fa-th-large"></span>
                </a>
            </div>
        <?php
            endif;
        ?>
    <br /><!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Page Title</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        
    </body>
    </html><div class="clearfixed"></div>

      <!--<div class="status"></div>-->
      <div class="content"></div>
            
    <?php if ( $a['pager'] == 'infscr' ) : ?>
    <nav class="pagination infscr-pager">
      <a href="#page-2" class="btn btn-primary">Load More</a>
    </nav>
    <?php endif; ?>
    </div>
    
  <?php $result = ob_get_clean();
  endif;

  return $result;
}
add_shortcode( 'ajax_filter', 'merchant_post_sc');

/**
* Pagination
*/
function merchange_ajax_pager( $query = null, $paged = 1 ) {

  if (!$query)
    return;

  $paginate = paginate_links([
    'base'      => '%_%',
    'type'      => 'array',
    'total'     => $query->max_num_pages,
    'format'    => '#page=%#%',
    'current'   => max( 1, $paged ),
    'prev_text' => 'Prev',
    'next_text' => 'Next'
  ]);

  if ($query->max_num_pages > 1) : ?>
    <ul class="cd-pagination move-buttons custom-icons">
      <?php foreach ( $paginate as $page ) :?>
        <li><?= $page; ?></li>
      <?php endforeach; ?>
    </ul>

    <script>
    jQuery(document).ready(function(e){
        jQuery("ul.cd-pagination li").on('click',function(){
           var content_size =   jQuery("ul#wpas-results-inner").size();
        //    alert(content_size);
        });
    });
    </script>
  <?php 
  endif;
}

function merchant_filter_posts() {
  if( !isset( $_POST['nonce'] ) || !wp_verify_nonce( $_POST['nonce'], 'bobz' ) )
    die('Permission denied');

    /**
     * Default response
     */
    $response = [
      'status'  => 500,
      'message' => 'Something is wrong, please try again later ...',
      'content' => false,
      'found'   => 0
    ];

    $post_type = sanitize_text_field($_POST['params']['post']);

    $tax  = sanitize_text_field($_POST['params']['tax']);
    $term = sanitize_text_field($_POST['params']['term']);

    $second_tax  = sanitize_text_field($_POST['params']['second_tax']);
    $second_term = sanitize_text_field($_POST['params']['second_term']);
    
    $page = intval($_POST['params']['page']);
    $qty  = intval($_POST['params']['qty']);

    /**
     * Check if term exists
     */
    if (!term_exists( $term, $tax) && $term != 'all-terms') :
      $response = [
        'status'  => 501,
        'message' => 'Term doesn\'t exist',
        'content' => 0
      ];
      die(json_encode($response));
    endif;
    /**
     * Tax query
     */
    if ($term == 'all-terms') : 
      $tax_qry[] = [
        'taxonomy' => $tax,
        'field'    => 'slug',
        'terms'    => $term,
        'operator' => 'NOT IN'
      ];
    else :
      $tax_qry[] = [
        'taxonomy' => $tax,
        'field'    => 'slug',
        'terms'    => $term,
      ];
    endif;
    /**
     * Setup query
     */
    $args = [
      'paged'          => $page,
      'post_type'      => $post_type,
      'post_status'    => 'publish',
      'posts_per_page' => $qty,
      'tax_query'      => array(
        'relation' => 'AND',
        $tax_qry,
        array(
          'taxonomy' => $second_tax,
          'field'    => 'slug',
          'terms'    => $second_term,
          'operator' => 'IN'
        )
      )          
    ];

  $qry = new WP_Query($args);
  ob_start();
  if ($qry->have_posts()) : ?>
    <?php  
    if( $second_tax == 'merchant_tag' ) :
        echo '<div id="wpas-results-inner" class="row">';
            while ($qry->have_posts()) : $qry->the_post();
                get_template_part('template-parts/merchant', 'summary');
            endwhile;
        echo '</div>';
        ?>
            <style>
                .btn-menu .btn, .btn-menu .btn-default, .btn-menu .btn-sm{
                    color: #4BB848;
                    border-color: #4BB848;
                }
                div.content #wpas-results-inner.display-grid{
                    display: flex;
                    flex-direction: row;
                    flex-wrap: wrap;
                    padding:0 15px;
                }
                .content div.display-grid .column {
                    display: flex;
                    flex-direction: column;
                    flex-basis:33.33%;
                    position:relative;
                    webkit-transform: translate3d(0,0,0);
                    -moz-transform: translate3d(0,0,0);
                    -ms-transform: translate3d(0,0,0);
                    -o-transform: translate3d(0,0,0);
                    transform: translate3d(0,0,0);
                    transition: box-shadow .2s cubic-bezier(0,.2,.4,1);
                    background-color: #ffffff;
                    box-shadow: 0 4px 8px 0 transparent, 0 6px 20px 0 rgba(0,0,0,.12);
                    margin:10px 0;
                }
                .content div.display-grid .column:hover{
                    background-color:rgba(75,184,72,0.3);
                }
                div.content #wpas-results-inner:not(.display-grid) .merchant-content{
                    padding:0 15px;
                }
                #wpas-results-inner section, .poster-card_body{
                    margin-bottom: 0;
                    padding: 5px;
                }
                @media (max-width:1200px){
                    .content div.display-grid .column {
                        display: flex;
                        flex-direction: column;
                        flex-basis:50%;
                        padding:10px;
                    }
                }
                @media (max-width:600px){
                    .content div.display-grid .column {
                        display: flex;
                        flex-direction: column;
                        flex-basis:100%;
                        padding:10px;
                    }
                }
            </style>
        <?php
    elseif( $second_tax == 'office_categories' ) :
        echo '<ul id="wpas-results-inner" class="row">';
        while ($qry->have_posts()) : $qry->the_post();
            get_template_part('template-parts/branch', 'summary');
        endwhile;
        echo '</ul>';
    endif;
    ?>
    <?php
    /**
      * Pagination
      */
    merchange_ajax_pager($qry,$page);
    $response = [
        'status'=> 200,
        'found' => $qry->found_posts
    ];  
  else :
    $response = [
      'status'  => 201,
      'message' => 'No posts found'
    ];
  endif;
  $response['content'] = ob_get_clean();
  die(json_encode($response));
}
add_action('wp_ajax_do_filter_posts', 'merchant_filter_posts');
add_action('wp_ajax_nopriv_do_filter_posts', 'merchant_filter_posts');
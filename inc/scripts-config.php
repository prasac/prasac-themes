<?php session_start();?>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery('.lightbox').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        },
    });
    
    remove_link_from_menu();
    //Call function for check old browser
    check_old_browser();

    jQuery("#filter-dropdown dt a").click(function(e) {
        e.preventDefault();
        jQuery("#filter-dropdown dd ul").toggle();
    });
                            
    jQuery("#filter-dropdown dd ul li a").click(function() {
      var text = jQuery(this).html();
      jQuery("#filter-dropdown dt a span").html(text);
      jQuery("#filter-dropdown dd ul").hide();
    }); 

     if(jQuery('body').hasClass('img-lazy')){
           jQuery(".img-lazy").unveil();
     }
     
   //Change font size
   jQuery("ul#font-size-switcher li#font-size-big>a").click(function(){
        jQuery('article.single-post,section.career,section.page,section.downloads,article.content-summary,div.timeline-body,div.page-title p,.content div,.content p,section.page-title,.calculator_list,section.single-content').css('font-size','150%');
   });

   jQuery("ul#font-size-switcher li#font-size-normal>a").click(function(){
        jQuery('article.single-post,section.career,section.page,section.downloads,article.content-summary,div.timeline-body,div.page-title p,.content div,.content p,section.page-title,.calculator_list,section.single-content').css('font-size','100%');
   });

    jQuery("ul.timeline>li:odd").addClass('timeline-inverted');

    if (jQuery(window).width() < 768) {
          jQuery(".dropdown-toggle").attr('data-toggle', 'dropdown');
           //Clone Sidebar
           jQuery(".sidebar").clone().prependTo("#buttom_sidebar");
    }

    // Bootstrap menu magic
    jQuery(window).resize(function() {
      if (jQuery(window).width() < 768) {
          jQuery(".dropdown-toggle").attr('data-toggle', 'dropdown');
           //Clone Sidebar
           jQuery(".sidebar").clone().prependTo("#buttom_sidebar");
      } else {
          jQuery(".dropdown-toggle").removeAttr('data-toggle dropdown');
      }
    });

   
    // Function that validates email address through a regular expression.
    function validateEmail(sEmail) {
        var filter = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }

    //Subscribe form in footer action herere
    jQuery("#btn-subscribe").click(function(){
        var $email = jQuery('#subscribe-email').val();
        if($email !=''){
            if(validateEmail($email)){
                jQuery('.subscribe-form').submit(function(e) {
                    e.preventDefault();
                    var formURL = '/wp-admin/admin-ajax.php'; 
                    var request_method = jQuery(this).attr("method");
                    var postData = jQuery(this).serialize();
                    jQuery.ajax({
                        url: formURL,
                        type: request_method,
                        data: postData,
                        success: function(data, textStatus, jqXHR) {
                            console.log(data);
                            if(data == 'Exist'){
                                jQuery(".subscribe-popup .panel").removeClass('panel-warning');
                                jQuery(".subscribe-popup .panel").removeClass('panel-danger');
                                jQuery(".modal-dialog .panel").removeClass('panel-primary');

                                jQuery(".subscribe-popup .panel").addClass('panel-warning');
                                jQuery(".subscribe-popup .panel #titleLabel").html('<i class="fa fa-check-circle fa-6"></i> <?php _e('[:en] Your email Exist[:kh]អុី​ម៉ែល​របស់​លោកអ្នក​បាន​ចូល​រួម​រួចហើយ​[:]'); ?>');
                                jQuery(".subscribe-popup .panel .modal-body").html('<?php _e('[:en]<p>Sorry, your email address has been found in our mailing list. Please try different email address.</p>[:kh]<p>សូមអភ័ទោស អុីម៉ែល​របស់លោក​អ្នក​មាន​រួចហើយ​ក្នុង​បញ្ញី​អុី​ម៉ែល​របស់​ពួក​យើង។ សូម​ព្យាយាម​អុី​ម៉ែល​ផ្សេង​ពីនេះ​</p>[:]'); ?>');
                                setTimeout(function() {
                                    jQuery("#subscribe_modal").modal('show'); 
                                }, 2000);
                                return false;
                            }else{
                                jQuery(".subscribe-popup .panel").removeClass('panel-warning');
                                jQuery(".subscribe-popup .panel").removeClass('panel-danger');
                                jQuery(".modal-dialog .panel").removeClass('panel-primary');

                                jQuery(".subscribe-popup .panel").addClass('panel-primary');
                                jQuery(".subscribe-popup .panel #titleLabel").html('<i class="fa fa-check-circle fa-6"></i> <?php _e('[:en]Successful Subscribed![:kh]ចូលរួម​យ៉ាង​ជោគជ័យ![:]'); ?>');
                                jQuery(".subscribe-popup .panel .modal-body").html('<?php _e('[:en]<p>You recently subscribed with PRASAC.</p><p>You will soon recieve an email when we have new promotion/information.</p><p>Thanks so much for your subscription.</p>[:kh]<p>លោកអ្នក​ទើប​តែ​ចូលរួម​ជា​មួយ​ប្រាសាក់</p><p>លោកអ្នក​នឹង​ទទួល​បាន​អុី​ម៉ែលនៅពេលដែល​ពួក​យើង​មាន​ព័ត៌មាន ឬប្រូម៉ូសិនថ្មី </p><p>អរគុណ​សម្រាប់​ការ​ចូលរួម​របស់​លោក​អ្នក!</p>[:]'); ?>');
                                jQuery("#subscribe_modal").modal('show');
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus,'errorThrown=>'+errorThrown);
                            jQuery(".subscribe-popup .panel").removeClass('panel-warning');
                            jQuery(".subscribe-popup .panel").removeClass('panel-danger');
                            jQuery(".subscribe-popup .panel").removeClass('panel-primary');

                            jQuery(".subscribe-popup .panel").addClass('panel-danger');
                            jQuery(".subscribe-popup .panel #titleLabel").html('<i class="fa fa-exclamation-triangle fa-6"></i> <?php _e('[:en] Unsuccess subscribe![:kh] ចូលរួមមិន​ជោគ​ជ័យ ![:]')?>');
                            jQuery(".subscribe-popup .panel .modal-body").html('<p><?php _e('[:en] Sorry, your subscription did not process success, please try again or contact to administrator to assist you.[:kh]សូម​អភ័យទោស ការចូល​​រួម​របស់​លោក​អ្នក​មិន​ជោគជ័យ​នោះ​ទេ សូម​ព្យាយាម​ម្ដងទៀត ឬ​ទាក់ទង​ទៅកាន់​ Administrator ដើម្បី​សម្រួលលោកអ្នក​[:]')?></p>');
                            jQuery("#subscribe_modal").modal('show');
                            return false;
                        }
                    });
                });
            }else{
                jQuery(".subscribe-popup .panel").removeClass('panel-warning');
                jQuery(".subscribe-popup .panel").removeClass('panel-danger');
                jQuery(".subscribe-popup .panel").removeClass('panel-primary');

                jQuery(".subscribe-popup .panel").addClass('panel-warning');
                jQuery(".subscribe-popup .panel #titleLabel").html('<i class="fa fa-exclamation-triangle fa-6"></i> <?php _e('[:en] Invalid email address![:kh] អុីម៉ែល​មិន​ត្រឹម​ត្រូវ ![:]')?>');
            jQuery(".subscribe-popup .panel .modal-body").html('<p><?php _e('[:en] Sorry! your email address entered does not correct, please check and try it again.[:kh] សូមអភ័យទោស ​អុី​ម៉ែល​របស់​លោក​អ្នកដែល​បាន​បញ្ចូល​មិន​ត្រឹមត្រូវទេ សូម​មេត្តា​ត្រួតពិនិត្យ ហើយ​ព្យាយាម​ម្ដងទៀត​[:]')?></p>');
                jQuery("#subscribe_modal").modal('show');
                return false;
            }
        }else{
            jQuery(".subscribe-popup .panel").removeClass('panel-warning');
            jQuery(".subscribe-popup .panel").removeClass('panel-danger');
            jQuery(".subscribe-popup .panel").removeClass('panel-primary');
            
            jQuery(".subscribe-popup .panel").addClass('panel-danger');
            jQuery(".subscribe-popup .panel #titleLabel").html('<i class="fa fa-exclamation-triangle fa-6"></i> <?php _e('[:en] Empty email address![:kh] គ្មានអីុ​ម៉ែល​នោះ​ទេ ![:]')?>');
            jQuery(".subscribe-popup .panel .modal-body").html('<p><?php _e('[:en] Sorry! Please enter your email address and try it again.[:kh] សូមអភ័យទោស សូម​បញ្ចូល​អុី​ម៉ែល​របស់​លោក​អ្នក ហើយ​ព្យាយាម​ម្ដងទៀត​[:]')?></p>');
            jQuery("#subscribe_modal").modal('show');
            return false;
        }
        jQuery("div.alert-wraper").hide();//slideDown('slow');
        jQuery(".close_footer").click(function(){
            jQuery("div.alert-wraper").hide();
        });
    });
    //End subscribe form here
    
    //unsubscribe Emails Form
    jQuery("#btn-unsubscribe").click(function(){
        var $email = jQuery('#unsubscribe-email').val();
        if($email !=''){
            if(validateEmail($email)){
                jQuery('#unsubscribe').submit(function(e) {
                    e.preventDefault();
                    var formURL = '/wp-admin/admin-ajax.php';
                    var request_method = jQuery(this).attr("method");
                    var postData = jQuery(this).serializeArray();
                    jQuery.ajax({
                        url: formURL,
                        type: request_method,
                        data: postData,
                        success: function(data, textStatus, jqXHR) {
                            console.log(data);
                            if(data == 'Not exist'){
                                jQuery(".subscribe-popup .panel").removeClass('panel-warning');
                                jQuery(".subscribe-popup .panel").removeClass('panel-danger');
                                jQuery(".modal-dialog .panel").removeClass('panel-primary');

                                jQuery(".subscribe-popup .panel").addClass('panel-warning');
                                jQuery(".subscribe-popup .panel #titleLabel").html('<i class="fa fa-check-circle fa-6"></i> <?php _e('[:en] Your email Exist[:kh]អុី​ម៉ែល​របស់​លោកអ្នក​បាន​ចូល​រួម​រួចហើយ​[:]'); ?>');
                                jQuery(".subscribe-popup .panel .modal-body").html('<?php _e('[:en]<p>Sorry, your email address does not found in our mailing list.</p>[:kh]<p>សូមអភ័ទោស អុីម៉ែល​របស់លោក​អ្នក​ពំុមាន​ក្នុង​បញ្ញី​អុី​ម៉ែល​របស់​ពួក​យើង​</p>[:]'); ?>');
                                // jQuery("#subscribe_modal").modal('show');
                                setTimeout(function() {
                                    jQuery('#subscribe_modal').modal('show');
                                }, 2000);
                                return false;
                            }else{
                                jQuery(".subscribe-popup .panel").removeClass('panel-warning');
                                jQuery(".subscribe-popup .panel").removeClass('panel-danger');
                                jQuery(".modal-dialog .panel").removeClass('panel-primary');

                                jQuery(".subscribe-popup .panel").addClass('panel-primary');
                                jQuery(".subscribe-popup .panel #titleLabel").html('<i class="fa fa-check-circle fa-6"></i> <?php _e('[:en]You have Successfully Unsubscribed![:kh]អ្នកបានចាកចេញដោយ​ជោគជ័យ![:]'); ?>');
                                jQuery(".subscribe-popup .panel .modal-body").html('<?php _e('[:en]<p>You recently unsubscribed with PRASAC.</p><p>We are so sad to see you leave us.</p>[:kh]<p>លោកអ្នក​ទើប​តែបានចាកចេញពីការទទួលដំណឹងពី​ប្រាសាក់</p><p>យើងខ្ញុំពិតជាមានការសោកស្តាយជាខ្លាំងចំពោះការចាកចេញរបស់អ្នក!</p>[:]'); ?>');
                                jQuery("#subscribe_modal").modal('show');

                            //     window.setTimeout(function(){
                            //         window.location.href = "https://www.prasac.com.kh/";
                            //     }, 5000);
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log(textStatus,'errorThrown=>'+errorThrown);
                            jQuery(".subscribe-popup .panel").removeClass('panel-warning');
                            jQuery(".subscribe-popup .panel").removeClass('panel-danger');
                            jQuery(".subscribe-popup .panel").removeClass('panel-primary');

                            jQuery(".subscribe-popup .panel").addClass('panel-danger');
                            jQuery(".subscribe-popup .panel #titleLabel").html('<i class="fa fa-exclamation-triangle fa-6"></i> <?php _e('[:en] Your unsubscribption was not successfully![:kh] ការចាកចេញរបស់អ្នកមិន​ជោគ​ជ័យទេ![:]')?>');
                            jQuery(".subscribe-popup .panel .modal-body").html('<p><?php _e('[:en] Sorry, your unsubscription request did not process success, please try again or contact to administrator to assist you.[:kh]សូម​អភ័យទោស ការចាកចេញ​របស់​លោក​អ្នក​មិន​ជោគជ័យ​នោះ​ទេ សូម​ព្យាយាម​ម្ដងទៀត ឬ​ទាក់ទង​ទៅកាន់​ Administrator ដើម្បី​សម្រួលលោកអ្នក​[:]')?></p>');
                            jQuery("#subscribe_modal").modal('show');
                            return false;
                        }
                    });
                });
            }else{
                jQuery(".subscribe-popup .panel").removeClass('panel-warning');
                jQuery(".subscribe-popup .panel").removeClass('panel-danger');
                jQuery(".subscribe-popup .panel").removeClass('panel-primary');

                jQuery(".subscribe-popup .panel").addClass('panel-warning');
                jQuery(".subscribe-popup .panel #titleLabel").html('<i class="fa fa-exclamation-triangle fa-6"></i> <?php _e('[:en] Invalid email address![:kh] អុីម៉ែល​មិន​ត្រឹម​ត្រូវ ![:]')?>');
                jQuery(".subscribe-popup .panel .modal-body").html('<p><?php _e('[:en] Sorry! your email address entered does not correct, please check and try it again.[:kh] សូមអភ័យទោស ​អុី​ម៉ែល​របស់​លោក​អ្នកដែល​បាន​បញ្ចូល​មិន​ត្រឹមត្រូវទេ សូម​មេត្តា​ត្រួតពិនិត្យ ហើយ​ព្យាយាម​ម្ដងទៀត​[:]')?></p>');
                jQuery("#subscribe_modal").modal('show');
                return false;
            }
        }else{
            jQuery(".subscribe-popup .panel").removeClass('panel-warning');
            jQuery(".subscribe-popup .panel").removeClass('panel-danger');
            jQuery(".subscribe-popup .panel").removeClass('panel-primary');
            
            jQuery(".subscribe-popup .panel").addClass('panel-danger');
            jQuery(".subscribe-popup .panel #titleLabel").html('<i class="fa fa-exclamation-triangle fa-6"></i> <?php _e('[:en] Empty email address![:kh] គ្មានអីុ​ម៉ែល​នោះ​ទេ ![:]')?>');
            jQuery(".subscribe-popup .panel .modal-body").html('<p><?php _e('[:en] Sorry! Please enter your email address and try it again.[:kh] សូមអភ័យទោស សូម​បញ្ចូល​អុី​ម៉ែល​របស់​លោក​អ្នក ហើយ​ព្យាយាម​ម្ដងទៀត​[:]')?></p>');
            jQuery("#subscribe_modal").modal('show');
            return false;
        }
    });//End Unsubscribe Emails Form

    youtube_popup();
    jQuery("#video_popup").click(function (e) {
        e.preventDefault();
        var video_link = jQuery(this).prop('href');
        // alert(video_link);
        jQuery.magnificPopup.open({
            type: 'iframe',
            midClick: false,
            items: {
                src: video_link
            },
            removalDelay:600,
            mainClass: 'mfp-fade',
            preloader: true,
            fixedContentPos: true,
            iframe: {
                patterns: {
                    youtube: {
                        index: 'youtube.com/', 
                        id: 'v=', 
                        src:'//'+video_link+'',
                    }
                }
            }
        });
    });
//==========End update==========
  <?php if( is_front_page() ):?>
	    // jQuery('.carouselss').carousel('pause');
        //===========Collapse fealture post=======
        jQuery(".fixOverlayDiv").each(function(e){
                jQuery(this).hover(
                    function() {
                        jQuery(this).find('.OverlayText .panel-collapse').collapse('show');
                    }, function() {
                        jQuery(this).find('.OverlayText .panel-collapse').collapse('hide');
                    }
                );
        });

        <?php
        $option = get_field('video_popup', 'option');
        $image_popup = get_field('image_random', 'option');
        $image_link = get_field('link','option');

        
        $fields = '[:en]image_slider_en[:kh]image_slider_kh[:]';
        $image_slider_field =   __($fields);
        $image_slider = get_field('image_slider_kh','option');
        if($option=='image_popup' && $image_popup):
            while(the_repeater_field('image_random','option')):
                $rows = get_field('image_random','option'); // get all the rows
                $rand_row = $rows[array_rand($rows)]; // get the first row
                $rand_row_image = $rand_row['image_random_source']; // get the sub field value
                $rand_row_link = $rand_row['link']; // get the sub field value
            endwhile;
        ?>
            var now, lastDatePopupShowed;
            now = new Date();
            var image_part = "<?= $rand_row_image; ?>";
            setTimeout(openPopup, 2000);
            function openPopup(){
                jQuery.magnificPopup.open({
                    items: { src: image_part },
                    type: 'image',
                    mainClass: 'mfp-fade',
                    image: {
                        markup: '<div class="mfp-figure">'+
                        '<div class="mfp-close"></div>'+
                        '<a href="<?php echo $rand_row_link ?>" target="_blank"><div class="mfp-img"></div></a>'+ // Floated left
                        '<div class="mfp-title"></div>'+ //This is floated right shows up on the right side
                        '<div class="mfp-bottom-bar">'+
                        '<div class="mfp-counter"><span id="counter">10</span> Sec</div>'+
                        '</div>'+
                        '</div>',
                        tError: '<a href="%url%">The image</a> could not be loaded.' // Error message
                    }
                });
            }
            var counter = 10;
            setInterval(function() {
                counter--;
                if (counter >= 0) {
                span = document.getElementById('counter');
                if(span != null){
                span.innerHTML = counter;
                }
                }
                // Display 'counter' wherever you want to display it.
                if (counter === 0) {
                jQuery.magnificPopup.close();
                clearInterval(counter);
                }
            }, 2000);
        <?php
        endif;
        if($option=='image_slideshow'):
            $i = 0;
            $rand_row_image='';
            $rand_row_link = '';
            $images = '';
            // var_dump($images_source);exit;
            // if($images_source):
                while( have_rows('image_slider','option') ): the_row();
                    $rand_row_link =    get_field('image_slider_link');
                    $images_source =  get_field($image_slider_field);

                    $rows = get_field('image_slider','option'); // get all the rows
                    $images_source = $rows[$i][$image_slider_field];
                    $rand_row_link = $rows[$i]['image_slider_link'];

                    $images .= '<section class="slick-slider"> <a href="'.$rand_row_link.'" class="slide-items"><img data-lazy="'.$images_source.'" src="'.$images_source.'" alt="" title="" class="img-responsive" style="margin:0 auto;"/></a></section>';
                    $i++;
                endwhile;
            
        ?>
                jQuery("#homepage_popup_modal .panel .modal-body").html('<div id="popup-slide-item"><?= $images; ?></div>');
                setTimeout(openPopup,100);
                
                function openPopup(){
                    jQuery("#homepage_popup_modal").modal('show');
                }
                var counter = 20;
                setInterval(function() {
                    counter--;
                    if (counter >= 0) {
                        span = document.getElementById('counter');
                        if(span != null){
                            span.innerHTML = counter;
                        }
                    }
                    // Display 'counter' wherever you want to display it.
                    if (counter === 0) {
                        jQuery("#homepage_popup_modal").modal('hide');
                        clearInterval(counter);
                    }
                }, 2000);


                /*==========Popup Slider===== */
                jQuery('#popup-slide-item').slick({
                    autoplay: true,
                    autoplaySpeed: 5000,
                    cssEase: 'linear',
                    fade: true,
                    dots: true,
                    arrows:false,
                    // nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"></button>',
                    // prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;"></button>'
                });
            <?php 
            // endif;
        endif;
            $video_popup = get_field('video_link', 'option');
            if($option=='video_url' && $video_popup):
                ?>
                var video_popup ="<?= $video_popup?>";
                jQuery(window).load(function () {
                    jQuery.magnificPopup.open({
                        type: 'iframe',
                        midClick: false,
                        autoplay:true,
                        items: {
                            src: video_popup
                        },
                        removalDelay:600,
                        mainClass: 'mfp-fade',
                        preloader: true,
                        fixedContentPos: true,
                        iframe: {
                            patterns: {
                                youtube: {
                                    index: 'youtube.com/', 
                                    id: 'v=', 
                                    src:'//'+video_popup+'',
                                }
                            }
                        }
                    });
                });
            <?php endif; ?>
            <?php
            $html_popup = get_field('html_popup', 'option');
            if($option=='html_popup'): ?>
                jQuery('#popupModal').modal('show');
            <?php endif; ?>

        /* Hero slider script config */
        jQuery('#single-item').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            cssEase: 'linear',
            fade: false,
            dots: true,
            arrows:true,
            nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"></button>',
            prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;"></button>'
        });

        
        /* Hero slider script config */
        /* Hero slider script config */
        jQuery('.popup-slider').slick({
            autoplay: true,
            autoplaySpeed: 5000,
            cssEase: 'linear',
            fade: false,
            dots: false,
            arrows:true,
            nextArrow: '<button type="button" data-role="none" class="slick-next slick-arrow" aria-label="Next" role="button" style="display: block;"></button>',
            prevArrow: '<button type="button" data-role="none" class="slick-prev slick-arrow" aria-label="Previous" role="button" style="display: block;"></button>'
        });
        
        

        jQuery('#product-feature').slick({
            dots: false ,
            infinite: true,
            autoplay      : true,
            autoplaySpeed: 5000,
            slidesToShow: 3,
            slidesToScroll: 1,
            responsive: [
                {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll:1
                }
                },
                {
                breakpoint: 520,
                settings: {
                    slidesToShow:1,
                    slidesToScroll: 1
                }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        jQuery('#lender-carousel').slick({
            infinite      : true,
            arrows        : false,
            autoplay      : true,
            autoplaySpeed : 2000,
            slidesToShow  : 6,
            slidesToScroll: 1
        });

        //Marquee text
        jQuery('.marquee-with-options').slick({
            lazyLoad       : 'ondemand',
            accessibility  : false,
            arrows         : false,
            infinite       : true,
            slidesToShow   :1,
            speed:1500,
            fade: true,
            autoplay:true,
            autoplaySpeed:3500,
        });
        <?php elseif(is_page('video')): ?>
            //Call script for video popup
            youtube_popup();
        <?php elseif( is_single() ): ?>
            //Call script for single page(Detail page)
            single_page_script();

  <?php elseif( is_page_template('page-apply-loan.php') ) : ?>
    <?php else:?>
      //Call function for active menu with mobile detet
      active_menu();
    <?php endif; ?>
    jQuery("ul.sidebar_content li.page_item_has_children ul.children").hide();
    jQuery("ul.sidebar_content li.page_item_has_children>a").click(function(e){
        e.preventDefault();
        jQuery(this).parent().find("ul.children").slideToggle();
    });
});  




    //All function to be called will be here

    function check_old_browser(){
      jQuery("#alert-wanring").hide();
      var nVer = navigator.appVersion;
      var nAgt = navigator.userAgent;
      var browserName  = navigator.appName;
      var fullVersion  = ''+parseFloat(navigator.appVersion); 
      var majorVersion = parseInt(navigator.appVersion,10);
      var nameOffset,verOffset,ix;
      if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
        browserName = "Microsoft Internet Explorer";
        fullVersion = nAgt.substring(verOffset+5);
        jQuery("#alert-wanring").show();
      }
    }

    function youtube_popup(){
        jQuery('.video').magnificPopup({
            type: 'iframe',
            gallery: { enabled:true },
            removalDelay:600,
            mainClass: 'mfp-fade',
            preloader: true,
            fixedContentPos: false,
            autoplay:true,
            iframe: {
                patterns: {
                    dailymotion: {
                        index: 'youtube.com',
                        id: function(url) {        
                            var m = url.match(/^.+youtube.com\/(video|hub)\/([^_]+)[^#]*(#video=([^_&]+))?/);
                            if (m !== null) {
                            if(m[4] !== undefined) { 
                                return m[4];
                            }
                            return m[2];
                            }
                            return null;
                        },
                        src: 'https://www.youtube.com/embed/%id%?autoplay=1&playlist=%id%&loop=1'
                    }
                }
            }
        });
    }

    function single_page_script(){
      jQuery('.with-caption').magnificPopup({
        type: 'image',
        gallery:{
          enabled:true
        },
        closeOnContentClick: true,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
          verticalFit: true,
          titleSrc: function(item) {
            return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
          }
        },
        zoom: {
          enabled: true
        }
      });
      
      jQuery('#single-content-slider').slick({
        autoplay: true,
        arrows: true,
        lazyLoad: 'ondemand',
        fade: false,
        dots: false
      });
      jQuery('#branch_atm_slider').slick({
        autoplay: true,
        arrows: true,
        lazyLoad: 'ondemand',
        fade: false,
        dots: false
      });
      
      jQuery('#content-slider').slick({
          autoplay: false,
          lazyLoad: 'ondemand',
          fade: false,
          dots: true
      });
     
      

      var $status = jQuery('.pagingInfo');
      var $slickElement = jQuery('#merchant-slider');
      $slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
          //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
          var i = (currentSlide ? currentSlide : 0) + 1;
          $status.text(i + ' of ' + slick.slideCount);
      });
      $slickElement.slick({
          autoplay: false,
          arrows: true,
          lazyLoad: 'ondemand',
          fade: false,
          dots: false,
          nextArrow: '.next',
          prevArrow: '.prev',
      });
    }


    function active_menu(){
        <?php
        $detect = new Mobile_Detect;
        if ( $detect->isMobile() ) :?>
            var current_url  = jQuery(location).attr('href');
            var current_url_split = current_url.split("/");
            var current_url_lastsegment = current_url_split[current_url_split.length - 2];//Get last segment of url
            jQuery("ul#mobile-menu li").removeClass(" current-menu-parent current_page_parent");//Remove all active main menu
            jQuery("ul#mobile-menu li ul.dropdown-menu li").removeClass("current_page_item");//Remove all active main menu
            jQuery("ul#mobile-menu>li>ul.dropdown-menu>li>a").each(function(e){
                    var current_sub_menu = jQuery(this).attr("href");
                    var current_sub_menu_split = current_sub_menu.split("/");
                    var find_active_sub_menu = jQuery.inArray(current_url_lastsegment,current_sub_menu_split);
                    if(current_sub_menu_split.length>3){
                        if(find_active_sub_menu > -1){
                            jQuery("ul#mobile-menu>li>ul.dropdown-menu>li").removeClass(' current_page_item active');//Remove active sub menu 
                            jQuery("ul#mobile-menu>li").removeClass(' current-menu-parent current_page_parent');//Remove active menu from main menu
                            
                            jQuery(jQuery(this).parent('li').parent('ul').parent('li')).addClass(' current-menu-parent current_page_parent');
                            jQuery(jQuery(this).parent('li')).addClass(' current_page_item active');
                        }
                    }
            });
        <?php
        else :?>
            var current_url  = jQuery(location).attr('href');
            var current_url_split = current_url.split("/");
            var current_url_lastsegment = current_url_split[current_url_split.length - 2];//Get last segment of url
            jQuery("ul#desktop-menu li").removeClass(" current-menu-parent current_page_parent");//Remove all active main menu
            jQuery("ul#desktop-menu li ul.sub-menu li").removeClass("current_page_item");//Remove all active main menu
            jQuery("ul#desktop-menu>li ul.sub-menu>li>a").each(function(e){
                var current_sub_menu = jQuery(this).attr("href");
                // console.log(current_sub_menu);
                // var current_sub_menu_split = current_sub_menu.split("/");
                // var find_active_sub_menu = jQuery.inArray(current_url_lastsegment,current_sub_menu_split);
                // console.log(current_url);
                // console.log(current_sub_menu);
                if(current_url == current_sub_menu){
                    jQuery("ul#desktop-menu>li").removeClass(' current-menu-parent current_page_parent');//Remove active menu from main menu
                    
                    jQuery(jQuery(this).parent('li').parent('ul.sub-menu').parent('div  ').parent('div.container').parent('div.sub-menu').parent('li')).addClass(' current-menu-parent current_page_item');
                    jQuery(jQuery(this).parent('li')).addClass(' current-menu-item');
                }
            });

            jQuery("ul#desktop-menu>li div.service-item ul.sub-menu-style-none>li>h3>a").each(function(e){
                var current_sub_menu = jQuery(this).attr("href");
                if(current_url == current_sub_menu){
                    jQuery("ul#desktop-menu>li").removeClass(' current-menu-parent current_page_parent');//Remove active menu from main menu
                    
                    jQuery(jQuery(this).parent('h3').parent('li').parent('ul.sub-menu-style-none').parent('div.service-item').parent('div.container').parent('div.sub-menu').parent('li')).addClass(' current-menu-parent current_page_item');
                    jQuery(jQuery(this).parent('h3').parent('li')).addClass(' current-menu-item');
                }
            });
            jQuery("ul#desktop-menu>li ul.sub-menu-style-none>li>a").each(function(e){
                var current_sub_menu = jQuery(this).attr("href");
                if(current_url == current_sub_menu){
                    jQuery("ul#desktop-menu>li").removeClass(' current-menu-parent current_page_parent');//Remove active menu from main menu
                    
                    jQuery(jQuery(this).parent('li').parent('ul.sub-menu-style-none').parent('div').parent('div.container').parent('div.sub-menu').parent('li')).addClass(' current-menu-parent current_page_item');
                    jQuery(jQuery(this).parent('li')).addClass(' current-menu-item');
                }
            });

            jQuery("ul#desktop-menu>li div.media-item").each(function(e){
                var current_sub_menu = jQuery(this).find('ul.sub-menu-style-none>li>a').attr("href");
                if(current_url == current_sub_menu){
                    jQuery("ul#desktop-menu>li").removeClass(' current-menu-parent current_page_parent');//Remove active menu from main menu
                    
                    //jQuery(jQuery(this).parent('li').parent('ul.sub-menu-style-none').parent('div').parent('div.container').parent('div.sub-menu').parent('li')).addClass(' current-menu-parent current_page_item');
                    jQuery(jQuery(this).parent('li')).addClass(' current-menu-parent current_page_item');
                    jQuery(this).find('ul.sub-menu-style-none>li>a').addClass(' current_page_item');
                }
            });
        <?php endif; ?>
    }
  function remove_link_from_menu(){
        
      //Disable link from main menu
      jQuery("ul#desktop-menu>li>a").each(function(e){
          var pageName = jQuery(this).attr('href');
          var pageSearch = "http://192.168.115.78/prasac/prasac-point/";
          if(pageSearch.search(pageName) == -1){
              jQuery(this).attr("href","#");//.css('cursor','default');
          }
          
      });
      //Disable link from sub menu
      jQuery("ul#desktop-menu>li>.sub-menu>li>.sub-menu").each(function(e){
          //alert(11);
      jQuery(this).parent('li').find('>a').attr("href","#");//.css('cursor','default');
      });

      //Disable link from menu sidebar
      jQuery(".sidebar ul.sidebar_content>li>.children").each(function(e){
          //alert(11);
        jQuery(this).parent('li').find('>a').attr("href","#");//.css('cursor','default');
      //jQuery(this).parent('li').find('>a').prepend('<i class="fa fa-plus caret_parent" aria-hidden="true"></i>');
      });
      
      //Toggle DropDown sidebar menu
      var child_num = jQuery("ul li.page_item_has_children").length;
      jQuery("ul li.page_item_has_children:nth-child(2) i.caret_parent").click(function(){
          jQuery("ul li.page_item_has_children:nth-child(2) ul.children").slideToggle("fast");
      });

      jQuery("ul li.page_item_has_children:nth-child(3) i.caret_parent").click(function(){
          jQuery("ul li.page_item_has_children:nth-child(3) ul.children").slideToggle("fast");
      });

      jQuery("ul li.page_item_has_children:nth-child(4) i.caret_parent").click(function(){
          jQuery("ul li.page_item_has_children:nth-child(4) ul.children").slideToggle("fast");
      });
      //Display and hid search box
      var search_text = jQuery("form.navbar-form input#search").val();
      if(search_text !=""){
          jQuery("form.navbar-form").css('display','block');
      }

      jQuery(".search_click").click(function(){
          // jQuery("form.navbar-form").slideToggle('fast');
          jQuery("form.navbar-form").addClass(' search');
          //jQuery(this).hide();
      });
      
      jQuery(document).on('click', function (event) {
          if (!jQuery(event.target).closest('.search_click').length && !jQuery(event.target).closest('#search').length) {
                //jQuery("form.navbar-form input#search").css('width','0');
                //jQuery("form.navbar-form").slideUp('fast');//.hide();
                jQuery("form.navbar-form").removeClass(' search');
                //jQuery('.search_click').show();
          }
      });

    jQuery(".sidebar_content .page_item a").each(function(){
      var active_report_sidebar = jQuery(this).attr("href");
      var current_url  = jQuery(location).attr('href');
      // console.log(active_report_sidebar==current_url);
      if (current_url.indexOf(jQuery(this).attr("href")) != -1) {
        jQuery(this).addClass(" active");
      }
    });

    jQuery("ul>li.page-item-547>a").remove();

    function menu_slide(){
      //responsive menu toggle
      jQuery("#menutoggle").click(function() {
        jQuery('.xs-menu').toggleClass('displaynone');
      });
      //add active class on menu
      jQuery('ul li').click(function(e) {
        e.preventDefault();
        jQuery('li').removeClass('active');
        jQuery(this).addClass('active');
      });
      //drop down menu	
      jQuery(".drop-down").hover(function() {
        jQuery(this).addClass('display-on');
      });

      jQuery(".drop-down").click(function() {
        jQuery('.mega-menu').addClass('display-on');
      });

      jQuery(".drop-down").mouseleave(function() {
        jQuery('.mega-menu').removeClass('display-on');
      });
    }
  }

jQuery(function(){
    jQuery(".drop-down").hover(            
            function() {
                jQuery('span', this).toggleClass("caret caret-up");                
            },
            function() {
                jQuery('span', this).toggleClass("caret caret-up");                
            });
});
</script>
    <script async src="https://www.youtube.com/iframe_api"></script>
    <script>
        function onYouTubeIframeAPIReady(){
            var youtube_id = document.getElementById("youtubeID");
            if(youtube_id !== null){
                var youtube_id_value = document.getElementById("youtubeID").value;
                var player;
                player = new YT.Player('ytplayer', {
                    videoId:youtube_id_value, // YouTube Video ID
                    width: 560,               // Player width (in px)
                    height: 316,              // Player height (in px)
                    playerVars: {
                    autoplay: 1,        // Auto-play the video on load
                    controls: 1,        // Show pause/play buttons in player
                    showinfo: 0,        // Hide the video title
                    modestbranding: 1,  // Hide the Youtube Logo
                    loop: 1,
                    playlist:youtube_id_value,            // Run the video in a loop
                    fs: 0,              // Hide the full screen button
                    cc_load_policy: 0, // Hide closed captions
                    iv_load_policy: 3,  // Hide the Video Annotations
                    autohide: 0         // Hide video controls when playing
                    },
                    events: {
                    onReady: function(e) {
                        e.target.mute();
                    }
                    }
                });
            }

            var youtube_class = document.getElementsByClassName("youtubeID");
            if(youtube_class !== null){
                var youtube_id_value = document.getElementsByClassName("youtubeID").value;
                var player;
                player = new YT.Player('ytplayer1', {
                    videoId:youtube_id_value, // YouTube Video ID
                    width: 560,               // Player width (in px)
                    height: 316,              // Player height (in px)
                    playerVars: {
                    autoplay: 1,        // Auto-play the video on load
                    controls: 1,        // Show pause/play buttons in player
                    showinfo: 0,        // Hide the video title
                    modestbranding: 1,  // Hide the Youtube Logo
                    loop: 1,
                    playlist:youtube_id_value,            // Run the video in a loop
                    fs: 0,              // Hide the full screen button
                    cc_load_policy: 0, // Hide closed captions
                    iv_load_policy: 3,  // Hide the Video Annotations
                    autohide: 0         // Hide video controls when playing
                    },
                    events: {
                    onReady: function(e) {
                        e.target.mute();
                    }
                    }
                });
            }
        }

        //Facebook Share function
        function shareSocial(url,type)
        {
          
            if(type=='facebook'){
                // FB.ui({
                //     display: 'popup',
                //     method: 'share',
                //     href: url,
                // }, function(response){});
                window.open(
                    'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(url), 
                    'facebook-share-dialog', 
                    'width=626,height=436'); 
            }

            if(type=='linkedin'){
                window.open(
                    'https://www.linkedin.com/shareArticle?mini=true&url='+encodeURIComponent(url), 
                    'linkedin-share-dialog', 
                    'width=626,height=436'); 
            }

            if(type=='twitter'){
                window.open(
                    'https://twitter.com/intent/tweet?url='+encodeURIComponent(url), 
                    'twitter-share-dialog', 
                    'width=626,height=436'); 
            }
        }

        function randomNumber(min, max) {
            return Math.floor(Math.random() * (max - min + 1) + min);
        };

        jQuery('#captchaCalculation').html([randomNumber(2, 10), '+', randomNumber(2,10), '='].join(' '));
        jQuery('form#feedback_form').bootstrapValidator({
            fields: {
                full_name: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Your fullname is required.[:kh]ឈ្មោះលោកអ្នក​ត្រូវតែ​បំពេញ[:]")?>'
                        }
                    }
                },
                email: {
                    validators: {
                        emailAddress: {
                            message: 'The value is not a valid email address'
                        }
                    }
                },
                phone_number: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Your fullname is required.[:kh]ឈ្មោះលោកអ្នក​ត្រូវតែ​បំពេញ[:]")?>'
                        }
                    }
                },
                SelectType: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Please select one.[:kh]សូមធ្វើការជ្រើសរើស[:]")?>'
                        }
                    }
                },
                selectFeedback: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Please select one.[:kh]សូមធ្វើការជ្រើសរើស[:]")?>'
                        }
                    }
                },
                file_attach: {
                    id: 'txtFile',
                    type: 'file',
                    validators: {
                        'file': {
                            extension: 'jpeg,png,pdf,jpg',
                            type: 'image/jpeg,image/png,image/jpg,application/pdf',
                            maxSize: 1024 * 1024,
                            message: 'The selected file is not valid'
                        }
                    }
                },
                txtmessage: {
                    validators: {
                        notEmpty: {
                            message: '<?php _e("[:en]Please input your messege.[:kh]សូមបំពេញ​មតិរបស់លោកអ្នក[:]")?>'
                        }
                    }
                },
                captcha_feedback:{
                    validators:{
                        notEmpty:{
                            message:'<?php _e("[:en]Number is required![:kh]សូមវាយបញ្ចូលលេខដែលបង្ហាញខាងឆ្វេង[:]")?>'
                        },
                        regexp: {
                            regexp: /^[0-9_\.]+$/,
                            message: '<?php _e("[:en]Incorrect![:kh]មិនត្រឹមត្រូវទេ![:]")?>'
                        }
                    }
                }
            },
        });

        function strcmp(a, b) {
            if(a === b) {
                return 0;
            }

            if (a > b) {
                return 1;
            }

            return -1;
        }
        
    </script>
<?php
if(!$detect->isMobile() && is_front_page()){
?>
    <script>
        // browser window scroll (in pixels) after which the "back to top" link is shown
        var offset = 300,
        //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
        offset_opacity = 1200,
        //duration of the top scrolling animation (in ms)
        scroll_top_duration = 700,
        //grab the "back to top" link
        $back_to_top = jQuery('.cd-top');
        //hide or show the "back to top" link
        jQuery(window).scroll(function(){
            ( jQuery(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
            if( jQuery(this).scrollTop() > offset_opacity ) {
            $back_to_top.addClass('cd-fade-out');
            }
        });
        //smooth scroll to top
        $back_to_top.on('click', function(event){
            event.preventDefault();
            jQuery('body,html').animate({
            scrollTop: 0 ,
            }, scroll_top_duration
            );
        });


        jQuery(window).on("scroll", function() {
            var scrollHeight = jQuery(document).height();
            var scrollPosition = jQuery(window).height() + jQuery(window).scrollTop();
            if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
                jQuery("div.alert-wraper").slideUp('slow');
                jQuery(".close_footer").click(function(){
                    jQuery("div.alert-wraper").hide();
                });
            }else{
                jQuery("div.alert-wraper").slideDown('slow');
            }
        });
    </script>
<?php
}
?>

<?php
add_action( 'init', 'people' );
function people() {
   $labels = array(
      'name' => _x( 'People', 'post type general name', 'your-plugin-textdomain' )
   );
   $args = array(
      'labels'             => $labels,
      'description'        => __( 'Description.', 'your-plugin-textdomain' ),
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'show_in_rest'       => true,
      'query_var'          => true,
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'rewrite'            => array( 'slug' => 'about','with_front' => FALSE),
      'menu_position'      => null,
      'menu_icon'          => 'dashicons-id-alt',
      'supports'           => array( 'title','editor' , 'thumbnail', 'author', 'post-formats')
   );
     register_post_type( 'people', $args );
}
// Register Custom Taxonomy
function people_taxonomy() {
  $labels = array(
    'name'                     => _x( 'Categories', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'            => _x( 'Category', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                => __( 'Category', 'text_domain' ),
  );
  $capabilities = array(
    'manage_terms'             => 'manage_categories',
    'edit_terms'               => 'manage_categories',
    'delete_terms'             => 'manage_categories',
    'assign_terms'             => 'edit_posts',
  );
  $args = array(
    'labels'                   => $labels,
    'hierarchical'             => true,
    'public'                   => true,
    'show_ui'                  => true,
    'show_admin_column'        => true,
    'show_in_nav_menus'        => true,
    'show_tagcloud'            => false,
    'rewrite'                  => array( 'slug' => 'about','with_front' => FALSE),
    'capabilities'             => $capabilities,
  );
  register_taxonomy( 'people_category', array( 'people' ), $args );
}
add_action( 'init', 'people_taxonomy',0);
/*********************************************
# Register custom field
*********************************************/
if( function_exists('acf_add_local_field_group') ):
acf_add_local_field_group(array(
	'key' => 'group_1',
	'title' => 'People',
  'description' => 'លោកអ្នកអាចបំពេញនៅព៌ត៍មានដូចជាមុខងារបុគ្គលិកនៅទីនេះបាន',
	'fields' => array (
		array(
			'key' => 'field_5689e724de9fe',
			'label' => 'Image',
			'name' => 'image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '20',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'full',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5689e744de9ff',
			'label' => 'Main Header',
			'name' => 'main_header',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => 20,
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array(
			'key' => 'field_5689e751dea00',
			'label' => 'Sub Header',
			'name' => 'sub_header',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => 20,
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array(
			'key' => 'field_5689e75bdea01',
			'label' => 'Url',
			'name' => 'url',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => 20,
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array(
			'key' => 'field_5689e80d971eb',
			'label' => 'Type',
			'name' => 'type',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => 20,
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'position',
			'field_type' => 'select',
			'allow_null' => 0,
			'add_term' => 1,
			'save_terms' => 1,
			'load_terms' => 0,
			'return_format' => 'object',
			'multiple' => 0,
        ),
        array(
			'key' => 'field_5689e75bdea0sdfeww1',
			'label' => 'Budget Spent',
			'name' => 'budget_spent',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => 50,
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
        ),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'people',
			),
		),
	),
));

endif;


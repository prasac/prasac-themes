<?php
add_action( 'init', 'cptui_register_years' );
function cptui_register_years() {
  
	$taxonomy_args = array(
    'labels'                => array( 'name' => 'Locations' ),
    'show_ui'               => true,		
    'show_in_menu'          => true,
    'show_tagcloud'         => false,
    'hierarchical'          => true,
    'show_in_rest'          => true,
    'query_var' 		    => true    
  );
  register_taxonomy( 'locations', array('people','office','merchant','library'), $taxonomy_args );
}
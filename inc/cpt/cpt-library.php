<?php
    add_action( 'init', 'library' );
    function library() {
        /**
         * Post Type: Libraries.
         */
        $args = array(
            'label'              => 'Libraries',
            'public'             => true,
            'show_ui'            => true,
            'show_in_rest'       => true,
            'publicly_queryable' => true,
            'menu_icon'          => 'dashicons-building',
            'supports'           => array( 'title','editor' , 'thumbnail', 'author', 'post-formats' ),
            'taxonomies' => array( 'locations', 'library_categories' )
        );
        register_post_type( 'library', $args );
    }
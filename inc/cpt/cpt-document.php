<?php
add_action( 'init', 'document' );
function document() {
   $labels = array(
      'name' => _x( 'Document', 'post type general name', 'your-plugin-textdomain' )
   );
   $args = array(
      'labels'             => $labels,
      'description'        => __( 'Description.', 'your-plugin-textdomain' ),
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'show_in_rest'       => true,
      'query_var'          => true,
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => null,
      'menu_icon'          => 'dashicons-media-document',
      'supports'           => array( 'title','editor','thumbnail', 'author')
   );
   register_post_type( 'document', $args );
}
/******************************************
# Register Custom Taxonomy
******************************************/

function document_taxonomy() {
  $labels = array(
    'name'                     => _x( 'Document Categories', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'            => _x( 'Document Category', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                => __( 'Category', 'text_domain' ),
  );
  $capabilities = array(
    'manage_terms'             => 'manage_categories',
    'edit_terms'               => 'manage_categories',
    'delete_terms'             => 'manage_categories',
    'assign_terms'             => 'edit_posts',
  );
  $args = array(
    'labels'                   => $labels,
    'hierarchical'             => true,
    'public'                   => true,
    'show_ui'                  => true,
    'show_admin_column'        => true,
    'show_in_nav_menus'        => true,
    'show_tagcloud'            => false,
    'rewrite'                  => array( 'slug' => 'reports' ),
    'capabilities'             => $capabilities,
  );
  register_taxonomy( 'document_category', array( 'document' ), $args );

}
add_action( 'init', 'document_taxonomy', 0 );

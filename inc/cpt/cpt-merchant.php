<?php
// Register Post Type : Merchant and Custom Taxonomy : Merchant Tag
add_action('init', 'register_merchant_entities');
add_action('save_post', 'register_merchant_entities');
function register_merchant_entities() {

  $taxonomy_args = array(
    'labels'            => array('name' => 'Merchant Tags'),
    'show_ui'           => true,
    'show_tagcloud'     => false,
    'hierarchical'      => true,
    'show_in_rest'      => true,
    'show_admin_column' => true,
  );
  register_taxonomy('merchant_tag', array('merchant'), $taxonomy_args);

  $args = array(
    'public'             => true,
    'label'              => 'Merchants',
    'supports'           => array('title','thumbnail'),
    'menu_icon'          => 'dashicons-store',
    'show_in_rest'       => true,
    'publicly_queryable' => true,
    // 'rewrite'            => array( 'slug' => 'scan-pay-partner','with_front' => FALSE),
    'supports'           => array( 'title','editor' , 'thumbnail', 'author', 'post-formats','order'),
    'taxonomies'         => array('merchant_tag', 'locations','merchant_category')
  );
  register_post_type('merchant', $args);
}

add_action( 'init', 'register_merchan_category' );
function register_merchan_category() {
	$taxonomy_args = array(
    'labels'            => array( 'name' => 'Merchant Category' ),
    'hierarchical'             => true,
    'public'                   => true,
    'show_ui'                  => true,
    'show_admin_column'        => true,
    'show_in_nav_menus'        => true,
    'show_tagcloud'            => false,
  );
  register_taxonomy( 'merchant_categories', array( 'merchant' ), $taxonomy_args );
  flush_rewrite_rules();
}

<?php
add_action( 'init', 'exchange_rate' );
function exchange_rate() {
   $labels = array(
      'name' => _x( 'Exchanges', 'post type general name', 'your-plugin-textdomain' )
   );
   $args = array(
      'labels'             => $labels,
      'description'        => __( 'Description.', 'your-plugin-textdomain' ),
      'public'             => true,
      'publicly_queryable' => true,
      'show_ui'            => true,
      'show_in_menu'       => true,
      'show_in_rest'       => true,
      'query_var'          => true,
      'rewrite'            => array( 'slug' => 'exchange' ),
      'rest_controller_class' => 'WP_REST_Posts_Controller',
      'capability_type'    => 'post',
      'has_archive'        => true,
      'hierarchical'       => false,
      'menu_position'      => null,
      'menu_icon'          => 'dashicons-chart-area',
      'supports'           => array( 'title', 'author' )
   );
   register_post_type( 'exchange', $args );
}

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
   'key' => 'group_5507ec1feac21',
   'title' => 'Exchange Rate',
   'fields' => array (
      array (
         'key' => 'field_54ebfdab668a7',
         'label' => 'USD Buy',
         'name' => 'usd_buy',
         'type' => 'number',
         'instructions' => '',
         'required' => 1,
         'conditional_logic' => 0,
         'wrapper' => array (
            'width' => '20',
            'class' => '',
            'id' => '',
         )
      ),
      array (
         'key' => 'field_54ebfdcd668a8',
         'label' => 'USD Sell',
         'name' => 'usd_sell',
         'type' => 'number',
         'instructions' => '',
         'required' => 1,
         'conditional_logic' => 0,
         'wrapper' => array (
            'width' => '20',
            'class' => '',
            'id' => '',
         )
      ),
      array (
         'key' => 'field_54ebfe07668aa',
         'label' => 'THB Buy',
         'name' => 'thb_buy',
         'type' => 'number',
         'instructions' => '',
         'required' => 1,
         'conditional_logic' => 0,
         'wrapper' => array (
            'width' => '20',
            'class' => '',
            'id' => '',
         )
      ),
      array (
         'key' => 'field_54ebfe2b668ab',
         'label' => 'THB Sell',
         'name' => 'thb_sell',
         'type' => 'number',
         'instructions' => '',
         'required' => 1,
         'conditional_logic' => 0,
         'wrapper' => array (
            'width' => '20',
            'class' => '',
            'id' => '',
         ),
      ),
   ),
   'location' => array (
      array (
         array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'exchange',
         )
      )
   )
));

endif;

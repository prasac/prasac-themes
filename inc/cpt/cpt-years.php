<?php
add_action( 'init', 'cptui_register_my_cpts' );
function cptui_register_my_cpts() {
  
	$taxonomy_args = array(
    'labels'                => array( 'name' => 'Years' ),
    'show_ui'               => true,		
    'show_in_menu'          => true,
    'show_tagcloud'         => false,
    'hierarchical'          => true,
    'show_in_rest'          => true,
    'query_var' 		    => true    
  );
  register_taxonomy( 'years', array('people'), $taxonomy_args );
}
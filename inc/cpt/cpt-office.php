<?php
add_action( 'init', 'cptui_register_my_cpts_office' );
function cptui_register_my_cpts_office() {
  
	$taxonomy_args = array(
    'labels'            => array( 'name' => 'Office Category' ),
    'hierarchical'             => true,
    'public'                   => true,
    'show_ui'                  => true,
    'show_admin_column'        => true,
    'show_in_nav_menus'        => true,
    'show_tagcloud'            => false,
  );
  register_taxonomy( 'office_categories', array( 'office' ), $taxonomy_args );

	/**
	 * Post Type: Offices.
	 */
	$args = array(
		'label'              => 'Offices',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'show_in_rest'       => true,
        'query_var'          => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
		'menu_icon'          => 'dashicons-building',
		'supports'   => array( 'title','editor' , 'thumbnail', 'author', 'post-formats'),
		'taxonomies' => array( 'locations', 'office_categories' )
	);
	register_post_type( 'office', $args );
}
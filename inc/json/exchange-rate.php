<?php
/* Your Database Name */
$DB_NAME = 'mywebsite_development';
$DB_HOST = 'localhost';
$DB_USER = 'root';
$DB_PASS = '';
/* Establish the database connection */
$conn= new mysqli($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
if ( mysqli_connect_errno() ) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

/* select all the weekly tasks from the table googlechart */
$query = "
    SELECT
    p.id,
    p.post_title,
    p.post_date,
   ((p1.meta_value + p2.meta_value)/2) AS 'usd_midRate',
   ((p3.meta_value + p4.meta_value)/2) AS 'thb_midRate'
FROM
    pr65ac2015_posts p
        INNER JOIN
    pr65ac2015_postmeta AS p1 ON p1.post_id = p.ID
        INNER JOIN
    pr65ac2015_postmeta AS p2 ON p2.post_id = p.ID
        INNER JOIN
    pr65ac2015_postmeta AS p3 ON p3.post_id = p.ID
        INNER JOIN
    pr65ac2015_postmeta AS p4 ON p4.post_id = p.ID
WHERE
    p1.meta_key = 'usd_buy'
    AND p2.meta_key = 'usd_sell'
    AND p3.meta_key = 'thb_buy'
    AND p4.meta_key = 'thb_sell'
    AND p.post_type = 'exchange'
    AND p.post_status = 'publish'
    ";
    $result = mysqli_query( $conn, $query );

    $table = array();
    $table['cols'] = array(
        array('label' => 'Date', 'type' => 'datetime'),
        array('label' => 'USD', 'type' => 'number' )
        // array('label' => 'THB', 'type' => 'number' )
    );

    $rows = array();
    while( $r = mysqli_fetch_assoc($result) ) {

        $year = date("Y", strtotime($r['post_date']));
        $month = date("m", strtotime($r['post_date']));
        $day = date("d", strtotime($r['post_date']));
        $hour = date("H", strtotime($r['post_date']));
        $minute = date("i", strtotime($r['post_date']));
        $second = date("s", strtotime($r['post_date']));
        $month = $month - 1;

        $temp = array();

        $table['rows'][] = array('c' => array(
            $temp = array('v' => "Date($year, $month, $day, $hour, $minute)"),
            $temp = array('v' => (int) $r['usd_midRate']),
            $temp = array('v' => (float) $r['thb_midRate'])
        ));
    }
$jsonTable = json_encode($table, JSON_PRETTY_PRINT);
print_r($jsonTable);
mysqli_close($conn);
?>

<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_5c37f5c45734b',
	'title' => 'Homepage Meta Tags',
	'fields' => array (
		array (
			'default_value' => '',
			'new_lines' => 'wpautop',
			'maxlength' => '',
			'placeholder' => '',
			'rows' => '',
			'key' => 'field_5c37f6084579521',
			'label' => 'Meta Description',
			'name' => 'meta_description',
			'type' => 'qtranslate_textarea',
			'instructions' => 'Please enter the description here',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '50',
				'class' => '',
				'id' => '',
			),
		),
		array (
			'default_value' => '',
			'new_lines' => 'wpautop',
			'maxlength' => '',
			'placeholder' => '',
			'rows' => '',
			'key' => 'field_5c37f87e4672c',
			'label' => 'Meta Keywords',
			'name' => 'meta_keywords',
			'type' => 'qtranslate_textarea',
			'instructions' => 'Please enter the keywords with comma separate (,)',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '50',
				'class' => '',
				'id' => '',
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => 'acf-options-header',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));
endif;
?>
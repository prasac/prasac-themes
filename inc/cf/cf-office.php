<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
	'key' => 'group_branchInformation',
	'title' => 'Information',
	'fields' => array (
		
		array (
			'key' => 'field_branchPhone',
			'label' => 'Phone',
			'name' => 'phone',
			'type' => 'text',
			'wrapper' => array (
				'width' => 20,
				'class' => '',
				'id' => '',
			),
			'placeholder' => '23 XXX XXX',
			'prepend' => '+855'
		),
		array (
			'key' => 'field_branchEmail',
			'label' => 'Email',
			'name' => 'email',
			'type' => 'email',
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 20,
				'class' => '',
				'id' => '',
			),
			'placeholder' => 'name@example.com'
		),
		array (
			'key' => 'field_branchLat',
			'label' => 'Latitude',
			'name' => 'latitude',
			'type' => 'number',
			'wrapper' => array (
				'width' => 20,
				'class' => '',
				'id' => '',
			)
		),
		array (
			'key' => 'field_branchLng',
			'label' => 'Longitude',
			'name' => 'longitude',
			'type' => 'number',
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 20,
				'class' => '',
				'id' => '',
			),
		),
		array (
			'key' => 'field_branchshortUrl',
			'label' => 'shorturl',
			'name' => 'shorturl',
			'type' => 'url',
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => 20,
				'class' => '',
				'id' => '',
			)
		),
		array (
			'key' => 'field_branchAddress',
			'label' => 'Address',
			'name' => 'address',
			'type' => 'textarea',
			'rows' => 3
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'office',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1
));

endif;
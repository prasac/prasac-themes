<?php get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <aside class="col-sm-3 col-md-3 hidden-xs"><?php get_sidebar(); ?></aside>
            <section class="col-xs-12 col-md-9 col-sm-9">
                <div class="hidden-xs hidden-sm">
                    <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                </div>
                <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                    <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                    <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                </ul>
                </section>
                <section class="col-xs-12 col-sm-`12 col-md-9 downloads">
                <div class="clearfixed"></div>
                <h3 class="green"><?php single_term_title(); ?></h3>
                <?php //have_posts()?term_description( $post->id, 'document_category'):'';
                ?>

                <?php 
                if (have_posts() ) :
                    if(is_tax( 'document_category', 'social-performance-dashboard') || is_tax( 'document_category', 'financial-performance-highlights')){
                        while (have_posts() ): the_post();
                            echo get_the_content();
                        endwhile;
                    }else{
                    ?>
                    <div class="row">
                    <?php
                    $i=1;
                    while (have_posts() ): the_post();
                        get_template_part('template-parts/document', 'summary');
                        $i++;
                    endwhile;
                    ?>
                </div>
                <?php
                    }
                else :
                    _e('[:en]Sorry, Document not found![:kh]សូមអភ័យទោស ពុំ​មាន​​​ឯកសារ​នោះ​ទេ![:]');
                endif;
                ?>
                <?php
                if (function_exists('wp_bootstrap_pagination')){
                    wp_bootstrap_pagination();
                } 
                ?>
                <div class="row">
                    <div class="col-xs-12 col-md-12">
                    <hr>
                        <div class="single-footer-share">
                            <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                            <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                            <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo get_permalink();?>','twitter');"></a>
                            <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo get_permalink();?>','linkedin');"></a>
                        </div>
                    </div>
                </div>
                
            </section>
        </main>
    </div>
</div>
<?php get_footer(); ?>

<?php 
/*
* template name: Monthly Calendar
*/
get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <section class="col-xs-12 col-sm-12 col-md-12">
            <div class="hidden-xs hidden-sm">
                <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                    <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                    <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                </ul>
            </div>
        </section>
        <section class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
            <?php
            //      $current_year = date("Y");
            //      $current_month  = date("m");
            //      $num_day = array();
            //      for($i = 1; $i <= 12; $i++) {
            //        $num_day  = cal_days_in_month(CAL_GREGORIAN,$i, $current_year);

            //        for($d = 1; $d <= $num_day; $d++) {
            //          echo '<button>'.$d.'</button>';
            //        }
            //        echo '<br />';

            //      }
                 
            //    echo '<br />';
            ?>
            <div id='wrap'>
                <div id='calendar'></div>
                <div style='clear:both'></div>
            </div>
        </section>
    </div>
</div>
<?php 
   
?>
    <style>
        #wrap {
            margin: 0 auto;
        }
        .custom-calendar{
            min-height:650px;
        }
            
        #external-events {
            float: left;
            width: 150px;
            padding: 0 10px;
            text-align: left;
            }
            
        #external-events h4 {
            font-size: 16px;
            margin-top: 0;
            padding-top: 1em;
            }
            
        .external-event { /* try to mimick the look of a real event */
            margin: 10px 0;
            padding: 2px 4px;
            background: #3366CC;
            color: #fff;
            font-size: .85em;
            cursor: pointer;
            }
            
        #external-events p {
            margin: 1.5em 0;
            font-size: 11px;
            color: #666;
            }
            
        #external-events p input {
            margin: 0;
            vertical-align: middle;
            }

        #calendar {
    /* 		float: right; */
            margin: 0 auto;
            background-color: #FFFFFF;
            border-radius: 6px;
            box-shadow: 0 1px 2px #C3C3C3;
            -webkit-box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
            -moz-box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
            box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
            margin-bottom:2rem;
            }

            @import url('https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i');
td.fc-day {

background:#FFF;

}
td.fc-today {
	background:#FFF !important;
	position: relative;


}

.fc-first th{
    background:#4db848 !important;
	color:#FFF;
	font-size:14px !important;
	font-weight:500 !important;

	}
.fc-event-inner {
    /* background:rgba(97, 187, 70, .3) !important; */
    color:#4DB848 !important;
    font-size: 12px!important;
    font-weight: 500!important;
    padding: 5px 2px!important;
}

.fc {
	direction: ltr;
	text-align: left;
	}
	
.fc table {
	border-collapse: collapse;
	border-spacing: 0;
	}

.fc td,
.fc th {
	padding: 0;
	vertical-align: top;
	}



/* Header
------------------------------------------------------------------------*/

.fc-header td {
	white-space: nowrap;
	padding: 15px 10px 0px;
}

.fc-header-left {
	width: 25%;
	text-align: left;
}
	
.fc-header-center {
	text-align: center;
	}
	
.fc-header-right {
	width: 25%;
	text-align: right;
	}
	
.fc-header-title {
	display: inline-block;
	vertical-align: top;
	margin-top: -5px;
}
	
.fc-header-title h2 {
	margin-top: 0;
	white-space: nowrap;
	font-size: 32px;
    font-weight: 100;
    margin-bottom: 10px;
}
	span.fc-button {
    border-color:#4db848;
    color: #9675ce;
    text-transform: capitalize;
}
.fc-state-down, .fc-state-active {
    background-color:#4db848 !important;
    color: #FFF !important;
    text-transform:case;
}
.fc .fc-header-space {
	padding-left: 10px;
	}
	
.fc-header .fc-button {
	margin-bottom: 1em;
	vertical-align: top;
	}
	
/* buttons edges butting together */

.fc-header .fc-button {
	margin-right: -1px;
	}
	
.fc-header .fc-corner-right,  /* non-theme */
.fc-header .ui-corner-right { /* theme */
	margin-right: 0; /* back to normal */
	}
	
/* button layering (for border precedence) */
	
.fc-header .fc-state-hover,
.fc-header .ui-state-hover {
	z-index: 2;
	}
	
.fc-header .fc-state-down {
	z-index: 3;
	}

.fc-header .fc-state-active,
.fc-header .ui-state-active {
	z-index: 4;
	}
	
	
	
/* Content
------------------------------------------------------------------------*/
	
.fc-content {
	clear: both;
	zoom: 1; /* for IE7, gives accurate coordinates for [un]freezeContentHeight */
	}
	
.fc-view {
	width: 100%;
	overflow: hidden;
	}
	
	

/* Cell Styles
------------------------------------------------------------------------*/

    /* <th>, usually */
.fc-widget-content {  /* <td>, usually */
	border: 1px solid #e5e5e5;
	}
.fc-widget-header{
    border-bottom: 1px solid #EEE; 
}	
.fc-state-highlight { /* <td> today cell */ /* TODO: add .fc-today to <th> */
	/* background: #fcf8e3; */
}

.fc-state-highlight > div > div.fc-day-number{
    background-color: #ff3b30;
    color: #FFFFFF;
    border-radius: 50%;
    margin: 4px;
}
	
.fc-cell-overlay { /* semi-transparent rectangle while dragging */
	background: #bce8f1;
	opacity: .3;
	filter: alpha(opacity=30); /* for IE */
	}
	


/* Buttons
------------------------------------------------------------------------*/

.fc-button {
	position: relative;
	display: inline-block;
	padding: 0 .6em;
	overflow: hidden;
	height: 1.9em;
	line-height: 1.9em;
	white-space: nowrap;
	cursor: pointer;
	}
	
.fc-state-default { /* non-theme */
	border: 1px solid;
	}

.fc-state-default.fc-corner-left { /* non-theme */
	border-top-left-radius: 4px;
	border-bottom-left-radius: 4px;
	}

.fc-state-default.fc-corner-right { /* non-theme */
	border-top-right-radius: 4px;
	border-bottom-right-radius: 4px;
	}

/*
	Our default prev/next buttons use HTML entities like ‹ › « »
	and we'll try to make them look good cross-browser.
*/

.fc-text-arrow {
	margin: 0 .4em;
	font-size: 2em;
	line-height: 23px;
	vertical-align: baseline; /* for IE7 */
	}

.fc-button-prev .fc-text-arrow,
.fc-button-next .fc-text-arrow { /* for ‹ › */
	font-weight: bold;
	}
	
/* icon (for jquery ui) */
	
.fc-button .fc-icon-wrap {
	position: relative;
	float: left;
	top: 50%;
	}
	
.fc-button .ui-icon {
	position: relative;
	float: left;
	margin-top: -50%;
	
	*margin-top: 0;
	*top: -50%;
	}


.fc-state-default {
	border-color: #ff3b30;
	color: #ff3b30;	
}
.fc-button-month.fc-state-default, .fc-button-agendaWeek.fc-state-default, .fc-button-agendaDay.fc-state-default{
    min-width: 67px;
	text-align: center;
	transition: all 0.2s;
	-webkit-transition: all 0.2s;
}
.fc-state-hover,
.fc-state-down,
.fc-state-active,
.fc-state-disabled {
	color: #333333;
	background-color: #FFE3E3;
	}

.fc-state-hover {
	color: #ff3b30;
	text-decoration: none;
	background-position: 0 -15px;
	-webkit-transition: background-position 0.1s linear;
	   -moz-transition: background-position 0.1s linear;
	     -o-transition: background-position 0.1s linear;
	        transition: background-position 0.1s linear;
	}

.fc-state-down,
.fc-state-active {
	background-color: #ff3b30;
	background-image: none;
	outline: 0;
	color: #FFFFFF;
}

.fc-state-disabled {
	cursor: default;
	background-image: none;
	background-color: #FFE3E3;
	filter: alpha(opacity=65);
	box-shadow: none;
	border:1px solid #FFE3E3;
	color: #ff3b30;
	}

	

/* Global Event Styles
------------------------------------------------------------------------*/

.fc-event-container > * {
	z-index: 8;
	}

.fc-event-container > .ui-draggable-dragging,
.fc-event-container > .ui-resizable-resizing {
	z-index: 9;
	}
	 
.fc-event {
	border: 1px solid #FFF; /* default BORDER color */
	background-color: #FFF; /* default BACKGROUND color */
	color: #919191;               /* default TEXT color */
	font-size: 12px;
	cursor: default;
}
.fc-event.chill{
    background-color: #f3dcf8;
}
.fc-event.info{
    background-color: #c6ebfe;
}
.fc-event.important{
    background-color: #FFBEBE;
}
.fc-event.success{
    background-color: #BEFFBF;
}
.fc-event:hover{
    opacity: 0.7;
}
a.fc-event {
	text-decoration: none;
	}
	
a.fc-event,
.fc-event-draggable {
	cursor: pointer;
	}
	
.fc-rtl .fc-event {
	text-align: right;
	}

.fc-event-inner {
	width: 100%;
	height: 100%;
	overflow: hidden;
	line-height: 15px;
	}
	
.fc-event-time,
.fc-event-title {
	padding: 0 1px;
	}
	
.fc .ui-resizable-handle {
	display: block;
	position: absolute;
	z-index: 99999;
	overflow: hidden; /* hacky spaces (IE6/7) */
	font-size: 300%;  /* */
	line-height: 50%; /* */
	}
	
	
	
/* Horizontal Events
------------------------------------------------------------------------*/

.fc-event-hori {
	border-width: 1px 0;
	margin-bottom: 1px;
	}

.fc-ltr .fc-event-hori.fc-event-start,
.fc-rtl .fc-event-hori.fc-event-end {
	border-left-width: 1px;
	/*
border-top-left-radius: 3px;
	border-bottom-left-radius: 3px;
*/
	}

.fc-ltr .fc-event-hori.fc-event-end,
.fc-rtl .fc-event-hori.fc-event-start {
	border-right-width: 1px;
	/*
border-top-right-radius: 3px;
	border-bottom-right-radius: 3px;
*/
	}
	
/* resizable */
	
.fc-event-hori .ui-resizable-e {
	top: 0           !important; /* importants override pre jquery ui 1.7 styles */
	right: -3px      !important;
	width: 7px       !important;
	height: 100%     !important;
	cursor: e-resize;
	}
	
.fc-event-hori .ui-resizable-w {
	top: 0           !important;
	left: -3px       !important;
	width: 7px       !important;
	height: 100%     !important;
	cursor: w-resize;
	}
	
.fc-event-hori .ui-resizable-handle {
	padding-bottom: 14px; /* IE6 had 0 height */
	}
	
	
	
/* Reusable Separate-border Table
------------------------------------------------------------*/

table.fc-border-separate {
	border-collapse: separate;
	}
	
.fc-border-separate th,
.fc-border-separate td {
	border-width: 1px 0 0 1px;
	}
	
.fc-border-separate th.fc-last,
.fc-border-separate td.fc-last {
	border-right-width: 1px;
	}
	

.fc-border-separate tr.fc-last td {
	
}
.fc-border-separate .fc-week .fc-first{
    border-left: 0;
}
.fc-border-separate .fc-week .fc-last{
    border-right: 0;
}
.fc-border-separate tr.fc-last th{
    font-size: 16px;
    font-weight: 300;
	line-height:3rem;
}
.fc-border-separate tbody tr.fc-first td,
.fc-border-separate tbody tr.fc-first th {
	border-top-width: 0;
	}

	

/* Month View, Basic Week View, Basic Day View
------------------------------------------------------------------------*/

.fc-grid th {
	text-align: center;
	}

.fc .fc-week-number {
	width: 22px;
	text-align: center;
	}

.fc .fc-week-number div {
	padding: 0 2px;
	}
	
.fc-grid .fc-day-number {
	float: right;
	padding: 0 2px;
	}
	
.fc-grid .fc-other-month .fc-day-number {
	opacity: 0.3;
	filter: alpha(opacity=30); /* for IE */
	/* opacity with small font can sometimes look too faded
	   might want to set the 'color' property instead
	   making day-numbers bold also fixes the problem */
	}
	
.fc-grid .fc-day-content {
	clear: both;
	padding: 2px 2px 1px; /* distance between events and day edges */
	}
	
/* event styles */
	
.fc-grid .fc-event-time {
	font-weight: bold;
	}
	
/* right-to-left */
	
.fc-rtl .fc-grid .fc-day-number {
	float: left;
	}
	
.fc-rtl .fc-grid .fc-event-time {
	float: right;
	}
	
	

/* Agenda Week View, Agenda Day View
------------------------------------------------------------------------*/

.fc-agenda table {
	border-collapse: separate;
	}
	
.fc-agenda-days th {
	text-align: center;
	}
	
.fc-agenda .fc-agenda-axis {
	width: 50px;
	padding: 0 4px;
	vertical-align: middle;
	text-align: right;
	white-space: nowrap;
	font-weight: normal;
	}

.fc-agenda .fc-week-number {
	font-weight: bold;
	}
	
.fc-agenda .fc-day-content {
	padding: 2px 2px 1px;
	}
	
/* make axis border take precedence */
	
.fc-agenda-days .fc-agenda-axis {
	border-right-width: 1px;
	}
	
.fc-agenda-days .fc-col0 {
	border-left-width: 0;
	}
	
/* all-day area */
	
.fc-agenda-allday th {
	border-width: 0 1px;
	}
	
.fc-agenda-allday .fc-day-content {
	min-height: 34px; /* TODO: doesnt work well in quirksmode */
	_height: 34px;
	}
	
/* divider (between all-day and slots) */
	
.fc-agenda-divider-inner {
	height: 2px;
	overflow: hidden;
	}
	
.fc-widget-header .fc-agenda-divider-inner {
	background: #eee;
	}
	
/* slot rows */
	
.fc-agenda-slots th {
	border-width: 1px 1px 0;
	}
	
.fc-agenda-slots td {
	border-width: 1px 0 0;
	background: none;
	}
	
.fc-agenda-slots td div {
	height: 20px;
	}
	
.fc-agenda-slots tr.fc-slot0 th,
.fc-agenda-slots tr.fc-slot0 td {
	border-top-width: 0;
	}
	
.fc-agenda-slots tr.fc-minor th.ui-widget-header {
	border-top-style: solid; /* doesn't work with background in IE6/7 */
}
#calendar .fc-view-agendaDay,.fc-view-agendaDay .fc-agenda-divider,.fc-view-agendaDay table.fc-agenda-slots, .fc-view-agendaDay table.fc-agenda-allday{
    display:none;
}

.fc-view-agendaDay .fc-border-separate td.fc-last{
    border:none;
}
	
	


/* Vertical Events
------------------------------------------------------------------------*/

.fc-event-vert {
	border-width: 0 1px;
	}

.fc-event-vert.fc-event-start {
	border-top-width: 1px;
	border-top-left-radius: 3px;
	border-top-right-radius: 3px;
	}

.fc-event-vert.fc-event-end {
	border-bottom-width: 1px;
	border-bottom-left-radius: 3px;
	border-bottom-right-radius: 3px;
	}
	
.fc-event-vert .fc-event-time {
	white-space: nowrap;
	font-size: 10px;
	}

.fc-event-vert .fc-event-inner {
	position: relative;
	z-index: 2;
	}
	
.fc-event-vert .fc-event-bg { /* makes the event lighter w/ a semi-transparent overlay  */
	position: absolute;
	z-index: 1;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #fff;
	opacity: .25;
	filter: alpha(opacity=25);
	}
	
.fc .ui-draggable-dragging .fc-event-bg, /* TODO: something nicer like .fc-opacity */
.fc-select-helper .fc-event-bg {
	display: none\9; /* for IE6/7/8. nested opacity filters while dragging don't work */
	}
	
/* resizable */
	
.fc-event-vert .ui-resizable-s {
	bottom: 0        !important; /* importants override pre jquery ui 1.7 styles */
	width: 100%      !important;
	height: 8px      !important;
	overflow: hidden !important;
	line-height: 8px !important;
	font-size: 11px  !important;
	text-align: center;
	cursor: s-resize;
	}
	
.fc-agenda .ui-resizable-resizing { /* TODO: better selector */
	_overflow: hidden;
	}
	
thead tr.fc-first{
    background-color: #f7f7f7;
}
table.fc-header{
    background-color: #FFFFFF;
    border-radius: 6px 6px 0 0;
}

.fc-week .fc-day > div .fc-day-number{
    font-size: 15px;
    margin: 2px;
    min-width: 19px;
    padding: 6px;
    text-align: center;
       width: 30px;
    height: 30px;
}
.fc-sun, .fc-sat{
    color: #b8b8b8;
}

.fc-week .fc-day:hover .fc-day-number{
    background-color:#4db848;
    border-radius: 50%;
    color: #FFFFFF;
    transition: background-color 0.2s;
}
.fc-week .fc-day.fc-state-highlight:hover .fc-day-number{
    background-color:  #ff3b30;
}
.fc-button-today{
    border: 1px solid rgba(255,255,255,.0);
}
.fc-view-agendaDay thead tr.fc-first .fc-widget-header{
    text-align: right;
    padding-right: 10px;
}

/*!
 * FullCalendar v1.6.4 Print Stylesheet
 * Docs & License: http://arshaw.com/fullcalendar/
 * (c) 2013 Adam Shaw
 */

/*
 * Include this stylesheet on your page to get a more printer-friendly calendar.
 * When including this stylesheet, use the media='print' attribute of the <link> tag.
 * Make sure to include this stylesheet IN ADDITION to the regular fullcalendar.css.
 */
 
 
 /* Events
-----------------------------------------------------*/
 
.fc-event {
	background: #fff !important;
	color: #000 !important;
	}
	
/* for vertical events */
	
.fc-event-bg {
	display: none !important;
	}
	
.fc-event .ui-resizable-handle {
    display: none !important;
}

table.fc-border-separate tbody tr td.fc-day:hover{
    background-color:rgba(97, 187, 70, .3);
}

.fc-state-highlight > div > div.fc-day-number {
    background-color: #4db848;
    color: #FFFFFF;
    border-radius: 50%;
    margin: 4px;
}
#calendar .fc-content{
    padding-bottom:10px;
}
ul.list li{
    line-height: 0.9;
}
table.fc-agenda-days tbody tr.fc-first th{
    background-color:transparent !important;
}

span.day-num{
    font-size: 18rem;
    font-weight: bold;
    margin:0 auto;
}

span.day-name{
    font-size: 2.2rem;
    font-weight: bold;
    /* margin: 10px auto; */
    display: block;
    /* margin-bottom: 22px; */
    position: absolute;
    right: 0;
    left: 0;
    top: 42%;
}

span.day-desc{
    font-size: 2.2rem;
    font-weight: bold;
    display: block;
    position: absolute;
    left: 0;
    right:0;
    color: #4db848;
}
span.day-lunar-desc{
    font-size: 3rem;
    position: absolute;
    left:4rem;
    color: #4db848;
    word-break: break-word;
    font-family: Moul;
    width: 10rem;
    top: 7rem;
    text-align: center;
}
div.image-promotion{
    position:absolute;
    bottom:0;
    right:0;
    left:0;
}

div.custom-calendar{
    overflow:hidden;
    text-align:center;
}

.daily-events{
    padding:10px;
    color:#4db848;
    display:block;
    text-align:left;
}
.daily-events p{
    margin:0;
}

.daily-events p:before{
    content:'→ ';
}

	

    </style>
     <?php 
    //  var_dump(getEvents());
    //  exit;
        // $events = array();
        // foreach(getEvents() as $row) {
        //     $event_lists = array();
        //     $event_lists['title'] = $row->event_title;
        //     $event_lists['start'] = new Date($row->year_num,$row->month_num,$row->day_num);
        //     $event_lists['description'] = $row->day_lunar;
        //     $event_lists['allDay'] = true;
        //     $event_lists['className'] = 'important';
        //     array_push($events, $event_lists);
        // }
    ?>
<?php get_footer(); ?>
<script src="<?php echo get_template_directory_uri();?>/js/fullcalendar.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/moment-with-locales.js"></script>

<script>

	jQuery(document).ready(function(e) {
        console.log(moment.locale('km'));
        console.log(moment().format('MMMM Do YYYY, h:mm:ss A'));
	    var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		/*  className colors
		className: default(transparent), important(red), chill(pink), success(green), info(blue)
		
		*/
		/* initialize the external events
		-----------------------------------------------------------------*/
	
		jQuery('#external-events div.external-event').each(function() {
		
			// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
			// it doesn't need to have a start or end
			var eventObject = {
				title: $.trim($(this).text()) // use the element's text as the event title
			};
			
			// store the Event Object in the DOM element so we can get to it later
			jQuery(this).data('eventObject', eventObject);
			
			// make the event draggable using jQuery UI
			jQuery(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});
			
		});
	
	
		/* initialize the calendar
        -----------------------------------------------------------------*/
        // jQuery.ajax({
        //     url: "/prasac/wp-admin/admin-ajax.php",
        //     type: "POST",
        //     dataType: 'json',
        //     data:jQuery("form#calendar_form").serializeArray(),
        //     success: function(data, textStatus, jqXHR) {
        //         console.log(data);
        //     },
        //     error: function(jqXHR, textStatus, errorThrown) {
        //         console.log(errorThrown);
        //     }
        // });
		var calendar =  jQuery('#calendar').fullCalendar({
			header: {
				left: '',
				center: 'title',
                right: 'prev,next today',
                timeFormat: {
                    agenda: 'H(:mm)' //h:mm{ - h:mm}'
                },
			},
            locale:'th',
			editable: true,
			firstDay:0, //  1(Monday) this can be changed to 0(Sunday) for the USA system
            selectable: true,
            eventLimit:true,
			defaultView: 'agendaDay',//agendaDay
			axisFormat: 'h:mm',
			columnFormat: {
                month: 'ddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
            // minTime:'0',
            // maxTime:'0',
            allDaySlot: true,
			selectHelper: true,
			// select: function(start, end, allDay) {
			// 	var title = prompt('Event Title:');
			// 	if (title) {
			// 		calendar.fullCalendar('renderEvent',
			// 			{
			// 				title: title,
			// 				start: start,
			// 				end: end,
			// 				allDay: allDay
			// 			},
			// 			true // make the event "stick"
			// 		);
			// 	}
			// 	calendar.fullCalendar('unselect');
            // },
            dayClick: function(date, allDay, jsEvent, view)
            {
                // jQuery('#calendar').fullCalendar('gotoDate', date);
                // jQuery('#calendar').fullCalendar('changeView','agendaDay');
                // jQuery("#calendar .fc-content").append("<div class='custom-calendar'><div class='day-name'></div><div class='day-num'></div><div class='daily-events'></div></div>");
                // jQuery("table.fc-agenda-days tbody tr.fc-first th").remove();
                // jQuery('#calendar').fullCalendar('clientEvents', function(event,eventNum) {
                //     var $viewname   =   jQuery('#calendar').fullCalendar('getView').name;
                //     if(moment(date).format('YYYY-MM-DD') == moment(event.start).format('YYYY-MM-DD') && $viewname=='agendaDay'){
                //         jQuery("#calendar .fc-content ul.list").append('<li>'+event.title+'</li>');
                //     }else{
                //         console.log('No Events for today');
                //     }
                // });
            },
            eventClick: function(calEvent, jsEvent, view) {
                // jQuery('#calendar').fullCalendar('gotoDate', date);
                // jQuery('#calendar').fullCalendar('changeView','agendaDay');
                // console.log(calEvent.title);
            },
            viewRender: function(view, element) {
                jQuery("div.custom-calendar").remove();
                // var $countevent =    jQuery('.fc-event').length;
                // console.log($eventList);
                if(view.name='agendaDay'){
                    jQuery("#calendar .fc-content").append("<div class='custom-calendar'><span class='day-desc'></span><span class='day-lunar-desc'></span><span class='day-num'></span><p><span class='day-name'></span></p><div class='daily-events'></div><div class='image-promotion'><img class='img-responsive' src='/prasac/wp-content/themes/prasac-themes/assets/images/930-x180.gif' /></div></div>");
                }
            },
            eventRender: function (event, element) {
                jQuery(".fc-event-container .fc-event").find(".fc-event-inner").append("<span class='fc-event-description'>"+event.description+"</span>");
            },
            eventAfterAllRender: function (view) {
                var $currentDate    =   jQuery("table.fc-agenda-days thead tr th.fc-col0").text().split(" ")
                var $eventList      =   jQuery(".fc-event-container .fc-event .fc-event-inner .fc-event-title").text();
                var $countevent =    jQuery('.fc-event').length;
                jQuery(".custom-calendar span.day-name").html($currentDate[0]);
                jQuery(".custom-calendar span.day-num").html($currentDate[1]);
                var $List = [];
                jQuery(".custom-calendar div.daily-events").html('');
                if($countevent && $eventList !=''){
                    jQuery(".fc-event-container .fc-event").each(function(){
                        var $eventTitle = jQuery(this).find(".fc-event-inner .fc-event-title").text();
                        jQuery(".custom-calendar div.daily-events").append("<p>"+$eventTitle+"</p>");
                    });
                }

                //Add daily description

                var $eventDesc = jQuery(".fc-event-container .fc-event .fc-event-inner .fc-event-description").html();
                var $eventSplit =   $eventDesc.split(',');
                jQuery(".custom-calendar span.day-desc").append($eventSplit[0]);
                jQuery(".custom-calendar span.day-lunar-desc").append($eventSplit[1]);
            },
            events:<?php echo json_encode(getEvents()); ?>,
			droppable: true, // this allows things to be dropped onto the calendar !!!
			drop: function(date, allDay) { // this function is called when something is dropped
			
				// retrieve the dropped element's stored Event Object
				var originalEventObject = jQuery(this).data('eventObject');
				
				// we need to copy it, so that multiple events don't have a reference to the same object
				var copiedEventObject = $.extend({}, originalEventObject);
				
				// assign it the date that was reported
				copiedEventObject.start = date;
				copiedEventObject.allDay = allDay;
				
				// render the event on the calendar
				// the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    jQuery('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
				
				// is the "remove after drop" checkbox checked?
				if (jQuery('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					jQuery(this).remove();
				}
				
			},
            timezone: 'Asia/Phnom_Penh',
		}).on('click', '.fc-button-next,.fc-button-prev', function() {
            var $currentDate    =   jQuery("table.fc-agenda-days thead tr th.fc-col0").text();
            var $eventList      =   jQuery(".fc-event-container .fc-event .fc-event-inner .fc-event-title").text();
            var $countevent =    jQuery(".fc-event-container .fc-event").length;
            var $arrayDate = $currentDate.split(" ");
            jQuery(".custom-calendar span.day-name").html($arrayDate[0]);
            jQuery(".custom-calendar span.day-num").html($arrayDate[1]);
            var $List = [];
            jQuery(".custom-calendar div.daily-events").html('');
            if($countevent && $eventList !=''){
                jQuery(".fc-event-container .fc-event").each(function(){
                    var $eventTitle = jQuery(this).find(".fc-event-inner .fc-event-title").text();
                    jQuery(".custom-calendar div.daily-events").append("<p>"+$eventTitle+"</p>");
                });
                
            }
        });
	});
</script>



<?php 
/*
* template name: Exchange Rate API
*/
  $args = array(
      'post_type'        => 'exchange',
      'post_parent'    => 547,
      'post_status'      => 'publish',
      'order'          => 'ASC',
      'orderby'        => 'menu_order'
  );
  $loop = new WP_Query( $args );
  if( $loop->have_posts() ):
    while( $loop->have_posts() ): $loop->the_post();
        $usd_buy  = number_format(get_field('usd_buy'));
        $usd_sell = number_format(get_field('usd_sell'));
        $thb_buy  = get_field('thb_buy');
        $thb_sell = get_field('thb_sell');
        $post_date = get_post_time('j F Y - g:i a');
        $latest_exchange_rate = array('usd_buy'=>$usd_buy,'usd_sell'=> $usd_sell,'thb_buy'=>$thb_buy,'thb_sell'=>$thb_sell,'published_date'=>$post_date);
    endwhile;
    // print_r($latest_exchange_rate);
   echo json_encode($latest_exchange_rate);
  endif;
?>
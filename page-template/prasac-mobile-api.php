<?php 
/*
* template name: Mobile API
*/
            $query = new WP_Query( array(
                'post_type'      => 'post',
                'posts_per_page' => 20,
                'post_status'    => 'publish'
            ));
            if( $query->have_posts() ):
                $k=1;
                while( $query->have_posts() ): $query->the_post();
                    $post_title = get_the_title(); 
                    $thumb_id =  get_post_thumbnail_id();
                    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
                    if ( has_post_thumbnail() ) {
                        $thumb_url = $thumb_url_array[0];
                    } else{
                        $thumb_url = get_template_directory_uri().'/assets/images/image_not_found.png';
                    }
                    $title  = get_the_title();
                    $post   =   $post->ID;
                    $description =  explode('[:kh]', get_the_excerpt());
                    $final_description = rtrim($description[1],'[:]');
                    $post_date = get_post_time('j F Y - g:i a');
                    $latest_exchange_rate[] = array('index'=> $k,'title'=>"$title",'thumbnail'=> "$thumb_url",'description'=>"$final_description",'post_date'=>"$post_date");
                    $k++;
                endwhile;
                wp_reset_postdata();
                echo json_encode($latest_exchange_rate);
            endif;


//   $args = array(
//       'post_type'        => 'post',
//       'posts_per_page' => 6,
//       'post_status'      => 'publish',
//       'order'          => 'ASC',
//   );
//   $query = new WP_Query( $args );
//   if( $query->have_posts()):
//     $latest_exchange_rate = array();
//     $k=1;
//     while($query->have_posts() ): $query->the_post();
//         $thumb_url_array = wp_get_attachment_image_src();
//         if ( has_post_thumbnail() ) {
//             $thumb_url = $thumb_url_array[0];
//         } else{
//             $thumb_url = get_template_directory_uri().'/assets/images/image_not_found.png';
//         }
//         $index = $k;
//         $title  = get_the_title();
//         // $thumb_url_array = wp_get_attachment_image_src($post->ID, 'full', t);
//         // $thumb_url = $thumb_url_array[0];
//         $post   = get_post($post->ID);
//         $description =  explode('[:kh]',get_the_excerpt());
//         $final_description = rtrim($description[1],'[:]');
//         $post_date = get_post_time('j F Y - g:i a');
//         $latest_exchange_rate[] = array('index'=> $index,'title'=>"$title",'thumbnail'=> "$thumb_url",'description'=>"$final_description",'post_date'=>"$post_date");
//         $k++;
//     endwhile;
//     print_r($latest_exchange_rate);
    // wp_reset_postdata();
//    echo json_encode($latest_exchange_rate);
//   endif;
?>
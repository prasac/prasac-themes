<?php
/**
 * Template Name: Calcualtor
 *
 */
get_header();
?>
<style>
 .calcualtor_title{
     border-bottom:1px solid #4DB848;
     padding:10px 0;
 }
 html:lang(kh) .calcualtor_title a{
     color:#ffffff;
     padding:0.65rem 2rem;
     background-color:#4DB848;
     border-top-left-radius:2rem;
 }
 
 html:lang(en-US) .calcualtor_title a{
     color:#ffffff;
     padding:0.4rem 2rem;
     background-color:#4DB848;
     border-top-left-radius:2rem;
 }

 .calculator_wraper{
    display: flex; /* equal height of the children */
    flex-wrap: wrap;
 }
 .calculator_list{
    display: flex;
    flex-direction: column;
 }
</style>
<div class="container-fluid">
    <main class="container">
        <section <?= post_class('col-xs-12 col-sm-9 col-md-9 page-title'); ?> >
                <div class="hidden-xs hidden-sm">
                        <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                        <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                            <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                            <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                        </ul>
                </div><br />
                <?php the_title('<h3 class="green">','</h3>') ?>
                <div class="calculator_wraper">
                <?php
                $args = array(
                    'post_type'      => 'page',
                    'posts_per_page' => -1,
                    'post_parent'    => $post->ID,
                    'order'          => 'ASC',
                    'orderby'        => 'menu_order'
                );
                $parent = new WP_Query( $args );
                if ( $parent->have_posts() ) : ?>
                    <?php 
                    $i = 0;
                    while ( $parent->have_posts() ) : $parent->the_post(); ?>
                        <?php 
                            // if($parent->posts[$i]->post_name == 'foreign-exchange-calculator'){
                        ?>
                            <!-- <div  class="col-xs-12 col-sm-6 col-md-6 calculator_list">
                                <h4 class="calcualtor_title green text-right"><a href="#" title="<?php //the_title(); ?>"><?php //the_title(); ?></a></h4>
                                <?php //the_excerpt(); ?>
                                <p class="text-right"><a href="<?php //echo get_site_url().'/services/exchange';?>"><?php //_e("[:en]Read More...[:kh]អាន​បន្ថែម...[:]");?></a></p>
                            </div> -->
                        <?php
                            // }else{
                        ?> 
                            <div class="col-xs-12 col-sm-6 col-md-6 calculator_list">
                                <h4 class="calcualtor_title green text-right"><a href="<?php the_permalink();?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
                                <?php the_excerpt(); ?>
                                <p class="text-right"><a href="<?php the_permalink();?>"><?php _e("[:en]Read More...[:kh]អាន​បន្ថែម...[:]");?></a></p>
                            </div>
                        <?php
                            // }
                        ?>
                        
                    <?php 
                    $i++;
                    endwhile;
                    ?>
                <?php endif; wp_reset_postdata(); ?>
            </div>
        </section>
        <aside class="col-sm-3 col-md-3">
            <?= get_sidebar(); ?>
        </aside>
    </main>
</div>
<?php get_footer(); ?>
<script>
    jQuery(document).ready(function(){
        //Change sidebar URL for converter calculator page to Forieng Exchange Page
        // jQuery("ul li.page_item a").each(function(){
        //     var $selected_link = jQuery(this).attr("href").split('/').pop();
        //     if($selected_link=='foreign-exchange-calculator'){
        //         jQuery(this).attr("href", function(index, old) {
        //             var $new_link = old.replace("calculators/currency-converter", "services/exchange");
        //             jQuery(this).attr("href",$new_link );
        //         });
        //     }
        // });
       
    });
</script>

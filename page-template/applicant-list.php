<?php 
/*
* template name: Applicant List
*/
get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <section class="col-xs-12 col-sm-9 col-md-9">
            <div class="hidden-xs hidden-sm">
                <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                    <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                    <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                </ul>
            </div>
            Applicant List
        </section>
    </div>
</div>
<?php get_footer(); ?>

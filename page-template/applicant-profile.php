<?php 
/*
* template name: Apllicant Profile
*/
?>
<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <title>Applicant Profile</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    </head>
    <body>   
        <style>
            body{
                background-color: rgba(75,184,72,.04);
            }
            .applicant{
                background-color:#ffffff;
                padding-bottom:10px;
            }

            @-webkit-keyframes mover {
                0% { transform: translateY(0); }
                100% { transform: translateY(-20px); }
            }
            @keyframes mover {
                0% { transform: translateY(0); }
                100% { transform: translateY(-20px); }
            }

            .applicant .applicant-form{
                margin: 0 7% 7%;
                border: 1px solid #4BB848;
                border-radius: 20px;
                padding-top: 20px;
            }
            /* .applicant-form>div:nth-child(1){
                padding-right: 7.5px;
            }
            .applicant-form>div:nth-child(2){
                padding-left: 7.5px;
            } */
            .btnRegister{
                float: right;
                border: none;
                border-radius: 1.5rem;
                padding: 2%;
                background: #0062cc;
                color: #fff;
                font-weight: 600;
                width: 50%;
                cursor: pointer;
            }
            .applicant .nav-tabs{
                margin-top: 3%;
                border: none;
                background: #0062cc;
                border-radius: 1.5rem;
                width: 28%;
                float: right;
            }
            .applicant .nav-tabs .nav-link{
                padding: 2%;
                height: 34px;
                font-weight: 600;
                color: #fff;
                border-top-right-radius: 1.5rem;
                border-bottom-right-radius: 1.5rem;
            }
            .applicant .nav-tabs .nav-link:hover{
                border: none;
            }
            .applicant .nav-tabs .nav-link.active{
                width: 100px;
                color: #0062cc;
                border: 2px solid #0062cc;
                border-top-left-radius: 1.5rem;
                border-bottom-left-radius: 1.5rem;
            }
            .applicant-title{
                text-align: center;
                padding: 25px;
                color: #495057;
            }
            .form-control:focus{
                border-color: #3bb54b7d;
                outline: 0;
                -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(75, 184, 72, 0.50);
                box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(75, 184, 72, 0.50);
            }
            .applicant .form-wrapper{
                display: contents;
            }
            .form-wrapper .form-group{
                margin-bottom: 20px;
                /* padding-left: 0; */
            }

            .form-wrapper label.col-form-label{
                text-align:left;
                justify-content:left;
            }
            .col-md-5.col-form-label>img{
                padding-right: 10px;
            }
            .col-form-label>img{
                padding-right: 10px;
            }
            @media (max-width: 767px) {
                .container-fluid {
                    padding: 0;
                }
                .container-fluid .container {
                    padding: 0;
                }
            }
        </style>

        <div class="container-fluid">
            <div class="container">
                <div class="applicant">
                    <img src="http://192.168.115.240/prasac/wp-content/uploads/2019/10/Applicant.png" class="img-fluid">
                    <div class="row">
                        <div class="col-md-12">
                        <h3 class="applicant-title">Applicant Details</h3></div>
                    </div>
                    <form class="form-inline form-wrapper">
                        <div class="row applicant-form" id="myTabContent">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-inline form-group">
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="col-form-label"><img src="http://192.168.115.240/prasac/wp-content/uploads/2019/10/icon-Full-Name.png" class="">Full Name</label>
                                        </div>
                                    </div>
                                    <input type="text" class="col-md-3 form-control" placeholder="First Name *" value="" />
                                    <div class="col-md-3">
                                        <label class="col-form-label">
                                        <img src="http://192.168.115.240/prasac/wp-content/uploads/2019/10/Position-Applied.png" class="">Position Apply</label>
                                    </div>
                                    <input type="text" class="col-md-3 form-control" placeholder="First Name *" value="" />
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-inline form-group">
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="col-form-label"><img src="http://192.168.115.240/prasac/wp-content/uploads/2019/10/icon-Phone.png" class="">Phone Number</label>
                                        </div>
                                    </div>
                                    <input type="text" class="col-md-3 form-control" placeholder="First Name *" value="" />
                                    <div class="col-md-3">
                                        <label class="col-form-label">
                                        <img src="http://192.168.115.240/prasac/wp-content/uploads/2019/10/icon-Email.png" class="">E-mail</label>
                                    </div>
                                    <input type="text" class="col-md-3 form-control" placeholder="First Name *" value="" />
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-inline form-group">
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="col-form-label"><img src="http://192.168.115.240/prasac/wp-content/uploads/2019/10/icon-Employment-Status.png" class="">Employment Status</label>
                                        </div>
                                    </div>
                                    <select class="col-md-3 form-control">
                                        <option class="hidden"  selected disabled>Please select...</option>
                                        <option>What is your Birthdate?</option>
                                        <option>What is Your old Phone Number</option>
                                        <option>What is your Pet Name?</option>
                                    </select>
                                    <div class="col-md-3">
                                        <label class="col-form-label">
                                        <img src="http://192.168.115.240/prasac/wp-content/uploads/2019/10/icon-Date-of-birth.png" class="">Date Of Birth</label>
                                    </div>
                                    <input type="text" class="col-md-3 form-control" placeholder="First Name *" value="" />
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="col-form-label"><img src="http://192.168.115.240/prasac/wp-content/uploads/2019/10/icon-current-address.png" class="">Current Address</label>
                                        </div>
                                    </div>
                                    <input type="text" class="col-md-9 form-control" placeholder="First Name *" value="" />
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="col-form-label"><img src="http://192.168.115.240/prasac/wp-content/uploads/2019/10/icon-description.png" class="">Description</label>
                                        </div>
                                    </div>
                                    <textarea class="col-md-9 form-control message" placeholder="Tell us about yourself..."></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <div class="row">
                                            <label class="col-form-label"><img src="http://192.168.115.240/prasac/wp-content/uploads/2019/10/icon-Upload-your-cv.png" class="">Upload Your CV</label>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <input type="file" name="file_attach" size="40" class="file_upload" id="file_upload" accept=".pdf,.doc,.png,.jpg,.jpeg" data-bv-field="file_attach">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <input type="submit" class="btnRegister"  value="Register"/> -->
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
/**
 * Template Name: Demo
 *
 * @package WordPress
 * @subpackage batman
 * @since batman 1.0.0
 */
get_header();
?>
<div class="container">
<div class="row">

  <aside class="col-sm-3 col-md-3"></aside>

  <section class="col-sm-9 col-md-9 page-title">
    <div class="page-title">
      <h3 class="green"><?php the_title(); ?> <div class="border"></div></h3>
    </div>
    <div id="chart_div"></div>
  </section>

</div>
</div>
<?php get_footer(); ?>
<script type="text/javascript">
  // Load the Visualization API and the piechart package.
  google.charts.load('current', {'packages':['annotationchart']});
  google.charts.setOnLoadCallback(drawChart);
  var templateDir = "<?php bloginfo('template_directory') ?>";
  function drawChart() {
    var jsonData = null
    var json = jQuery.ajax({
      url: templateDir + "/inc/json/exchange-rate.php",
      dataType: "json",
      async: false,
      success: (
      function(data) {
        jsonData = data;
      })
    }).responseText;
    // Create our data table out of JSON data loaded from server.
    var obj = window.JSON.stringify(jsonData);
    var tableData = new google.visualization.DataTable(obj);

    // Instantiate and draw our chart, passing in some options.
    var chart = new google.visualization.AnnotationChart(document.getElementById('chart_div'));
    var options = {
      hAxis: {
        format: 'MMM dd, yyyy',
        gridlines: {count: 15}
      },
      vAxis: {
        title: 'Currency Rate',
      },
      legend: {position: 'right'},
    };
    chart.draw(tableData, options);
    console.log(obj);
  }
</script>
<?php
$custom_terms = get_terms( 'locations', array( 'hide_empty' => true ) );

foreach( $custom_terms as $custom_term ) {
    wp_reset_query();
    $args = array(
        'post_type' => 'merchant',
        'tax_query' => array(
          'relation' => 'AND',
            array(
              'taxonomy' => 'locations',
              'field'    => 'slug',
              'terms'    => $custom_term->slug,
              'operator' => 'IN'
            ),
            array(
              'taxonomy' => 'merchant_tag',
              'field'    => 'slug',
              'terms'    => 'dinning-entertainment',
              'operator' => 'IN'
            )
        )
     );
     $loop = new WP_Query($args);
     if($loop->have_posts()) {
        echo '<h2>'.$custom_term->name.'</h2>';

        while($loop->have_posts()) : $loop->the_post();
            echo '<a href="'.get_permalink().'">'.get_the_title().'</a><br>';
        endwhile;
     }
}
?>
<iframe src="https://www.google.com/maps/d/embed?mid=1BP5EX6EdGi0s-VVht8v8HFP01w8" width="640" height="480"></iframe>
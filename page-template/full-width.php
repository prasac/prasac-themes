<?php 
/*
* template name: Full Width Content
*/
get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <section class="col-xs-12 col-sm-9 col-md-9 page">
            <div class="hidden-xs hidden-sm">
                <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                    <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                    <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                </ul>
            </div>
            <?php 
            $images = get_field('flexible_content');
            if( $images ) : ?>
                <!-- Slick slider carousel for gallery -->
                <div id="full-width-slider" style="margin-bottom:25px;">
                    <?php 
                    foreach( $images[0]['photo'] as $image ) : ?>
                        <section class="slick-slide">
                            <a class="slide-item" href="<?= $image['url']; ?>">
                            <img data-lazy="<?= $image['url']; ?>" alt="" class="img-responsive" title="">
                            </a>
                        </section>
                    <?php endforeach; ?>
                </div>
            <?php endif;?>
            <?php
            if (have_posts()):
                while (have_posts()): the_post();
                    echo '<div clasS="row"><div class="col-md-12"><h3 class="green">'.get_the_title().'</h3></div></div>';
                    the_content();
                endwhile;
            else :
                get_template_part('template-parts/content', 'none');
            endif;
            ?>
            <?php 
            $sidebar_banner = get_field('side_bar_banner');
            if($sidebar_banner){ ?>
                <img src="<?= $sidebar_banner;?>" class="img-responsive" alt="" title="" style="margin:0 auto;" />    
            <?php
            }
            ?>
        </section>
    </div>
</div>
<?php get_footer(); ?>

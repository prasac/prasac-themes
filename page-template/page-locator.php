<?php
/*
* template name: Locator
*/

?>
<?php get_header(); ?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <aside class="col-sm-3 col-md-3" id="category_sidebar_top">
                <div class="sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </aside>
            <section class="col-sm-9 col-md-9">
                <h3 class="green"><?= get_the_title(); ?></h3>
                <?php
                    if (have_posts()):
                    while (have_posts()): the_post();
                        the_content();
                    endwhile;
                    endif;
                ?>
                <?php
                if( is_page('atm-location') ) :
                    echo do_shortcode('[ajax_filter post="office" tax="locations" active="all-term" per_page="9" second_tax="office_categories" second_term="atm-location"]');
                elseif( is_page('branch-location') ) :
                    echo do_shortcode('[ajax_filter post="office" tax="locations" active="all-term" per_page="9" second_tax="office_categories" second_term="branch-location"]');
                elseif( is_page('cdm-location') ) :
                    echo do_shortcode('[ajax_filter post="office" tax="locations" active="all-term" per_page="9" second_tax="office_categories" second_term="cdm-location"]');
                elseif( is_page('library-location') ) :
                    echo do_shortcode('[ajax_filter post="office" tax="locations" active="all-term" per_page="15" second_tax="office_categories" second_term="library-location"]');
                endif;
                ?>
            </section>
        </main>
    </div>
</div>
<?php get_footer(); ?>

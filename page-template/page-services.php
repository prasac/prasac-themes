<?php
/**
 * Template Name: Service
 *
 * @package WordPress
 * @subpackage batman
 * @since batman 1.0.0
 */
get_header();
?>
<div class="container-fluid">
    <div class="container">
        <main class="row">
            <aside class="col-xs-12 col-sm-3 col-md-3 hidden-xs">
                    <div class="sidebar">
                    <?= do_shortcode('[wpb_childpages]'); ?>
                    </div>
            </aside>
            <section <?= post_class('col-xs-12 col-sm-9 col-md-9 page-title'); ?> >
                <div class="hidden-xs hidden-sm">
                        <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
                        <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
                            <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
                            <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
                        </ul>
                </div>
                <?php
                if ( have_posts() ): while ( have_posts() ) : the_post();
                    get_template_part('template-parts/content', 'page');
                endwhile; endif;
                ?>
                <?php
                if ( have_rows('flexible_content') ) :
                    //Check if flexible content exist
                    while ( have_rows('flexible_content') ) : the_row();
                        if ( get_row_layout() == 'product_feature' ) :
                            //Product feature
                            get_template_part('template-parts/feature', 'product');
                        elseif ( get_row_layout() == 'question_feature' ):
                            //Question feature
                            get_template_part('template-parts/feature', 'question');
                        endif;
                    endwhile;
                    //End flexible content
                endif; ?>
            </section>
        </main>
    </div>
</div>
<?php get_footer(); ?>

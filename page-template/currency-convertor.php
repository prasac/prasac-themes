<?php
/**
 * Template Name: Currency Convertor
 *
 * @package WordPress
 * @subpackage batman
 * @since batman 1.0.0
 */
?>
  <section class="page-title">
    <?php
    $args = array(
      'post_type'        => 'exchange',
      'showposts'    => 1,
      'post_status'      => 'publish',
      'suppress_filters' => true
    );
    $loop = new WP_Query( $args );
    ?>
    <?php if( $loop->have_posts() ): ?>
      <?php while( $loop->have_posts() ): $loop->the_post(); ?>
      <?php
        $usd_buy  = get_field('usd_buy');
        $usd_sell = get_field('usd_sell');
        $thb_buy  = get_field('thb_buy');
        $thb_sell = get_field('thb_sell');
      ?>
      <div class="row">
      <section class="">
      <div class="panel panel-default">
        <div class="panel-heading"><h4 class="panel-title"><?php _e('[:en]Currency Converter[:kh]កម្មវិធីគណនាប្រាក់ប្តូរ[:]')?></h4></div>
        <div class="panel-body" style="padding:8px;">
            <div class="form-group">
               <label for="amount"><?php _e('[:en]Amount[:kh]ចំនួន[:]')?></label>
              <input type="text" name="amount" class="form-control input-md input value1" id="amount" placeholder="<?php _e('[:en]Amount[:kh]ចំនួន[:]')?>">
            </div><!-- End Group Amount -->
            <div class="row">
              <section class="col-xs-12 col-sm-12 col-md-12">
                 <div class="form-group">
                    <label for="select-one"><?php _e('[:en]From[:kh]ពី​[:]')?></label>
                    <select class="form-control" id="select-one">
                       <option value="">- Select Currency -</option>
                       <option value="khr">Khmer Riel (KHR)</option>
                       <option value="usd">US Dollar (USD)</option>
                       <option value="thb">Thai Baht (THB)</option>
                    </select>
                 </div>
              </section>
              <section class="col-xs-12 col-sm-12 col-md-12">
                 <label for="select-two"><?php _e('[:en]To[:kh]ទៅ[:]')?></label>
                    <select class="form-control" id="select-two">
                       <option value="">- Select Currency -</option>
                       <option value="khr">Khmer Riel (KHR)</option>
                       <option value="usd">US Dollar (USD)</option>
                       <option value="thb">Thai Baht (THB)</option>
                    </select>
              </section>
            </div><!-- End Select Group -->
            <div class="form-group">
            <p></p>
            <input type="button" class="btn btn-block btn-golden" onClick="calculation()" value="<?php _e('[:en]Calculate[:kh]គណនា​[:]')?>" />
            </div>
             <p></p>
            <div class="form-group"><div class="form-control text-right"  placeholder=""  disabled ><span id="result"></span><span id="result-amount"></span><span id="result-amounts"></span></div></div>
            <br />
             <div  class="text-right"><strong class="disclaimer-control green"><?php _e('[:en]Note :[:kh]សម្គាល់ ៖ [:]')?></strong></div>
              <p class="disclaimer-content text-left">
                <?php
                $string = "ការប្ដូរប្រាក់ខ្វែងកើតឡើងដោយ​ឆ្លង​កាត់​ប្រាក់​រៀល។ លទ្ធផល​ដែល​គណនា​ដោយ​កម្ម​វិធី​គណនា​ប្រាក់​ប្តូរ​ខាង​លើ​គ្រាន់​តែ​ជា​ការ​បង្ហាញ​ជូន​តែ​ប៉ុណ្ណោះ​ ។ លទ្ធផល​ដែល​បង្ហាញ​ចេញ​មក​អាច​ខុស​គ្នា​ពី​លទ្ធផល​ជាក់​ស្តែង​ ។";
                if(get_locale()=='en_US'){
                  $string = "Cross currency exchange occurs through Riel. The results used by the Currency Converter are indicative only. The actual results may differ from result above";
                }
                echo $string;
                ?>
              </p>
        </div><!-- End Panel Body -->
      </div><!-- End Panel -->
      </section>
      </div>
      <?php endwhile; ?>

    <?php endif; ?>
  </section>

  <style>
   input.form-control,select.form-control{
     height:30px;
     color:#353535;
     font-size:12px;
   }
   label{
     font-size:12px;
     color:#353535;
   }
   .form-group{
     margin-bottom:5px;
   }
   #result{
     font-size:14px;
     color:red;
     text-align:right;
   }
  </style>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.js"></script>
<script>
jQuery('#amount').number( true, 2 );//Convert
function calculation() {
  var amount = document.getElementById('amount').value;
  amount = amount.replace(/,\s?/g, "");
  var currency1 = document.getElementById('select-one').value;
  var currency2 = document.getElementById('select-two').value;

  var usd_buy = <?php echo $usd_buy; ?>;
  var usd_sell = <?php echo $usd_sell; ?>;
  var thb_buy = <?php echo $thb_buy; ?>;
  var thb_sell = <?php echo $thb_sell; ?>;
  switch (currency1 + ' ' + currency2) {
    case "khr khr":
      var y = amount * 1;
      document.getElementById('result').innerHTML = "KHR";
      document.getElementById('result-amount').innerHTML = parseFloat(Math.round(y * 100) / 100);
      break;
    /* KHR to USD */
    case "khr usd":
      var x = currency2 = usd_sell;
      var y = amount / x;
      document.getElementById('result').innerHTML = "USD ";
      document.getElementById('result-amount').innerHTML = parseFloat(Math.round(y * 100) / 100);
      break;
    /* KHR to THB */
    case "khr thb":
      var x = currency2 = thb_sell;
      var y = amount / x;
      document.getElementById('result').innerHTML = "THB ";
      document.getElementById('result-amount').innerHTML = parseFloat(Math.round(y * 100) / 100);
      break;
    /* USD to USD */
    case "usd usd":
      var y = amount * 1;
      document.getElementById('result').innerHTML = "USD ";
      document.getElementById('result-amount').innerHTML = parseFloat(Math.round(y * 100) / 100);
      break;
    /* USD to KHR */
    case "usd khr":
      var x = currency2 = usd_buy;
      var y = amount * x;
      document.getElementById('result').innerHTML = "KHR ";
      document.getElementById('result-amount').innerHTML = parseFloat(Math.round(y * 100) / 100);
      break;
    /* USD to THB */
    case "usd thb":
      var x = currency2 = usd_buy / thb_sell;
      var y = amount * x;
      document.getElementById('result').innerHTML = "USD ";
      document.getElementById('result-amount').innerHTML = parseFloat(Math.round(y * 100) / 100);
      break;
    /* THB to THB */
    case "thb thb":
      var y = amount * 1;
      document.getElementById('result').innerHTML = "THB ";
      document.getElementById('result-amount').innerHTML = parseFloat(Math.round(y * 100) / 100);
      break;
    /* THB to USD */
    case "thb usd":
      var x = currency2 = usd_sell / thb_buy;
      var y = amount / x;
      document.getElementById('result').innerHTML = "USD ";
      document.getElementById('result-amount').innerHTML = parseFloat(Math.round(y * 100) / 100);
      break;
    case "thb khr":
      var x = currency2 = thb_buy;
      var y = amount * x;
      document.getElementById('result').innerHTML = "KHR ";
      document.getElementById('result-amount').innerHTML = parseFloat(Math.round(y * 100) / 100);
  }
  jQuery('span#result-amount').number( true, 2 );
}

jQuery(window).on("load",function(){
  jQuery(".disclaimer-content").hide();
  jQuery(".disclaimer-control").css('cursor','pointer');
  jQuery(".disclaimer-control").click(function(){
       jQuery(".disclaimer-content").slideToggle();
  });
});
</script>
<style>
  .disclaimer-content{
    font-size:13px;
  }
</style>
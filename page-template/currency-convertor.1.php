<?php
/**
 * Template Name: Currency Convertor
 *
 * @package WordPress
 * @subpackage batman
 * @since batman 1.0.0
 */
get_header();
?>
<div class="container">
<div class="row">

  <aside class="col-sm-3 col-md-3"></aside>

  <section class="col-sm-9 col-md-9 page-title">
    <?php
    $args = array(
      'post_type'        => 'exchange',
      'showposts'    => 1,
      'post_status'      => 'publish',
      'suppress_filters' => true
    );
    $loop = new WP_Query( $args );
    ?>
    <?php if( $loop->have_posts() ): ?>
      <div class="page-title">
        <h3 class="green"><?php the_title(); ?> <div class="border"></div></h3>
      </div>
      <?php while( $loop->have_posts() ): $loop->the_post(); ?>
      <?php
        $usd_buy  = get_field('usd_buy');
        $usd_sell = get_field('usd_sell');
        $thb_buy  = get_field('thb_buy');
        $thb_sell = get_field('thb_sell');
      ?>
      <div class="row">
      <section class="col-sm-6 col-md-6">
      <div class="panel panel-default">
        <div class="panel-heading"><h4 class="panel-title">Currency Converter</h4></div>
        <div class="panel-body">
          <form action="POST" role="form">
            <div class="form-group">
              <input type="number" name="amount" class="form-control input-md input value1" id="amount" placeholder="Amount">
            </div><!-- End Group Amount -->
            <div class="row">
              <section class="col-xs-6 col-sm-6 col-md-6">
                 <div class="form-group">
                    <label for="select-one">From:</label>
                    <select class="form-control" id="select-one">
                       <option value="">- Select Currency -</option>
                       <option value="khr">Khmer Riel (KHR)</option>
                       <option value="usd">US Dollar (USD)</option>
                       <option value="thb">Thai Baht (THB)</option>
                    </select>
                 </div>
              </section>
              <section class="col-xs-6 col-sm-6 col-md-6">
                 <label for="select-two">To:</label>
                    <select class="form-control" id="select-two">
                       <option value="">- Select Currency -</option>
                       <option value="khr">Khmer Riel (KHR)</option>
                       <option value="usd">US Dollar (USD)</option>
                       <option value="thb">Thai Baht (THB)</option>
                    </select>
              </section>
            </div><!-- End Select Group -->
            <div class="form-group">
            <input type="button" class="btn btn-block btn-golden" onClick="calculation()" value="calculate" />
            </div>
            <div class="form-group"><div class="form-control" type="number"  placeholder="" id="result" value="" disabled></div></div>
          </form><!-- End Form -->
        </div><!-- End Panel Body -->
      </div><!-- End Panel -->
      </section>
      </div>
      <?php endwhile; ?>

    <?php endif; ?>
  </section>
<script>
function calculation() {
  var amount = document.getElementById('amount').value;
  var currency1 = document.getElementById('select-one').value;
  var currency2 = document.getElementById('select-two').value;

  var usd_buy = <?php echo $usd_buy; ?>;
  var usd_sell = <?php echo $usd_sell; ?>;
  var thb_buy = <?php echo $thb_buy; ?>;
  var thb_sell = <?php echo $thb_sell; ?>;

  switch (currency1 + ' ' + currency2) {
    case "khr khr":
      var y = amount * 1;
      document.getElementById('result').innerHTML = "KHR " + parseFloat(Math.round(y * 100) / 100);
      break;
    /* KHR to USD */
    case "khr usd":
      var x = currency2 = usd_sell;
      var y = amount / x;
      document.getElementById('result').innerHTML = "USD " + parseFloat(Math.round(y * 100) / 100).toFixed(2);
      break;
    /* KHR to THB */
    case "khr thb":
      var x = currency2 = thb_sell;
      var y = amount / x;
      document.getElementById('result').innerHTML = "THB " + parseFloat(Math.round(y * 100) / 100).toFixed(2);
      break;
    /* USD to USD */
    case "usd usd":
      var y = amount * 1;
      document.getElementById('result').innerHTML = "USD " + parseFloat(Math.round(y * 100) / 100).toFixed(2);
      break;
    /* USD to KHR */
    case "usd khr":
      var x = currency2 = usd_buy;
      var y = amount * x;
      document.getElementById('result').innerHTML = "KHR " + parseFloat(Math.round(y * 100) / 100);
      break;
    /* USD to THB */
    case "usd thb":
      var x = currency2 = usd_buy / thb_sell;
      var y = amount * x;
      document.getElementById('result').innerHTML = "USD " + parseFloat(Math.round(y * 100) / 100).toFixed(2);
      break;
    /* THB to THB */
    case "thb thb":
      var y = amount * 1;
      document.getElementById('result').innerHTML = "THB " + parseFloat(Math.round(y * 100) / 100).toFixed(2);
      break;
    /* THB to USD */
    case "thb usd":
      var x = currency2 = usd_sell / thb_buy;
      var y = amount / x;
      document.getElementById('result').innerHTML = "USD " + parseFloat(Math.round(y * 100) / 100).toFixed(2);
      break;
    case "thb khr":
      var x = currency2 = thb_buy;
      var y = amount * x;
      document.getElementById('result').innerHTML = "KHR " + parseFloat(Math.round(y * 100) / 100);
  }
}
</script>

</div>
</div>

<?php get_footer(); ?>

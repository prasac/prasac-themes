<?php
 /*
* template name: National Career Fair 2019 Application Form
*/
get_header();
error_reporting(0);
?>
<style>
.btn-bs-file {
    position: relative;
}

.btn-bs-file input[type="file"] {
    position: absolute;
    top: -9999999;
    filter: alpha(opacity=0);
    opacity: 0;
    width: 0;
    height: 0;
    outline: none;
    cursor: inherit;
}

.green {
    color: #4DB848 !important;
    font-weight: 600 !important;
    text-transform: capitalize;
}

.h1,
.h2,
.h3,
h1,
h2,
h3 {
    margin-top: 20px;
    margin-bottom: 10px;
}

.wrapper-container{
    background-color:#eeeeee;
    padding:10px;
}
@media (max-width: 768px) {
    section.wrapper-container{
        padding:10px 0;
    }
    form .form-group {
        padding:0;
        margin-bottom: 0.8rem;
    }
}
</style>

<?php 
//================If form submitted========
    if(isset($_POST['btn-apply'])){
        $upload_dir   = wp_upload_dir();
        $files = glob($upload_dir['basedir'].'/career_applicant/*'); //get all file names
        foreach($files as $file){
            if(is_file($file)){
                unlink($file); //delete file
            }
        }
        $applied_position = isset($_POST['applied_position'])?trim($_POST['applied_position']):'';
        $applied_branch = isset($_POST['applied_branch'])?trim($_POST['applied_branch']):'';
        $full_name_kh = isset($_POST['full_name_kh'])?trim($_POST['full_name_kh']):'';
        $full_name_en = isset($_POST['full_name_en'])?trim($_POST['full_name_en']):'';
        $gender = isset($_POST['gender'])?$_POST['gender']:'';
        $dateofbirth = isset($_POST['dateofbirth'])?$_POST['dateofbirth']:'';
        $pob_address = isset($_POST['pob_address'])?$_POST['pob_address']:'';
        $current_address = isset($_POST['current_address'])?$_POST['current_address']:'';
        $phone_number = isset($_POST['phone_number'])?$_POST['phone_number']:'';
        $applicant_lavel = isset($_POST['applicant_lavel'])?$_POST['applicant_lavel']:'';
        $applicant_skill = isset($_POST['applicant_skill'])?$_POST['applicant_skill']:'';
        $work_experience = isset($_POST['work_experience'])?$_POST['work_experience']:'';
        $work_experience_year = isset($_POST['work_experience_year'])?$_POST['work_experience_year']:'';
        $work_experience_company = isset($_POST['work_experience_company'])?$_POST['work_experience_company']:'';
        $reciever_email ='recruitment@prasac.com.kh';//recruitment@prasac.com.kh/ $_POST['receiver_email']

        $html = '
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <link href="'.get_template_directory_uri().'/assets/phpmailer/style.css" rel="stylesheet">
            <body>
            <style>
            textarea{  
                overflow:hidden;
                padding:5px;
                display:block;
                border-radius:10px;
                border:1px solid #000000;
            }
            </style>
            <form action="#" method="post">
                <div style="border:0px solid blue;float:left;width:100%;padding:0;">
                    <h1 class="green" style="text-align:center;">National Career Fair Application Form</h1>
                </div>
                <div class="form-item-wrapper">
                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;">
                                <input style="float:right;" name="full_name_kh" size="70"  class="form-control khmer" required type="text" value="'.$full_name_kh.'">
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;Applicant Name​ Khmer:</div>
                        </div>
                    </div>


                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;">
                                <input name="full_name_en" size="70"  class="form-control khmer" required type="text" value="'.$full_name_en.'">
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;Applicant Name​ English:</div>
                        </div>
                    </div>

                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;">
                                <input name="gender" size="70" class="form-control khmer" required type="text" value="'.$gender.'">
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;Gender:</div>
                        </div>
                    </div>

                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;">
                                <input  name="dateofbirth" size="70"  class="form-control khmer" required type="text" value="'.$dateofbirth.'">
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;DOB:</div>
                        </div>
                    </div>

                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;">
                                <input name="current_address" size="70"  class="form-control khmer" required type="text" value="'.$current_address.'">
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;Current Address:</div>
                        </div>
                    </div>

                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;"> 
                                <input  name="phone_number" size="70"  class="form-control khmer" required type="text" value="'.$phone_number.'">
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;Phnom Number:</div>
                        </div>
                    </div>


                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;">
                                <input  name="applicant_lavel" size="70"  class="form-control khmer" required type="text" value="'.$applicant_lavel.'">
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;Education:</div>
                        </div>
                    </div>


                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;">
                                <textarea  name="applicant_skill" cols="70" rows="4"  class="form-control khmer">'.$applicant_skill.'</textarea>
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;SKill(s):</div>
                        </div>
                    </div>


                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;">
                                <textarea  name="work_experience" cols="50" rows="4"  class="form-control khmer">'.$work_experience.'</textarea>
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;Work Experience:</div>
                        </div>
                    </div>

                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;">
                                <input  name="work_experience_year" size="70" class="form-control khmer" value="'.$work_experience_year.'" />
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;Work Experience in Year:</div>
                        </div>
                    </div>

                    <div class="form-item-wrapper">
                        <div class="col-12" style="font-weight:lighter;">
                            <div style="float:right;width:70%;">
                                <textarea  name="work_experience_company" cols="50" rows="4"  class="form-control khmer">'.$work_experience_company.'</textarea>
                            </div>
                            <div style="width:30%;font-weight:lighter;float:left;">&nbsp;&nbsp;Work Experience Company:</div>
                        </div>
                    </div>
                </div>
            </form>
        </body>';


        $mpdf_path =  get_template_directory().'/assets/MPDF57/mpdf.php';
        require_once($mpdf_path);
        $mpdf = new mPDF('s',    // mode - default ''
                    'A4',    // format - A4, for example, default ''
                    '',     // font size - default 0
                    '',    // default font family
                    4,    // 15 margin_left
                    4,    // 15 margin right
                    4,     // 16 margin top
                    4,    // margin bottom
                    '',     // 9 margin header
                    4,     // 9 margin footer
                    'P');
        $mpdf->AddPage('P', // L - landscape, P - portrait 
                    '', '', '',
                    4, // margin_left
                    4, // margin right
                    4, // margin top
                    4, // margin bottom
                    9, // margin header
                    10); // margin footer
        $mpdf->debug = true;
        // var_dump($mpdf);
        $mpdf->allow_charset_conversion = true;
        // $mpdf->setAutoBottomMargin = 'stretch';
        // $allow_charset_conversion = true
        $mpdf->useActiveForms = true;
        // $mpdf->formUseZapD = false;
        $mpdf->form_border_color = '0.6 0.6 0.72';
        $mpdf->form_button_border_width = '2';
        $mpdf->form_button_border_style = 'S';
        $mpdf->form_radio_color = '0.0 0.0 0.4'; // radio and checkbox
        $mpdf->form_radio_background_color = '0.9 0.9 0.9';            
        $mpdf->SetHTMLFooter('<div class="pdf-footer"><div class="footer-left">Natoinal Career Fair 2019</div><div class="footer-right">Page {PAGENO} of {nbpg}</div></div>');
        $stylesheet1 = file_get_contents(get_template_directory().'/assets/phpmailer/style.css');
        $mpdf->WriteHTML($stylesheet1,1);
        $mpdf->WriteHTML($html);
        $currentDate = date("Ymd");
        $pdfname = $applied_position."_".$currentDate."_".$full_name_en."_".$applied_branch.".pdf";
        $mpdf->Output($upload_dir['basedir'].'/career_applicant/'.$pdfname,"F");

        $phpmailer_path =  get_template_directory().'/assets/phpmailer/PHPMailerAutoload.php';
        require_once($phpmailer_path);
        global $error;
        $mail = new PHPMailer();
        $mail->CharSet = "UTF-8";
        $mail->IsSMTP();
        $mail->SMTPDebug =0;
        // $mail->SMTPAuth = true;
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );
        $mail->Port = 25;
        $mail->Host = "mail.prasac.com.kh";
        $mail->Username = "kimhim.hom@prasac.com.kh";
        $mail->Password = '$PassWord@159753';
        $mail->IsHTML(true);
        $mail->FromName = $full_name_en;
        $mail->setFrom('info@prasac.com.kh',$full_name_en);
        $mail->AddReplyTo($candidate_email,$full_name_en);
        $mail->addBCC('kimhim.hom@prasac.com.kh');
        $final_position = trim($applied_position);
        $mail->AddAddress($reciever_email);
        $mail->AddAttachment($upload_dir['basedir'].'/career_applicant/'.$pdfname);
        $mail->Subject = "National Career Fair Application for ".$final_position;
        $her_his = $gender=="ប្រុស"?"his":"her";
        
        $mail_message_head ="<html>
                                <title>Application Form</title>
                                <head></head>
                                <style>
                                table tbody tr td{
                                        font-family:Khmer OS Content !important;
                                    }
                                </style>
                                <body style='marging:0;background-color:#F6F6F6;'>
                                    <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#F6F6F6'>
                                        <tbody>
                                            <tr>
                                                <td align='center' valign='top' style='padding:40px 20px 20px 20px'>
                                                    <table href='https://fonts.googleapis.com/css?family=Content|Roboto' cellpadding='20' cellspacing='0' align='center' style='width:600px;heigh:auto;background-color:#ffffff'>
                                                        <tr>
                                                            <td colspan='2' align='left'>
                                                                <a href='#'><img src='https://www.prasac.com.kh/wp-content/uploads/2017/12/Logo_50_Increased-03.png' style='background-repeat:no-repeat;background-size:70px 70px; width:70px;'></a>
                                                            </td>
                                                        </tr>
                                                        <tr><td colspan='2' align='left'>";
                                $mail_message_footer = "</td></tr><tr>
                                                            <td colspan='2' align='center' style='padding:0;'>
                                                                <a href='#'><img src='https://www.prasac.com.kh/wp-content/uploads/2018/11/curse.png' style='background-repeat:no-repeat;background-size:600px 92px; width:600px;padding:0;'></a>
                                                            </td>
                                                        </tr>
                                                        <tr style='background-color:#F6F6F6'>
                                                            <td style='padding:0;' colspan='2' align='center'>
                                                                <table cellpadding='0' cellspacing='0' align='center'>
                                                                    <tr style='padding:10px 0 20px 0'>
                                                                        <td align='center' valign='middle' style='color:#707070;font-size:10px;line-height:10px;'>
                                                                            <a style='color:#F6F6F6;' href='https://www.facebook.com/prasacmfi/' rel='noopener' target='_blank'>
                                                                                <img alt='facebook' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/facebook.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;line-height:10px;display:inline-block;vertical-align:top'>
                                                                            </a>&nbsp;&nbsp;
                                                                            <a style='color:#F6F6F6;' href='https://www.youtube.com/user/PrasacMFI/featured' rel='noopener' target='_blank'>
                                                                                <img alt='youtube' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/youtube.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;display:inline-block;vertical-align:bottom'>
                                                                            </a>&nbsp;&nbsp;
                                                                            <a style='color:#F6F6F6;' href='https://twitter.com/prasaccambodia' rel='noopener' target='_blank'>
                                                                                <img alt='twitter' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/Twitter.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;display:inline-block;vertical-align:bottom'>
                                                                            </a>&nbsp;&nbsp;
                                                                            <a style='color:#F6F6F6;' href='https://www.linkedin.com/company/prasac/' rel='noopener' target='_blank'>
                                                                                <img alt='linkedin' src='https://www.prasac.com.kh/wp-content/uploads/2018/12/linkedin.png' width='30' height='auto' border='0' hspace='0' vspace='0' style='color:#707070;display:inline-block;vertical-align:bottom'>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </body>
                            </html>";

        $body ="<b>Dear Recruitmemt Team</b>,<br/>
        <p>New applicant applied for $final_position, please find  $her_his detail information in attached file.</p><br />
        <b>Note:</b>This appliation form was sent from National Career Fair 2019 Application Form in Prasac website<br/>
        Here is the online linke <a href='https://www.prasac.com.kh/national-career-fair-2019'>National Career Fair 2019 Application Form</a>";
        $messages = $mail_message_head;
        $messages .= $body;
        $messages .= $mail_message_footer;
        $mail->Body    = $messages;
        if(!$mail->Send())
        {
            echo '
            <div class="container-fluid">
                <div class="container"><br />
                    <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <span class="glyphicon glyphicon-hand-right"></span> <strong>Application Form is fail submited!</strong>
                    <hr class="message-inner-separator">
                    <p>Please contact to administrator to fix it.</p>
                    <p>Press Ctr+F5 to refresh page or click <input type="button" value="Career Page" onClick="https://www.prasac.com.kh/kh/careers/online-application-form" class="btn btn-success btn-sm"></p>
                    </div>
                </div>
            </div>';
        }else{
            echo '
            <div class="container-fluid">
                <div class="container"><br />
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <span class="glyphicon glyphicon-cross"></span> <strong>Application form is successfully submitted.</strong></div>
                </div>
            </div>';
        }

    }
?>
<div class="container-fluid">
    <div class="container">
        <br />
        <section class="wrapper-container">
            <form action="" method="post" id="national_career_fair_form" enctype="multipart/form-data">
                <!-- Form Name -->
                <legend style="border:none;">
                    <div class="col-md-12">
                        <h3 class="green text-center">ទម្រង់បែបបទសុំបំពេញការងារ​</h3>
                    </div>
                </legend>
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label col-form-label" for="applied_position">តួនាទីស្នើសុំ <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <select name="applied_position" class="form-control" id="applied_position">
                                            <option value="TELLER">TELLER</option>
                                            <option value="CUSTOMER SERVICE OFFICER">CUSTOMER SERVICE OFFICER</option>
                                            <option value="RELATIONSHIP MANAGER">RELATIONSHIP MANAGER</option>
                                            <option value="CREDIT OFFICER">CREDIT OFFICER</option>
                                            <option value="SME LOAN OFFICER">SME LOAN OFFICER</option>
                                            <option value="LOAN ADMINISTRATION OFFICER">LOAN ADMINISTRATION OFFICER</option>
                                            <option value="REGIONAL AUDITOR">REGIONAL AUDITOR</option>
                                            <option value="REGIONAL IT OFFICER">REGIONAL IT OFFICER</option>
                                            <option value="REGIONAL ADMIN OFFICER">REGIONAL ADMIN OFFICER</option>
                                            <option value="PR OFFICER">PR OFFICER</option>
                                            <option value="VARIOUS IT POSITION">VARIOUS IT POSITION</option>
                                            <option value="VOLUNTEER">VOLUNTEER</option>
                                        </select>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="applied_branch">សាខាស្នើសុំ៖ <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <select class="form-control" name="applied_branch" id="applied_branch">
                                            <?php echo getlocation(5)?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label col-form-label">ឈ្មោះជាភាសាខ្មែរ <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <input name="full_name_kh" placeholder="" class="form-control" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">ឈ្មោះជាភាសាអង់គ្លេស <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <input name="full_name_en" placeholder="" class="form-control" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label">ភេទ <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <input type="radio" name="gender" value="ប្រុស" id="male" checked />
                                            <label class="radio-inline" for="male">ប្រុស&nbsp;</label>
                                            <input type="radio" name="gender" value="ស្រី" id="female"/>
                                            <label class="radio-inline" for="female">ស្រី</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="dateofbirth">ថ្ងៃខែឆ្នាំកំណើត <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <input type="text" name="dateofbirth" id="dateofbirth" class="form-control"/>
                                            <span class="input-group-addon"><i class="fa fa-calendar calendar-dateofbirth"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="pob_address">ទីកន្លែងកំណើត <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <textarea name="pob_address" class="form-control" id="pob_address"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="current_address">អាសយដ្ឋានបច្ចុប្យន្ន <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <textarea name="current_address" class="form-control" id="current_address"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="phone_number">លេខទូរស័ព្ទ<span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <input name="phone_number" class="form-control" id="phone_number" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-xs-12 col-md-5 control-label" for="current_address">កំរិតសិក្សា <span class="required_field">*</span></label>
                                    <div class="col-xs-6 col-md-4">
                                        <select name="applicant_lavel" class="form-control">
                                            <option value="">ជ្រើសរើស</option>
                                            <option value="បរិញ្ញាបត្រជាន់ខ្ពស់">បរិញ្ញាបត្រជាន់ខ្ពស់</option>
                                            <option value="បរិញ្ញាបត្រ">បរិញ្ញាបត្រ</option>
                                            <option value="បរិញ្ញាបត្ររង">បរិញ្ញាបត្ររង</option>
                                            <option value="វិទ្យាល័យ">វិទ្យាល័យ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-6 col-md-3">
                                        <select name="applicant_lavel" class="form-control">
                                            <option value="បញ្ចប់ការសិក្សា">បញ្ចប់ការសិក្សា</option>
                                            <option value="ឆ្នាំទី 4">ឆ្នាំទី 4</option>
                                            <option value="ឆ្នាំទី 3">ឆ្នាំទី 3</option>
                                            <option value="ឆ្នាំទី 2">ឆ្នាំទី 2</option>
                                            <option value="ឆ្នាំទី 1">ឆ្នាំទី 1</option>
                                            <option value="ផ្សេងៗ">ផ្សេងៗ</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="applicant_skill">ជំនាញ <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <textarea name="applicant_skill" class="form-control" id="applicant_skill"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="work_experience">បទពិសោធន៍ការងារ<span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <textarea name="work_experience" class="form-control" id="work_experience"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="work_experience_year">រយៈពេលបម្រើ​ការងារ (ឆ្នាំ) <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <input type="text"  name="work_experience_year" class="form-control" id="work_experience_year" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5 control-label" for="work_experience_company">ក្រុមហ៊ុន​ ឬ​ ស្ថាប័ន <span class="required_field">*</span></label>
                                    <div class="col-md-7">
                                        <textarea name="work_experience_company" class="form-control" id="work_experience_company"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <input type="hidden" name="action" value="careerfair2019"/>
                            <div class="col-md-12 text-center">
                            <br>
                            <button type="submit" class="btn btn-success" name="btn-apply" id="apply_now"> <i class="fa fa-envelope"></i> ដាក់ពាក្យឥឡូវនេះ</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </section>
        <br />
    </div>
</div>

<?php
get_footer();
?>
<style>
.has-feedback .form-control {
    padding-right: 0;
}

div.form-group.has-error>.form-control,
div.form-group.has-error .input-group .form-control,
div.form-group.has-error div>.form-control {
    border-color: #a94442;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
}

#ab_row2 div.form-group .form-control,
#ab_row3 div.form-group .form-control,
#ab_row4 div.form-group .form-control,
#ab_row5 div.form-group .form-control {
    border: 1px solid #ccc;
}

span.required_field {
    color: red;
}

.form-group {
    margin-bottom: 0;
    padding: 4px 8px;
}

.panel.panel-horizontal {
    display: table;
    width: 100%;
}

.panel.panel-horizontal>.panel-heading,
.panel.panel-horizontal>.panel-body,
.panel.panel-horizontal>.panel-footer {
    display: table-cell;
}

.panel.panel-horizontal>.panel-heading,
.panel.panel-horizontal>.panel-footer {
    border: 0;
    vertical-align: middle;
}

.panel.panel-horizontal>.panel-heading {
    border-right: 1px solid #ddd;
    border-top-right-radius: 0;
    border-bottom-left-radius: 4px;
}

.panel.panel-horizontal>.panel-footer {
    border-left: 1px solid #ddd;
    border-top-left-radius: 0;
    border-bottom-right-radius: 4px;
}

label {
    font-weight: lighter;

}

.form-horizontal .radio-inline {
    padding-top: 0;
}

.form-group input[type="checkbox"] {
    /* display:block; */
}

.checkbox-danger input[type="checkbox"]:checked+label::before {
    background-color: #d9534f;
    border-color: #d9534f;
}

.checkbox-danger input[type="checkbox"]:checked+label::after {
    color: #fff;
}

input[type="radio"]:checked+label {
    color: #4DB848;
    font-weight: lighter;
}

.col-md-1>input {
    padding: 2px;
}

#success_message {
    display: none;
}

.panel-heading .accordion-toggle:after {
    /* symbol for "opening" panels */
    font-family: 'Glyphicons Halflings';
    /* essential for enabling glyphicon */
    content: "\e114";
    /* adjust as needed, taken from bootstrap.css */
    float: right;
    /* adjust as needed */
    color: grey;
    /* adjust as needed */
}

.panel-heading .accordion-toggle.collapsed:after {
    /* symbol for "collapsed" panels */
    content: "\e080";
    /* adjust as needed, taken from bootstrap.css */
}

h4.panel-title {
    /* font-family:"Moul"; */
    font-size: 1.6em;
    font-weight: light;
}

.btn-bs-file {
    position: relative;
}

.btn-bs-file input[type="file"] {
    position: absolute;
    top: -9999999;
    filter: alpha(opacity=0);
    opacity: 0;
    width: 0;
    height: 0;
    outline: none;
    cursor: inherit;
}

.checkbox-inline,
.radio-inline {
    padding-left: 5px;
}
</style>
<script>
//Show image profile immediately
jQuery("#filebrowse").change(function() {
    readURL(this);
    readURLPDF(this);
});


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            jQuery('#img-upload').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURLPDF(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            jQuery('#img-upload-pdf').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
</script>

<style>
div#ui-datepicker-div.ui-datepicker {
    z-index: 2 !important;
}

#ui-datepicker-div div.ui-datepicker-header {
    background-image: none;
    background-color: #F2B01D;
    color: #555555;
    font-size: 12px;
}

.ui-datepicker table {
    font-size: 0.7em;
}

.ui-widget-header .ui-icon,
.ui-icon,
.ui-widget-content .ui-icon {
    background-image: url("<?php bloginfo('template_url');?>/assets/images/ui-icons_ffffff_256x240.png");
}

.ui-datepicker td span,
.ui-datepicker td a {
    text-align: center;
}

.ui-datepicker {
    width: 14em;
}

a.ui-state-default.ui-state-active {
    background: #F2B01D;
    color: #fff;
}

.draggable-header .highslide-heading {
    position: absolute;
    margin: -3px 0.4em;
}

.modal {
    position: fixed;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: rgba(0, 0, 0, 0.7);
}

.modal .chart {
    height: 90%;
    width: 100%;
    max-width: none;
}

.ui-state-hover,
.ui-widget-content .ui-state-hover,
.ui-widget-header .ui-state-hover,
.ui-state-focus,
.ui-widget-content .ui-state-focus,
.ui-widget-header .ui-state-focus {
    color: #F2B01D;
    border: 1px solid #F2B01D;
}

.input-group-addon {
    font-size: 13px;
}

.highcharts-reset-zoom,
.highcharts-button-box {
    background-color: transparent;
    fill: transparent;
    stroke: none;
}

.highcharts-reset-zoom text {
    color: #F2B01D !important;
    fill: #F2B01D !important;
}

.highcharts-reset-zoom text :hover {
    color: #4DB848 !important;
    fill: #4DB848 !important;
}

/*button.ui-datepicker-current.ui-state-default{
                    font-size:13px;
                    background: #F2B01D;
                    color: #fff;
                    padding:0 2px;
                }*/
</style>
<script>
jQuery(document).ready(function() {
    jQuery('#submit-application').click(function(e) {
        var formURL = '/prasac/wp-admin/admin-ajax.php';
        var request_method = "POST";
        var postData = jQuery("form#employment_form").serializeArray();
        jQuery.ajax({
            url: formURL,
            type: request_method,
            data: postData,
            success: function(data, textStatus, jqXHR) {
                console.log(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });


    // var $family_status = $(".family_status:checked").val();
    jQuery(".not-signle-row,.only-for-married").hide();

    jQuery(".marital_status").click(function() {
        var $family_status = jQuery(this).val();
        if ($family_status == 'Single') {
            jQuery(".not-signle-row,.only-for-married").fadeOut("200");
        } else if ($family_status == 'Married') {
            jQuery(".not-signle-row,.only-for-married").fadeIn("200");
        } else if ($family_status == 'Widow/Widower') {
            jQuery(".not-signle-row").fadeIn("200");
            jQuery(".only-for-married").fadeOut("200");
        }
    });

    var start = new Date();
    start.setFullYear(start.getFullYear() - 70);
    var end = new Date();
    end.setFullYear(end.getFullYear() - 18);
    var endyearemployment = new Date();
    endyearemployment.setFullYear(endyearemployment.getFullYear());
   
    
    jQuery("#dateofbirth").datepicker({
        dateFormat: 'dd/mm/yy',
        autoclose: true,
        changeYear: true,
        changeMonth: true,
        minDate: start,
        maxDate: end,
        yearRange: start.getFullYear() + ':' + end.getFullYear()
    });
    jQuery('.calendar-dateofbirth').click(function() {
        jQuery("#dateofbirth").focus();
    });

    
    jQuery("#dateofemployment,.experience_date_employfrom0,.experience_date_employto0,.experience_date_employfrom1,.experience_date_employto1,.experience_date_employfrom2,.experience_date_employto2,.experience_date_employfrom3,.experience_date_employto3,.experience_date_employfrom4,.experience_date_employto4").datepicker({
        dateFormat: 'dd/mm/yy',
        autoclose: true,
        changeYear: true,
        changeMonth: true,
        minDate: start,
        maxDate: endyearemployment,
        yearRange: start.getFullYear() + ':' + endyearemployment.getFullYear()
    });
    jQuery('.calendar-ex-date-from0').click(function() {
        jQuery(".experience_date_employfrom0").focus();
    });
    jQuery('.calendar-ex-date-to0').click(function() {
        jQuery(".experience_date_employto0").focus();
    });

    jQuery('.calendar-ex-date-from1').click(function() {
        jQuery(".experience_date_employfrom1").focus();
    });
    jQuery('.calendar-ex-date-to1').click(function() {
        jQuery(".experience_date_employto1").focus();
    });

    jQuery("#comfirm_date,#apply_date").datepicker({
        dateFormat: 'dd/mm/yy'
    }).datepicker('setDate', new Date()).attr('readonly', 'readonly');


    jQuery('#national_career_fair_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            applied_position: {
                validators: {
                    notEmpty: {
                        message: 'សូមបំពេញតួនាទីដែលលោក​អ្នក​ចង់ដាក់ពាក្យ​'
                    }
                }
            },
            applied_branch: {
                validators: {
                    notEmpty: {
                        message: 'សូម​ជ្រើសរើស​ឈ្មោះ​សាខាដែលលោកអ្នក​ចង់​ដាក់ពាក្យ​'
                    }
                }
            },
            full_name_kh: {
                validators: {
                    notEmpty: {
                        message: '<?php _e('[:en]Please enter your name in Khmer[:kh]សូមបញ្ចូល​ឈ្មោះ​លោក​អ្នក​ជា​អក្សរ​ខ្មែរ!')?>',
                    },
                    regexp: { 
                        regexp: /^[ក-អ្ក ្ខ ្គ ្ឃ ្ង , ្ច ្ឆ ្ជ ្ឈ ្ញ , ្ដ ្ឋ ្ឌ ្ឍ ្ណ , ្ត ្ថ ្ទ ្ធ ្ន , ្ប ្ផ ្ព ្ភ ្ម , ្យ ្រ ្ល ្វ ្ស ្ហ ្ឡ ្អ ា ិ ី ឹ ឺ ុ ូ ួ ើ ឿ ៀ េ ែ ៃ ោ ៅ ុំ ំ ាំ ះ ឥ ឦ ឧ ឩ ឪ ឫ ឬ ឭ ឮ ឯ ឰ ឱ ឳ ៉់ ់ ៊ ៍ ័ ៏ ៎ ៌ \.]+$/, 
                        message: '<?php _e('[:en]Please enter your name in Khmer[:kh]សូមបញ្ចូល​ឈ្មោះ​លោក​អ្នក​ជា​អក្សរ​ខ្មែរ!')?>',
                    } 
                }
            },
            full_name_en: {
                validators: {
                    notEmpty: {
                        message: '<?php _e('[:en]Please enter your name in English[:kh]សូមបញ្ចូល​ឈ្មោះ​លោក​អ្នក​ជា​អក្សរ​អង់គ្លេស!')?>',
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9_ \.]+$/,
                        message: '<?php _e('[:en]Please enter your name in English[:kh]សូមបញ្ចូល​ឈ្មោះ​លោក​អ្នក​ជា​អក្សរ​អង់គ្លេស!')?>',
                    },
                }
            },
            dateofbirth: {
                validators: {
                    notEmpty: {
                        message: 'សូមបញ្ចូលថ្ងៃខែឆ្នាំកំណើតរបស់លោកអ្នក!',
                    },
                    date: {
                        format: 'DD/MM/YYYY',
                        message: 'The format is DD/MM/YYYY'
                    },
                }
            },
            pob_address: {
                validators: {
                    notEmpty: {
                        message: 'សូមបញ្ចូលទីកន្លែងកំណើតរបស់លោកអ្នក!',
                    },
                }
            },
            current_address: {
                validators: {
                    notEmpty: {
                        message: 'សូមបញ្ចូលទីកន្លែងបច្ចុប្បន្ន​របស់លោកអ្នក!',
                    },
                }
            },
            phone_number: {
                validators: {
                    stringLength: {
                        min: 9,
                        max: 10,
                        message: 'សូមត្រួតពិនិត្យ​លេខទូរសព្ទរបស់លោកអ្នកម្ដងទៀត!',
                    },
                    notEmpty: {
                        message: 'សូមបញ្ចូលលេខទូរសព្ទរបស់លោកអ្នក!',
                    },
                    regexp: {
                        regexp: /^[0-9_ \.]+$/,
                        message: 'សូមបញ្ចូលលេខទូរសព្ទរបស់លោកអ្នកជាលេខឡាតាំង!'
                    },
                }
            },
            applicant_lavel:{
                validators: {
                    notEmpty: {
                        message: 'សូមជ្រើសរើស​កំរិតសិក្សា​របស់​លោកអ្នក​!',
                    },
                },
            },
            applicant_skill:{
                validators: {
                    notEmpty: {
                        message: 'សូមបញ្ចូល​ជំនាញ​របស់​លោកអ្នក​!',
                    },
                },
            },
            work_experience: {
                validators: {
                    notEmpty: {
                        message: 'សូមបញ្ចូល​បទពិសោធន៍ការងាររបស់លោកអ្នក!',
                    },
                }
            },
            work_experience_year: {
                validators: {
                    stringLength: {
                        min:1,
                        max:5,
                        message: 'រយៈពេលបទពិសោធន៍​ការងារ​របស់លោកអ្នក​ចាប់​ពី​ 1 ដល់​ 5 ឆ្នាំ',
                    },
                    regexp: {
                        regexp: /^[0-5_ \.]+$/,
                        message: 'រយៈពេលបទពិសោធន៍​ការងារ​របស់លោកអ្នក​ចាប់​ពី​ 1 ដល់​ 5 ឆ្នាំ',
                    },
                }
            },
            work_experience_company: {
                validators: {
                    notEmpty: {
                        message: 'Please  enter your Village name.',
                    },
                }
            },
        }
    });

    jQuery('#dateofbirth').on('change show', function(e) {
        jQuery('#national_career_fair_form').bootstrapValidator('revalidateField', 'dateofbirth');
    });
});
</script>
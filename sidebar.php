<?php 
global $post;
if( is_category() ): ?>
<div class="sidebar">
  <div class="sidebar_title"><?php _e('[:en]Media[:kh]ព័ត៌មាន​[:]'); ?></div>    
  <ul class="sidebar_content list">
    <?php wp_list_pages('sort_column=menu_order&post_status=publish&title_li=&child_of=6796&echo=1'); ?>
  </ul>
</div>
<?php elseif( is_page() ) : ?>
    <?= do_shortcode('[wpb_childpages]'); ?>
<?php elseif(is_single() || is_tax('people_category') ) : ?>
<div class="sidebar">
  <div class="sidebar_title"><?php _e('[:en]About PRASAC[:kh]អំពីប្រាសាក់​[:]'); ?></div>
  <ul class="sidebar_content list" id="accordion">
    <?php
    $post_type = 'people';
    $tax = 'people_category';
    $tax_terms = get_terms($tax,'hide_empty=0&exclude=1');
    //list the taxonomy
    foreach ($tax_terms as $tax_term) {
      $args = array ( 'taxonomy' => $tax, 'terms' => $tax_term->slug );
      $query = new WP_Query ($args);
      $article_count = $query->post_count;
      _e('
      <li class="page_item">
        <a class="" href="'.esc_attr(get_term_link($tax_term, $tax)).'" title="'.sprintf( __( "View all posts in %s" ), $tax_term->name ).'">'.$tax_term->name.'</a>
      </li>
      ');
    }
    ?>
  </ul>
</div>
<?php elseif( is_tax('document_category') ): ?>
<div class="sidebar">
  <div class="sidebar_title"><?php _e('[:en]Reports[:kh]របាយការណ៏​[:]'); ?></div>
  <ul class="sidebar_content list" id="accordion">
    <?php
    $post_type = 'document';
    $tax = 'document_category';
    $tax_terms = get_terms($tax,'hide_empty=0&exclude=1');
    //list the taxonomy
    foreach ($tax_terms as $tax_term) {
      $args = array ( 'taxonomy' => $tax, 'terms' => $tax_term->slug );
      $query = new WP_Query ($args);
      $article_count = $query->post_count;
      _e('
      <li class="page_item ">
      <a class="" href="'.esc_attr(get_term_link($tax_term, $tax)).'" title="'.sprintf( __( "View all posts in %s" ), $tax_term->name ).'">'.$tax_term->name.'</a>
      </li>
      ');
    }
    ?>
  </ul>
</div>

<?php elseif (is_single() && is_page_template('page-media.php')): ?>
<div class="sidebar hidden-xs">
  <div class="sidebar_title"><?php _e('[:en]Recently Post[:kh]អត្ថបទថ្មីៗ[:]'); ?></div>
  <div class="sidebar_content list">
  <?php $popular = new WP_Query(array('post_type'=>'post','posts_per_page'=>10,'status'=>'publish'));
  ?>
  <ul class="sidebar_content list">
  <?php
  while ($popular->have_posts()) : $popular->the_post(); ?>
      <li class="page_item"><a  href="<?php the_permalink(); ?>" title="<?= get_the_title(); ?>" ><?php the_title(); ?></a></li>
  <?php endwhile;wp_reset_postdata(); ?>
  </ul>
  </div>
</div>
<?php endif; ?>
<?php
  if( qtrans_getLanguage() == 'en' ) {
    $image = get_field('sidebar_banner');
  } elseif( qtrans_getLanguage() == 'kh' ) {
    $image = get_field('sidebar_banner_kh');
  }
  if( !empty($image) ):
    $image_url = $image['url'];
    $image_alt = $image['alt'];
    $post_id = get_field('sidebar_banner_link', false, false);
?>
  <div class="sidebar_banner hidden-xs">
    <a href="<?php echo get_the_permalink($post_id); ?>" title="<?php echo get_the_title($post_id); ?>">
      <img class="img-responsive" src="<?= $image_url; ?>" alt="<?= $image_alt; ?>" style="margin:0 auto;">
    </a>
  </div>
<?php endif; ?>


<?php
/**
 * Template Name: Library
 *
 * @package WordPress
 * @subpackage growing_2gether
 * @since Growing2Gether 1.0.0
 */
get_header();
?>
<div id="body" class="container-fluid">
<div class="container">
<aside id="sidebar" class="col-sm-3 col-md-3 page-title">
  <div class="sidebar">
      <?= do_shortcode('[wpb_childpages]'); ?>
  </div>
</aside>
<!-- Body -->
<section id="content" class="col-sm-9 col-md-9 content">
    <div class="hidden-xs hidden-sm">
      <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
    </div>
    <ul id="font-size-switcher" class="nav navbar-nav pull-right hidden-xs single">
      <li id="font-size-normal"><a href="#" title="Standard Font Size">A-</a></li>
      <li id="font-size-big"><a href="#" title="Increase Font Size">A+</a></li>
    </ul>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <?php if( have_posts() ): ?>
                <?php while( have_posts() ): the_post(); ?>
                    <div class="page-title">
                        <h3 style="border-bottom:none;" class="green"><?php the_title();?></h3>
                    </div>
                    <?= the_content(); ?>
                <?php endwhile; ?>
            <?php endif ?>
            <div id="map-canvas" class="" style="width:auto; height: 500px;"></div>
            <?php
                global $post; 
                $current_date = current_time('Ymd'); 
                $args = array(
                    'posts_per_page' =>  -1,
                    'post_type'      => 'library',
                    'post_status'    => 'publish',
                    'orderby'        =>'post_date',
                    'order'			 => 'DESC',
                );
                $loop = new WP_Query($args);
                if ( $loop->have_posts() ):
                    /* lat/lng data will be added to this array */
                    $locations=array();
                    while( $loop->have_posts() ): $loop->the_post();
                        $locations[] = array( 'name'=>get_the_title(), 'lat'=>get_field('library_lat'), 'lng'=>get_field('library_lng'),'description'=>get_the_content());
                        ?>
                        <!-- <span id="latlng" data-latlng="<?php //echo get_field('library_lat').','.get_field('library_lng'); ?>"></span> -->
                        <!-- <span id="content_text" class=""><?php //echo the_content(); ?></span> -->
                <?php
                    endwhile;
                    $markers = json_encode( $locations );
                endif;
            ?>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="table-responsive">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php _e('[:en]PRASAC LIBRARIES[:kh]បណ្ណាល័យប្រាសាក់[:]');?></h3>
                        <div class="pull-right">
                            <span class="clickable filter" data-toggle="tooltip" title="Toggle table filter" data-container="body">
                                <i class="glyphicon glyphicon-filter"></i>
                            </span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <input type="text" class="form-control" id="dev-table-filter" data-action="filter" data-filters="#dev-table" placeholder="Filter Developers" />
                    </div>
                    <table class="table table-bordered table-hover" id="dev-table">
                        <thead>
                            <tr>
                                <th class="text-center"><?php _e('[:en]№[:kh]ល.រ[:]') ?></th>
                                <th class="text-center"><?php _e('[:en]Library Name[:kh]ឈ្មោះបណ្ណាល័យ[:]') ?></th>
                                <th class="text-center"><?php _e('[:en]Library Photo[:kh]រូបថត[:]') ?></th>
                                <th class="text-center"><?php _e('[:en]Library Detail[:kh]ព័ត៌មានលម្អិត[:]') ?></th>
                                <th class="text-center"><?php _e('[:en]Library Province[:kh]ខេត្ត[:]') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            if ( $loop->have_posts() ):
                                $count =1;
                                $i=0;
                                while( $loop->have_posts() ): $loop->the_post();
                                    global $post;
                                    $terms = wp_get_post_terms($post->ID, 'locations', array("fields" => "all"));

                                    $thumb_id =  get_post_thumbnail_id();
                                    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'full', true);
                                    if ( has_post_thumbnail() ) {
                                        $thumb_url = $thumb_url_array[0];
                                    } else{
                                        $thumb_url = 'https://dummyimage.com/700x350/f2f2f2/4aa818&text=loading';
                                    }    
                                    ?>
                                    <tr>
                                        <td class="text-center"><?php echo $count;?></td>
                                        <td class="map-link" data-markerid="<?php echo $count-1;?>"><?= the_title();?></td>
                                        <td class="text-center">
                                            <a class="lightbox" href="<?php echo $thumb_url;?>">Click Here</a>
                                        </td>
                                        <td><?= the_title();?></td>
                                        <td>
                                            <?php  
                                            $output = array();
                                            foreach($terms as $term_single) {
                                                $output[] = $term_single->name;
                                            }
                                            $pro       =   '[:en]'.implode(" ", $output).'[:kh]'.implode(",", $output);
                                            $provinces  =   __($pro);
                                            print ($provinces);
                                            ?>
                                        </td>
                                    </tr>
                                <?php 
                                $count++;
                                $i++;
                                endwhile;
                            endif;
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-12">
        <hr>
            <div class="single-footer-share">
                <strong class="green"><?php _e('[:en]Share to[:kh]ចែករំលែក[:]');?></strong>
                <a class="fa fa-facebook" href="javascript:shareSocial('<?php echo get_permalink();?>','facebook');"></a>
                <a class="fa fa-twitter" href="javascript:shareSocial('<?php echo get_permalink();?>','twitter');"></a>
                <a class="fa fa-linkedin" href="javascript:shareSocial('<?php echo get_permalink();?>','linkedin');"></a>
                <div class="fb-like pull-right" data-href="<?php echo get_permalink();?>" data-width="" data-layout="button" data-action="like" data-size="small" data-share="false"></div>
            </div>
        </div>
    </div>
</section>
</div>
</div>
<?php get_footer(); ?>
<style>
		.clickable{
		    cursor: pointer;   
		}

		.panel-heading div {
			margin-top: -18px;
			font-size: 15px;
		}
		.panel-heading div span{
			margin-left:5px;
		}
		/* .panel-body{
			display: none;
        } */
        .panel-heading span
        {
            margin-top: -9px;
            font-size: 15px;
            margin-right: -12px;
            color:red;
        }
        .panel-body a{
            color:#393318;
        }
        .panel-body a:before{
            font-size:15px;
            color:red;
        }
        .panel-body a:hover{
            color:#4db848;
        }

        .clickable {
            display: inline-block;
            padding: 6px 12px;
            border-radius: 4px;
            cursor: pointer;
        }
        table#dev-table tbody tr{
            cursor:pointer;
        }
</style>
<script>
    jQuery(window).on("load",function(){
        <?php
            echo "var markers=$markers;\n";
        ?>
        var element = jQuery(this);
        var map, infowindow;
        function initialize(myCenter) {
            var imgPoint = "<?= esc_url( get_template_directory_uri() ); ?>/assets/images/library-marker.png";
            var image = new google.maps.MarkerImage(imgPoint);
            var mapProp = {
                center: myCenter,
                zoom:12,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);
            // Info window content
            var map;
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                mapTypeId: 'roadmap'
            };
            var googlemarkers = new Array();
            // Add multiple markers to map
            var infoWindow = new google.maps.InfoWindow({maxWidth:500}), marker, i;
            
            // Place each marker on the map
            for( i = 0; i < markers.length; i++ ) {
                var position = new google.maps.LatLng(markers[i]['lat'], markers[i]['lng']);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    title: markers[i]['name'],
                    icon: image,
                    animation: google.maps.Animation.DROP
                });
                // Add info window to marker    
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infoWindow.setContent(markers[i]['description']);
                        infoWindow.open(map, marker);
                        map.setZoom(12);
                        map.setCenter(marker.getPosition());
                    }
                })(marker, i));

                // Center the map to fit all markers on the screen
                map.fitBounds(bounds);
                // Add marker to markers array
                googlemarkers.push(marker);
            }

            // Set zoom level
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
                // this.setZoom(14);
                google.maps.event.removeListener(boundsListener);
            });

            //============Trigger action click on table to show infowindow on Google Map
            jQuery('table tbody tr td.map-link').on('click', function () {
                google.maps.event.trigger(googlemarkers[jQuery(this).data('markerid')], 'click');
            });
            
        }
        initialize(new google.maps.LatLng('12.680031','104.918253'));
        

        jQuery("#filter_input").on("keyup", function() {
            var value = jQuery(this).val().toLowerCase();
            jQuery("#data-list-library tr").filter(function() {
                jQuery(this).toggle(jQuery(this).text().toLowerCase().indexOf(value) > -1);
            });
        });
        
    });
</script>
<script>
    (function(){
    'use strict';
	var $ = jQuery;
	$.fn.extend({
		filterTable: function(){
			return this.each(function(){
				jQuery(this).on('keyup', function(e){
					jQuery('.filterTable_no_results').remove();
					var $this = jQuery(this), 
                        search = $this.val().toLowerCase(), 
                        target = $this.attr('data-filters'), 
                        $target = jQuery(target), 
                        $rows = $target.find('tbody tr');
                        
					if(search == '') {
						$rows.show(); 
					} else {
						$rows.each(function(){
							var $this = jQuery(this);
							$this.text().toLowerCase().indexOf(search) === -1 ? $this.hide() : $this.show();
						})
						if($target.find('tbody tr:visible').size() === 0) {
							var col_count = $target.find('tr').first().find('td').size();
							var no_results = jQuery('<tr class="filterTable_no_results"><td colspan="'+col_count+'">No results found</td></tr>')
							$target.find('tbody').append(no_results);
						}
					}
				});
			});
		}
	});
	jQuery('[data-action="filter"]').filterTable();
})(jQuery);

jQuery(function(){
    // attach table filter plugin to inputs
	jQuery('[data-action="filter"]').filterTable();
	
	jQuery('.container').on('click', '.panel-heading span.filter', function(e){
		var $this = jQuery(this), 
		$panel = $this.parents('.panel');
		$panel.find('.panel-body').slideToggle();
		if($this.css('display') != 'none') {
			$panel.find('.panel-body input').focus();
		}
	});
	jQuery('[data-toggle="tooltip"]').tooltip();
});
</script>
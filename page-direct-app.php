<?php
 /*
* template name: Direct Download
* Autor: Kimhim HOM
* Autor Contact: 093240717
*/
?>
<?php
    require get_template_directory() . '/inc/Mobile_Detect.php';
    $detect = new Mobile_Detect;

    if( $detect->isiOS() ){
        header('Location:https://apps.apple.com/app/id971426760');
    }elseif( $detect->isAndroidOS() ){
        header('Location:https://play.google.com/store/apps/details?id=com.prasac.ibanking.prasacmbanking');
    }else{
        header('Location:https://ibanking.prasac.com.kh/');
    }
?>